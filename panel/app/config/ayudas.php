<?php
return array(
				'configurarip' => array(
												0 => array(
															'url' => "https://fmeuropa.com/configuracion-del-softphone-telephone-para-mac-os-x/",
															'title' => "Configuración del softphone 'Telephone' para Mac OS X"
														  ),
												1 => array(
															'url' => "https://fmeuropa.com/configurar-asterisk/",
															'title' => "Configurar Asterisk con tu cuenta de Flash Telecom"
														  ),
												2 => array(
															'url' => "https://fmeuropa.com/configuracion-del-gigaset-a510-ip/",
															'title' => "Configuración del Gigaset A510 IP con tu cuenta de Flash Telecom"
														  ),
												3 => array(
															'url' => "https://fmeuropa.com/grandstream-gxp1450/",
															'title' => "Configuración de Grandstream GXP1450 con tu cuenta de Flash Telecom"
														  ),
												4 => array(
															'url' => "https://fmeuropa.com/zoiper-iphone-app-guia-de-instalacion/",
															'title' => "Configura tu cuenta de Flash Telecom en Zoiper para iPhone"
														  ),
												5 => array(
															'url' => "https://fmeuropa.com/instalacion-y-configuracion-de-zoiper-en-android/",
															'title' => "Instalación y Configuración de Zoiper en Android"
														  ),
												6 => array(
															'url' => "https://fmeuropa.com/instalacion-y-configuracion-de-windows-zoiper/",
															'title' => "Instalación y Configuración de Zoiper en Windows"
														  ),
												7 => array(
															'url' => "https://fmeuropa.com/configuracion-de-cuenta-sip-en-cisco-spa-112/",
															'title' => "Configuración de cuenta sip en Cisco Spa 122"
														  )
										
									  )
			);
?>