<?php
class AdministracionController extends BaseController
{
	
  	public function showClientes($page = 1)
  	{
    	if($page < 1)
		{
			return Redirect::to('clientes');
		}    
    
    	$clientes = User::where('id_cliente', '>', 0)->where('nombre_fiscal', '!=', '')->orderBy('nombre_fiscal')->get();
		
    	return View::make('content/administracion/clientes', array(
      		'title'           => 'Clientes',
      		'clientes'        => $clientes,
      		'selectedCliente' => Session::get('number', NULL),
			'id_administrador' => Session::get('user')->getID()
    	));
  	}
	
	public function setCliente($id_cliente,$id_administrador)
	{
		Session::flush();		
		Session::put('simulador-origen', $id_administrador);
		return $this->intercambiar_usuario($id_cliente);
	}
	
	public function regresarSimular($id_administrador)
	{
		Session::flush();
		return $this->intercambiar_usuario($id_administrador,'clientes','Retorno a la cuenta de administración');
	}
	
	private function intercambiar_usuario($id_usuario_destino,$redirect = 'numbers',$msg = NULL)
	{
		$usuario_destino = User::find($id_usuario_destino);
		
		$distribuidor = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
		
		if($distribuidor !== NULL)
		{
			$dominio = explode(".",$distribuidor[0]->dominio);
			Session::put('id_distribuidor', $distribuidor[0]->id);
			Session::put('logo', $distribuidor[0]->logo);
			Session::put('distribuidor', $distribuidor[0]->nombre);
			Session::put('dominio', $dominio[1].".".$dominio[2]);
			Session::put('telefono', $distribuidor[0]->telefono);
			if(Session::get('dominio') != 'fmeuropa.com'){
				Session::put('ocultar','si');
			}else{
				Session::put('ocultar','no');
			}
			Session::put('css',$distribuidor[0]->css);
			Session::put('dominio_master',$distribuidor[0]->dominio_master);
			Session::put('prefijo',$dominio[1]);
		}
		else
		{
			Session::put('id_distribuidor', 1);
			Session::put('logo', 'logo-in-logo.png');
			Session::put('distribuidor', 'Flash Telecom');
			Session::put('dominio', 'fmeuropa.com');
			Session::put('telefono', '800007766');
			Session::put('logo-alta','logo-web.png');
			Session::put('ocultar','no');
			Session::put('css','common.css');
			Session::put('dominio_master','http://www.fmeuropa.com');
			Session::put('prefijo','');
		}
		
		if (FALSE !== ($user = $this->flash->getUserByAccessData($usuario_destino->usr_usuario, $usuario_destino->usr_pass)))
	  	{   
			$clientes = DB::table('cliente')->where('id_distribuidor', '=', Session::get('id_distribuidor'))->get();
			
			Session::put('user', $user);
			Session::put('clientes',$clientes);
			
			$user_registrado = User::find($user->getID());
			
			$results = DB::table('cliente')->select('id_distribuidor','master')->where('id_cliente', '=', $user->getID())->get();
			
			if($results[0]->id_distribuidor == Session::get('id_distribuidor'))
			{
				Session::put('master',$results[0]->master);
				if($msg)
				{		  
					return Redirect::to($redirect)->with('info',$msg);
				}
				else
				{
					return Redirect::to($redirect)->with('info','Bienvenido a la cuenta de '.$usuario_destino->nombre_fiscal);
				}
			}
		}
	}
 	
}
?>