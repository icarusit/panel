<?php

/**
 * Controlador para la gestión de saldo
 */
class BalanceController extends BaseController
{
  /**
   * Muestra ventana carga de saldo
   * 
   * @return type
   */
  public function show()
  {     
    return View::make('content.balance.balance', array(
      'title'  => 'Recarga de saldo'
    ));    
  }
  
  /**
   * Almacena el intento de recarga y redirige a la pasarela de pago
   * 
   * @return Redirect
   */
	public function pay()
  	{
    	$amount = (int)Input::get('amount');    
    	$allowed = array(5, 10, 15, 20, 25, 30, 40, 50, 70, 100, 150, 200);
    
    	if(!in_array($amount, $allowed))
		{
      		$error = "Lo sentimos, el importe no es válido.";      
      		return Redirect::to('balance')->with('error', $error);
    	}
		else
		{
      		$order = 'UB' . date('Ym') . rand(1000, 9999);     

			$forma_pago = Input::get('forma_pago');
			
			if(Payment::register($amount, $order,'saldo',null,$forma_pago))
	  		{		  
		  		
		  		switch($forma_pago)
		  		{
			  		case "redsys":
			  		{
				  		$urlOK = URL::to('balance/success');
        				$urlKO = URL::to('balance/error');
        
        				return Payment::redirectToGateway($amount, $order, $urlOK, $urlKO);
				  		break;
			  		}
			  		case "paypal":
			  		{
						$urlOK = URL::to('balance-paypal/resultado');
        				$urlKO = URL::to('balance-paypal/retorno');
        
        				return PaymentPaypal::redirectToGateway($amount, $order, $urlOK, $urlKO,'Recarga');
				  		break;
			  		}
		  		}        
      		}
	  		else
	  		{
        		return Redirect::to('balance');
      		}
    	}
 	}
  
  /**
   * Método que se ejecuta tras el callback del TPV
   * 
   * El usuario no llega nunca a esta ruta, sólo RedSys
   * tras el POST de confirmación del pago.
   * 
   * @param string $order  Referencia transacción
   * @param float  $amount Total 
   */
	public function callback($order, $amount)
  	{
    	$row = Balance::where('referencia', '=', $order)->firstOrFail();      
      
    	if($row->resultado == 'NA' && $row->tipo = 'saldo')
		{
      		//
      		// Actualizamos el estado de la operación y recargamos
      		// el saldo en la cuenta del usuario
      		//
      		$row->resultado = 'OK';
      		$row->save();
      		$amount = $amount / 100;

      		$user = User::find($row->id_cliente);
          
      		if($user !== null)
			{
        		$user->userbalance+= $amount;
        		$user->save();
        		$this->refresh();
 
        		// Notificamos tanto al cliente como a Flash de la recarga.
        		$subject = "Notificación de recarga de saldo en tu cuenta de Flash Telecom";
        		$subjectFlash = $subject . " (ID cliente {$user->id_cliente})";

        		$header = "Has recargado <strong>{$amount}&euro;</strong> en tu cuenta";             
        		$subheader = "Tu saldo actual es de <strong>{$user->userbalance}&euro;</strong>.";
        		$body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
        		"</strong> has efectuado correctamente una recarga de <strong>{$amount}&euro;
        		</strong> con tarjeta de crédito en tu cuenta de Flash Telecom.</p>
        		<p>La referencia de la transacción para futuras consultas es <strong>{$order}
        		</strong>.</p><p>Gracias por tu confianza.</p><p>El equipo de Flash Telecom.</p>";
							
				$content = array(
				  'header'    => $header,
				  'subheader' => $subheader,
				  'body'      => $body
				);

        		$this->flash->sendMail($user->mail_1, $subject, $content);
      		}
    	}
  	}
  
  /**
   * Muestra la ventana de recarga errónea
   */
  public function showError()
  {
    if(
       ($order    = Input::get('Ds_Order', null)) !== null &&
       ($response = Input::get('Ds_Response', null)) !== null
      ) {
      $row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      $currentBalance = $this->user->getBalance();
      
      if($row !== null) {
        $info = "<p>No hemos recibido correctamente la notificacion de pago. Comprueba si se ha añadido el saldo y has recibido un email de confirmacion.</p>";
    
        return Redirect::to('balance')->with('error', $error);
      } else {
        return Redirect::to('balance');                                                                                                   
      }                                                                                                                                    
    } else {                                                                                                                               
      return Redirect::to('balance');                                                                                                     
    }
  }
  
  /**
   * Muestra la ventana de recarga exitosa
   */
 	public function showSuccess()
  	{
    	if(($version        = Input::get('Ds_SignatureVersion', null)) !== null &&
		   ($params       = Input::get('Ds_MerchantParameters', null)) !== null &&
		   ($signatureRecibida     = Input::get('Ds_Signature', null)) !== null)
		{
			$obj_api = Payment::RedsysAPI();
			$decode =  $obj_api->decodeMerchantParameters($params);
			$obj_api->stringToArray($decode);
			$codigoRespuesta = $obj_api->getParameter('Ds_Response');
			$randomKey       = Payment::getRandomKey();
			$signatureCalculada = $obj_api->createMerchantSignatureNotif($randomKey,$params);
			$order = $obj_api->getParameter('Ds_Order');
			
      		$row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      		$currentBalance = $this->user->getBalance();
      
      		if($row !== null)
			{
        		$info = "<p>Gracias. Has recargado <strong>{$row->importe} €</strong> de saldo "
        		. "en tu cuenta.</p><p>Tu saldo actual es de <strong>{$currentBalance} €</strong>.</p>"
        		. "<p>Recibirás en los siguientes minutos un correo con la factura y estará también disponible "
        		. "en el apartado de 'Mis facturas'</p>";
    
        		return Redirect::to('balance')->with('info', $info);
      		}
			else
			{
        		return Redirect::to('balance');                                                                                                   
      		}                                                                                                                                    
    	}
		else
		{                                                                                                                               
      		return Redirect::to('balance');                                                                                                     
    	}
  	}
	public function resultadoPaypal()
  	{
		if($_POST)
		{
			// Obtenemos los datos en formato variable1=valor1&variable2=valor2&...
       	 	$raw_post_data = file_get_contents('php://input');

        	// Los separamos en un array
        	$raw_post_array = explode('&',$raw_post_data);

        	// Separamos cada uno en un array de variable y valor
        	$myPost = array();
        	foreach($raw_post_array as $keyval)
			{
           	 	$keyval = explode("=",$keyval);
            	if(count($keyval) == 2)
                	$myPost[$keyval[0]] = urldecode($keyval[1]);
        	}

        	// Nuestro string debe comenzar con cmd=_notify-validate
        	$req = 'cmd=_notify-validate';
        	if(function_exists('get_magic_quotes_gpc'))
			{
            	$get_magic_quotes_exists = true;
        	}
			foreach($myPost as $key => $value)
			{
            	// Cada valor se trata con urlencode para poder pasarlo por GET
            	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)
				{
                	$value = urlencode(stripslashes($value)); 
            	}
				else
				{
                	$value = urlencode($value);
            	}

            	//Añadimos cada variable y cada valor
            	$req .= "&$key=$value";
        	}
			$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');

        	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        	curl_setopt($ch, CURLOPT_POST, 1);
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
			
			if( !($res = curl_exec($ch)) )
			{
            	// Ooops, error. Deberiamos guardarlo en algún log o base de datos para examinarlo después.
            	curl_close($ch);
            	return Redirect::to('balance');
        	}
			curl_close($ch);
			
			if (strcmp ($res, "VERIFIED") == 0)
			{
				if(strtolower($_POST['payment_status']) == 'pending' || strtolower($_POST['payment_status']) == 'completed')
				{
					$order = $_POST['custom'];
					
					$row = Balance::where('referencia', '=', $order)->firstOrFail();      
		  
					if($row->resultado == 'NA' && $row->tipo = 'saldo')
					{
						//
						// Actualizamos el estado de la operación y recargamos
						// el saldo en la cuenta del usuario
						//
						$row->resultado = 'OK';
						$row->save();
						$amount = $row->importe;
				
						$user = User::find($row->id_cliente);
						  
						if($user !== null)
						{
							$user->userbalance+= $amount;
							$user->save();
							$this->refresh();
				 
							// Notificamos tanto al cliente como a Flash de la recarga.
							$subject = "Notificación de recarga de saldo en tu cuenta de Flash Telecom";
							$subjectFlash = $subject . " (ID cliente {$user->id_cliente})";
				
							$header = "Has recargado <strong>{$amount}&euro;</strong> en tu cuenta";             
							$subheader = "Tu saldo actual es de <strong>{$user->userbalance}&euro;</strong>.";
							$body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
							"</strong> has efectuado correctamente una recarga de <strong>{$amount}&euro;
							</strong> con Paypal en tu cuenta de Flash Telecom.</p>
							<p>La referencia de la transacción para futuras consultas es <strong>{$order}
							</strong>.</p><p>Gracias por tu confianza.</p><p>El equipo de Flash Telecom.</p>";
									
							$content = array(
								'header'    => $header,
								'subheader' => $subheader,
								'body'      => $body
							);
				
							$this->flash->sendMail($user->mail_1, $subject, $content);
						}
					}					
				}
			}
			elseif(strcmp ($res, "INVALID") == 0)
			{
        		return Redirect::to('balance');
			}			
		}
		else
		{
			return Redirect::to('balance');
		}
  	}
	public function retornoPaypal()
	{
		if($order = Input::get('custom', null) !== null)
		{
			$order = $_POST['custom'];
      		$row = Payment::where('referencia', '=', $order)->firstOrFail();
      
     		$currentBalance = $this->user->getBalance();
      
      		if($row !== null && $row->resultado == 'OK')
			{
        		$info = "<p>Gracias. Has recargado <strong>{$row->importe} €</strong> de saldo "
        		. "en tu cuenta.</p><p>Tu saldo actual es de <strong>{$currentBalance} €</strong>.</p>"
        		. "<p>Recibirás en los siguientes minutos un correo con la factura y estará también disponible "
        		. "en el apartado de 'Mis facturas'</p>";
    
        		return Redirect::to('balance')->with('info', $info);
      		}
			else
			{
        		$error = "<p>Lo sentimos, ha habido algún problema con el pago de la recarga.</p>";    
        		return Redirect::to('balance')->with('error', $error);                                                                                                  
      		}                                                                                                                                    
    	}
		else
		{                                                                                                                               
      		return Redirect::to('balance');                                                                                                     
    	}
	}
}
