<?php

use FlashTelecom\FlashTelecom;

class BaseController extends Controller
{
  /**
   * Instancia de la API de Flash
   * 
   * @var FlashTelecom\FlashTelecom $flash
   */
  protected $flash;
  
  /**
   * Instancia de la clase Mobile_Detect
   * 
   * @var
   */
  protected $mobileDetect;
  
  /**
   * Número en el que estamos trabajando. Está almacenado como variable de
   * sesión.
   * 
   * @var Number $number
   */
  protected $number;
  
  /**
   * Usuario que está con la sesión iniciada
   * 
   * @var User $user
   */
  protected $user;
 
  /**
   * Directorio de datos
   *
   * NOTA: Esto no debería estar aquí sino en un fichero .env
   *
   * @var string $dataPath
   */
  protected $dataPath = '/mnt/nfs_asterisk/data/';
  //protected $dataPath = '/home/flash/data/';
 
  public function __construct()
  {
    // Instancia de nuestro wrapper API
    $this->flash = new FlashTelecom(
      '935e17a904a9947d3f392b4b43c99a7a5932089a',
      'http://localhost/api/public'
    );
    
    // Instancia del detector de móvil
    $this->mobileDetect = new Mobile_Detect();
    
    // Si tenemos un número con el que estamos trabajando,
    // lo almacenamos como propiedad en el controlador 
    // base
    if (($number = Session::get('number', NULL)) !== NULL) { 
      $this->number = $number;
      $this->refresh();
    }
    
    // Si se ha iniciado sesión almacenamos el usuario como
    // propiedad en el controlador base
    if (($user = Session::get('user', NULL)) !== NULL) {
      $this->user = $user;
      
      $user = User::find($user->getID());      
      Session::put('panel_mode', $user->panel_mode);
    }   

    if(Session::get('is_mobile', NULL) === NULL) {
      Session::put('is_mobile', $this->mobileDetect->isMobile());
    }
  }
  
  /**
   * Refresca el objeto del número de la sesión cuando hemos hecho cambios
   * (post, put, delete)
   */
  public function refresh()
  {
    if (($cachedNumber = Session::get('number', NULL)) !== NULL) {
      $number = $this->flash->number($cachedNumber->getNumber());
      Session::put('number', $number);

      $this->number = $number;
    }
    
    if(($cachedUser = Session::get('user', NULL)) !== NULL) {
      $user = $this->flash->user($cachedUser->getID());
      Session::put('user', $user);
      
      $this->user = $user;
    }
  }


  
  /**
   * Manera supercutre de notificar por HipChat de eventos del panel
   * 
   * Ya refactorizaremos. Esto es de Chema y chuta en HipChat
   
  public function hipchat($msg, $room = 'flash', $color = 'yellow')
  {
    $url = 'http://84.20.5.98/lkasjjlsui3i9878oujoisjdklfjlskd.php';
    $msg = rawurlencode($msg);    
    
    file_get_contents($url . "?room=$room&msg=$msg&color=$color");        
  }

*/

  	/**
  	 * Manera supercutre de notificar por Mattermost de eventos del panel
   	* 
   	* Ya refactorizaremos.
  	*/ 
  	public function hipchat($msg, $sala = 'fin9jp71jjbkbk6rfrshpfer5e', $color = 'yellow')
  	{
    	//$url = 'http://84.20.5.98/lkasjjlsui3i9878oujoisjdklfjlskd_.php';
		//$url = "http://sip3.fmeuropa.com:8065/hooks/".$sala;
		$url = "http://core2.fmeuropa.com:8065/hooks/".$sala;
    	//$msg = rawurlencode($msg);    
    
    	//file_get_contents($url . "?sala=$sala&msg=$msg&color&user=Web"); 
	
		$user = "Web";
	
		$header = array();
		$header[] = 'Content-type: application/json';
		//$params = '{"message": "' . $msg . '", "color": "' . $color . '", "notify": true}';
		//$params = '{"icon_url": "https://pbs.twimg.com/profile_images/2626206044/5p3pjsaawpncxdsz8xdw_normal.png", "username": "'.$user.'", "text": "' . $msg .'"}';
		
		$params = array(
							"icon_url" => "https://pbs.twimg.com/profile_images/2626206044/5p3pjsaawpncxdsz8xdw_normal.png",
							"username" => $user,
							"text"     => $msg
						);
		$params_json = json_encode($params);    	
		$grooms = array();
		$grooms[] = $sala;                                                                       
		foreach($grooms as $groom)
		{                              
			$c = curl_init();
	  		curl_setopt($c, CURLOPT_URL, $url);
	  		curl_setopt($c, CURLOPT_VERBOSE, TRUE);
	  		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
	  		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	  		curl_setopt($c, CURLOPT_POST, TRUE);
	  		curl_setopt($c, CURLOPT_HTTPHEADER, $header);
	  		curl_setopt($c, CURLOPT_POSTFIELDS, $params_json);
	  		curl_exec($c);
	  		curl_close($c);
	  		sleep(1);  
		}
  	}
	
	public function hipchat_base64($msg, $sala = 'fin9jp71jjbkbk6rfrshpfer5e', $color = 'yellow')
  	{
		$msg = base64_decode($msg);
		$this->hipchat($msg,$sala, $color);
  	}



  /**
   * Cambia el modo de uso del panel
   * 
   * @param string $mode
   */
  public function setPanelMode($mode)
  {
    $rowUser = User::find($this->user->getID());    
    $rowUser->panel_mode = $mode === 'advanced' ? 'advanced' : 'basic';    
    $rowUser->save();
    
    $return = Input::get('redirect', null);
    
    return $return === null
      ? Redirect::to('/')
      : Redirect::to($return);
  }  
}
