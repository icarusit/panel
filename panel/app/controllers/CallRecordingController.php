<?php

class CallRecordingController extends BaseController
{
 /**
   * Muestra configuración de la grabación de llamadas
   * 
   * @return type
   */
	public function show()
  	{ 
    	// Para la cabecera del marco con la lista de grabaciones, mostrar
    	// la fecha de hace 15 días
    	$fifteenDaysAgoDate = new \DateTime('20 days ago');
    	$strFifteenDaysAgoDate = $fifteenDaysAgoDate->format('d/m/Y');
    
    	// Recuperamos flag de si ha firmado el cliente o no
    	// el contrato para LOPD de las grabaciones
    	$user = User::find($this->user->getID());
    	$lopdContract = $user->contrato_grabacion == 1 ? true : false;
    
    	// Recuperamos las grabaciones del número
    	$recordings = $this->number->getRecordings();
		
		$numero_filtro = Input::get('numero_filtro');
		if($numero_filtro && $numero_filtro != '' && $numero_filtro != NULL)
		{
			$resultado_filtro = array();
			foreach($recordings as $record)
			{
				if(stristr($record->caller, $numero_filtro))
				{
					$resultado_filtro[] = $record;
				}
			}
			$recordings = $resultado_filtro;
		}

    	$perPage = 10;
    	$currentPage = Input::get('page', 1) - 1;

    	if($recordings !== false)
		{
      		$pagedRecordings = array_slice($recordings, $currentPage * $perPage, $perPage);
    	}
		else
		{
      		$pagedRecordings = array();
    	}

    	$paginator = Paginator::make($pagedRecordings, count($recordings), $perPage);        
    
    	if($currentPage > ($paginator->getLastPage() - 1))
      		return Redirect::to('callrecording');    
    
    	return View::make('content/callrecording', array(
      		'title'                 => 'Grabación de llamadas',
      		'lopdContract'          => $lopdContract,
      		'isCallRecordingActive' => $this->number->isCallRecordingActive(),
      		'fifteenDaysAgoDate'    => $strFifteenDaysAgoDate,
      		'recordings'            => $pagedRecordings,
			'numero_filtro'         => $numero_filtro,
      		'paginator'             => $paginator
    	));
  	}
  
  /**
   * Activa o desactiva la grabación de llamadas
   */
  public function setCallRecording()
  {
    // Recuperamos flag de si ha firmado el cliente o no
    // el contrato para LOPD de las grabaciones
    $user = User::find($this->user->getID());
    $lopdContract = $user->contrato_grabacion == 1 ? true : false;
    
    if($lopdContract) {
      $callRecording = Input::get('callrecording');
      $return = Input::get('return', 'callrecording');

      $this->number->setCallRecording($callRecording === 'enable' ? TRUE : FALSE);
      $this->refresh();

      $info = 'El servicio de grabación de llamadas ha sido correctamente <strong>';
      $info.= $callRecording === 'enable' ? 'activado' : 'desactivado';
      $info.= '</strong>.';

      return Redirect::to($return)->with('info', $info);
    }
  }
  
  /**
   * Elimina una grabación
   * 
   * @param string $recording Nombre de la grabación
   */
  public function remove($recording)
  {
    $idUser = Session::get('user')->getID();
    $number = $this->number->getNumber();

    $path = $this->dataPath . $idUser . '/recordings/' . $number . '/';
    
    if(file_exists($path . $recording)) {
      unlink($path . $recording); 
      $info = "La grabación $recording ha sido borrada satisfactoriamente.";
      return Redirect::to('callrecording')->with('info', $info);
    } else {
      return Redirect::to('callrecording');
    }    
  }
  
  	/**
   	* Descarga una grabación
   	* 
   	* @param string $recording Nombre de la grabación
   	*/
	public function download($recording)
  	{
	  	$number = $this->number->getNumber();
		$idUser = Session::get('user')->getID();

    	$path = $this->dataPath . $idUser . '/recordings/' . $number . '/';
    
    	if(file_exists($path . $recording))
		{
    		$content = $this->number->getRecording($recording);

    		$response = Response::make($content, 200);
    
    		$response->header('Content-Type', 'audio/x-wav, audio/wav');
    		$response->header('Content-Description', 'File Transfer');
    		$response->header('Content-length', strlen($content));
    		$response->header('Content-Disposition', "attachment;filename=" . $recording);

    		return $response;
		}
		else
		{
			$info = "La grabación aún no esta disponible, debe esperar unos minutos para intentarlo nuevamente";
			return Redirect::to('callrecording')->with('info', $info);
		}
  	}

  /**
   * Descarga locuciones en bloque
   */
  public function downloadBulk()
  {
    $period = Input::get('period', 'today');
    $user   = Session::get('user');
    $number = $this->number;
    
    $strNumber = $number->getNumber();
    $idUser    = $user->getID();
    
    if(!in_array($period, array('today', 'yesterday', 'all', 'custom')))
      $period = 'today';

    $zip = new ZipArchive();
    $zipFileName = '/tmp/calls-' . $number . '-' . $period . '.zip';
    if($zip->open($zipFileName, ZipArchive::CREATE) !== true) {
      return Redirect::to('callrecording')->with('error', 'Ha habido un problema generando el ZIP.');
    }

    if($period === 'custom') {
      $periodFrom  = Input::get('period-from',  null);
      $periodUntil = Input::get('period-until', null);
      
      if($periodFrom === null || $periodUntil === null) {
        return Redirect::to('callrecording')->with('error', 'Rango inválido');
      } else {
        $periodFrom.=  ' 00:00:00';
        $periodUntil.= ' 23:59:59';
        $dtFrom  = DateTime::createFromFormat('d/m/Y H:i:s', $periodFrom);
        $dtUntil = DateTime::createFromFormat('d/m/Y H:i:s', $periodUntil);
        
        if($dtFrom === false || $dtUntil === false) {
          return Redirect::to('callrecording')->with('error', 'Rango inválido');
        } else {
          $recordings = $this->getRecordings($period, $dtFrom, $dtUntil);        
        }
      }
    } else {
      $recordings = $this->getRecordings($period);
    }
    if($recordings !== false) { 
      foreach($recordings as $recording) {
        $zip->addFile($recording['file']);
      }    
    
      $zip->close();
 
      $fp = fopen($zipFileName, 'rb');
      
      header('Content-Type: application/zip');
      header('Content-Description: File Transfer');      
      header('Content-Length: ' . filesize($zipFileName));
      header('Content-Disposition: attachment;filename="' . basename($zipFileName) . '"');

      fpassthru($fp);
      fclose($fp);
      exit;
    } else {
      return Redirect::to('callrecording')->with('error', 'No hay grabaciones para el periodo escogido');
    }
  }
  
  /**
   * Método para recuperar las grabaciones.
   * 
   * Esto es casi copia exacta de lo que tenemos en la API y deberíamos mejorarlo
   * ahí.
   * 
   * @param string   $period
   * @param DateTime $periodFrom
   * @param DateTime $periodUntil
   * 
   * @return array|boolean
   */
  private function getRecordings($period = 'today', DateTime $periodFrom = null, DateTime $periodUntil = null)
  {
    $user   = Session::get('user');
    $number = $this->number;
    
    $strNumber = $number->getNumber();
    $idUser    = $user->getID();
    
    $numberRecordingsPath = $this->dataPath . $idUser . '/recordings/' . $strNumber;
    
    $recs = array();

    if($period === 'custom' && $periodFrom->diff($periodUntil)->days > 15) {
      return false;
    }
    
    if(is_dir($numberRecordingsPath)) {
      chdir($numberRecordingsPath);

      array_multisort(array_map('filemtime', ($files = glob("*.*"))), SORT_DESC, $files);

      foreach($files as $file) {        
        list($caller, $strDate) = explode('-', $file);

        $year  = substr($strDate, 0, 4);
        $month = substr($strDate, 4, 2);
        $day   = substr($strDate, 6, 2);
                   
        $hour   = substr($strDate, 8, 2);
        $minute = substr($strDate, 10, 2);
        $second = substr($strDate, 12, 2);
 
        $dateTime = $year . '-' . $month . '-' . $day;
        $dateTime.= ' ' . $hour . ':' . $minute . ':' . $second;

        $dtDateTime = new \DateTime($dateTime);        
        
        $fifteenDaysAgo = new \DateTime('15 days ago');
        $dtDateTimeToday = new \DateTime('today');
        $dtDateTimeYesterday = new \DateTime('yesterday');

        if($period === 'today' && $dtDateTime->format('Y-m-d') != $dtDateTimeToday->format('Y-m-d'))
          continue;
        
        if($period === 'yesterday' && $dtDateTime->format('Y-m-d') != $dtDateTimeYesterday->format('Y-m-d')) 
          continue;
        
        if($period === 'custom') {
          if($dtDateTime <= $periodFrom) {
            break;
          } elseif($dtDateTime >= $periodUntil) {
            continue;
          }
        } else if($period === 'all') {
          if($dtDateTime < $fifteenDaysAgo) {
            break;
          }
        }
        
        $recs[] = array(
          'caller'   => $caller,
          'datetime' => $dateTime,
          'file'     => $file
        );
      }
    }
    
    // Si tenemos contenido en el array, lo ordenamos
    // por fecha descendente
    if(!empty($recs)) {
      usort($recs, function($rec1, $rec2) {
        $v1 = strtotime($rec1['datetime']);
        $v2 = strtotime($rec2['datetime']);
        
        return $v2 - $v1;
      });
    }
    
    return !empty($recs) ? $recs : FALSE;        
  }
}
