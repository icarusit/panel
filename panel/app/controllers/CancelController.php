<?php
/**
 * Controlador para la baja del servicio
 */
class CancelController extends BaseController
{
  /**
   * Muestra ventana de cancelación del servicio
   */
  public function show($number = false)
  {
    $user = Session::get('user');
    
    if($number === false) {
      $number = $this->number;
    } else {
      $number = Number::where('numero_visible', '=', $number)
              ->where('id_cliente', '=', $user->getID())
              ->first();
      if($number !== null) {
        $number = $this->flash->number($number->numero_visible);
        Session::put('number', $number);
      } else {
        return Redirect::to('numbers');
      }
    }
    
    return View::make('content.cancel', array(
      'title'           => 'Baja del servicio :-(',
        
      'number'          => $number->getNumber()
    ));
  }
  
  /**
   * Método que se ejecuta tras el post
   */
  public function cancel()
  {
    $reasons = Input::get('cancel_reasons', NULL);
    
    if($reasons !== NULL && strlen($reasons) > 10) {
      $info = "Hemos tramitado la baja de tu número <strong>{$this->number->getNumber()}</strong>.
              <br><br>Desde este momento no recibirás más llamadas en él.<br><br>
              Si tienes alguna pregunta adicional, no dudes en llamarnos al 800
              00 77 66.<br><br>Gracias.";

      $error = "Ha ocurrido un error al dar de baja tu número.<br><br>Por favor,
        ponte en contacto con nosotros en el 800 00 77 66.<br><br>";
      
      $number = Number::find($this->number->getID());
      $user = $this->number->getUser();
      $id_user = $this->number->getUser()->getID();
      
      $number->suspension = 1;
      $number->tipo = '';
      $number->id_cliente_baja = $id_user;
      $number->fecha_baja = date('Y-m-d H:i:s');
      $number->id_cliente = 791;
      
      $cancellation = new Cancellation();
      
      $cancellation->id_number = $number->id_numero;
      $cancellation->id_user = $id_user;
      $cancellation->date_sign_up = $number->fecha_alta;
      $cancellation->date_cancellation = (new \DateTime())->format('Y-m-d H:i:s');
      $cancellation->message = $reasons;      
      
      if($number->save() && $cancellation->save()) {        
        $subject = "Confirmación de baja del número {$number->numero_visible} desde la web";
        $header = "Has dado de baja tu número <strong>{$number->numero_visible}</strong> :(";
        $body = "<p>Te confirmamos que has dado de baja el número <strong>{$number->numero_visible}
          </strong> desde tu zona de cliente de .".Session::get('distribuidor')."</p>
          <p>De ahora en adelante ya no recibirás llamadas en tu número.</p>
          <p>Los motivos de la baja han sido los siguientes:
          <p style='font-style: italic;'>$reasons</p>
          <p>Si no querías dar de baja tu número o tienes alguna pregunta
          adicional, no dudes en llamarnos al 800 00 77 66.</p>
          <p>Gracias.</p>";
       
        $content = array(
          'header' => $header,
          'body'   => $body
        );
		
		if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->getEmail(), $subject, $content);
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de números ".Session::get('distribuidor')."");
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->getEmail());
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}
        
        $this->hipchat(':poop: El usuario **' . $user->getName() . ' (ID ' . $user->getID() .
        ')** ha procesado la baja de su número **' . $this->number->getNumber() . '** desde la web. La razón ha sido: '. str_replace("\r\n", " ",$reasons),'rfghqo1saifkxnm1cjxy89ba9h');
        
        Session::forget('number');
        return Redirect::to('numbers')->with('info', $info);                
      } else {
        return Redirect::to('cancel')->with('error', $error);
      }
    } else {
      $error = "Para tramitar la baja online es OBLIGATORIO que escribas tus
        motivos de la baja.<br><br>Esto nos ayudará a ver qué estamos haciendo mal
        y cómo podemos mejorarlo en un futuro.<br><br>Gracias.";
      
      return Redirect::to('cancel')->with('error', $error);
    }
  }
}
