<?php
/**
 * Controlador para el cambio de plan de un número
 */
class ChangePlanController extends BaseController
{  
  /**
   * Método que muestra las opciones de planes
   * disponibles para el producto contratado
   * 
   * @param string $number Número (opcional)
   */
  public function show($number = null)
  {    
    $data = array();    

    $number = $number === null ? $this->number : $this->flash->number($number);
    
    // Comprobamos que el usuario sea dueño del número
    $user = User::find($this->user->getID());
    if(!$user->hasNumber($number->getNumber())) {
      return Redirect::to('numbers');
    }        

    Session::put('number', $number);    
    $this->number = $number;
    
    $rowNumber = Number::find($number->getID());
    
    $data['number'] = $rowNumber;
    
    $currentProfile = Profile::find($rowNumber->id_perfil);   
    $data['currentProfile'] = $currentProfile;
    
    $canChangePlan = $this->canChangePlan();
    $data['canChangePlan'] = $canChangePlan;
    
    if(!$canChangePlan) {
      $data['changePlanDate'] = new DateTime($rowNumber->change_plan_date);
      $data['nextPossibleChangePlanDate'] = new DateTime('first day of next month');
      
      return View::make('content.plan.change-plan', $data);
    }
    
    if($numberHasCustomIPPlan = $currentProfile->getInfoIPPlan() !== false) {
      $infoPlan = $numberHasCustomIPPlan;
      
      $data['infoPlan'] = $currentProfile->getInfoIPPlan();
      $data['currentProfilePrice'] = $currentProfile->cuota;
      $data['observaciones'] = $currentProfile->observaciones;      
    } else {
      $canOrderProfiles = Profile::where('product_type', '=', $currentProfile->product_type)
                                  ->where('can_order_online', '=', true)
                                  ->get();

      foreach($canOrderProfiles as &$profile) { 
        $profile->pros = $currentProfile->getDiffPros($profile);
      }

      $data['canOrderProfiles'] = $canOrderProfiles;
    }
    
    $data['numberHasCustomIPPlan'] = $numberHasCustomIPPlan;

    if($numberHasCustomIPPlan) {
      $view = 'content.plan.change-custom-ip-plan';
    } else {
      $view = 'content.plan.change-plan';
    }
    
    return View::make($view, $data);    
  }
  
  /**
   * Método que se ejecuta tras haber elegido el usuario
   * el cambio de plan
   */
  public function postChange()
  {
    // Comprobamos que el usuario sea dueño del número
    $user = User::find($this->user->getID());
    if(!$user->hasNumber($this->number->getNumber())) {
      return Redirect::to('numbers');
    }
    
    if($this->canChangePlan()) {
      $profileId = Input::get('profile-id', null);
      
      if($profileId === null) {
        return Redirect::to('change-plan');
      } else {
        $rowNumber = Number::find($this->number->getID());
        
        $rowNumber->perfil_original = $rowNumber->id_perfil;
        $rowNumber->change_plan_date = (new \DateTime())->format('Y-m-d H:i:s');        
        $rowNumber->id_perfil = $profileId;
        
        $oldProfile = Profile::find($rowNumber->perfil_original);
        $currentProfile = Profile::find($profileId);
        
        if($rowNumber->save()) {          
          // Enviamos correo a cliente notificando el cambio
          // de plan
          $subject = "Has cambiado el plan de tu número {$rowNumber->numero_visible}";
          $subjectFlash = "El cliente con ID {$user->id_cliente} ha cambiado el plan del número {$rowNumber->numero_visible}";

          $header = "Has cambiado el plan de tu número <strong>{$rowNumber->numero_visible}</strong>";
          $subheader = "El nuevo plan es {$currentProfile->perfil}";

          $body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
          "</strong> has efectuado correctamente el cambio de perfil de tu número
          <strong>{$rowNumber->numero_visible}</strong>.</p>
          <p>El plan antiguo era <strong>{$oldProfile->perfil}</strong> y el nuevo plan
          <strong>{$currentProfile->perfil}</strong>.</p>
          <p>Para cualquier consulta adicional puedes contestar este mismo correo.</p>
          <p>Gracias por tu confianza.</p><p>El equipo de ".Session::get('distribuidor').".</p>";

          $content = array(
            'header'    => $header,
            'subheader' => $subheader,
            'body'      => $body
          );

                    
          //$this->flash->sendMail('info@fmeuropa.com', $subjectFlash, $content); 
		  
		  if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content); 
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}         
          
          return View::make('content.plan.plan-changed', array(
            'number'         => $rowNumber->numero_visible,
            'currentProfile' => $currentProfile
          ));
        }
      }
    } else {
      return Redirect::to('change-plan');
    }
  }
  
  /**
   * Método que se ejecuta tras haber elegido el usuario
   * el cambio de plan cuando es plan personalizado IP
   */
  public function postPlanCustomIP()
  {
    $user   = User::find($this->user->getID());
    $number = Number::find($this->number->getID());
    
    $newProfileText = Input::get('profile', null);
    
    if($newProfileText !== null) {
      $currentProfile = Profile::find($this->number->getProfile()->getID());
      $curProfileInfo = $currentProfile->getInfoIPPlan();
      
      $newProfile = Profile::getIPPlanByText($newProfileText);
      $newProfileInfo = $newProfile->getInfoIPPlan();      

      // Reparto equitativo de número de canales por líneas IP
      // Si no es un cociente entero, el resto se lo sumamos a la primera 
      // línea SIP 
      $channels_per_sip = floor($newProfileInfo->channels / $newProfileInfo->sip_accounts);
      $channels_first_sip = $channels_per_sip + ($newProfileInfo->channels % $newProfileInfo->sip_accounts);

      if($newProfile instanceof Profile) {
        /////////////////
        // Cuentas SIP //
        /////////////////
        $diff = abs($curProfileInfo->sip_accounts - $newProfileInfo->sip_accounts);
        
        if($newProfileInfo->sip_accounts > $curProfileInfo->sip_accounts) {
          /////////////////
          // Prioridades //
          /////////////////

          // Varios escenarios aquí, si el plan nuevo no es PBX, las prioridades
          // las dejamos a uno
          if(!$newProfileInfo->pbx) {
            $priority = 1;
          } else {
            // Si es PBX, recuperamos la última prioridad que tenía antes, sólo
            // el caso de que el plan antiguo fuera PBX
            if($curProfileInfo->pbx) {
              $result = DB::select('SELECT MAX(prioridad) as `priority` FROM sippeers WHERE callerid = ?', array($number->numero_visible));              
              $priority = (int)$result[0]->priority;
            } else {
              // El perfil antiguo no era PBX y el nuevo sí. Actualizamos
              // las cuentas actuales con la prioridad, empezando por la
              // primera
              $priority = 1;
              
              $sipAccounts = SIPPeers::where('callerid', '=', $number->numero_visible)
                                      ->get();
              
              foreach($sipAccounts as $sipAccount) {
                $sipAccount->prioridad = $priority;
                $priority++;
              }
            }          
          }

          //
          // Creamos las cuentas
          //
          $sipAccounts = array();          
          for($i=0; $i<$diff; $i++) {
            $sipAccounts[] = SIPPeers::add($user, $number, $priority, !$i ? $channels_first_sip : $channels_per_sip, true);            
          }
        } elseif ($newProfileInfo->sip_accounts < $curProfileInfo->sip_accounts) {          
          // Eliminamos la diferencia de cuentas
          $accountsToRemove = SIPPeers::where('callerid', '=', $number->numero_visible)
                              ->take($diff)
                              ->get();
          
          foreach($accountsToRemove as $account)
            $account->delete();         
        }        
        
        // Con las cuentas SIP restantes intentamos repartir equitativamente 
        // el número de canales por líneas IP
        $sipAccounts = SIPPeers::where('callerid', '=', $number->numero_visible)->get();

        $i = 0;
        foreach($sipAccounts as $account) {
          $account->{'call-limit'} =  !$i ? $channels_first_sip : $channels_per_sip;
          $account->save();
          $i++;
        }        
        
        // Actualizamos cambio de perfil en el número
        $number->perfil_original = $number->id_perfil;
        $number->change_plan_date = (new \DateTime())->format('Y-m-d H:i:s');         
        $number->id_perfil = $newProfile->id;
        $number->save();        
        $this->refresh();     
                
        // Y mandamos el e-mail con el resumen final de la configuración
        $subject = "Has modificado el plan de tu número IP {$number->numero_visible}";
        $subjectFlash = "El cliente con ID {$user->id_cliente} ha cambiado el plan del número IP {$number->numero_visible}";

        $header = "Has cambiado la configuración IP de tu número <strong>{$number->numero_visible}</strong>";
        $subheader = "El nuevo plan tiene la opción de <strong>centralita " . ($newProfileInfo->pbx ? 'activada' : 'desactivada') .
        "</strong>, <strong>{$newProfileInfo->sip_accounts}</strongs> cuentas SIP con un total de "
        . "<strong>{$newProfileInfo->channels}</strong> canales de voz.";
        
        $body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
        "</strong> has efectuado cambios en la configuración IP del número
        <strong>{$number->numero_visible}</strong>.</p>
        <p>La nueva cuota mensual es <strong>{$newProfile->cuota} €</strong>.</p>          
        <p>Este es el resumen de tu nueva configuración IP:</p>";
		if(Session::get('id_distribuidor') == 1){
        $sipAccountsTable = '' . 
        '<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
          '<tr>' . 
            '<th>Host</th>' . 
            '<th>Usuario</th>' . 
            '<th>Contraseña</th>' . 
            '<th>Códecs válidos</th>' . 
            '<th>Softphone ya configurado</th>' . 
          '</tr>';
		}else{
			$sipAccountsTable = '' . 
        '<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
          '<tr>' . 
            '<th>Host</th>' . 
            '<th>Usuario</th>' . 
            '<th>Contraseña</th>' . 
            '<th>Códecs válidos</th>' . 
            '<th>Softphone</th>' . 
          '</tr>';
		}
        foreach($sipAccounts as $sipAccount) {
			if(Session::get('id_distribuidor') == 1){
           $sipAccountsTable.= '' . 
            '<tr>' . 
              '<td>213.139.7.254</td>' . 
              "<td>{$sipAccount->defaultuser}</td>" . 
              "<td>{$sipAccount->secret}</td>" . 
              '<td>G729, G711 (A-law), GSM</td>' . 
              '<td>' . 
                "<a href='https://www.zoiper.com/en/page/81cb7377842f6306ec688b4105efe6a9?u={$sipAccount->defaultuser}&h=sip.fmeuropa.com&p={$sipAccount->secret}&o=&t=&x=&a=&tr='" . 
                 " title='Descargar Zoiper preconfigurado'>Zoiper</a>" .
              '</td>' .
            '</tr>'; 
			}else{
				$sipAccountsTable.= '' . 
            '<tr>' . 
              '<td>213.139.7.254</td>' . 
              "<td>{$sipAccount->defaultuser}</td>" . 
              "<td>{$sipAccount->secret}</td>" . 
              '<td>G729, G711 (A-law), GSM</td>' . 
              '<td>' . 
                "<a href='http://www.zoiper.com/en/voip-softphone/download/zoiper3'" . 
                 " title='Descargar Zoiper'>Zoiper</a>" .
              '</td>' .
            '</tr>'; 
			}
        }

        $sipAccountsTable.= '</table>';

        $body.= $sipAccountsTable . "<p>Para cualquier consulta adicional puedes contestar este mismo correo
        o llamarnos al número gratuito <strong>123</strong> desde tu terminal VoIP.</p>
        <p>Gracias por tu confianza.</p><p>El equipo de ".Session::get('distribuidor').".</p>";

        $content = array(
          'header'    => $header,
          'subheader' => $subheader,
          'body'      => $body
        );

        $this->flash->sendMail($user->mail_1, $subject, $content); 
        
        $info = "<p>Has modificado la configuración del número correctamente.</p>" .
                "<p>Te hemos enviado el resumen de la nueva configuración por e-mail.</p>";        
        return Redirect::to('numbers')->with('info', $info);         
      } else {
        return Redirect::to('change-plan');  
      }
    } else {
      return Redirect::to('change-plan');
    }
  }
  
  /**
   * Método que comprueba si el usuario puede cambiar o no
   * el plan
   * 
   * @return boolean
   */
  private function canChangePlan()
  {
    $number = Number::find($this->number->getID());

    if($number->change_plan_date === null
       || $number->change_plan_date === '0000-00-00 00:00:00') {
      return true;
    } else {
      $dateToday  = new DateTime();
      $dateChange = new \DateTime($number->change_plan_date);
      $validDate  = new \DateTime('last day of last month');
      
      if($dateToday->format('Y-m-d') === $dateChange->format('Y-m-d')) {
        return true;
      } else {
        if($validDate >= $dateChange) {
          return true;
        } else {
          return false;
        }
      }
    }    
  }  
}
