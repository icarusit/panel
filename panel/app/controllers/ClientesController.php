<?php

use \FlashTelecom\Number;

class ClientesController extends BaseController
{  
  /**
   * Muestra la página con los clientes del distribuidor
   * 
   * @param integer $page
   * @return type
   * 
   * @todo si la consulta es vía ajax, limitar a 5 para
   * no hacer una petición tan tocha cada reload de la página
   */
  public function showClientes($page = 1)
  {
    if($page < 1) { return Redirect::to('misclientes'); }    
    
    $user = Session::get('clientes');
	$dist = Session::get('id_distribuidor');

    $clientesCount = count($user);
	
	$clientes = $user;
	
	$perPage = 16;
	$totPages = floor($clientesCount / $perPage);

    if (isset($totPages) && ($page - 1) > $totPages)
      return Redirect::to('misclientes/' . $totPages);
    
    return View::make('content/clientes', array(
      'title'          => 'Mis clientes',
      'currentPage'    => $page,
      'numberOfPages'  => $totPages,
      'clientes'        => $user,
      'selectedCliente' => Session::get('number', NULL)
    ));
  } 
  
  /**
   * Devuelve los números del usuario para poner en la barra
   * superior. 
   * 
   * @todo   Es una burrada, ya que si sólo vamos a mostrar 5 números
   *         sólo deberíamos pedir 5 números. Refactorizar junto con
   *         la API.
   * 
   * @return string Cadena json
   */
  public function returnTopBarNumbers()
  {
    $user = Session::get('user');
    $numbers = $user->numbers(1);
    
    return json_encode(array_slice($numbers->user_numbers, 0, 5));
  }
  
  /**
   * Fija en la sesión el número actual en el que está el usuario
   * trabajando.
   * 
   * @param string $strNumber Número a fijar
   */
  public function setCliente($strCliente, $page = 1)
  {
    if($page < 1) { return Redirect::to('misclientes'); }    
    $clientes = DB::table('cliente')->select('id_cliente','nombre_fiscal')->where('id_distribuidor', '=', Session::get('id_distribuidor'))->where('usr_usuario','=',$strCliente)->get();
    if ($clientes) {      
      try {
        $cliente = $this->flash->user($clientes[0]->id_cliente)->numbers($page)->user_numbers;
		$numbersCount = count($cliente);
        // Borramos los filtros de las estadísticas
        Session::forget('filters');
        
        //Session::put('numbers', $cliente);
		
		$perPage = 16;
		$totPages = floor($numbersCount / $perPage);
        return View::make('content/clientesnumbers', array(
		  'title'          => 'Números del cliente ' . $clientes[0]->nombre_fiscal,
		  'currentPage'    => $page,
		  'numberOfPages'  => $totPages,
		  'numbers'        => $cliente,
		  'clienteNombre'  => $clientes[0]->nombre_fiscal,
		  'usuario'		   => $strCliente
		));
      } catch(\FlashTelecom\NotAFlashNumberException $e) {
        Session::forget('numbers');
      }
    }
    
    return Redirect::to(URL::previous());
  }
  
 	
}

?>
