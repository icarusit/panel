<?php
/**
 * Controlador para la agenda de contacto
 */
class ContactAgendaController extends BaseController
{
	var $id_usuario;
	function ContactAgendaController()
	{
		parent::__construct();
		$this->id_usuario = Session::get('user')->getID();
	}
  	/**
    * Muestra la ventana principal
    */
  	public function show()
  	{
    	$listaContactos = ContactAgenda::where('id_client', '=', $this->id_usuario)->get();
     
    	return View::make('content.contact_agenda.list', array(
      	'number'  => $this->number->getNumber(),
      	'contactos' => $listaContactos
    	));
  	}
  
  	/**
   	* Método que añade un nuevo número de origen
   	* a la lista blanca del número para llamadas
   	* salientes vía el proxy
   	*/
  	public function add()
  	{
    	$name = Input::get('name');
		$number = Input::get('number');

    	$rules = array(
      		'number' => 'required|numeric',
			'name' => 'required'
    	);
    
    	$messages = array(
      		'number.required' => 'El campo Número es obligatorio',          
     		'number.numeric'  => 'El campo debe ser numérico.',
			'name.required' => 'El campo Nombre es obligatorio',
    	);
    
    	$validator = Validator::make(Input::all(), $rules, $messages);

    	if($validator->fails())
		{
      		return Redirect::to('contact-agenda')->withErrors($validator);
    	}
		else
		{
      		$contactAgenda = new ContactAgenda;

      		$contactAgenda->id_client = $this->id_usuario;
      		$contactAgenda->number    = $number;
      		$contactAgenda->name    = $name;
      
      		if($contactAgenda->save())
			{
        		$info = "El contacto <strong>".$contactAgenda->name."</strong> ha sido añadido correctamente.";
        		return Redirect::to('contact-agenda')->with('info', $info);
      		}
			else
			{
        		$error = "Ha habido algún problema al añadir el contacto a la lista. Inténtelo de nuevo más tarde.";        
        		return Redirect::to('contact-agenda')->with('error', $error);
      		}
    	}
  	}
	
	public function formUpdate($idContact)
	{
		$contactToUpdate = ContactAgenda::find($idContact);
		if($contactToUpdate)
		{
			return View::make(	'content.contact_agenda.update', array(
      							'number'  => $this->number->getNumber(),
      							'contacto' => $contactToUpdate
    						));
		}
		else
		{
			
		}
	}
	
	public function update()
  	{
		$idContact = Input::get('id');
		$contactToUpdate = ContactAgenda::find($idContact);
		if($contactToUpdate)
		{			
			$name = Input::get('name');
			$number = Input::get('number');
	
			$rules = array(
				'number' => 'required|numeric',
				'name' => 'required'
			);
		
			$messages = array(
				'number.required' => 'El campo Número es obligatorio',          
				'number.numeric'  => 'El campo debe ser numérico.',
				'name.required' => 'El campo Nombre es obligatorio',
			);
		
			$validator = Validator::make(Input::all(), $rules, $messages);
	
			if($validator->fails())
			{
				return Redirect::to('contact-agenda/update/'.$idContact)->withErrors($validator);
			}
			else
			{	
				$contactToUpdate->number    = $number;
				$contactToUpdate->name    = $name;
		  
				if($contactToUpdate->save())
				{
					$info = "El contacto <strong>".$contactToUpdate->name."</strong> ha sido modificado correctamente.";
					return Redirect::to('contact-agenda')->with('info', $info);
				}
				else
				{
					$error = "Ha habido algún problema al actualizar el contacto a la lista. Inténtelo de nuevo más tarde.";        
					return Redirect::to('contact-agenda/update/'.$idContact)->with('error', $error);
				}
			}
		}
		else
		{
			
		}
  	}
  
  	/**
   	* Método para borrar números de la lista de origen
   	*/
  	public function remove($idContact)
  	{    
    	$contactToRemove = ContactAgenda::find($idContact);
    	if($contactToRemove->delete())
		{
      		$info = "El contacto  ha sido eliminado de la lista.";
      		return Redirect::to('contact-agenda')->with('info', $info);
    	}
		else
		{
      		return Redirect::to('contact-agenda');
    	}
  	}
}
