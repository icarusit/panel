<?php

class DoNotDisturbController extends BaseController
{
  /**
   * Muestra ventana del modo No Molestar
   * 
   * @return View
   */
  public function showDND()
  {    
    return View::make('content.donotdisturb', array(
      'title'       => 'Modo No Molestar',
      'isDNDActive' => $this->number->isDNDActive()
    ));
  }
  
  /**
   * Fija el modo DND
   */
  public function setDND()
  {
    $dnd = Input::get('dndmode');
    $return = Input::get('return', 'dnd');
    
    $this->number->setDND($dnd === 'enable' ? TRUE : FALSE);
    $this->refresh();
    
    $info = 'El modo "No molestar" ha sido correctamente <strong>';
    $info.= $dnd === 'enable' ? 'activado' : 'desactivado';
    $info.= '</strong>.';
    
    return Redirect::to($return)->with('info', $info);
  }
}
