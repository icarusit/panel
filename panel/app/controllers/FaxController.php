<?php

class FaxController extends BaseController
{
  /**
   * Muestra los faxes recibidos
   * 
   * @return type
   */
  public function showReceivedFaxes()
  {
    // ÑAPA ALERT    
    // Para los números que migramos a SS7 el histórico
    // de faxes estará en 'faxtomail_old'
    $rowNumber = Number::find($this->number->getID());       
    
    $signUpDate = $this->number->getSignUpDate();
    
    $faxes = ReceivedFax::on('flashmedia')
            ->whereRaw('did = ? OR did = ?', array($this->number, $rowNumber->faxtomail_old))
            ->where('fecha', '>=', $signUpDate->format('Y-m-d H:i:s'))
            ->orderBy('fecha', 'desc')
            ->paginate(15);
    
    return View::make('content.fax.received', array(
      'title' => 'Faxes recibidos',
      'faxes' => $faxes
    ));
  }
  
  /**
   * Muestra los faxes enviados
   * 
   * @return type
   */
  public function showSentFaxes()
  {
    // ÑAPA ALERT    
    // Para los números que migramos a SS7 el histórico
    // de faxes estará en 'faxtomail_old'
    $rowNumber = Number::find($this->number->getID());           
    
    $signUpDate = $this->number->getSignUpDate();
    
    $sql = 'SELECT a.id as `id`, a.from_address_header as `from`, a.dnid as `to`,
      a.nro_paginas as `no_of_pages`, b.estado as `status`, b.ultima_fecha as
      `date` FROM flashmedia.buzon_imap a, flashmedia.mail2fax b WHERE (a.did = '.$this->number.'
      '.(trim($rowNumber->faxtomail_old) != ''?'OR a.did = "'.$rowNumber->faxtomail_old.'"':'').') AND b.ultima_fecha >= "' . $signUpDate->format('Y-m-d H:i:s') . '" 
      AND a.id = b.buzon_imap ORDER BY `date` DESC LIMIT 15';

    
    $faxes = DB::connection('flashmedia')->select($sql, array());    
   
    return View::make('content.fax.sent', array(
      'title' => 'Faxes enviados',
      'faxes' => $faxes
    ));
  }
  
  /**
   * Muestra la configuración de Fax
   * 
   * @return type
   */
  public function showConfig()
  {
    $number = Number::find($this->number->getID());
    
    $mailfax = array(
      '0' => array(
        'mail' => $number->mailfaxes,
        'rx'   => (bool)$number->recibirfax1,
        'send' => true
      ),
      '1' => array(
        'mail' => $number->mailfaxes2,
        'rx'   => (bool)$number->recibirfax2,
        'send' => true
      ),
      '2' => array(
        'mail' => $number->mailfaxes3,
        'rx'   => (bool)$number->recibirfax3,
        'send' => true
      ),
      '3' => array(
        'mail' => $number->mailfaxes4,
        'rx'   => (bool)$number->recibirfax4,
        'send' => true
      ),                     
      '4' => array(
        'mail' => $number->mailfaxes5,
        'rx'   => (bool)$number->recibirfax5,
        'send' => true
      )
    );    
    
    return View::make('content.fax.config', array(
      'mailfax' => $mailfax
    ));
  }
  
  /**
   * Método para fijar el email principal donde enviar-recibir faxes
   * 
   * Lo usamos durante el alta, cuando el número no está
   * en la sesión (ÑAPA)
   * 
   * @todo Falta validar email
   */
  public function saveMainMailfax()
  {
    if(($mailfax = Input::get('mailfax', null)) !== null &&
        ($number = Input::get('number', null)) !== null) {
      $redirect = Input::get('redirect');
      $number = Number::where('numero_visible', '=', $number)
              ->where('id_cliente', '=', Session::get('user')->getID())
              ->first();
      
      $number->mailfaxes = $mailfax;       
      $number->recibirfax1 = 1;     
              
      if($number->save()) {
        $info = 'La configuración de fax ha sido correctamente actualizada';
        return Redirect::to($redirect)->with('info', $info);
      } else {
        $info = 'Ha habido un error actualizando la configuración de fax';
        return Redirect::to($redirect)->with('error', $error);        
      }
    }
  }
  
  /**
   * Guarda la configuración del fax
   */
  public function saveConfig()
  {
    if(($mailfaxes = Input::get('mailfax', null)) !== null) {      
      $validationMessages = array(
        'email' => 'Comprueba las direcciones de correo electrónico. Alguna no parece válida'
      );
      
      foreach($mailfaxes as $mailfax) {
        if(trim($mailfax['mail']) !== '') {        
          $validator = Validator::make(
            array('mail' => $mailfax['mail']),
            array('mail' => 'email'),
            $validationMessages 
          );
          
          if($validator->fails()) {
            return Redirect::to('fax/config')->withErrors($validator);
          }
        }
      }
      
      $number = Number::find($this->number->getID());      
      
      foreach($mailfaxes as $i => $mailfax) {
        if($i === 0) {
          $number->mailfaxes = $mailfax['mail'];          
          $number->recibirfax1 = isset($mailfax['rx'])
            ? $mailfax['rx']
            : 0;
        } else {
          $number->{'mailfaxes' . ($i+1)} = $mailfax['mail'];
          $number->{'recibirfax' . ($i+1)} = isset($mailfax['rx'])
            ? $mailfax['rx']
            : 0;
          if(isset($mailfax['rx'])) {
            
          }

          if(isset($mailfax['send'])) {}
        }
      }
      
      if($number->save()) {
        $info = 'La configuración de fax ha sido correctamente actualizada';
        return Redirect::to('fax/config')->with('info', $info);
      } else {
        $info = 'Ha habido un error actualizando la configuración de fax';
        return Redirect::to('fax/config')->with('error', $error);        
      }
    } else {      
      return Redirect::to('fax/config');
    }
  }
  
  /**
   * Genera el certificado de envío de un fax
   */
  public function downloadCertificate($idFax)
  {        
    $sql = 'SELECT a.dnid as `to`, a.nro_paginas as `pages`, b.ultima_fecha as
      `date` FROM flashmedia.buzon_imap a, flashmedia.mail2fax b WHERE a.id = b.buzon_imap
       AND a.id = ? LIMIT 1';

    $query = DB::connection('flashmedia')->select($sql, array($idFax));
    
    $fax = reset($query);          
    
    if($fax !== false) {
      $date = \DateTime::createFromFormat('Y-m-d H:i:s', $fax->date);
      
      $content = View::make('content/fax_certificate', array(
        'from'  => $this->number,
        'to'    => $fax->to,
        'date'  => $date->format('d/m/Y'),
        'time'  => $date->format('H:i:s'),
        'pages' => $fax->pages
      ));
         
      $html2pdf = new HTML2PDF('P', 'A4', 'es');
      $html2pdf->writeHTML($content);      
      
      $response = Response::make($html2pdf->output());
      $response->header('Content-Type', 'application/pdf');
      $response->header('Content-length', strlen($fax->archivo_pdf));
      $response->header('Content-Disposition', "attachment;filename=fax_certificate_" . $idFax . ".pdf");
      
      return $response;
    }
  }
  
  /**
   * Descarga un fichero de fax recibido
   * 
   * @param integer $idFax
   */
  public function downloadReceivedFax($idFax)
  {
    // ÑAPA ALERT    
    // Para los números que migramos a SS7 el histórico
    // de faxes estará en 'faxtomail_old'
    $rowNumber = Number::find($this->number->getID());           
    
    $fax = ReceivedFax::on('flashmedia')->find($idFax);
    
    if($fax->did === $this->number->getNumber()
            || $fax->did === $rowNumber->faxtomail_old) {      
      $response = Response::make($fax->archivo_pdf, 200);

      $title = "fax_" . $fax->callerid . "_" . $fax->did;
      
      $response->header('Content-Type', 'application/pdf');
      $response->header('Content-length', strlen($fax->archivo_pdf));
      $response->header('Content-Disposition', "attachment;filename=" . $title . ".pdf");

      return $response;
    } else {
      return Redirect::to('fax/received');
    }      
  }
  
  /**
   * Descarga un fichero de fax enviado
   * 
   * @param integer $idFax
   */
  public function downloadSentFax($idFax)
  {
    // ÑAPA ALERT    
    // Para los números que migramos a SS7 el histórico
    // de faxes estará en 'faxtomail_old'
    $rowNumber = Number::find($this->number->getID());      
    
    $sql = 'SELECT pdf_attachment as `content`, did FROM buzon_imap WHERE id = ? AND (did = ? OR did = ?) LIMIT 1;';    
    $query = DB::connection('flashmedia')->select($sql, array($idFax, $this->number, $rowNumber->faxtomail_old));
    
    $fax = reset($query);      
    
    $response = Response::make($fax->content, 200);

    $title = "fax_" . $fax->did;

    $response->header('Content-Type', 'application/pdf');
    $response->header('Content-length', strlen($fax->content));
    $response->header('Content-Disposition', "attachment;filename=" . $title . ".pdf");

    return $response;    
  }
  
  /**
   * Envía un fax recibido por correo electrónico
   * 
   * @param integer $idFax
   */
  public function sendReceivedFaxByEmail($idFax)
  {    
    $email = Input::get('email', NULL);
    
    if($email !== NULL && filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return 'El fax ha sido correctamente reenviado a la dirección <strong>' .
              $email . '</strong>.';
    } else {
      return 'Ha habido un error al mandar el fax vía e-mail. Comprueba la dirección
        de correo introducida o inténtalo de nuevo más tarde.';
    }
  }
    
  /**
   * Envía un fax enviado por correo electrónico
   * 
   * @param integer $idFax
   */
  public function sendSentFaxByEmail($idFax)
  {
    $email = Input::get('email', NULL);
    
    if($email !== NULL && filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return 'El fax ha sido correctamente reenviado a la dirección <strong>' .
              $email . '</strong>.';
    } else {
      return 'Ha habido un error al mandar el fax vía e-mail. Comprueba la dirección
        de correo introducida o inténtalo de nuevo más tarde.';
    }
  } 
}

?>
