<?php

class FlatRatesController extends BaseController
{
  /**
   * Muestra ventana de las tarifas planas contratadas
   * 
   * @return View
   */
  public function show()
  {    
    return View::make('content.flatrates.flatrates', array(
      'title'     => 'Tarifas planas contratadas',
      'flatrates' => $this->getNumberFlatRates(),
      'profile'   => $this->number->getProfile()
    ));
  }  
  
  /**
   * Método que devuelve las tarifas planas contratadas del número
   * 
   * @return type
   */
  public function getNumberFlatRates()
  {
    $sql = 'SELECT fr.name, fr.description, nfr.minutes_left, fr.minutes, 
      nfr.since FROM flatrates fr, number_flatrates nfr WHERE id_number = ? AND
      nfr.id_flatrate = fr.id_flatrate ORDER BY fr.order ASC;';
    
    $flatrates = DB::select($sql, array($this->number->getID()));
    
    return $flatrates;
  }
  
  /**
   * Método que muestra la ventana de contratación de tarifas planas
   * 
   * @return View
   */
  	public function order()
  	{
    	$flatrates = FlatRate::where('can_order_online', '=', true)
                          ->orderBy('order')
                          ->get();
		$idUser = Session::get('user')->getID();
		$cliente = User::find($idUser);
    
    	return View::make('content.flatrates.order', array(
      		'title'     => 'Contratar tarifa plana',
      		'flatrates' => $flatrates,
			'cliente'   => $cliente
    	));
  	}
  
  /**
   * Se ejecuta al iniciar pedido desde tarifas planas
   */
  	public function postOrder()
  	{
    	$flatrate = Input::get('flatrate-id', null);
    
    	$flatrate = FlatRate
      		::where('id_flatrate', '=', $flatrate)               
      		->where('can_order_online', '=', true)->first();

    	// Si es válida la tarifa plana escogida
    	if($flatrate instanceof FlatRate)
		{
      		$total = $flatrate->monthly_fee;
			
			/*
			Obtener el iva de la ficha del cliente
			*/			
			$idUser = Session::get('user')->getID();
			$cliente = User::find($idUser);
			$iva = $cliente->IVA;
			if($iva > 0)
			{
				$total = $total + $total*$iva/100;
			}
      
      		// Referencia del pago
      		$order = 'TP' . $flatrate->id_flatrate . '-' . rand(1000000, 9999999);
    
      		// Almacenamos el intento de pago de tarifa plana
      		if(Payment::register($total, $order, 'tplana'))
			{
        		$urlOK = URL::to('flatrates/order/success');
        		$urlKO = URL::to('flatrates/order/error');
        		return Payment::redirectToGateway($total, $order, $urlOK, $urlKO);
      		}
			else
			{
        		$error = "Lo sentimos, ha habido un error.";
        		return Redirect::to('flatrates/order')->with('error', $error);          
      		}
    	}
		else
		{
      		$error = "Lo sentimos, las tarifa plana escogida no es válida.";
      		return Redirect::to('flatrates/order')->with('error', $error);          
    	}
  	}
  
  /**
   * Método que se ejecuta tras el callback del TPV
   * 
   * El usuario no llega nunca a esta ruta, sólo RedSys
   * tras el POST de confirmación del pago.
   * 
   * @param string $order  Referencia transacción
   * @param float  $amount Total 
   */
  public function callback($order, $amount)
  {
    $row = Payment::where('referencia', '=', $order)->firstOrFail();      

    if($row->resultado == 'NA' && $row->tipo = 'tplana') {
      //
      // Actualizamos el estado de la operación
      //
      $row->resultado = 'OK';
      $row->save();
      
      // Qué tarifa plana es
      $idFlatrate = substr($order, 2, strpos($order, '-'));
      $flatrate = FlatRate::find($idFlatrate);
     
      // Añadimos la tarifa plana al usuario
      $numberFR = new NumberFlatRate();

      $numberFR->id_number    = $row->id_numero;      
      $numberFR->id_flatrate  = $idFlatrate;
      $numberFR->minutes_left = $flatrate->minutes;
      $numberFR->active       = true;
      $numberFR->since        = (new DateTime())->format('Y-m-d H:i:s');
                
      if($numberFR->save()) { 
        // Notificamos al cliente de la contratación de la TP.
        $subject = "Has contratado la tarifa plana {$flatrate->name}";
        $header = "Has contratado la tarifa plana <strong>{$flatrate->name}</strong>";
        $subheader = "{$flatrate->description} (<strong>{$flatrate->minutes}</strong>).";
        $body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
        "</strong> has contratado la tarifa plana <strong>{$flatrate->name} " .
        "({$flatrate->description})</strong> por <strong>{$flatrate->monthly_fee}€</strong> " .
        "al mes.</p><p>La referencia de la transacción para futuras consultas es <strong>{$order}" . 
        "</strong>.</p><p>Gracias por tu confianza.</p><p>El equipo de ".Session::get('distribuidor').".</p>";
                    
        $content = array(
          'header'    => $header,
          'subheader' => $subheader,
          'body'      => $body
        );

        $user = User::find($row->id_cliente);
		
		if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content);
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}
      }
    }
  }
  
  /**
   * Muestra la ventana de error con el pago de tarifa plana
   */
  public function showError()
  {
    if(
       ($order    = Input::get('Ds_Order', null)) !== null &&
       ($response = Input::get('Ds_Response', null)) !== null
      ) {
      $row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      if($row !== null) {
        $info = "<p>Lo sentimos, ha habido algún problema con el pago de la tarifa plana.</p>";
    
        return Redirect::to('flatrates/order')->with('error', $error);          
      } else {
        return Redirect::to('flatrates/order');                                                                                                   
      }                                                                                                                                    
    } else {                                                                                                                               
      return Redirect::to('flatrates/order');                                                                                                     
    }
  }
  
  /**
   * Muestra la ventana de pago correcto de tarifa plana
   */
  public function showSuccess()
  {
    if(
       ($order    = Input::get('Ds_Order', null)) !== null &&
       ($response = Input::get('Ds_Response', null)) !== null
      ) {
      $row = Payment::where('referencia', '=', $order)->firstOrFail();

      if($row !== null) {
        $info = "<p>Has contratado correctamente la tarifa plana.</p>";
        
        return Redirect::to('flatrates/order')->with('info', $info);
      } else {
        return Redirect::to('flatrates/order');                                                                                                   
      }                                                                                                                                    
    } else {                                                                                                                               
      return Redirect::to('flatrates/order');                                                                                                     
    }
  }  
}
