<?php

class ForwardsController extends BaseController
{
	var $prefijos_no_validos = array(	0 => '902',
	  									1 => '901',
										2 => '803',
										3 => '806',
										4 => '807',
										5 => '0034902',
										6 => '34902'
										);
  /**
   * Muestra la ventana de los desvíos
   * 
   * @return type
   */
  public function showForwards()
  {
    $mainForward = $this->number->getMainForward();
    $forwards    = $this->number->getForwards();
    $idUser      = $this->number->getUser()->getID();        
   
    // La API nos pasa como primer desvío secundario el desvío principal, 
    // simplifica las cosas para el Asterisk, así que rodamos el array
    // una posición hacia adelante. Si hacemos la API pública ya decidiremos.
    if(is_array($forwards))
      $forwards = array_slice($forwards, 1);    
   
    $sipAccounts = SIPPeers::getAccounts($idUser);

    return View::make('content/forwards', array(
      'title'       => 'Desvíos',
      'mainForward' => $mainForward,
      'sipAccounts' => $sipAccounts,
      'forwards'    => $forwards
    ));
  }
  
  /**
   * Método que usamos para fijar el desvío principal en el proceso de alta
   * 
   * Cutrillo, porque no tenemos aun el número en la sesión
   * 
   * @todo Validar número de desvío
   */
  public function setMainForwardSignUp()
  {
    if(($forward = Input::get('main-forward', null)) !== null &&
        ($number = Input::get('number', null)) !== null) {
      $redirect = Input::get('redirect');
      $number = Number::where('numero_visible', '=', $number)
              ->where('id_cliente', '=', Session::get('user')->getID())
              ->first();			  
	  
      //OBTENER EL PERFIL ASOCIADO
	  $perfil_asociado = Profile::where('id', '=', $number->id_perfil)->first();
	  if($perfil_asociado)
	  {
		  //OBTENER EL TIEMPO DEl FICHERO DE CONFIGURACION
		  $timeout_default = Config::get('timeouts.'.$perfil_asociado->product_type);
		  if(!$timeout_default)
		  {
			  $timeout_default = Config::get('timeouts.default');
		  }
	  }
	  else
	  {
		 $timeout_default = Config::get('timeouts.default'); 
	  }
	  $timeout = (int)Input::get('main-forward-timeout', $timeout_default);
    
      $setMainForward = ($panelMode = Session::get('panel_mode', false)) === 'advanced'
        ? $this->flash->number($number->numero_visible)->setMainForward($forward, $timeout)
        : $this->flash->number($number->numero_visible)->setMainForward($forward);  

      if ($setMainForward === true) {
        $info = "El desvío principal para <strong>{$this->number}</strong> ha 
          sido correctamente actualizado a <strong>{$forward}</strong>.";

        return Redirect::to($redirect)->with('info', $info);      
      } else {
        $error = 'No se ha podido modificar el número de desvío. Consulta los 
          formatos en la ayuda desplegable para más información';

        return Redirect::to($redirect)->with('error', $error);      
      } 
    }
  }
  
  /**
   * Fija el desvío principal
   */
  	public function setMainForward()
  	{
    	$forward = Input::get('main-forward');
    	$timeout = (int)Input::get('main-forward-timeout', 20);
    	$redirect = Input::get('redirect', 'forwards');
    	if(is_numeric($forward)  && $this->validar_prefijos($forward) == false)
		{
			$error = 'No se ha podido modificar el número de desvío. El mismo no puede iniciar con los números <strong>'.implode(',',$this->prefijos_no_validos).'</strong>';
			return Redirect::to($redirect)->with('error', $error);
		}
		
		$setMainForward = ($panelMode = Session::get('panel_mode')) === 'advanced'
			? $this->number->setMainForward($forward, $timeout)
			: $this->number->setMainForward($forward,$timeout);  

		if ($setMainForward === true)
		{
			$this->refresh();
	  
			$info = "El desvío principal para <strong>{$this->number}</strong> ha sido correctamente actualizado a <strong>{$forward}</strong>.";		  
			return Redirect::to($redirect)->with('info', $info);      
		}
		else
		{
			$error = 'No se ha podido modificar el número de desvío. Consulta los formatos en la ayuda desplegable para más información';		  
			return Redirect::to($redirect)->with('error', $error);      
		}
  	}
  
  	private function validar_prefijos($numero_forward)
  	{	  	
		foreach($this->prefijos_no_validos as $prefijo_no)
		{
			if(strpos($numero_forward,$prefijo_no) === 0)
			{
				return false;
			}
		}
		return true;
	}
  
  /**
   * Fija los desvíos secundarios
   */
  public function setForwards()
  {
    $forwards = Input::get('forward');
    $timeouts = Input::get('forward-timeout', null);
    
    foreach($forwards as $i => &$forward) {
      $timeout = is_array($timeouts)
        ? (int)$timeouts[$i]
        : 20;

      $forward = array(
        'forward' => $forward,
        'timeout' => $timeout
      );
    }   
  
    if($this->number->setForwards($forwards)) {
      $this->refresh();
      
      $info = "Los desvíos secundarios para el número 
        <strong>{$this->number}</strong> han sido correctamente 
        actualizados.";
      
      return Redirect::to('forwards')->with('info', $info);
    } else {      
      $error = 'No se han podido modificar los desvíos secundarios. Consulta los 
        formatos en la ayuda desplegable para más información';
      
      return Redirect::to('forwards')->with('error', $error);      
    }
  }
  
  /**
   * Elimina un desvio secundario
   */
  public function remove($number)
  {
    if ($this->number->removeForward($number)) {
      $this->refresh();
    
      $info = "El desvío <strong>{$number}</strong> ha sido correctamente
        eliminado de la lista de desvíos secundarios.";
      
      return Redirect::to('forwards')->with('info', $info);    
    } else {
      $error = "Ha ocurrido un error al intentar eliminar el desvío.";
      
      return Redirect::to('forwards')->with('error', $error);          
    }
  }
  
  /**
   * Método que devuelve el contenido para la ventana modal
   * de las locuciones
   */
  public function returnModal()
  {
    $viewData = array();    
    $forwards = FALSE;
    
    $user   = $this->number->getUser();
    $idUser = $user->getID();
    
    if(!is_null($jsonAction = Input::get('jsonAction', NULL))) {
      $action = json_decode($jsonAction);
      
      if (!is_null($action) && isset($action->type)) {
        if($action->type === 'forward') {
          $forwards = $action->data->forwards;
          
          if(isset($action->data->operatorSpeech->name)) {
            $viewData['operatorSpeechSelected'] = $action->data->operatorSpeech->name;
          } else {
            $viewData['operatorSpeechSelected'] = false;
          }
        }
      }
    }
    
    $viewData['forwards'] = $forwards; 
    $viewData['speeches'] = $user->speeches()->get();    
    
    if(!isset($viewData['operatorSpeechSelected']))
      $viewData['operatorSpeechSelected'] = false;
    
    $viewData['sipAccounts'] = $sipAccounts = SIPPeers::getAccounts($idUser);
    
    return View::make('components.actions.modals.forward', $viewData);
  }  
}

?>
