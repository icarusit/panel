<?php

class FreeStatsController extends BaseController
{
	var $filtro_parametros = array();
	var $idUser;
	
	function __construct()
	{
		$this->idUser = Session::get('user')->getID();
	}
  /**
   * Método que añade filtros a las estadísticas
   * 
   * @return boolean Devolverá false si no se ha podido
   *                 añadir el filtro
   */
	public function addFilter($filter, $value)
  	{
    	if($filter === 'start' || $filter === 'end')
		{
      		if(\DateTime::createFromFormat('d/m/Y', $value) !== false)
			{
        		Session::put('filters.' . $filter,$value);
      		}
    	}
		else
		{
			Session::put('filters.' . $filter, $value);
		}
  	}
  
  /**
   * Método que hace el filtrado de las llamadas
   */
	public function filter()
  	{    
    	// Filtros
    	$filters = Input::only('called', 'start', 'end');

    	foreach($filters as $filter => $value)
		{
			Session::forget('filters.'.$filter);
			
      		$this->addFilter($filter, $value);
    	}    
    	return $this->showStats();
  	}
    
  /**
   * Recupera las estadísticas
   * 
   * @param  boolean $export
   * @return array
   */
	protected function getStats($export = false)
  	{		
    	$filters = Session::get('filters');
		
    	$queryParams = array();
		
		//Filtro para la fecha inicio
		if(isset($filters['start']))
		{
        	$dtStart = \DateTime::createFromFormat('d/m/Y', $filters['start']);
        	$queryParams[] = 'start >= "' . $dtStart->format('Y-m-d') . ' 00:00:00"';       
      	}
		else
		{
			//$queryParams[] = 'start >= "' . $res[0]->date . '"';
		}			
		//Filtro para la fecha fin
      	if(isset($filters['end']))
		{
        	$dateTime = \DateTime::createFromFormat('d/m/Y', $filters['end']);
        	$queryParams[] = 'end <= "' . $dateTime->format('Y-m-d') . ' 23:59:59"';
      	}
		
		//Filtro para saliente
		if(isset($filters['called']) && trim($filters['called']) != '')
		{
			$queryParams[] = 'dst = "'.$filters['called'].'"';
		}
		else
		{
			$sqlDestinos = 'SELECT dst FROM cdr  WHERE id_user = '.$this->idUser." group by dst"; 
    		$userNumbersDest = DB::select($sqlDestinos);
			$destinos = array();			
			foreach($userNumbersDest as $number)
			{
				$destinos[] = "'".$number->dst."'";
			}
			$queryParams[] = 'dst in (' . implode(',',$destinos) . ')';
		}
		
		$queryParams[] = 'id_user = '.$this->idUser;
		$queryParams[] = 'channel like "SIP/%"';
		
    	$this->filtro_parametros = $queryParams;
		
    	if($export)
		{
      		$calls = Cdr::whereRaw(implode(' and ', $queryParams))
              ->orderBy('start', 'desc')->get();        
    	}
		else
		{
      		$calls = Cdr::whereRaw(implode(' and ', $queryParams))
              ->orderBy('start', 'desc')->paginate(15);
    	}    
    	return $calls;
	}  
  
  	/**
   	* Muestra la ventana de estadísticas
   	*/
  	public function showStats()
  	{		
    	$viewData = array();
    	$viewData['title'] = 'Estadísticas';
    
    	$isMobile = Session::get('is_mobile');
    	$viewData['isMobile'] = $isMobile;
    
    	$filters = Session::get('filters', array());
    
    	$viewData['filters'] = $filters;

    	// Texto público de los filtros
    	$viewData['filter_public'] = array(
      		'called' => 'Llamadas a ',
      		'start'  => 'Fecha igual o posterior a',
      		'end'    => 'Fecha igual o anterior a'
    	);
    
    	// Números SIP del usuario
    	$userNumbers = FreeSipAccounts::where('id_user', '=', $this->idUser)                       
                        ->get();
		
		// Números Destinos del usuario
		$sqlDestinos = 'SELECT dst FROM cdr  WHERE id_user = '.$this->idUser." group by dst"; 
    	$userNumbersDest = DB::select($sqlDestinos);

		$viewData['userNumbersDest'] = $userNumbersDest; 		
    	$viewData['userNumbers'] = $userNumbers;    
    
    	$viewData['calls'] = $this->getStats();	
		$viewData['totales'] = $this->getTotales();
    
    	return View::make('content.stats.free_stats', $viewData);      
  	}
  
  	public function getTotales()
  	{
		$total_minutos = 0;
		$total_costo = 0;    
    	$summary = Cdr::select(DB::raw('ROUND(SUM(duration)/60, 2) as total_minutos'),DB::raw('ROUND(SUM(credit_before) - SUM(credit_after),4) as total_costo'))->whereRaw(implode(' and ', $this->filtro_parametros))->get();
		if(sizeof($summary) > 0)
		{
			$summary = $summary[0];
			$total_minutos = $summary->total_minutos;
			$total_costo = $summary->total_costo; 
		}
		return array(
						'total_minutos' => $total_minutos,
						'total_costo' => $total_costo
					);
  	}
  
  /**
   * Método para exportar las estadísticas
   */
  public function export()
  {
    $calls = $this->getStats(true);    
    
    $content = "Comienzo\tOrigen\tDestino\tDuracion\tFinal\tSaliente (EUR)\n";

    foreach($calls as $call) { 
      $start = (new DateTime($call->start))->format('d/m/Y H:i:s');
      $end   = (new DateTime($call->end))->format('d/m/Y H:i:s');
                  
      $cost_out = number_format(($call->credit_before - $call->credit_after), 4,',',' ');                    

      $content.= $start . "\t" . $call->CuentaSip() . "\t" . $call->dst . "\t";
      $content.= gmdate('H:i:s', $call->duration) . "\t" . $end;
      $content.= "\t" . $cost_out . "\n";
    }

    $response = Response::make($content, 200);

    $response->header('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
    $response->header('Content-length', strlen($content));
    $response->header('Content-Disposition', "attachment;filename=flash_telecom_stats.xls");

    return $response;          
  }
  
}
?>