<?php
/**
 * Controlador para la gestion de grupos
 */
class GroupController extends BaseController
{
	var $id_usuario;
	function GroupController()
	{
		parent::__construct();
		$this->id_usuario = Session::get('user')->getID();
	}
  	/**
    * Muestra la ventana principal
    */
  	public function show()
  	{
    	$listaGrupos = Group::where('id_client', '=', $this->id_usuario)->get();		
     
    	return View::make('content.group.list', array(
      	'number'  => $this->number->getNumber(),
      	'grupos' => $listaGrupos
    	));
  	}
  
  	/**
   	* Método que añade un nuevo número de origen
   	* a la lista blanca del número para llamadas
   	* salientes vía el proxy
   	*/
  	public function add()
  	{
    	$name = Input::get('name');

    	$rules = array(
			'name' => 'required'
    	);
    
    	$messages = array(
			'name.required' => 'El campo Nombre es obligatorio'
    	);
    
    	$validator = Validator::make(Input::all(), $rules, $messages);

    	if($validator->fails())
		{
      		return Redirect::to('groups')->withErrors($validator);
    	}
		else
		{
      		$nuevoGrupo = new Group;

      		$nuevoGrupo->id_client = $this->id_usuario;
      		$nuevoGrupo->name    = $name;
      
      		if($nuevoGrupo->save())
			{
        		$info = "El grupo <strong>".$nuevoGrupo->name."</strong> ha sido añadido correctamente.";
        		return Redirect::to('group/update/'.$nuevoGrupo->id)->with('info', $info);
      		}
			else
			{
        		$error = "Ha habido algún problema al añadir el grupo a la lista. Inténtelo de nuevo más tarde.";        
        		return Redirect::to('groups')->with('error', $error);
      		}
    	}
  	}
	
	public function formUpdate($idGroup)
	{
		$groupToUpdate = Group::find($idGroup);
		if($groupToUpdate)
		{
			if($groupToUpdate->contacts == NULL || trim($groupToUpdate->contacts) == '')
			{
				$listaMiembros = array();
			}
			else
			{
				$listaMiembros = explode('&',$groupToUpdate->contacts);
			}
			
			$sipAccounts = SIPPeers::where('id_cliente', '=', $this->id_usuario)
                ->orderByRaw("name asc")->get();
			
			return View::make(	'content.group.update', array(
      							'number'  => $this->number->getNumber(),
      							'grupo' => $groupToUpdate,
								'miembros' => $listaMiembros,
								'cuentas_sip' => $sipAccounts
    						));
		}
		else
		{
			
		}
	}
	
	public function update()
  	{
		$idGroup = Input::get('id');
		$groupToUpdate = Group::find($idGroup);
		if($groupToUpdate)
		{			
			$name = Input::get('name');
	
			$rules = array(
				'name' => 'required'
			);
		
			$messages = array(
				'name.required' => 'El campo Nombre es obligatorio'
			);
		
			$validator = Validator::make(Input::all(), $rules, $messages);
	
			if($validator->fails())
			{
				return Redirect::to('group/update/'.$idGroup)->withErrors($validator);
			}
			else
			{	
				$groupToUpdate->name    = $name;
		  
				if($groupToUpdate->save())
				{
					$info = "El grupo <strong>".$groupToUpdate->name."</strong> ha sido modificado correctamente.";
					return Redirect::to('group/update/'.$idGroup)->with('info', $info);
				}
				else
				{
					$error = "Ha habido algún problema al actualizar el grupo a la lista. Inténtelo de nuevo más tarde.";        
					return Redirect::to('group/update/'.$idGroup)->with('error', $error);
				}
			}
		}
		else
		{
			$error = "Error el grupo no existe.";        
        	return Redirect::to('groups')->with('error', $error);
		}
  	}
	
	function addsip()
	{
		$idGroup = Input::get('id');
		$grupo = Group::find($idGroup);
		if($grupo)
		{
			$sip = Input::get('sip');
	
			$rules = array(
				'sip' => 'required',
			);
		
			$messages = array(
				'sip.required' => 'El campo Número es obligatorio'
			);
		
			$validator = Validator::make(Input::all(), $rules, $messages);
	
			if($validator->fails())
			{
				return Redirect::to('group/update/'.$idGroup)->withErrors($validator);
			}
			else
			{	
				$contactos = trim($grupo->contacts);
				$sip = 'SIP/'.$sip;
				
				if($contactos == NULL || $contactos == '')
				{
					$grupo->contacts = $sip;
				}
				else
				{
					$contactos = explode('&',$grupo->contacts);
					$contactos[] = $sip;
					$grupo->contacts = implode('&',$contactos);
				}
		  
				if($grupo->save())
				{
					$info = "El contacto del grupo <strong>".$grupo->name."</strong> ha sido adicionado.";
					return Redirect::to('group/update/'.$idGroup)->with('info', $info);
				}
				else
				{
					$error = "Ha habido algún problema al agregar el contacto al grupo. Inténtelo de nuevo más tarde.";        
					return Redirect::to('group/update/'.$idGroup)->with('error', $error);
				}
			}			
		}
		else
		{
			$error = "Error el grupo no existe.";        
        	return Redirect::to('groups')->with('error', $error);
		}
	}
	
	function addtelefono()
	{
		$idGroup = Input::get('id');
		$grupo = Group::find($idGroup);
		if($grupo)
		{
			$numero = Input::get('numero');
	
			$rules = array(
				'numero' => 'required|numeric',
			);
		
			$messages = array(
				'numero.required' => 'El campo Número es obligatorio',          
     			'numero.numeric'  => 'El campo debe ser numérico.'
			);
		
			$validator = Validator::make(Input::all(), $rules, $messages);
	
			if($validator->fails())
			{
				return Redirect::to('group/update/'.$idGroup)->withErrors($validator);
			}
			else
			{	
				$contactos = trim($grupo->contacts);
				$numero = 'SIP/NSG/'.$numero;
				
				if($contactos == NULL || $contactos == '')
				{
					$grupo->contacts = $numero;
				}
				else
				{
					$contactos = explode('&',$grupo->contacts);
					$contactos[] = $numero;
					$grupo->contacts = implode('&',$contactos);
				}
		  
				if($grupo->save())
				{
					$info = "El contacto del grupo <strong>".$grupo->name."</strong> ha sido adicionado.";
					return Redirect::to('group/update/'.$idGroup)->with('info', $info);
				}
				else
				{
					$error = "Ha habido algún problema al agregar el contacto al grupo. Inténtelo de nuevo más tarde.";        
					return Redirect::to('group/update/'.$idGroup)->with('error', $error);
				}
			}			
		}
		else
		{
			$error = "Error el grupo no existe.";        
        	return Redirect::to('groups')->with('error', $error);
		}
	}
  
  	/**
   	* Método para borrar un grupo
   	*/
  	public function remove($idGroup)
  	{    
    	$groupToRemove = Group::find($idGroup);
    	if($groupToRemove->delete())
		{
      		$info = "El grupo  ha sido eliminado de la lista.";
      		return Redirect::to('groups')->with('info', $info);
    	}
		else
		{
      		return Redirect::to('groups');
    	}
  	}
	
	function removemiembro($idGroup,$miembro)
	{		
		$grupo = Group::find($idGroup);
		if($grupo)
		{	
			$rules = array(
				'miembro' => 'required',
			);
		
			$messages = array(
				'miembro.required' => 'El campo Número es obligatorio'
			);
		
			$validator = Validator::make(array('miembro' => $miembro), $rules, $messages);
	
			if($validator->fails())
			{
				return Redirect::to('group/update/'.$idGroup)->withErrors($validator);
			}
			else
			{
				$miembro = str_ireplace("-",'/',$miembro);	
				$contactos = trim($grupo->contacts);
				$contactos = explode('&',$contactos);
				$contactos_actualizado = array();
				
				foreach($contactos as $contacto)
				{
					if($miembro != $contacto)
					{
						$contactos_actualizado[] = $contacto;
					}
				}
				
				$grupo->contacts = implode('&',$contactos_actualizado);
		  
				if($grupo->save())
				{
					$info = "El miembro del grupo <strong>".$grupo->name."</strong> ha sido removido.";
					return Redirect::to('group/update/'.$idGroup)->with('info', $info);
				}
				else
				{
					$error = "Ha habido algún problema al remover el miembro del grupo. Inténtelo de nuevo más tarde.";        
					return Redirect::to('group/update/'.$idGroup)->with('error', $error);
				}
			}			
		}
		else
		{
			$error = "Error el grupo no existe.";        
        	return Redirect::to('groups')->with('error', $error);
		}
	}
}
