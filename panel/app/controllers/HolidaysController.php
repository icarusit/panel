<?php

class HolidaysController extends BaseController
{
  
  /**
   * Devuelve los días festivos nacionales
   * 
   * @todo Sacar de aquí y meter en tabla general y hacer un modelo
   */
  public function getNationalHolidaysDays()
  {
    return array(
      (object)array(
        'date'   => '01/01',
        'detail' => 'Año nuevo'
      ),
      (object)array(
        'date'   => '06/01',
        'detail' => 'Epifanía del Señor'
      ),
      (object)array(
        'date'   => '25/03',
        'detail' => 'Viernes Santo'
      ),            
      (object)array(
        'date'   => '01/05',
        'detail' => 'Día del trabajador'
      ),    
      (object)array(
        'date'   => '15/08',
        'detail' => 'Asunción de la Virgen'
      ),        
      (object)array(
        'date'   => '12/10',
        'detail' => 'Día de la Hispanidad'
      ),
      (object)array(
        'date'   => '01/11',
        'detail' => 'Día de la Hispanidad'
      ),
      (object)array(
        'date'   => '06/12',
        'detail' => 'Día de la Inmaculada Concepción'
      ),
      (object)array(
        'date'   => '08/12',
        'detail' => 'Día de la Inmaculada Concepción'
      ),
      (object)array(
        'date'   => '25/12',
        'detail' => 'Día de Navidad'
      )
    );
  }
  
  /**
   * Muestra la ventana de festivos
   */
  public function show()
  {   
    $number   = $this->number;    
    $holidays = $number->holidays();

    $nationalHolidaysAction = $holidays->getNationalHolidaysAction();

    // Esto es un poco cutre, pero tenemos prisa, ya refactorizaremos
    $result = DB::select('SELECT festnac_activo FROM numero WHERE id_numero = ' . $this->number->getID());

    $nationalHolidays = $result[0]->festnac_activo == 1 ? true : false;

    // Días festivos nacionales
    $nationalHolidaysCollection = $this->getNationalHolidaysDays();
    
    // Dias festivos personalizados    
    $holidays = $number->getHolidays();
    
    return View::make('content.holidays.holidays', array(
      'title' => 'Festivos',
        
      'nationalHolidays'           => $nationalHolidays,
      'nationalHolidaysAction'     => $nationalHolidaysAction,
      'nationalHolidaysCollection' => $nationalHolidaysCollection,

      'holidays' => $holidays
    ));
  }
  
  /**
   * Activar-desactivar los festivos nacionales
   */
  public function toggleNationalHolidays() 
  {
    $return = Input::get('return', 'holidays');
    $nationalHolidays = Input::get('national_holidays');
    $nationalHolidaysBool = $nationalHolidays === 'enable' ? 1 : 0;
    
    $sql = "UPDATE numero SET festnac_activo = $nationalHolidaysBool, festnac_accion = '' WHERE id_numero = " . $this->number->getID();

    DB::update($sql);
    
    $this->refresh();

    $info = 'Los festivos nacionales han sido correctamente <strong>' .
          ($nationalHolidaysBool === 1 ? 'activados' : 'desactivados') .
          '</strong>.'; 
    
    if($nationalHolidaysBool === 1) {
      $info.= '<br><br>Si es la primera vez que activas este servicio, debes
        configurar una acción a realizar, de lo contrario, la llamada seguirá
        su curso normal hasta los horarios y desvíos.';
    }
    
    return Redirect::to($return)->with('info', $info);     
  }

  /**
   * Responde al POST para guardar la configuración y activación-desactivación
   * de los festivos nacionales
   * 
   * @todo Validar accion lado servidor
   */
  public function saveNationalHolidays()
  {
    $return = Input::get('return', 'holidays'); 
    $active = Input::get('national-holidays-toggle', false);
    $action = Input::get('national-holidays-action', '');
    
    if($active && $action !== '') {
      $sql = "UPDATE numero SET festnac_activo = 1, festnac_accion = ? WHERE id_numero = " . $this->number->getID();
      DB::update($sql, array($action));     
      $this->refresh();
      $info = 'El servicio de festivos nacionales ha sido correctamente activado.';      
    } elseif($active && $action === '') {
      $error = 'Debes configurar una acción a realizar en los días festivos nacionales '
              . 'antes de activar el servicio';
      return Redirect::to($return)->with('error', $error);
    } elseif(!$active) {
      $sql = "UPDATE numero SET festnac_activo = 0, festnac_accion = '' WHERE "
              . "id_numero = " . $this->number->getID();
      DB::update($sql);
      $this->refresh();
      $info = 'Los festivos nacionales han sido correctamente <strong>desactivados</strong>.';         
    }
    
    return Redirect::to($return)->with('info', $info);
  }
  
  /**
   * Almacena los días de vacaciones/festivos
   * 
   * Validar acción
   */
  public function save()
  {
    $idHoliday = Input::get('holiday_id', null);
    $from      = Input::get('from');
    $until     = Input::get('until', null);
    $action    = Input::get('action_data');
   
    $idNumber = $this->number->getID();   
    
    if($idHoliday === null) {
      $holiday = new Holiday;
      $action = Input::get('action_data');    
      $edit = false;
    } else {
      $holiday = Holiday::find($idHoliday);      
      $action = Input::get('action_data_' . $idHoliday);
      $edit = true;      
    }

    $holiday->id_number = $idNumber;
    $holiday->active    = true;
    
    /* Validación fecha desde */
    $dtFrom = \DateTime::createFromFormat('d/m/Y', $from);
    if($dtFrom === false) {
      $error = 'No has introducido fecha "Desde" o ha ocurrido un error de formato.';
      return Redirect::to('holidays')->with('error', $error);      
    }
    
    $holiday->from = $dtFrom->format('Y-m-d'); 
    
    /* Validación acción introducida */
    if(trim($action) === '' || !((new Action(json_decode($action)))->isValid())) {
      $error = 'La acción para el festivo no puede estar en blanco o la acción '
              . 'configurada no es válida.';
      return Redirect::to('holidays')->with('error', $error);
    }
    
    $holiday->action    = $action;   
     
    /* Validación fecha hasta */
    if(trim($until) !== '') {
      $dtUntil = \DateTime::createFromFormat('d/m/Y', $until);
      if($dtUntil === false) {      
        $error = 'Has introducido fecha "Hasta" pero el formato no es correcto.';
        return Redirect::to('holidays')->with('error', $error);
      }
      
      $holiday->until = $dtUntil->format('Y-m-d');
    }
    
    if($holiday->isValid()) {
      if($holiday->save()) {
        $this->refresh();
        
        $info = 'El festivo ha sido correctamente ' . ($edit ? 'modificado' : 'añadido') . '.';
        return Redirect::to('holidays')->with('info', $info);
      } else {
        $error = 'Ha habido algún error ' . ($edit ? 'modificando' : 'añadiendo') . ' el festivo.';        
        return Redirect::to('holidays')->with('error', $error);
      }
    } else {
      $error = 'El festivo introducido no ha sido ' . ($edit ? 'modificado' : 'añadido') .
      ' ya que o bien es erróneo por ser la fecha "Hasta" anterior a "Desde", está repetido o
      se solapa con algún otro período de los que ya tienes añadidos.';

      return Redirect::to('holidays')->with('error', $error);
    }
  }
  
  /*
   * Elimina un día de vacaciones o festivo
   * 
   * @param integer $idHoliday ID del festivo a eliminar
   */
  public function remove($idHoliday)
  {
    $idNumber = $this->number->getID();

    $holiday = Holiday::
        where('id_number',  '=', $idNumber)
      ->where('id_holiday', '=', $idHoliday)
      ->take(1);   

    if($holiday->delete()) {
      $info = 'El festivo ha sido correctamente eliminado.';
      return Redirect::to('holidays')->with('info', $info);
    } else {
      $error = 'Ha habido algún problema eliminado el festivo.';
      return Redirect::to('holidays')->with('error', $error); 
    }
  }
}

?>
