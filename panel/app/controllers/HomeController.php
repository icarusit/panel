<?php

class HomeController extends BaseController
{
  /**
   * Aquí empieza todo
   */
  public function init()
  {
    if (FALSE === ($user = Session::get('user', FALSE))) {
      return $this->showLogin();      
    } else {
      return Redirect::to('numbers');
    }
  }
  
  /**
   * Muestra la ventana de login
   */    
  public function showLogin()
  {
    $redirect = Input::get('redirect', NULL);
	
	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
	
    return View::make('log_in', array('redirect' => $redirect,'distribuidor'=>$results));
  }  
  
  /**
   * Método que se ejecuta tras pulsar "Iniciar sesión" en la ventana de login
   * 
   * @return void
   */
  public function postLogin()
  {
    // Validamos primero los datos introducidos por el usuario
    $rules = array(
      'user' => 'required',
      'password' => 'required'
    );
    
    $messages = array(
      'required' => 'El campo :attribute es obligatorio.',
    );
    
    $validator = Validator::make(Input::all(), $rules, $messages);
    
    if ($validator->fails())
	{
      return Redirect::to('login')->withErrors($validator);
    }
	else
	{
      $in_user     = Input::get('user');
      $in_password = Input::get('password');
      $redirect    = Input::get('redirect', NULL);
    
      if (FALSE !== ($user = $this->flash->getUserByAccessData($in_user, md5($in_password))))
	  {   
	    $clientes = DB::table('cliente')->where('id_distribuidor', '=', Session::get('id_distribuidor'))->get(); 
        Session::put('user', $user);
		Session::put('clientes',$clientes);
		/*
		Insertar traza en la tabla acceso
		*/ 
		$request = Request::instance();
		$user_registrado = User::find($user->getID());
		
		$results = DB::table('cliente')->select('id_distribuidor','master')->where('id_cliente', '=', $user->getID())->get();
		
		if($results[0]->id_distribuidor == Session::get('id_distribuidor'))
		{
			Session::put('master',$results[0]->master);
			$nuevo_acceso = new Acceso;      
			$nuevo_acceso->id_cliente = $user_registrado->id_cliente;
			$nuevo_acceso->usuario    = $in_user;
			$nuevo_acceso->fecha_hora  = (new \DateTime())->format('Y-m-d H:i:s');
			$nuevo_acceso->ip    = $request->getClientIp();
			$nuevo_acceso->save();
		  
			return $redirect === NULL ? Redirect::to('numbers') : Redirect::to($redirect);
		}
		else
		{
			return Redirect::to('login')->with('error', 'El usuario no pertenece a este distribuidor');
		}
      }
	  else
	  {
        return Redirect::to('login')->with('error', 'El usuario no existe o la contraseña no es válida');
      }
    }
  }
  
  /**
   * Método que cierra la sesión
   */
  public function logout()
  {
    Session::flush();   
    Session::flash('info', 'Has cerrado la sesión correctamente.'); 
    return Redirect::to('login');
  }
  
  /**
   * Método para la recuperación del password
   */
  public function forgotPassword()
  {
    return View::make('remember_password');
  }
  
  /**
   * Método para mostrar ventana de generación de nuevo password
   */
  public function setNewPassword()
  {
    return View::make('restore_password');
  }
}
