<?php

class HooksController extends BaseController
{
  /**
   * Muestra ventana de los hooks
   * 
   * @return View
   */
  public function showHooks()
  { 
    $hookTypes = array(
      'call' => 'Momento en el que se reciba una llamada',
      'call_status' => 'Momento en el que cambia el estado de la llamada'
    ); 

    $hooks = Hook::where('id_number', '=', $this->number->getID())->get();

    return View::make('content.hooks', array(
      'title'     => 'Hooks',
      'hookTypes' => $hookTypes,
      'hooks'     => $hooks
    ));
  }
  
  /**
   * Añade un hook
   */
  public function add()
  {
    $event = Input::get('event');
    $url = Input::get('url');  
 
    $validationMessages = array(
      'required' => 'El campo :attribute es requerido.',
      'url'      => 'La URL suministrada no parece válida.'
    );

    $validator = Validator::make(
      array('url' => $url),
      array('url' => 'required|url'),
      $validationMessages 
    );

    if($validator->fails()) {
      return Redirect::to('hooks')->withErrors($validator);
    } else {
      // Buscamos si se ha añadido previamente un hook
      // para el mismo evento que se está intentando añadir
      $hook = Hook::where('id_number', '=', $this->number->getID())
                  ->where('event', '=', $event)
                  ->take(1)
                  ->get();

      if(count($hook) > 0) {
        $error = "Ya tienes el hook $event definido.";
  
        return Redirect::to('hooks')->with('error', $error);
      } else {
        $hook = new Hook;

        $hook->id_number = $this->number->getID();
        $hook->event = $event;
        $hook->action = $url;
        $hook->order = 0;

        if($hook->save()) {     
          $info = "El hook ha sido correctamente añadido.";

          return Redirect::to('hooks')->with('info', $info);
        } else {
          $error = "Ha habido un error al almacenar el hook.";

          return Redirect::to('hooks')->with('error', $error);
        }
      }
    }
  }

  /**
   * Método para eliminar hooks
   *
   * @param integer $idHook
   */
  public function remove($idHook)
  {
    $hook = Hook::find($idHook);

    if($hook->id_number == $this->number->getID()) {
      $hook->delete();
      $info = "El hook ha sido correctamente eliminado.";
    } else {
      $info = NULL;
    }

    return Redirect::to('hooks')->with('info', $info);
  }
}
