<?php
class InvoicesController extends BaseController
{
  /**
   * Muestra ventana de facturas
   */
  public function showInvoices()
  {
    $idUser = Session::get('user')->getID();

    $user = User::find($idUser);
    
    $invoices = Invoice::where('id_cliente', '=', $idUser)
            ->whereRaw('fecha > DATE_SUB(now(), INTERVAL 12 MONTH)')
            ->orderBy('fecha', 'desc')->paginate(15);
    
    return View::make('content.invoices', array(
      'title'    => 'Mis facturas',
      'invoices' => $invoices,
      'balance'  => $user->balance
    ));
  }
  
  /**
   * Ruta descarga de factura
   * 
   * @param integer $invoice Número de factura
   */
  public function download($invoice)
  {
    $idUser = Session::get('user')->getID();

    $sql = "SELECT mime, archivo FROM facturas WHERE num_factura = ? AND
      id_cliente = ? LIMIT 1";
     
    $result = DB::select($sql, array($invoice, $idUser));

    if(!empty($result)) {
      $content = $result[0]->archivo;
      $mimetype = $result[0]->mime;

      $response = Response::make($content, 200);
      
      $response->header('Content-Type', $mimetype);
      $response->header('Content-length', strlen($content));
      $response->header('Content-Disposition', "attachment;filename=invoice" . $invoice . ".pdf");

      return $response;
    } else { return Redirect::to('invoices'); }
  } 
  
  /*
  pdf de la factura
  */ 
	function pdf($numerofactura)
	{
		$objInvoice = Invoice::where("num_factura","=",$numerofactura)->first();
		if($objInvoice)
		{
			$this->modelo_factura($objInvoice->id);
		}
	}
	
	function descargar_modelo_online($key,$idioma = NULL)
	{
		$resultado = DB::select("SELECT * FROM  facturas WHERE MD5(num_factura*1004) =  '".$key."'",array());
		if(sizeof($resultado) > 0)
		{
			$this->modelo_factura($resultado[0]->id,$idioma);
		}
		else
		{
			
		}		
	}
	
	function modelo_factura($idInvoice,$idioma = NULL)
	{
		$objInvoice = Invoice::find($idInvoice);
		if($objInvoice)
		{
			$objCliente = User::find($objInvoice->id_cliente);
			if($idioma == NULL)
			{
				$pais = Countries::find($objCliente->id_country);
				App::setLocale($pais->idioma);
			}
			else
			{
				App::setLocale($idioma);
			}
			
			header ("Content-Disposition: attachment; filename=factura_".$objInvoice->num_factura.".pdf;" );
			header ("Content-Type: application/force-download");
			$pdf = new Ezpdf;
			$euro_diff = array(33=>'Euro');
			$pdf->selectFont(app_path() . '/classes/fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding','differences'=>$euro_diff));
			
			$pdf->setStrokeColor(1,0.3,0.1);
			$pdf->setLineStyle(4);
			$pdf->line(30,800,240,800);
			$pdf->setColor(1,0.3,0.1);
			$pdf->addText(300,780,24,Lang::get('factura.factura'),0);
			$pdf->setColor(0.33,0.33,0.33);
			
			$pdf->addJpegFromFile(app_path() . '/classes/img/logofmeuropa.JPG',50,755,200,33);
			
			$pdf->addText(70,750,10,"FLASH MEDIA EUROPA, S.L.",0);
			$pdf->addText(80,730,10,utf8_decode(Lang::get('factura.direccion_1')),0);
			$pdf->addText(80,710,10,utf8_decode(Lang::get('factura.direccion_2')),0);
			$pdf->addText(80,690,10,utf8_decode(Lang::get('factura.direccion_3')),0);
			
			
			$pdf->addText(300,760,12,utf8_decode($objCliente->nombre_fiscal),0);
			$n_recortado = $pdf->addTextWrap(300,740,150,12,utf8_decode($objCliente->direccion));
			$y_coordenada = 720;
			while($n_recortado)
			{
				$n_recortado = $pdf->addTextWrap(300,$y_coordenada,150,12,$n_recortado);
				$y_coordenada = $y_coordenada - 20;
			}
			$pdf->addText(300,$y_coordenada,12,utf8_decode($objCliente->cp.' '.$objCliente->poblacion.' '.$objCliente->provincia),0);
			$pdf->addText(300,$y_coordenada - 20,12,"N.I.F ".utf8_decode($objCliente->NIF),0);
			
			$pdf->addText(30,300,7,utf8_decode(Lang::get('factura.texto_izquierda')),-90);
			
			$pdf->setColor(1,0.3,0.1);
			$pdf->addText(123,648,12,utf8_decode(Lang::get('factura.factura_numero')),0);
			$pdf->setColor(0.33,0.33,0.33);
			$pdf->addText(180,648,12,$objInvoice->num_factura,0);
			
			$pdf->setColor(1,0.3,0.1);
			$pdf->addText(300,648,12,utf8_decode(Lang::get('factura.fecha')),0);
			$pdf->setColor(0.33,0.33,0.33);
			$pdf->addText(340,648,12,date('d/m/Y',strtotime($objInvoice->fecha)),0);
			
			$pdf->setColor(1,0.3,0.1);
			$pdf->addText(120,590,12,utf8_decode(Lang::get('factura.concepto')),0);
			$pdf->addText(500,590,12,utf8_decode(Lang::get('factura.importe')),0);
			$pdf->setStrokeColor(1,0.3,0.1);
			$pdf->setLineStyle(1);
			$pdf->line(120,580,550,580);
			$pdf->setColor(0.33,0.33,0.33);
			
			$conceptos = FacturaConcepto::where('factura','=',$objInvoice->num_factura)->get();
			
			$y_coordenada = 560;
			foreach($conceptos as $cp)
			{
				$importe = $cp->importe;
				$pdf->addText(120,$y_coordenada,10,utf8_decode($cp->concepto),0);
				$pdf->addTextWrap(448,$y_coordenada,100,10,round($importe,2).' !','right');
				//$pdf->addText(518,$y_coordenada,10,round($importe,2).' !',0);	
				$y_coordenada = $y_coordenada - 15;
				if($y_coordenada < 150)
				{
					$pdf->ezNewPage();
					$y_coordenada = 680;
					$pdf->setColor(1,0.3,0.1);
					$pdf->addText(120,700,12,utf8_decode(Lang::get('factura.concepto')),0);
					$pdf->addText(500,700,12,utf8_decode(Lang::get('factura.importe')),0);
					$pdf->setStrokeColor(1,0.3,0.1);
					$pdf->setLineStyle(1);
					$pdf->line(120,690,550,690);
					$pdf->setColor(0.33,0.33,0.33);
				}
			}		
			
			
			if($y_coordenada < 350)
			{
				$pdf->ezNewPage();
			}
			$pdf->setColor(1,0.3,0.1);
			$pdf->setStrokeColor(1,0.3,0.1);
			$pdf->line(120,335,550,335);
			
			$pdf->filledRectangle(290,320,260,15);
			$pdf->setColor(1,1,1);
			$pdf->addText(350,322,10,utf8_decode(Lang::get('factura.base_imponible')),0);
			$pdf->addText(430,322,10,utf8_decode(Lang::get('factura.iva')),0);
			$pdf->addText(500,322,10,utf8_decode(Lang::get('factura.total')),0);
			$pdf->setColor(0.33,0.33,0.33);
			
			$importe_total = str_replace(',','.',$objInvoice->total);
			$importe_base_imponible = str_replace(',','.',$objInvoice->base_imponible);
			$pdf->addText(350,300,12,$objInvoice->base_imponible.' !',0);
			$pdf->addText(430,300,12,((float)$importe_total - (float)$importe_base_imponible).' !',0);
			$pdf->addText(500,300,12,$objInvoice->total.' !',0);
			
			$pdf->setColor(1,0.3,0.1);
			$pdf->addText(120,250,12,utf8_decode(Lang::get('factura.cuenta_cargo')),0);
			$pdf->setColor(0.33,0.33,0.33);
			$pdf->addText(410,250,12,$objCliente->ccc_pagos,0);
			$pdf->setColor(1,0.3,0.1);
			$pdf->line(120,245,550,245);
			
			$pdf->addText(120,220,12,utf8_decode(Lang::get('factura.transferencias')),0);
			$pdf->setColor(0.33,0.33,0.33);
			$pdf->line(120,215,550,215);
			$pdf->addText(120,200,12,utf8_decode(Lang::get('factura.ing')),0);
			//$pdf->addText(430,200,12,utf8_decode("SANTANDER"),0);
			
			$pdf->addText(200,200,12,utf8_decode(Lang::get('factura.ing_numero')),0);
			//$pdf->addText(360,180,12,utf8_decode("ES48 0049 1848 7222 1034 23 99"),0);
			
			
			$pdf->addText(120,90,7,utf8_decode(Lang::get('factura.texto_pie_1')),0);
			$pdf->addText(120,75,7,utf8_decode(Lang::get('factura.texto_pie_2')),0);
			$pdf->addText(120,60,7,utf8_decode(Lang::get('factura.texto_pie_3')),0);
			$pdf->addText(120,45,7,utf8_decode(Lang::get('factura.texto_pie_4')),0);
			$pdf->addText(120,30,7,utf8_decode(Lang::get('factura.texto_pie_5')),0);
			
			$pdf->addJpegFromFile(app_path() . '/classes/img/eca.jpg',510,35,40,60);
			
			//$pdf->ezStream();
			print $pdf->ezOutput(1);
		}
	}
	/**
	* Método para pago de factura
	* 
	* @param integer $idInvoice
	*/
 	public function pay($idInvoice)
  	{
    	$invoice = Invoice::find($idInvoice);
    
    	if($invoice !== NULL)
		{
      		$amount = $invoice->total;
      		$order = 'IN' . rand(100, 999) . $invoice->id;
    
      		// Almacenamos el intento de pago de factura
			$forma_pago = Input::get('forma_pago');
				
			if(!$forma_pago)
			{
				$forma_pago = "redsys";
			}
			
      		if(Payment::register($amount, $order, 'factura', $invoice->id,$forma_pago))
	  		{				
				
				switch($forma_pago)
		  		{
			  		case "redsys":
			  		{
				  		$urlOK = URL::to('invoices/success');
        				$urlKO = URL::to('invoices/error');
        
        				return Payment::redirectToGateway($amount, $order, $urlOK, $urlKO);
				  		break;
			  		}
			  		case "paypal":
			  		{
						$urlOK = URL::to('invoices-paypal/resultado');
        				$urlKO = URL::to('invoices-paypal/retorno');
        
        				return PaymentPaypal::redirectToGateway($amount, $order, $urlOK, $urlKO,'Factura');
				  		break;
			  		}
		  		}
        		//return Payment::redirectToGateway($amount, $order, URL::to('invoices/success'), URL::to('invoices/error'));
      		}
    	}
		else
		{
      		return Redirect::to('invoices');
    	}
  	}
  
  	/**
   	* Se ejecuta cuando el pago de una factura es correcto
   	*/
  	public function showSuccess()
  	{
    	if(($version        = Input::get('Ds_SignatureVersion', null)) !== null &&
		   ($params       = Input::get('Ds_MerchantParameters', null)) !== null &&
		   ($signatureRecibida     = Input::get('Ds_Signature', null)) !== null)
		{
			$obj_api = Payment::RedsysAPI();
			$decode =  $obj_api->decodeMerchantParameters($params);
			$obj_api->stringToArray($decode);
			$codigoRespuesta = $obj_api->getParameter('Ds_Response');
			$randomKey       = Payment::getRandomKey();
			$signatureCalculada = $obj_api->createMerchantSignatureNotif($randomKey,$params);
			$order = $obj_api->getParameter('Ds_Order');
			
      		$row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      		$idInvoice = substr($order, 5, strlen($order));                    
      		$invoice = Invoice::find($idInvoice);
      
      		if($invoice !== null)
			{
        		$info = "Gracias. La factura <strong>{$invoice->num_factura}</strong>
        				por un importe de <strong>{$invoice->total} €</strong> ha sido 
        				correctamente pagada.";
        
        		return Redirect::to('invoices')->with('info', $info);    
      		}
			else
			{
        		return Redirect::to('invoices');
      		}
    	}
		else
		{
      		return Redirect::to('invoices');
    	}    
  	}
  
  /**
   * Se ejecuta cuando el pago de una factura es incorrecto
   */
  public function showError()
  {
    if(
       ($order    = Input::get('Ds_Order', null)) !== null &&
       ($response = Input::get('Ds_Response', null)) !== null
      ) {
      $row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      $idInvoice = substr($order, 5, strlen($order));                    
      $invoice = Invoice::find($idInvoice);
      
      if($invoice !== null) {
        $error = "Lo sentimos. La factura <strong>{$invoice->num_factura}</strong>
        no ha sido correctamente pagada.";
        
        return Redirect::to('invoices')->with('error', $error);    
      } else {
        return Redirect::to('invoices');
      }
    } else {
      return Redirect::to('invoices');
    }    
  }
  
  /**
   * Método que se ejecuta tras el callback del TPV
   * 
   * El usuario no llega nunca a esta ruta, sólo RedSys
   * tras el POST de confirmación del pago.
   * 
   * @param string $order  Referencia transacción
   * @param float  $amount Total   
   */
  public function callback($order, $amount)
  {
    $row = Payment::where('referencia', '=', $order)->firstOrFail();
      
    if($row->markAsPaid()) {
      //$order = 'IN' . rand(100,999) . $idInvoice;
      $idInvoice = substr($order, 5, strlen($order));
                    
      $invoice = Invoice::find($idInvoice);
      
      if($invoice !== null) {
        $invoice->estado = 'Pagada';
          
        if($invoice->save()) {
          // Enviamos los correos al usuario y a Flash
          $user = User::find($invoice->id_cliente);

          $subject = "Notificación de pago de factura " . $invoice->num_factura;
              
          $header = "Has abonado la factura <strong>{$invoice->num_factura}</strong>";             
 
          $body = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i')
          . "</strong> has efectuado correctamente el pago de la factura " .
          "<strong>{$invoice->num_factura}</strong> con importe de <strong>" .
          "{$invoice->total}€</strong> desde la zona de clientes de Flash Telecom.</p>" .
          "<p>La referencia de la transacción para futuras consultas es <strong>" .
          "{$order}</strong>.</p><p>Gracias por tu confianza.</p><p>Equipo de Flash Telecom.</p>";

          $content = array('header' => $header, 'body' => $body);

          $this->flash->sendMail($user->mail_1, $subject, $content);
        }
      }
    }
  }
  
  /**
   * Recupera el número de facturas pendiente de pago (Tarjeta, Transferencia)
   * del cliente
   */
  public function getNoOfOutstandingInvoices()
  {
    if(Request::ajax()) {
      $idUser = Session::get('user')->getID();

      return count(Invoice::where('id_cliente', '=', $idUser)
              ->whereRaw('fecha > DATE_SUB(now(), INTERVAL 3 MONTH) AND estado IN ("Tarjeta", "Transferencia")')
              ->get());
    } else {
      return Redirect::to('invoices');
    }
  }
  	public function GetresultadoPaypal()
	{
		Log::error('RECEPCION GET:'.serialize($_GET));
		Log::error('RECEPCION POST:'.serialize($_POST));
	}
  	public function resultadoPaypal()
  	{
		Log::error('RECEPCION:'.serialize($_POST));

		if($_POST)
		{
			Log::error('PASO 2');
			// Obtenemos los datos en formato variable1=valor1&variable2=valor2&...
       	 	$raw_post_data = file_get_contents('php://input');
Log::error('PASO 3');
        	// Los separamos en un array
        	$raw_post_array = explode('&',$raw_post_data);
Log::error('PASO 4');
        	// Separamos cada uno en un array de variable y valor
        	$myPost = array();
        	foreach($raw_post_array as $keyval)
			{
           	 	$keyval = explode("=",$keyval);
            	if(count($keyval) == 2)
                	$myPost[$keyval[0]] = urldecode($keyval[1]);
        	}
Log::error('PASO 5');
        	// Nuestro string debe comenzar con cmd=_notify-validate
        	$req = 'cmd=_notify-validate';
        	if(function_exists('get_magic_quotes_gpc'))
			{
            	$get_magic_quotes_exists = true;
        	}
			foreach($myPost as $key => $value)
			{
            	// Cada valor se trata con urlencode para poder pasarlo por GET
            	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)
				{
                	$value = urlencode(stripslashes($value)); 
            	}
				else
				{
                	$value = urlencode($value);
            	}

            	//Añadimos cada variable y cada valor
            	$req .= "&$key=$value";
        	}
			Log::error('PASO 6'.$req);
			$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');

        	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        	curl_setopt($ch, CURLOPT_POST, 1);
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        	curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		Log::error('PASO 7');
			if( !($res = curl_exec($ch)) )
			{
            	// Ooops, error. Deberiamos guardarlo en algún log o base de datos para examinarlo después.
            	curl_close($ch);
            	return Redirect::to('balance');
        	}
			curl_close($ch);
			Log::error('PASO 8'.$res);
			if (strcmp ($res, "VERIFIED") == 0)
			{
				if(strtolower($_POST['payment_status']) == 'pending' || strtolower($_POST['payment_status']) == 'completed')
				{
					$order = Input::get('custom');
					$order = $_POST['custom'];
					
					$row = Payment::where('referencia', '=', $order)->firstOrFail();     
		  Log::error('PASO 9');
					if($row->markAsPaid())
					{Log::error('PASO 10 Pagada'.$_POST['custom']);
						//$order = 'IN' . rand(100,999) . $idInvoice;
      					$idInvoice = substr($order, 5, strlen($order));
						$invoice = Invoice::find($idInvoice);
						  
						if($invoice !== null)
						{Log::error('PASO 11');
							$invoice->estado = 'Pagada';
							if($invoice->save())
							{Log::error('PASO 12');
				 					// Enviamos los correos al usuario y a Flash
          							$user = User::find($invoice->id_cliente);
									$subject = "Notificación de pago de factura " . $invoice->num_factura;
              
								  	$header = "Has abonado la factura <strong>{$invoice->num_factura}</strong>";             
						 
								  	$body = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i')
								  . "</strong> has efectuado correctamente el pago de la factura " .
								  "<strong>{$invoice->num_factura}</strong> con importe de <strong>" .
								  "{$invoice->total}€</strong> desde la zona de clientes de Flash Telecom.</p>" .
								  "<p>La referencia de la transacción para futuras consultas es <strong>" .
								  "{$order}</strong>.</p><p>Gracias por tu confianza.</p><p>Equipo de Flash Telecom.</p>";
						
								  	$content = array('header' => $header, 'body' => $body);
						
								  	$this->flash->sendMail($user->mail_1, $subject, $content);
							}
						}
					}					
				}
			}
			elseif(strcmp ($res, "INVALID") == 0)
			{
				Log::error('PASO 13');
        		//return Redirect::to('invoices');
			}			
		}
		else
		{Log::error('PASO 14');
			//return Redirect::to('balance');
		}
  	}
	public function retornoPaypal()
	{
		Log::error('RECEPCION retorno final:'.serialize($_POST));		
		if($order = Input::get('custom', null) !== null)
		{
			$order = $_POST['custom'];

      		$row = Payment::where('referencia', '=', $order)->firstOrFail();
      
      		$idInvoice = substr($order, 5, strlen($order)); 
			                   
      		$invoice = Invoice::find($idInvoice);
      
      		if($row !== null && $row->resultado == 'OK')
			{
        		$info = "Gracias. La factura <strong>{$invoice->num_factura}</strong>
        				por un importe de <strong>{$invoice->total} €</strong> ha sido 
        				correctamente pagada.";
    			
        		return Redirect::to('invoices')->with('info', $info);
      		}
			else
			{
        		$error = "Lo sentimos. La factura <strong>{$invoice->num_factura}</strong>
        		no ha sido correctamente pagada.";				 
				   
        		return Redirect::to('invoices')->with('error', $error);                                                                                                  
      		}                                                                                                                                    
    	}
		else
		{                                                                                                                               
      		return Redirect::to('invoices');                                                                                                     
    	}
	}
}

?>
