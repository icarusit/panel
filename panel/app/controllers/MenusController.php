<?php

class MenusController extends BaseController
{
  /**
   * Muestra la ventana de configuración de menús (de momento es sólo uno,
   * pero la idea es que se pueda hacer más de uno más adelante y meterlo
   * como acción).
   * 
   * @return View
   */  
  public function showMenus()
  {
    $dataView = array();
    
    // Locuciones del usuario disponibles
    $user = $this->number->getUser();    
    $dataView['speeches'] = $user->speeches()->get();           
    
    // Opciones del menú
    $number = Number::find($this->number->getID());
    
    $dataView['enabled'] = $number->menu_active;
    
    if(($menu = json_decode($number->menu)) !== NULL) { 
      $dataView['selectedSpeech'] = isset($menu->data->speech) 
              ? $menu->data->speech
              : FALSE;      
      
      $options = isset($menu->data->options)
              ? $menu->data->options
              : FALSE;
      
      if($options !== FALSE) {
        foreach($options as &$option) {
          $action = new Action($option);

          $option->summary = $action->parse();
        }
      }
      
      $dataView['options'] = $options;
    } else {
      $dataView['selectedSpeech']  = FALSE;
      $dataView['options'] = FALSE;      
    }
    
    // Título de la vista
    $dataView['title']   = 'Menús';
    
    return View::make('content.menus.menus', $dataView);
  }
  
  /**
   * Activa-desactiva el servicio de menú
   */
  public function setConfig()
  {
    $menu = Input::get('menu-toggle', NULL);
    
    if($menu !== NULL) {
      $number = Number::find($this->number->getID());
      
      if((($cfgMenu = json_decode($number->menu)) !== NULL) &&
          isset($cfgMenu->data->speech) && isset($cfgMenu->data->options)) { 
        $number->menu_active = $menu ? true : false;

        $number->save();

        $msg = "El servicio de menú ha sido correctamente <strong>%s</strong>";
        $msg = sprintf($msg, $menu ? 'activado' : 'desactivado');

        return Redirect::to('menus')->with('info', $msg);          
      } else {
        $error = "Debes seleccionar una locución para el menú y añadir al menos
          una opción al menú para activar el servicio.";

        return Redirect::to('menus')->with('error', $error);                    
      }
    } else {
        return Redirect::to('menus');
    }
  }

  /**
   * Cambia las opciones del menú.
   * 
   * Relaciona una acción con una opción numérica
   */
  public function setOptions()
  {
    $dtmf = (int)Input::get('dtmf');
    $action = Input::get('action_data', null);
    
    if($action === null) {
      $action = Input::get('action_data_' . $dtmf);
    }
    
    $number = Number::find($this->number->getID());
    
    if(($menu = json_decode($number->menu)) === NULL) {
      $menu = $this->bootstrapMenu();
    }

    if(!isset($menu->data->options)) {
      $menu->data->options = new stdClass();
    }
    
    if(trim($action) === '' || !((new Action(json_decode($action)))->isValid())) {
      $error = 'La acción para el dígito <strong>' . $dtmf . '</strong> no puede'
              . ' estar en blanco o la acción configurada no es válida.';
      return Redirect::to('menus')->with('error', $error);
    }
    
    $menu->data->options->{$dtmf} = json_decode($action);
    
    $number->menu = json_encode($menu);    
    
    if($number->save()) {
      $info = "La acción para la opción <strong>$dtmf</strong> ha sido correctamente configurada";
      return Redirect::to('menus')->with('info', $info);    
    } else {
      $error = "Ha habido un error guardando la configuración.";
      return Redirect::to('menus')->with('error', $error);          
    }
  }
  
  /**
   * Elimina una opción del menú
   * 
   * @param integer $dtmf
   */
  public function removeOption($dtmf)
  {
    $number = Number::find($this->number->getID());
 
    try {
      $menu = json_decode($number->menu);      
      unset($menu->data->options->{$dtmf});    
      $number->menu = json_encode($menu);
    
      if($number->save()) {
        $info = "La acción para la opción <strong>$dtmf</strong> ha sido correctamente eliminada";
        return Redirect::to('menus')->with('info', $info);            
      } else {
        $error = "Ha habido un error guardando la configuración.";
        return Redirect::to('menus')->with('error', $error);                  
      }
    } catch(\Exception $e) {
      $error = "Ha habido un error guardando la configuración.";
      return Redirect::to('menus')->with('error', $error);                
    }    
  }
  
  /**
   * Fija la locución elegida para el menú
   */
  public function setSpeech()
  {
    $selectedSpeech = Input::get('select-menu-speech', NULL);
    
    if($selectedSpeech !== NULL) {
      $speech = Speech::find($selectedSpeech);      
      $number = Number::find($this->number->getID());    
      
      $menu = $number->menu;    
      
      if(($menu = json_decode($number->menu)) === NULL) {
        $menu = $this->bootstrapMenu();
      }
      
      $menu->data->speech = new stdClass();
      $menu->data->speech->name = $speech->public_name;
      $menu->data->speech->filename = $speech->filename;
        
      $number->menu = json_encode($menu);
      
      if($number->save()) {
        $info = "Has seleccionado la locución <strong>{$speech->public_name}</strong> para el menú.";
        return Redirect::to('menus')->with('info', $info);
      } else {
        $info = "Ha habido un error seleccionando la locución.";
        return Redirect::to('menus')->with('error', $info);
      }      
    }
  }
  
  /**
   * Crea la estructura básica del menú en la tabla
   * 
   * No debería ir aquí sino en un modelo
   */
  private function bootstrapMenu()
  {
    $menu = new stdClass();
    
    $menu->type = 'menu';
    $menu->data = new stdClass();

    return $menu;
  }
}

?>
