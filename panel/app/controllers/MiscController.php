<?php
/**
 * Controlador opciones adicionales del número
 */
class MiscController extends BaseController
{
  /**
   * Muestra ventana de opciones adicionales del número
   */
  public function showMisc()
  {
    $number = $this->number;
    $rowNumber = Number::find($number->getID());
    $hasSpeechCredit = $rowNumber->speech_credit ? true : false;
    $settings = NumberSettings::find($this->number->getID());    
    
    return View::make('content.misc.misc', array(
      'title'           => 'Más opciones',
      'number'          => $number,
      'displayCallerID' => $number->isDisplayCallerIDActive(),
      'hasSpeechCredit' => $hasSpeechCredit,
      'settings'        => $settings
    ));
  }
  
  /**
   * Fija el estado del servicio mostrar caller id
   */
  public function setDisplayCallerID()
  {
    $displayCallerID = Input::get('displayCallerID');
    
    $this->number->setDisplayCallerID($displayCallerID === 'enable' ? TRUE : FALSE);
    $this->refresh();
    
    $info = 'El servicio de mostrar tu número en las llamadas recibidas ha sido <strong>';
    $info.= $displayCallerID === 'enable' ? 'activado' : 'desactivado';
    $info.= '</strong>.';
    
    return Redirect::to('misc')->with('info', $info);
  }
  
  /** 
   * Método que se ejecuta tras el post de activar-desactivar
   * servicio de locución de poco saldo o agotado.
   */
  public function setSpeechCredit()
  {
    $speechCreditToggle = (bool)Input::get('speech-credit-toggle', false);
    
    $number = $this->number;
    $rowNumber = Number::find($number->getID());
    $rowNumber->speech_credit = $speechCreditToggle;
    
    if($rowNumber->save()) {
      $info = 'Has <strong>' . ($speechCreditToggle ? 'activado' : 'desactivado') .
              '</strong> el servicio de locución de saldo en llamadas salientes.';
      return Redirect::to('misc')->with('info', $info);
    }    
  }  
  
  /**
   * Método que actualiza los parámetros de bloqueo de llamadas entrantes
   * 
   * @return
   */
  public function settings()
  {
    $idNumber = $this->number->getID();
    
    $settings = NumberSettings::find($idNumber);
   
    if($settings === NULL) {
      $settings = new NumberSettings;
      $settings->id_number = $idNumber;
    }
    
    $inputs = Input::except(array('_token'));
    
    foreach($inputs as &$input) {
      $input = $input == "false" ? FALSE : TRUE;
    }
    
    $settings->fill($inputs);
    
    if($settings->save()) {
      $info = 'El bloqueo de llamadas ha sido actualizado correctamente.';
    
      return Redirect::to('misc')->with('info', $info);
    } else {
      $error = 'Ha ocurrido un error actualizando el bloqueo de llamadas.';
    
      return Redirect::to('misc')->with('error', $error);      
    }
  }  
}
