<?php
/**
 * Controlador para la administración de llamadas perdidas
 */
class MissedCallsController extends BaseController
{
  /**
   * Muestra configuración de las llamadas perdidas
   * 
   * @return type
   */
	public function show()
  	{
    	$viewData = array();
    
    	$missedCalls = $this->number->getOptions()->missed_calls;

    	$viewData['title']   = 'Llamadas perdidas';
    	$viewData['enabled'] = $missedCalls->enabled;
    	$viewData['email']   = $missedCalls->email;
		
		$Num = Number::where('numero_visible','=',$this->number->getNumber())->first();
		$viewData['email2']   = $Num->mailperdidas2;
		
		/*
		Configuración de Notificación whatsapp
		*/
		//-------------------------------------------
		$accion = 'perdidas';
		$idUser = Session::get('user')->getID();
		$infoNumberWP = NumeroWhatsApp::where('idUser','=',$idUser)->where('numero_visible','=',$this->number->getNumber())->where('accion','=',$accion)->first();
		if($infoNumberWP)
		{
			$viewData['wp']   = $infoNumberWP->numerowp;
			$viewData['wp_activo']   = $infoNumberWP->activo;
			$viewData['wp_estado_api']   = $infoNumberWP->activo_api;
		}
		else
		{
			$viewData['wp']   = '';
			$viewData['wp_activo']   = 0;
		}
		$viewData['return']   = 'missed-calls';
		$viewData['accion']   = $accion;
		//-------------------------------------------
	
		$numero_filtro = Input::get('numero_filtro');
		$viewData['numero_filtro']   = trim($numero_filtro);
		
		$listmissedcalls = MissCall::where('called', '=', $this->number->getNumber());
		if($numero_filtro != '')
		{
			$listmissedcalls = $listmissedcalls->whereRaw('caller like ("%'.$numero_filtro.'%")');
		}
    	$listmissedcalls = $listmissedcalls->orderBy('date_time', 'desc')->paginate(10);
		$viewData['listmissedcalls']   = $listmissedcalls;

    	return View::make('content.missedcalls.missedcalls', $viewData);
  	}
  
  /**
   * Activa o desactiva el servicio de llamadas perdidas
   * 
   * @todo Actualizar con método de API
   */
  public function setMissedCalls()
  {
    $enabled = Input::get('missed-calls-toggle');  
    $return = Input::get('return', 'missed-calls');
    
    $enabled = $enabled ? 1 : 0;
    
    if($enabled) {
      $missedCalls = $this->number->getOptions()->missed_calls;
      
      if($missedCalls->email === null) {
        $error = 'Debes configurar un correo electrónico donde recibir las '
                . 'notificaciones de llamadas perdidas antes de activar el '
                . 'servicio.';
        
        return Redirect::to('missed-calls')->with('error', $error);
      }
    }
    
    $sql = "UPDATE numero SET perdidas = $enabled WHERE numero_visible = ? LIMIT 1";

    if(!($res = DB::update($sql, array($this->number->getNumber())))) {
      $error = 'Ha habido un problema cambiando la configuración del servicio.';
      return Redirect::to($return)->with('error', $error);
    } else {
      $info = 'Has <strong>' . ($enabled ? 'activado' : 'desactivado') . '</strong> 
        correctamente el servicio de llamadas perdidas';
      $this->refresh();        
      return Redirect::to($return)->with('info', $info);        
    } 
  }    
  
  /**
   * Fija la dirección de correo electrónico a la que notificar con los avisos
   * de llamadas perdidas
   */
 	public function setEmail()
  	{
    	$email = Input::get('missedcalls-email', NULL);
		$email2 = Input::get('missedcalls-email2', NULL);
    
    	if(!is_null($email))
		{
      		$sql = 'UPDATE numero SET mailperdidas = ?,mailperdidas2 = ? WHERE numero_visible = ? LIMIT 1';
      
      		if(!DB::update($sql, array($email,$email2,$this->number->getNumber())))
	  		{
        		$error = 'Ha habido un problema cambiando la dirección de correo 
          				donde recibir las notificaciones de llamada perdida, ¿has usado
          				la misma dirección?';
        		return Redirect::to('missed-calls')->with('error', $error);
      		}
	  		else
	  		{
				$infoNumber = Number::where('numero_visible','=',$this->number->getNumber())->first();
				$infoNumber->mailperdidas = $email;
				$infoNumber->mailperdidas2 = $email2;
				$infoNumber->update();
				
        		$info = "Recibirás las notificaciones de llamadas perdidas en la dirección
          				<strong>$email ".($email2?' y '.$email2:'')."</strong>";
        		$this->refresh();
        		return Redirect::to('missed-calls')->with('info', $info);
      		}
    	}
  	}
}