<?php
/**
 * Controlador para las notificaciones
 */
class NotificacionController extends BaseController
{
  /**
   * 
   * @return type
   */
  public function show()
  {
    
  }
  
  public function sendMessage($notificacion_id,$titulo,$texto)
  	{
		$content = array(
		  "en" => $texto
		  );
		
		$fields = array(
		  'app_id' => "b6db0b28-8cfe-48df-8e1a-fac6b44b8a9b",
		  'isChromeWeb' => true,
		  'isAnyWeb' => true,
		  'headings' => array('en' => $titulo),
		  /*'included_segments' => array('All'),*/
		  'include_player_ids' => array(0=>$notificacion_id),
		  //'send_after' => 'Fri May 02 2014 00:00:00 GMT-0700 (PDT)',
		  'data' => array("foo" => "bar"),
		  'contents' => $content
		);
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
							   'Authorization: Basic NWExNDRiZWEtZDJiMS00ZmIxLWFkNTctNjE4OTJjZTY2MWVi'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
  	}
  	public function enviarNotificacion()
  	{
		$numero = Input::get('numero');
		//Buscar el usuariowhere
		$number = Number::where('numero_visible','=',$numero)->first();
		if($number)
		{
			$user = User::find($number->id_cliente);
			if($user)
			{
				$notificaciones = ClienteNotificacion::where('cliente_id','=',$number->id_cliente)->get();
				if(sizeof($notificaciones) > 0)
				{
					
					foreach($notificaciones as $notificacion)
					{
						$titulo = Input::get('titulo');
						$texto = Input::get('texto');
						$response = $this->sendMessage($notificacion->notificacion_id,$titulo,$texto);
						print json_encode($response);
					}
				}
				else
				{
					print 'No existen notificaciones';
				}
			}
			else
			{
				print 'usuario no asociado';
			}
		}
		else
		{
			print 'Numero no encontrado';
		}
	}
	public function setSubscriptorNotificacion()
	{
		$idUser = Session::get('user')->getID();
		$id = Input::get('id');
		$registration = Input::get('registration');		
		
		if($id != '')
		{
			/*
			Obtener las notificaciones que corresponden al usuario flash
			*/
			$notificaciones_reservadas = array();
			$notificaciones_usuario_flash = ClienteNotificacion::where('cliente_id','=',7)->get();
			//print_r($notificaciones_usuario_flash);
			foreach($notificaciones_usuario_flash as $nuf)
			{
				$notificaciones_reservadas[] = $nuf->notificacion_id;
			}
			/*
			Si la notificacion id que se envia ya esta tomada por el usuario flash no ingresar
			*/
			if(!in_array($id,$notificaciones_reservadas))
			{
				$existe = ClienteNotificacion::where('cliente_id','=',$idUser)->where('notificacion_id','=',$id)->count();
				if($existe == 0)
				{
					$nueva_subscripcion = new ClienteNotificacion;
					$nueva_subscripcion->cliente_id = $idUser;
					$nueva_subscripcion->notificacion_id = $id;
					$nueva_subscripcion->notificacion_registration = $registration;
					$nueva_subscripcion->save();
					print $idUser;
				}
			}
			else
			{
				print 'esta reservado';
			}
		}
	}
}
