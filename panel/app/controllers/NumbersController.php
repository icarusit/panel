<?php

use \FlashTelecom\Number;

class NumbersController extends BaseController
{  
  /**
   * Muestra la página con los números del usuario
   * 
   * @param integer $page
   * @return type
   * 
   * @todo si la consulta es vía ajax, limitar a 5 para
   * no hacer una petición tan tocha cada reload de la página
   */
  	public function showNumbers($page = 1)
  	{
    	if($page < 1) { return Redirect::to('numbers'); }    
    
    	$user = Session::get('user');
		
    	$idUser = $user->getID();        
    	$user_login = User::find($idUser);
		
		$link_alta = true;
		if($user_login->pendiente == 'No facturar')
		{
			$link_alta = false;
		}

    	$numbers = $user->numbers($page);

    	if (isset($numbers->number_of_pages) && ($page - 1) > $numbers->number_of_pages)
      		return Redirect::to('numbers/' . $numbers->number_of_pages);
    
    	return View::make('content/numbers', array(
		  'title'          => 'Mis números',
		  'currentPage'    => $numbers->current_page,
		  'numberOfPages'  => $numbers->number_of_pages,
		  'numbers'        => $numbers->user_numbers,
		  'selectedNumber' => Session::get('number', NULL),
		  'link_alta'      => $link_alta
    	));
  	} 
  
  /**
   * Devuelve los números del usuario para poner en la barra
   * superior. 
   * 
   * @todo   Es una burrada, ya que si sólo vamos a mostrar 5 números
   *         sólo deberíamos pedir 5 números. Refactorizar junto con
   *         la API.
   * 
   * @return string Cadena json
   */
  public function returnTopBarNumbers()
  {
    $user = Session::get('user');
    $numbers = $user->numbers(1);
    
    return json_encode(array_slice($numbers->user_numbers, 0, 5));
  }
  
  /**
   * Fija en la sesión el número actual en el que está el usuario
   * trabajando.
   * 
   * @param string $strNumber Número a fijar
   */
  public function setNumber($strNumber)
  {
    $user = Session::get('user');
	$userID = $user->getIdDist();
    if ($user->hasNumber($strNumber) || (Session::get('master') == 'si' && Session::get('id_distribuidor') == $userID)) {      
      try {
        $number = $this->flash->number($strNumber);
        $signUpDate = $number->getSignUpDate();

        // Borramos los filtros de las estadísticas
        Session::forget('filters');
        
        Session::put('number', $number);
		
		return Redirect::to(URL::to('summary'));
      } catch(\FlashTelecom\NotAFlashNumberException $e) {
        Session::forget('number');
      }
    }
    
    return Redirect::to(URL::previous());
  }
  
 	
}

?>
