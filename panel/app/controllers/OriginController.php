<?php

class OriginController extends BaseController
{
  /**
   * Muestra los desvíos por origen del número
   * 
   * @return type
   */
  	public function showOrigin()
  	{
    	// Recuperamos la lista de prefijos/provincias
    	$prefixes = $this->flash->api('/prefixes');

    	// Recuperamos las acciones de provincia    
    	$numberPrefixesActions = $this->number->getActions();

    	if(is_array($numberPrefixesActions))
		{
      		foreach($numberPrefixesActions as &$numberPrefixesAction)
			{
        		$action = new Action($numberPrefixesAction->action);

        		$numberPrefixesAction->summary = $action->parse();
      		}
    	}
    
    	return View::make('content.origin.origin', array(
      		'title'    => 'Acciones por origen',
      		'prefixes' => $prefixes,
      		'numberPrefixesActions' => $numberPrefixesActions
    	));
  	}
  
  /**
   * Añade un nuevo desvío por origen sobre el número
   */
  	public function add()
  	{
    	$idPrefixAction = Input::get('id_prefix_action', null);    
    	$origin = Input::get('province');
    	$idNumber = $this->number->getID();

    	// Buscamos si ya existe una acción para ese número
    	// y la provincia seleccionada
    	$originAction = OriginAction::where('id_prefix', '=', $origin)
                ->where('id_number', '=', $idNumber)
                ->whereRaw($idPrefixAction === null ? '1' : 'id_prefix_action != ' . $idPrefixAction)
                ->get();

    	if(count($originAction) > 0)
		{
      		$error = "Ya tienes una acción configurada para esta provincia.";
      		return Redirect::to('origin')->with('error', $error);
    	}
		else
		{    
      		if($idPrefixAction === null)
			{
        		$originAction = new OriginAction;
        		$action = Input::get('action_data');
        		$edit = false;
      		}
			else
			{      
        		$originAction = OriginAction::find($idPrefixAction);
        		$action = Input::get('action_data_' . $idPrefixAction);
        		$edit = true;
      		}
      
      		if(trim($action) === '' || !((new Action(json_decode($action)))->isValid()))
			{
        		$error = 'La acción por origen no puede estar en blanco o la acción '
                		. 'configurada no es válida.';
        		return Redirect::to('origin')->with('error', $error);
      		}
      
      		$originAction->id_number = $idNumber;
      		$originAction->id_prefix = $origin;
      		$originAction->action    = $action;

      		if($originAction->save())
			{
        		$this->refresh();
        		$info = 'La acción por origen ha sido correctamente ' .
                		($edit ? 'modificada' : 'añadida') . '.';
        		return Redirect::to('origin')->with('info', $info); 
      		}
			else
			{
        		$error = 'Ha habido algún problema al ' . ($edit ? 'modificar' : 'añadir') .
                		' la acción por origen.';
        		return Redirect::to('origin')->with('error', $error);
      		}
    	}
  	}
  
  /**
   * Método para eliminar una acción por origen
   * 
   * @param type $idAction
   */  
  public function remove($idAction)
  {
    $idNumber = $this->number->getID();
    $originAction = OriginAction::find($idAction);
    
    if(!is_null($originAction) && $originAction->id_number == $idNumber) {
      $originAction->delete();
      $this->refresh();
      
      $info = "La acción por origen ha sido correctamente eliminada.";      
      return Redirect::to('origin')->with('info', $info);
    }
  }
}

?>
