<?php
/**
 * Controlador para las llamadas salientes desde números "analog"
 */
class OutgoingCallsController extends BaseController
{
  /**
   * Muestra la ventana principal
   */
  public function show()
  {
    $number = $this->number;
    $whitelistedNumbers = ProxyNumberWhitelist::where('id_number', '=', $number->getID())->get();
     
    return View::make('content.outgoing_calls', array(
      'number'  => $number->getNumber(),
      'numbers' => $whitelistedNumbers
    ));
  }
  
  /**
   * Método que añade un nuevo número de origen
   * a la lista blanca del número para llamadas
   * salientes vía el proxy
   */
  public function add()
  {
    $number = $this->number;
    $origin = Input::get('number');

    $rules = array(
      'number' => 'required|numeric|unique:proxy_number_whitelist,caller'
    );
    
    $messages = array(
      'number.required' => 'El campo número es obligatorio',          
      'number.numeric'  => 'El campo debe ser numérico.',
      'number.unique'   => 'El número ya existe en la lista de orígenes.'
    );
    
    $validator = Validator::make(Input::all(), $rules, $messages);

    if($validator->fails()) {
      return Redirect::to('outgoing-calls')->withErrors($validator);
    } else {
      $numberRow = new ProxyNumberWhitelist;

      $numberRow->id_number = $number->getID();
      $numberRow->caller    = $origin;
      $numberRow->active    = true;
      
      if($numberRow->save()) {
        $info = "El número <strong>$origin</strong> ha sido correctamente añadido.";
        return Redirect::to('outgoing-calls')->with('info', $info);
      } else {
        $error = "Ha habido algún problema al añadir el número a la lista. Inténtelo de nuevo más tarde.";        
        return Redirect::to('outgoing-calls')->with('error', $error);
      }
    }
  }
  
  /**
   * Método para borrar números de la lista de origen
   */
  public function remove($idNumber)
  {
    $user = User::find($this->user->getID());
    
    $numberToRemove = ProxyNumberWhitelist::find($idNumber);
    $assocNumber    = Number::find($numberToRemove->id_number);
    $userOwnsNumber = $user->hasNumber($assocNumber->numero_visible);
	$userID 		= $this->user->getIdDist();

    if(($numberToRemove instanceof ProxyNumberWhitelist) && ($userOwnsNumber || Session::get('master') == 'si' && Session::get('id_distribuidor') == $userID)) {
      $numberToRemove->delete();
      $info = "El número de origen <strong>{$numberToRemove->caller}</strong> ha sido " .
              "eliminado de la lista.";        

      return Redirect::to('outgoing-calls')->with('info', $info);
    } else {
      return Redirect::to('outgoing-calls');
    }
  }
  
  /**
   * Método que activa-desactiva los números en la lista de origen
   */
  public function toggle()
  {
    $idNumber = Input::get('idNumber', null);

    if($idNumber === null) return 'false';
    
    $user = User::find($this->user->getID());
    
    $numberToToggle = ProxyNumberWhitelist::find($idNumber);
    $assocNumber    = Number::find($numberToToggle->id_number);
    $userOwnsNumber = $user->hasNumber($assocNumber->numero_visible);
    
    if(($numberToToggle instanceof ProxyNumberWhitelist) && $userOwnsNumber) {
      $toggleTo = !$numberToToggle->active;
      $numberToToggle->active = $toggleTo;
      $numberToToggle->save();
      return 'true';
    }
  }
}
