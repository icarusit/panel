<?php

class PaymentController extends BaseController
{
  /**
   * Método que se ejecuta en el callback del TPV
   */
    public function callback()
	{
		//paso 3 de la guia		
		Log::error('RECEPCION:'.serialize($_POST));
		if(($version        = Input::get('Ds_SignatureVersion', null)) !== null &&
		   ($params       = Input::get('Ds_MerchantParameters', null)) !== null &&
		   ($signatureRecibida     = Input::get('Ds_Signature', null)) !== null)
		{
	
			//Paso 2 de la guia
			$obj_api = Payment::RedsysAPI();
			Log::error('PASOS: paso el paso 2');
			//Paso 4 de la guia
			$decode =  $obj_api->decodeMerchantParameters($params);			
			Log::error('PASOS: decode '.serialize($decode));
			$obj_api->stringToArray($decode);
			$codigoRespuesta = $obj_api->getParameter('Ds_Response');
			Log::error('PASOS: codigo respuesta '.$codigoRespuesta);
			//Paso 5 de la guía
			$randomKey       = Payment::getRandomKey();
			$signatureCalculada = $obj_api->createMerchantSignatureNotif($randomKey,$params);
			Log::error('PASOS: signatureCalculada '.$signatureCalculada);
			//Comparacion
		
			if($signatureCalculada === $signatureRecibida)
			{
				$order = $obj_api->getParameter('Ds_Order');
				$amount =  $obj_api->getParameter('Ds_Amount');

				$resultado =  $obj_api->getParameter('Ds_Response');
				$autorizacion =  trim($obj_api->getParameter('Ds_AuthorisationCode'));
				if(((int)$resultado >=0 && (int)$resultado <= 99) && $autorizacion != '') 
				{				
					// En función del prefijo de la referencia de pago, determinamos
					// el controlador al que redirigimos para enviar correo al usuario,
					// marcar la factura como pagada, sumar saldo, etc.
					$controllers = array(
					  'IN' => 'InvoicesController',
					  'UB' => 'BalanceController',
					  'TP' => 'FlatRatesController'
					);
					
					$prefix = substr($order, 0, 2);
					Log::error('PASOS: Orden '.$order);
					Log::error('PASOS: Amount '.$amount);
					if(isset($controllers[$prefix]))
					{          
						return App::make($controllers[$prefix])->callback($order, $amount);
					}
					else
					{
						Log::error('PASOS: No controlador encontrado ');
						// Si no existe, debe haber sido un error nuestro porque en principio
						// no deberíamos estar aquí si es un listillo porque no debe haber
						// podido generar la referencia, de todas formas redigirimos a la home
						return Redirect::to('/');          
					}
				}
				else
				{
					Log::error('PASOS: Firma no valida ');
				}
			}
			else
			{
				Log::error('PASOS: Firma no valida ');
			}
		}
		else
		{
			Log::error('PASOS: Datos inválidos ');
		}
	}
	  
  public function callback_old()
  {
    if(($order        = Input::get('Ds_Order', null)) !== null &&
       ($amount       = Input::get('Ds_Amount', null)) !== null &&
       ($response     = Input::get('Ds_Response', null)) !== null &&            
       ($merchantCode = Input::get('Ds_MerchantCode', null)) !== null &&  
       ($currency     = Input::get('Ds_Currency', null)) !== null &&              
       ($cbSignature  = Input::get('Ds_Signature', null)) !== null) {

      // Comprobamos coincidencia de la firma y que la respuesta
      // esté entre 0000 y 0099 (indica pago correcto)
      $signature = strtoupper(sha1($amount.$order.$merchantCode.$currency.$response.Payment::getRandomKey()));

      if($signature === $cbSignature && (int)$response >= 0 && (int)$response <= 99)
	  {
       	// En función del prefijo de la referencia de pago, determinamos
        // el controlador al que redirigimos para enviar correo al usuario,
        // marcar la factura como pagada, sumar saldo, etc.
        $controllers = array(
          'IN' => 'InvoicesController',
          'UB' => 'BalanceController',
          'TP' => 'FlatRatesController'
        );
        
        $prefix = substr($order, 0, 2);
        
        if(isset($controllers[$prefix]))
		{          
          return App::make($controllers[$prefix])->callback($order, $amount);
        }
		else
		{
          // Si no existe, debe haber sido un error nuestro porque en principio
          // no deberíamos estar aquí si es un listillo porque no debe haber
          // podido generar la referencia, de todas formas redigirimos a la home
          return Redirect::to('/');          
        }
      }
    }
  }
  
  /**
   * Método que guarda la forma de pago de las cuotas
   * del cliente
   */
  	public function update()
  	{
    	$paymentMethod = Input::get('payment-method', null);
    	$bankAccountNumber = trim(Input::get('bank-account-number', null));
        
    	$redirect = Input::get('redirect', 'me');
    	$redirectError = Input::get('redirect', 'welcome/payment-method');
    
    	$user = Session::get('user');
    	$user = User::find($user->getID());
    
    	$user->pendiente = "Facturable";   
    
    	//
    	// Recibo domiciliado
    	//
    	if($paymentMethod === 'direct-debit')
		{
			$validBankAccount = Payment::checkIBAN($bankAccountNumber);
      		//$validBankAccount = Payment::isBankAccountNumberValid($bankAccountNumber);
      
			if(!$validBankAccount)
	  		{
        		$error = "El número de cuenta introducido no es válido, por favor, inténtalo"
                	. " de nuevo";
        		return Redirect::to($redirectError)->with('error', $error);
      		}
      
      		$user->remesar = "Si";
      		$user->ccc_pagos = $bankAccountNumber;
    		//
    		// Pago vía tarjeta o transferencia
    		//
    	}
		elseif($paymentMethod === 'regular')
		{
      		$user->remesar = "Tarjeta";      
    	}
		elseif($paymentMethod === 'remesar')
		{
			$user->remesar = "Transferencia"; 
		}
    
		if($user->save())
		{
      		$info = "Se han almacenado correctamente tus datos de pago.";
      		return Redirect::to($redirect)->with('info', $info);
    	}
		else
		{
      		$error = "Ha habido algún error almacenando tus datos de pago.";
      		return Redirect::to($redirectError)->with('error', $error);      
    	}
  	}
}
