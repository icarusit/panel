<?php
class PrefacturaController extends BaseController
{
  /**
   * Muestra ventana de facturas
   */
  	public function enviar()
  	{
		$AnnoMes = date('Ym',strtotime('now'));
		$mes = date('m',strtotime('now'));
		$mes = $mes == 13?1:$mes+1;
		$subject = 'LÍNEAS A FACTURAR EL DÍA 1 DE '.$this->mes_es($mes);
  		//Obtener los clientes
		$clientes = User::where('id_cliente', '>', 0)->orderBy('nombre_fiscal')->get();
		$envios = 0;
		foreach($clientes as $cliente)
		{
			$cliente_fecha_baja = date('Ym',strtotime($cliente->fecha_baja));
			if($cliente_fecha_baja <= $AnnoMes || $cliente->fecha_baja == NULL)
			{			
				//Obtener los numeros del cliente
				$numeros  = Number::where('id_cliente','=',$cliente->id_cliente)->get();
				
				$lineas = array();
				$total = 0;
				$flatrate = false;
				foreach($numeros as $numero)
				{
					$AnnoMes_baja = date('Ym',strtotime($numero->fecha_baja));
					if($AnnoMes_baja <= $AnnoMes || $numero->fecha_baja == NULL)
					{
						$perfil = Profile::find($numero->id_perfil);
						if($perfil)
						{
							$importe_con_descuento = $perfil->cuota - ($perfil->cuota*$numero->descuento)/100;
							$importe = $importe_con_descuento + ($importe_con_descuento*$cliente->IVA)/100;
							//Obtener las tarifas flatrates
							$sql = "SELECT f.* FROM flatrates as f JOIN number_flatrates as nf ON nf.id_flatrate = f.id_flatrate WHERE nf.id_number = ?";     
    						$tarifas_flatrates = DB::select($sql, array($numero->id_numero));
							
							$lineas[] = array(
												'nombre' => $perfil->perfil.': '.$numero->numero_visible,
												'cuota' => $perfil->cuota,'impuesto' => $cliente->IVA,
												'importe' => $importe,
												'tarifas_flatrates' => $tarifas_flatrates
											);
							$total+=$perfil->cuota;
						}
					}
				}			
				//Enviar correo
				if(sizeof($lineas) > 0)
				{
					$content = array(
										'header'    => $subject,
										'subheader' => '',
										'body'      => $this->plantillaCorreo($cliente,$subject,$lineas,$total)
									);
 					$this->flash->sendMail('alexturruella@gmail.com', $subject, $content);
					if($envios == 1)
					{
						return 0;
					}
					$envios++;
				}
				
			}
		}
  	}
	
	function plantillaCorreo($cliente,$asunto,$lineas,$total)
	{
		//$pais = Countries::find($cliente->id_country);
		//App::setLocale($pais->idioma);
		$cuerpo = '';
		//$cuerpo.= '<h3>'.$asunto.'</h3>';
		$cuerpo.= 'Cliente: '.$cliente->nombre_fiscal.'<br/>';
		$cuerpo.= '</hr>';
		$cuerpo.= 'Líneas';
		$cuerpo.= '</hr>';
		$cuerpo.= '<table>';
			$cuerpo.= '<thead>';
				$cuerpo.= '<tr>';
					$cuerpo.= '<th align="left" colspan="2">Concepto</th>';
					$cuerpo.= '<th align="right">Importe (€)</th>';
				$cuerpo.= '</tr>';
			$cuerpo.= '</thead>';
			$cuerpo.= '<tbody>';
				foreach($lineas as $linea)
				{
					$cuerpo.= '<tr>';
						$cuerpo.= '<td colspan="2" align="left">'.$linea['nombre'].'</td>';
						$cuerpo.= '<td align="right">'.$linea['cuota'].' € + IVA = '.$linea['importe'].' €</td>';
					$cuerpo.= '<tr>';
					
					if(isset($linea['tarifas_flatrates']) && sizeof($linea['tarifas_flatrates']) > 0)
					{
						foreach($linea['tarifas_flatrates'] as $tarifa)
						{
							$cuerpo.= '<tr>';
								$cuerpo.= '<td align="left">&nbsp;&nbsp;&nbsp;</td>';
								$cuerpo.= '<td>'.$tarifa->name.'</td>';
								$cuerpo.= '<td align="right">'.$tarifa->monthly_fee.' €  + IVA = '.($tarifa->monthly_fee + ($tarifa->monthly_fee*$cliente->IVA)/100).' €</td>';
							$cuerpo.= '<tr>';
						}
					}
				}
			$cuerpo.= '</tbody>';
		$cuerpo.= '</table>';
		/*$cuerpo.= '<br/>';
		$cuerpo.= '<br/>';
		$cuerpo.= 'Total: '.$total;
		$cuerpo.= '<br/>';
		$cuerpo.= 'IVA: '.$cliente->IVA;
		$cuerpo.= '<br/>';
		$cuerpo.= 'Total + IVA: '.($total + ($total*$cliente->IVA)/100);
		*/
		
		return $cuerpo;
	}
	
	function mes_es($mes)
	{
		$meses = array(
							1 => 'enero',
							2 => 'febrero',
							3 => 'marzo',
							4 => 'abril',
							5 => 'mayo',
							6 => 'junio',
							7 => 'julio',
							8 => 'agosto',
							9 => 'septiembre',
							10 => 'octubre',
							11 => 'noviembre',
							12 => 'diciembre'
						);
		return strtoupper($meses[$mes]);
	}
 	
}

?>