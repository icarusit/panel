<?php
use Panel\Model\Queue;
class QueueController extends BaseController
{
  	/**
  	 * Muestra ventana de la centralita
   	* 
   	* @return View
   	*/
	public function showQueues()
  	{ 
    	// Recuperamos todas las cuentas IP del usuario
    	$idUser = Session::get('user')->getID();    
    	$sipPeers = SIPPeers::where('id_cliente', '=', $idUser)->get();
    
    	// Recuperamos la cola de llamada actual del número 
    	$idNumber = $this->number->getID();
    	$numberRow = Number::where('id_numero', '=', $idNumber)->firstOrFail();
    
    	$queue = $this->parseQueue($numberRow->cuenta_sip);
		
		$listaGrupos = Group::where('id_client', '=', $idUser)->get();
    
    	return View::make('content.queue', array(
      		'title'    => 'Cola de llamada',
      		'sipPeers' => $sipPeers,
      		'queue'    => $queue,
			'grupos'   => $listaGrupos         
    	));
  	}
  
  	/**
   	* Método protegido que parsea la cadena de cuenta_sip
   	* 
   	* Esto debería ir en un modelo
   	*/
	protected function parseQueue($str)
  	{
		//buscar la lista de grupos
		$idUser = Session::get('user')->getID();
		$listaGrupos = Group::where('id_client', '=', $idUser)->get();
		foreach($listaGrupos as $grupo)
		{
			$str = str_replace($grupo->contacts,$grupo->name, $str);
		}
    	return Queue::parse($str);
  	}
  
  	/**
   	* Método para ordenar la cola de llamada
   	*/
	public function order()
  	{
    	$postOrder = Input::get('order', NULL);
    	$tipo_primer_elemento = 'SIP';
    	if($postOrder !== NULL)
		{      
      		foreach($postOrder as $i => &$cascade)
			{
        		foreach($cascade as $j => &$simult)
				{
					$simult = trim($simult);
					//verificar si es un grupo
					$grupo = Group::where('name', '=', $simult)->first();
					if($grupo)
					{
						$simult = $grupo->contacts;
					}
					else
					{
						if(is_numeric($simult))
						{
							$simult = 'SIP/NSG/' .trim($simult);
							if($i == 0 && $j == 0)
							{
								$tipo_primer_elemento = 'NUMERO';
							}
						}
						else
						{
							$simult = 'SIP/' .trim($simult);	
						}
					}
        		}        
        		$cascade = implode('&', $cascade);
      		}      
      		$strQueue = implode(',', $postOrder);
      
	  		if($tipo_primer_elemento == 'SIP')
			{
      			$strQueue = substr($strQueue, strlen('SIP/'), strlen($strQueue));
			}
			elseif($tipo_primer_elemento == 'NUMERO')
			{
				//$strQueue = substr($strQueue, strlen('SIP/NSG/'), strlen($strQueue));
			}
    	}
		else
		{
      		$strQueue = '';
    	}

    	$number = Number::find($this->number->getID());
    	$number->cuenta_sip = $strQueue;
    
    	echo $number->save() ? TRUE : FALSE;
  	}
}
