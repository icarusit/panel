<?php

class RatesController extends BaseController
{
  /**
   * Muestra ventana calculadora de tarifas llamadas
   * 
   * @return View
   */
  public function showRates()
  {
    $country = Input::get('country', null);

    if($country !== null) {
      Session::put('country', $country);
    } else {
      $country = Session::get('country', null);
    }
    
    $rates = $this->getRates($country);
    
    $perPage = 15;
    $currentPage = Input::get('page', 1) - 1;
    $pagedRates = array_slice($rates, $currentPage * $perPage, $perPage);
    $paginator = Paginator::make($pagedRates, count($rates), $perPage);    
    
    if(Request::ajax()) {
      echo json_encode(array(
        'rates'        => $pagedRates,
        'paginator'    => (string)$paginator->links()
      ));
    } else {      
      if($currentPage > ($paginator->getLastPage() - 1))
        return Redirect::to('rates');
      
      return View::make('content.rates.rates', array(
        'title'        => 'Tarifas',
        'rates'        => $pagedRates,
        'paginator'    => $paginator,
        'userHasTaxes' => $this->user->hasTaxes()
      ));    
    }  
  }
  
  	/*
  	rates para ser consumidos desde el protal
  	*/
  	public function portalRates()
  	{
		$country = Input::get('country', null);

    	if($country !== null)
		{
      		Session::put('country', $country);
    	}
		else
		{
      		$country = Session::get('country', null);
    	}
    
    	$rates = $this->getRatesPortal($country);
    
    	$perPage = 10;
    	$currentPage = Input::get('page', 1) - 1;
    	$pagedRates = array_slice($rates, $currentPage * $perPage, $perPage);    
		
    	if(isset($_GET['callback']))
		{
			header('content-type: text/javascript');
			print $_GET['callback'] . '(' . json_encode(array(
												 'rates'        => $pagedRates
										)) . ')';
		}
		else
		{
			header('content-type: application/json');
			
      		print json_encode(array(
       							 'rates'        => $pagedRates
      					));
		}
  	}
  
  /**
   * Recupera las tarifas
   * 
   * OJO. Este método debe ser lentísimo. Intenté hacerlo con una sola consulta
   * pero me lié y no supe hacerlo. Volver aquí cuando aprenda MySQL en condiciones
   * 
   * @param string  $like País (se buscara con comodines a los lados)
   * 
   * @todo Esto debería ir en los modelos
   */
  public function getRates($like = null)
  {
    $idUser = $this->user->getID();
    
    //    
    //    $sql = "SELECT c.country, c.country_es, r.detail, r.initial_connection_charge, 
    //      ROUND(r.initial_connection_charge * (1 + cl.IVA/100), 4) as 'initial_connection_charge_plus_taxes',
    //      ROUND((r.rate - rd.discount) * (1 + cl.IVA/100), 4) as 'rate_plus_taxes', r.prefix,
    //      ROUND(r.rate - rd.discount, 4) as 'rate' FROM prefixes_rates r, cliente cl 
    //      LEFT JOIN prefixes_rates_discount rd ON rd.id_prefix_rate = r.id_prefix_rate
    //      JOIN countries c ON r.id_country = c.id_country
    //      WHERE cl.id_cliente = ?;";
    //    
    
    // Recuperamos los descuentos para el usuario
    $sql = "SELECT id_prefix_rate, ROUND(discount, 4) as `discount` FROM prefixes_rates_discount WHERE id_user = ?";    
    $rawDiscounts = DB::select($sql, array($idUser));    

    $discounts = array();
    foreach($rawDiscounts as $discount)
      $discounts[$discount->id_prefix_rate] = (float)$discount->discount;

    // IVA del cliente
    $iva = (int)User::find($idUser)->IVA;
    
    // Tarifas
    $sql = "SELECT c.iso, r.id_prefix_rate, c.country, c.country_es, r.detail, r.detail_es, 
      ROUND(r.initial_connection_charge, 4) as `initial_connection_charge`, 
      ROUND(r.initial_connection_charge * (1 + $iva/100), 4) as 'initial_connection_charge_plus_taxes',
      ROUND(r.rate * (1 + $iva/100), 4) as 'rate_plus_taxes', r.prefix, 
      ROUND(r.rate, 4) as 'rate' FROM prefixes_rates r
      LEFT JOIN countries AS c ON r.id_country = c.id_country";
    
    if($like !== null) {
      $sql.= " WHERE UPPER(c.country_es) LIKE UPPER(?) OR UPPER(r.detail_es) LIKE UPPER(?)";
      $sql.= " OR UPPER(c.country) LIKE UPPER(?) OR UPPER(r.detail) LIKE UPPER(?)";
    }

    $sql.= " GROUP BY r.detail_es ORDER BY c.country IS NULL, c.country_es ASC";

    if($like !== null) {
      $rates = DB::select($sql, array('%' . $like . '%', '%' . $like . '%', '%' . $like . '%', '%' . $like . '%'));  
    } else {
      $rates = DB::select($sql);  
    }

    foreach($rates as $key => &$rate) {
      $discount = isset($discounts[$rate->id_prefix_rate]) 
        ? $discounts[$rate->id_prefix_rate]
        : 0;
      
      $rate->rate = (float)$rate->rate;
      $rate->rate_plus_taxes = (float)$rate->rate_plus_taxes;
      $rate->initial_connection_charge = (float)$rate->initial_connection_charge;
      $rate->initial_connection_charge_plus_taxes = (float)$rate->initial_connection_charge_plus_taxes;
      
      if($discount > 0) {
        $rate->rate = round($rate->rate - $discount, 4);
        $rate->rate_plus_taxes = round(($rate->rate - $discount) * (1 + $iva/100), 4);
      }

      $rates[$key] = $rate;
    }
    
    return $rates;  
  }
  
  /*
  Ge rates para el protal
  */
  	public function getRatesPortal($like = null)
  	{    
    	// Tarifas
    	$sql = "SELECT c.iso, r.id_prefix_rate, c.country, c.country_es, r.detail, r.detail_es, 
		  ROUND(r.initial_connection_charge, 4) as `initial_connection_charge`, 
		  ROUND(r.initial_connection_charge * (1), 4) as 'initial_connection_charge_plus_taxes',
		  ROUND(r.rate * (1), 4) as 'rate_plus_taxes', r.prefix, 
		  ROUND(r.rate, 4) as 'rate' FROM prefixes_rates r
		  LEFT JOIN countries AS c ON r.id_country = c.id_country";
    
    	if($like !== null)
		{
      		$sql.= " WHERE UPPER(c.country_es) LIKE UPPER(?) OR UPPER(r.detail_es) LIKE UPPER(?)";
      		$sql.= " OR UPPER(c.country) LIKE UPPER(?) OR UPPER(r.detail) LIKE UPPER(?)";
    	}

    	$sql.= " GROUP BY r.detail_es ORDER BY c.country IS NULL, c.country_es ASC";

    	if($like !== null)
		{
      		$rates = DB::select($sql, array('%' . $like . '%', '%' . $like . '%', '%' . $like . '%', '%' . $like . '%'));  
    	}
		else
		{
      		$rates = DB::select($sql);  
    	}

    	foreach($rates as $key => &$rate)
		{
      		$discount = isset($discounts[$rate->id_prefix_rate])?$discounts[$rate->id_prefix_rate]:0;
      
      		$rate->rate = (float)$rate->rate;
      		$rate->rate_plus_taxes = (float)$rate->rate_plus_taxes;
      		$rate->initial_connection_charge = (float)$rate->initial_connection_charge;
      		$rate->initial_connection_charge_plus_taxes = (float)$rate->initial_connection_charge_plus_taxes;
      		$rates[$key] = $rate;
    	}    
    	return $rates;  
  	}
  
  /**
   * Método que calcula la tarificación de una llamada
   * 
   * @param string $destNumber Número de destino
   */
  public function calculate($destNumber)
  {  
    // Si tiene pinta de número español, metemos el 34 delante
    if(in_array(substr($destNumber, 0, 1), [6, 7, 8, 9]) && strlen($destNumber) == 9)
      $destNumber = '34' . $destNumber;

    // Si tiene pinta de internacional, le quitamos los "00"
    if(substr($destNumber, 0, 2) == "00") 
      $destNumber = substr($destNumber, 2, strlen($destNumber));
    
    echo json_encode($this->flash->api('/v1/rate/' . $this->user->getID(), array('to' => $destNumber)));
  }    
  
  /**
   * Método para exportar las estadísticas
   */
  public function export()
  {
    $username = $this->user->getUsername();

    $rates = $this->getRates();
    
    $content = "Pais\tDetalle\tTarifa (EUR/min)\tTarifa (+ impuestos) (EUR/min)\tEstablecimiento (EUR)\tEstablecimiento (+ impuestos) (EUR)\n";

    foreach($rates as $rate) {
      $content.= $rate->country_es . "\t" . $rate->detail .
              "\t" . $rate->rate . "\t" . $rate->rate_plus_taxes . "\t" .
              $rate->initial_connection_charge . "\t" .
              $rate->initial_connection_charge_plus_taxes . "\n";
    }

    $encodedContent = mb_convert_encoding($content, 'UTF-16LE', 'UTF-8');

    $response = Response::make(chr(255) . chr(254) . $encodedContent, 200);

    $response->header('Content-Description', 'File Transfer');
    $response->header('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
    $response->header('Content-Disposition', "attachment;filename=flash_telecom_{$username}_rates.xls");
    $response->header('Content-Transfer-Encoding', 'binary');
    $response->header('Content-length', strlen($encodedContent));     

    return $response;          
  }  
}
