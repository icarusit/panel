<?php
class ReportsController extends BaseController
{
	var $meses = array(
						1 => 'Enero',
						2 => 'Febrero',
						3 => 'Marzo',
						4 => 'Abril',
						5 => 'Mayo',
						6 => 'Junio',
						7 => 'Julio',
						8 => 'Agosto',
						9 => 'Septiembre',
						10 => 'Octubre',
						11 => 'Noviembre',
						12 => 'Diciembre'
					  );
					  
  	public function show($anno = NULL,$tipo = NULL)
  	{
		require_once(app_path() . '/helpers/funciones.php');
		
		//Opciones de filtrado		
    	if($anno == NULL)
		{
			$anno = Input::get('anno');
			if($anno == NULL)
			{
				$anno = date('Y',strtotime('now'));
			}
		}
		$tipo = Input::get('tipo');
		if($tipo == NULL)
		{
			$tipo = 'fecha_alta';
		}
		
		//Vista
    	return View::make('content/reports/anual', array(
      		'title'            => $this->titulo_reporte($tipo,$anno),
			'extremos'         => $this->minimo_maximo_anno($tipo),
			'anno'             => $anno,
			'tipo'             => $tipo,
			'resumen'          => $this->resumen($tipo,$anno),
			'resumen_clientes' => $tipo == 'fecha_alta'?$this->resumen_clientes($tipo,$anno):NULL,
			'id_administrador' => Session::get('user')->getID()
    	));
  	}
	
	public function showMensual($anno,$mes,$campo)
	{
		require_once(app_path() . '/helpers/funciones.php');
		//Vista
		$semanas = $this->semanasMes($mes,$anno);
		
    	return View::make('content/reports/mensual', array(
      		'title'            => $this->titulo_reporte($campo,$anno,$mes),
			'semanas'          => $semanas,
			'anno'             => $anno,
			'mes'              => $mes,
			'tipo'             => $campo,
			'resumen'          => $this->resumen_mensual($semanas,$anno,$mes,$campo),
			'resumen_clientes' => $campo == 'fecha_alta'?$this->resumen_mensual_clientes($semanas,$anno,$mes,$campo):NULL,
			'id_administrador' => Session::get('user')->getID()
    	));
	}
	
	private function resumen_mensual($semanas,$anno,$mes,$campo)
	{		
		$selects = array();
		foreach($semanas as $key => $semana)
		{
			$selects[] = 'SUM(IF(DAY(n.'.$campo.') between '.$semana['inicio'].' AND '.$semana['fin'].',1,0)) as "'.$semana['identificador'].'"';
		}
		$sql = "SELECT 
					a.name as 'PERFIL',
					".implode(',',$selects)."
					
					FROM perfil_agrupacion as a left join perfiles as p on a.id=p.id_agrupacion left join numero as n on p.id=n.id_perfil where MONTH(n.".$campo.") = ".$mes." AND YEAR(n.".$campo.") = ".$anno." group by a.id order by a.orden";
					
		$res = DB::select($sql);
		if(count($res) > 0)
		{
			return $res;
		}
		else
		{
			return array();
		}
	}
	
	private function resumen_mensual_clientes($semanas,$anno,$mes,$campo)
	{		
		$selects = array();
		foreach($semanas as $key => $semana)
		{
			$selects[] = 'SUM(IF(DAY(c.'.$campo.') between '.$semana['inicio'].' AND '.$semana['fin'].',1,0)) as "'.$semana['identificador'].'"';
		}
		$sql = "SELECT 
					".implode(',',$selects)."
					
					FROM cliente as c where MONTH(c.".$campo.") = ".$mes." AND YEAR(c.".$campo.") = ".$anno."";
					
		$res = DB::select($sql);
		if(count($res) > 0)
		{
			return $res[0];
		}
		else
		{
			return array();
		}
	}
	
	
	private function resumen($campo,$anno)
	{
		$sql = "SELECT 
					a.name as 'PERFIL',
					SUM(IF(MONTH(n.".$campo.") = 1,1,0)) as enero,
					SUM(IF(MONTH(n.".$campo.") = 2,1,0)) as febrero,
					SUM(IF(MONTH(n.".$campo.") = 3,1,0)) as marzo,
					SUM(IF(MONTH(n.".$campo.") = 4,1,0)) as abril,
					SUM(IF(MONTH(n.".$campo.") = 5,1,0)) as mayo,
					SUM(IF(MONTH(n.".$campo.") = 6,1,0)) as junio,
					SUM(IF(MONTH(n.".$campo.") = 7,1,0)) as julio,
					SUM(IF(MONTH(n.".$campo.") = 8,1,0)) as agosto,
					SUM(IF(MONTH(n.".$campo.") = 9,1,0)) as septiembre,
					SUM(IF(MONTH(n.".$campo.") = 10,1,0)) as octubre,
					SUM(IF(MONTH(n.".$campo.") = 11,1,0)) as noviembre,
					SUM(IF(MONTH(n.".$campo.") = 12,1,0)) as diciembre 
					
					FROM perfil_agrupacion as a left join perfiles as p on a.id=p.id_agrupacion left join numero as n on p.id=n.id_perfil where YEAR(n.".$campo.") = ".$anno." group by a.id order by a.orden";
		$res = DB::select($sql);
		if(count($res) > 0)
		{
			return $res;
		}
		else
		{
			return array();
		}
	}
	
	private function resumen_clientes($campo,$anno)
	{
		$sql = "SELECT 
					SUM(IF(MONTH(c.".$campo.") = 1,1,0)) as enero,
					SUM(IF(MONTH(c.".$campo.") = 2,1,0)) as febrero,
					SUM(IF(MONTH(c.".$campo.") = 3,1,0)) as marzo,
					SUM(IF(MONTH(c.".$campo.") = 4,1,0)) as abril,
					SUM(IF(MONTH(c.".$campo.") = 5,1,0)) as mayo,
					SUM(IF(MONTH(c.".$campo.") = 6,1,0)) as junio,
					SUM(IF(MONTH(c.".$campo.") = 7,1,0)) as julio,
					SUM(IF(MONTH(c.".$campo.") = 8,1,0)) as agosto,
					SUM(IF(MONTH(c.".$campo.") = 9,1,0)) as septiembre,
					SUM(IF(MONTH(c.".$campo.") = 10,1,0)) as octubre,
					SUM(IF(MONTH(c.".$campo.") = 11,1,0)) as noviembre,
					SUM(IF(MONTH(c.".$campo.") = 12,1,0)) as diciembre 
					
					FROM cliente as c where YEAR(c.".$campo.") = ".$anno."";
		$res = DB::select($sql);
		if(count($res) > 0)
		{
			return $res[0];
		}
		else
		{
			return array();
		}
	}
	
	private function minimo_maximo_anno($campo = 'fecha_alta')
	{
		$extremos = array('inicio' => 0,'fin'=>'');
		$sql = 'SELECT MAX(YEAR('.$campo.')) as fin,MIN(YEAR('.$campo.')) as inicio FROM `numero` where '.$campo.' != 0';      
      	$res = DB::select($sql);

      	if(count($res) > 0)
		{
			$extremos['inicio'] = $res[0]->inicio;
			$extremos['fin'] = $res[0]->fin;
		}
		return $extremos;		
	}
	
	private function titulo_reporte($tipo,$anno,$mes = NULL)
	{
		switch($tipo)
		{
			case 'fecha_alta':
			{
				if($mes > 0)
				{
					return 'Reporte mensual de Altas del mes '.$this->meses[$mes].' del año '.$anno;
				}
				return 'Reporte anual de Altas del año '.$anno;
				break;
			}
			case 'fecha_baja':
			{
				if($mes > 0)
				{
					return 'Reporte mensual de Bajas del mes '.$this->meses[$mes].' del año '.$anno;
				}
				return 'Reporte anual de Bajas del año '.$anno;
				break;
			}
		}
	}
	
	private function semanasMes($mes,$anno)
	{		
		$ultimo_dia = date("d",mktime(0,0,0,$mes+1,0,$anno));
		$semanas = array();
		$cantidad_semanas = 1;
		$inicio = 1;
		$fin = 0;
		$dia_semana = '';
		for($i = 1;$i<=$ultimo_dia;$i++)
		{
			$fecha = mktime(0,0,0,$mes,$i,$anno);
			$dia_semana = date('w',($fecha));
			if($dia_semana == 0)
			{
				$semanas[$cantidad_semanas] = array('inicio' => $inicio,'fin'=>$i,'identificador' => 'del_'.$inicio.'_al_'.$i,'label' => 'del '.$inicio.' al '.$i);
				$inicio = $i+1;
				$cantidad_semanas++;
			}
		}
		$ultima_semana = end($semanas);
		if($ultima_semana['fin'] != $ultimo_dia)
		{
			$semanas[$cantidad_semanas] = array('inicio' => $inicio,'fin' => $ultimo_dia,'identificador' => 'del_'.$inicio.'_al_'.$ultimo_dia,'label' => 'del '.$inicio.' al '.$ultimo_dia);
		}
		return $semanas;
	}
 	
}
?>