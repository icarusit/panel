<?php

class SIPAccountsController extends BaseController
{
  /**
   * Muestra ventana de las cuentas SIP
   * 
   * @return View
   */
  public function showSIPAccounts()
  {    
    $number = $this->number;
    $idUser = $number->getUser()->getID();

    // Ordenación
    $sortBy = Input::get('sort_by', 'status');
    $sort   = Input::get('sort',    'desc');

    if(!in_array($sort, array('asc', 'desc'))) {
      $sort = 'desc';
    }
    
    if(!in_array($sortBy, array('status'))) {
      $sortBy = 'status';
    }
    
    $sipAccounts = SIPPeers::where('id_cliente', '=', $idUser)
                ->orderByRaw("(ipaddr != '' && ipaddr != '(null)') $sort");

    
    return View::make('content.sipaccounts.sipaccounts', array(
      'title'       => 'Cuentas SIP',
      'sort'        => $sort,
      'sortBy'      => $sortBy,
      'sipAccounts' => $sipAccounts->paginate(15)
    ));
  }  
    
  /**
   * Muestra ventana edición cuenta SIP
   * 
   * @param integer $idSipAccount
   */
  public function edit($idSipAccount)
  {
    $sipAccount = SIPPeers::where('id_cliente', '=', $this->user->getID())
                    ->where('id_serial', '=', $idSipAccount)
                    ->first();
    
    if($sipAccount !== null) {    
      return View::make('content.sipaccounts.edit', array(
        'sipAccount' => $sipAccount
      ));
    } else {
      return Redirect::to('sip-accounts');
    }
  }
  
  /**
   * Actualiza los parámetros de la cuenta SIP
   */
  public function update()
  {
    $input = Input::only('name', 'password', 'comments', 'id_sip_account');

    $sipAccount = SIPPeers::find($input['id_sip_account']);
    
    if((int)$sipAccount->id_cliente !== $this->user->getID())
      return Redirect::to('sip-accounts');
    
    if($sipAccount->defaultuser === $input['name']
        && $sipAccount->secret === $input['password']
        && $sipAccount->comments === $input['comments']) {
      return Redirect::to('sip-accounts/' . $sipAccount->id_serial);
    }
    
    $rules = array(
      'name'     => 'required|alpha_dash|unique:sippeers,defaultuser,' . $sipAccount->id_serial . ',id_serial',
      'password' => 'required|between:4,40|alpha_dash'
    );
    
    $messages = array(
      'alpha_dash' => 'Los campos nombre y contraseña deben tener únicamente letras, números, guiones y guiones bajos.',
      'between'    => 'La contraseña debe tener como mínimo 8 caracteres.',
      'unique'     => 'Ya existe una cuenta con ese mismo nombre.',
      'required'   => 'El campo :attribute es obligatorio.'
    );
    
    $validator = Validator::make(
      $input,
      $rules,
      $messages
    );
    
    if ($validator->fails()) {    
      return Redirect::to('sip-accounts/' . $input['id_sip_account'])->withErrors($validator);
    } else {
      $oldName = $sipAccount->defaultuser;
      var_dump($oldName);
      var_dump($input['name']);
      $sipAccount->name        = $input['name'];      
      $sipAccount->defaultuser = $input['name'];
      $sipAccount->secret      = $input['password'];
      $sipAccount->comments    = $input['comments'];
      
      // Actualizamos la cola de llamada de todos los números
      // del usuario si se ha cambiado el nombre de la cuenta SIP
      $userNumbers = Number::where('id_cliente', '=', $this->user->getID())
                          ->get();
      
      foreach($userNumbers as &$userNumber) {       
        $userNumber->cuenta_sip = preg_replace('/' . $oldName . '/', $input['name'], $userNumber->cuenta_sip);
        $userNumber->save();
      }
      
      if($sipAccount->save()) {
        $info = "Los datos de la cuenta SIP han sido actualizados.";
        
        return Redirect::to('sip-accounts/' . $sipAccount->id_serial)->with('info', $info);
      } else {
        $error = "Ha ocurrido un error actualizando los datos de la cuenta SIP.";
        
        return Redirect::to('sip-accounts/' . $sipAccount->id_serial)->with('error', $error);        
      }
    }
  }
}
