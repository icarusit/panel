<?php

class SchedulesController extends BaseController
{
  /**
   * Muestra la ventana de los horarios
   * 
   * @return type
   */
  public function showSchedules()
  {
    // Equivalencia índice día semana con texto día semana
    $weekDays = $this->getWeekDays();
    
    // Índice que corresponde al día de hoy
    $today = new \DateTime();
    $todayWeekDay = (int)$today->format('N') - 1;
    
    return View::make('content.schedules.schedules', array(
      'title'            => 'Horarios',
      'weekSchedules'    => $this->number->getSchedules(),
      'weekDays'         => $weekDays,
      'todayWeekDay'     => $todayWeekDay
    ));
  }

  /**
   * Método que devuelve los días de la semana
   *
   * @todo No debería estar aquí sino en algún tipo de helper
   * @return array Días de la semana
   */
  protected function getWeekDays() 
  {
    return array(
      0 => array('monday', 'Lunes', 'lunes'),
      1 => array('tuesday', 'Martes', 'martes'),
      2 => array('wednesday', 'Miércoles', 'miércoles'),
      3 => array('thursday', 'Jueves', 'jueves'),
      4 => array('friday', 'Viernes', 'viernes'),
      5 => array('saturday', 'Sábado', 'sábados'),
      6 => array('sunday', 'Domingo', 'domingos')
    );
  }
 
  /**
   * Guarda los horarios definidos por el usuario
   */
  public function save()
  {
    $idSchedule = Input::get('schedule_id', null);    
    $weekDay    = Input::get('schedule_week_day');
    $from       = Input::get('schedule_from');
    $until      = Input::get('schedule_until');
    
    if($idSchedule === null) {
      $schedule = new Schedule;
      $action = Input::get('action_data');    
    } else {      
      $schedule = Schedule::find($idSchedule);      
      $action = Input::get('action_data_' . $idSchedule);
    }    
        
    $url = 'schedules#' . $this->getWeekDays()[$weekDay][0];            
        
    if(trim($action) === '' || !((new Action(json_decode($action)))->isValid())) {
      $error = 'La acción del horario no puede estar en blanco o la acción '
              . 'configurada no es válida.';
      return Redirect::to($url)->with('error', $error);
    }
    
    $schedule->id_number = $this->number->getID();
    $schedule->active    = TRUE;    
    $schedule->week_day  = $weekDay;
    $schedule->from      = $from;
    $schedule->until     = $until;    
    $schedule->action    = $action;       

    if($schedule->isValid()) {
      if(!$schedule->save()) {
        $error = 'Ha habido algún error modificando los horarios.';
        return Redirect::to($url)->with('error', $error);      
      } else {
        $this->refresh();
        $info = 'Los horarios han sido correctamente modificados.';
        return Redirect::to($url)->with('info', $info);                
      }
    } else {
      $error = 'No se ha agregado el horario ya que el horario introducido 
        solapa con los horarios que ya tienes configurados.';      
      return Redirect::to($url)->with('error', $error);            
    }
  }

  /**
   * Método para eliminar horarios
   *
   * @param integer $idSchedule
   *
   * @return
   */
  public function remove($idSchedule)
  {
    $schedule = Schedule::find($idSchedule);
    $weekDay = $schedule->week_day;
    $deletedOk = $schedule->delete();

    $this->refresh(); 

    $url = 'schedules#' . $this->getWeekDays()[$weekDay][0];    
    
    if($deletedOk) {
      $this->refresh();
      $info = 'El horario ha sido correctamente eliminado.';
      return Redirect::to($url)->with('info', $info);          
    } else {
      $error = 'Ha habido algún problema eliminando el horario.';
      return Redirect::to($url)->with('error', $error);     
    }
  }
  
  /**
   * Método para repetir horarios de Lunes a Viernes
   *
   * @param integer $weekDay Día de la semana
   * 
   * @return
   */
  public function repeat($weekDay)
  {    
    $idNumber = $this->number->getID();

    // 0 => lunes, 6 => domingo    
    
    if($weekDay < 0 || $weekDay > 6) {
      $errorMsg = 'Ha habido un problema repitiendo los horarios.';
      return Redirect::to($url)->with('error', $errorMsg);
    }
   
    $error = false;
    
    // Repitiendo días entre semana
    if($weekDay < 5) {
      $firstDay = 0;
      $lastDay  = 4;
    // Repitiendo findes
    } else {
      $firstDay = 5;
      $lastDay  = 6;
    }

    // Borramos primero los horarios de los otros días
    $otherWeekDaySchedules = Schedule::
        where('week_day', '>=', $firstDay)
      ->where('week_day', '<=', $lastDay)
      ->where('week_day', '!=', $weekDay)
      ->where('id_number', '=', $idNumber)
      ->delete();

    // Recuperamos los horarios del día proporcionado
    $weekDaySchedules = Schedule::
        where('week_day', '=', $weekDay)
      ->where('id_number', '=', $idNumber)
      ->get(); 

    for($i=$firstDay; $i<=$lastDay; $i++) {
      if($i!=$weekDay) {
        foreach($weekDaySchedules as $wdSchedule) {
          $schedule = new Schedule;

          $schedule->id_number = $idNumber;
          $schedule->active    = TRUE;
          $schedule->week_day  = $i;
          $schedule->from      = $wdSchedule->from;
          $schedule->until     = $wdSchedule->until;
          $schedule->action    = $wdSchedule->action;
         
          if(!$schedule->save()) $error = TRUE; 
        }        
      }
    }  

    $url = 'schedules#' . $this->getWeekDays()[$weekDay][0];
    
    if($error) {
      $errorMsg = 'Ha habido un problema repitiendo los horarios.';
      return Redirect::to($url)->with('error', $errorMsg);
    } else {
      $this->refresh();
      $infoMsg = 'Los horarios han sido repetidos satisfactoriamente.';
      return Redirect::to($url)->with('info', $infoMsg); 
    } 
  } 
}

?>
