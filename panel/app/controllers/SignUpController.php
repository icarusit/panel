<?php

class SignUpController extends BaseController
{ 
  /**
   * Cuando ya se ha terminado el proceso de alta
   */
  public function allSet()
  {
	  $results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
    return View::make('alta.all_set',array('distribuidor'=>$results));
  }
  
  /**
   * Muestra la ventana de selección de producto
   */
  	public function showProductSelection()
  	{
		$codigo_afiliacion = Session::get('codigo_afiliacion');
		
		$link_alta = true;
		$user = Session::get('user');
		if($user)
		{
			$idUser = $user->getID();     
			$user_login = User::find($idUser);
			
			
			if($user_login->pendiente == 'No facturar')
			{
				$link_alta = false;
			}
		}
		
    	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get(); 
    	return View::make('alta.product_selection', array(
      		'title' => 'Alta - Elige el servicio que estás buscando',
	  		'distribuidor' => $results,
			'link_alta' => $link_alta
    	));
  	}
	
	public function altaAfiliacion($codigo_afiliacion)
	{
		Session::put('codigo_afiliacion',$codigo_afiliacion);
		return $this->showProductSelection();
	}
 
  /**
   * Muestra la ventana de selección de plan una vez
   * escogido el producto
   * 
   * @param string $productType
   */
  public function showPlans($productType, $number = null)
  {
    $plans = Profile::getByProductType($productType);   
    $pros = array();
    
    // Si tenemos número, comprobamos que sea efectivamente de Flash    
    if($number !== null) {
      try {
        $fNumber = $this->flash->number($number);
        $idUser = $fNumber->getUser()->getID();
        if($idUser !== 137) return Redirect::to('alta');
        $number = Number::where('numero_visible', '=', $number)->first();
      } catch (\FlashTelecom\NotAFlashNumberException $ex) {
        return Redirect::to('alta');
      }
    }
    
    if(!count($plans)) {
      return Redirect::to('alta');
    } else {
      foreach($plans as $plan) {
        $index = $plan->product_type . '_' . $plan->plan;
        $pros[$index] = $plan->getPros();
      }
      $results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
      $viewData = array(
        'title'     => 'Alta - Elige el plan más adecuado a tus necesidades',
        'plans'     => $plans,
        'plansPros' => $pros,
		'distribuidor' => $results
      );

      if($number !== null) {
        $viewData['number'] = $number;
      }
      
      return View::make('alta.plan_selection', $viewData);
    }
  }
  
  /**
   * Muestra el formulario de datos de alta-login
   * de usuario
   */
  	public function showUserForm($plan = null, $number = null, $portability = false)
  	{
    	// ÑAPA ALERT:
    	//
    	// Si alguno de estos parámetros es null, entendemos que
    	// estos valores están en la sesión, si no, mandamos
    	// a alta inicio
    	if($plan === null || $number === null)
		{
    		if(($number = Session::get('alta-number', null)) === null || ($plan = Session::get('alta-plan', null)) === null)
		 	{
        		if($plan !== null)
				{
          			$profile = Profile::getByPostString($plan);
          			$infoIPPlan = $profile->getInfoIPPlan();
          			
					if($portability == true)
					{
						if($infoIPPlan->num_dids > 2)
						{
							return Redirect::to('alta');
						}
					}
          			else
					{
						if($infoIPPlan->num_dids > 0)
						{
							return Redirect::to('alta');
						}
					}
        		}
      		}
    	}
    	// END ÑAPA ALERT
        $results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get(); 
		return View::make('alta.user_forms', array(
		  'title'       => 'Alta - Tus datos de usuario',
			
		  'number'      => $number,
		  'plan'        => $plan,
		  'portability' => $portability,
		  'distribuidor'=> $results
		));
  	}
	
	public function validar_mail($mail)
	{
		$packet = "";
		$url = "https://chema.ga/emailvalidator/index.php?email=".$mail;
		$ctx = stream_context_create(
			array(
			  'http'=>array(
				'header'=>"Content-type: application/x-www-form-urlencoded",
				'method'=>'POST',
				'content'=>$packet
			  )
			)
		  );
		//header('Content-Type: application/json');
  		$resultado = file_get_contents($url, 0, $ctx);
		$resultado = json_decode($resultado);
		if($resultado->valid == false)
		{
			$pos = stripos(strtolower($resultado->message), 'banned domain');
			if($pos === false)
			{
				$resultado->message = 'El email es incorrecto';
			}
			else
			{
				$resultado->message = 'No admitimos emails de ese dominio';
			}
		}
		print json_encode($resultado);
	}
  
  /**
   * 
   */
  	public function proceedToCheckout()
  	{
		$number      = Input::get('alta-number', null);
		$plan        = Input::get('alta-plan', null);   
		$portability = Input::get('portability', false);
		
		
		
		$annualBilling = Input::get('annual-billing', false);
		$annualBilling = $annualBilling === 'true' ? true : false;
		Session::put('annual-billing', $annualBilling);
		
		$profile     = Profile::getByPostString($plan);
		
		if(in_array($profile->getType(), array('voip', 'pbx')))
		{
			$infoIPPlan  = $profile->getInfoIPPlan();      
		}

		if($plan === null)
		{ 
		  	return Redirect::to('alta');
		}
		else
		{
			if($portability)
			{
				
			}
			else
			{
				$numero_cupon        = Input::get('cupon', null);
				$cupon = Cupon::where('name', '=', $numero_cupon)->first();
				if($cupon)
				{
					$numero  = Number::where('numero_visible', '=', $number)->first();
					if($numero)
					{
						$aplicar_descuento = false;
						if($cupon->duration == NULL && $cupon->duration == '')
						{
							//tiempo ilimitado
							if($cupon->due_date <= date('Y-m-d',strtotime('now')))
							{
								$aplicar_descuento = true;
							}
						}
						else
						{
							//tiempo limitado
							//obtener lafecha limite con lasumade los dias a due_date
							$fecha_limite = $this->dateadd($cupon->due_date,$cupon->duration);
							$fecha_actual = date('Y-m-d',strtotime('now'));

							if($cupon->due_date <= $fecha_actual && $fecha_limite >= $fecha_actual)
							{
								$aplicar_descuento = true;
							}
						}
						if($aplicar_descuento == true)
						{
							$numero->descuento = $cupon->value;
							$numero->save();
						}
					}
				}
			}
			
		  	if(($user = Session::get('user', null)) === null)
			{   
				return $this->showUserForm($plan, $number, $portability);
		  	}
		  
		  	if($portability)
			{  
				return $this->confirmPortability($plan, $number);
		  	}
			else
			{
				if(in_array($profile->getType(), array('voip', 'pbx')))
				{ 
			  		if(!$infoIPPlan->num_dids)
					{
						return $this->assignFreeSipAccount($plan);
			  		}
					else
					{
						return $this->assignNumberIP($plan, $number);
			  		}
				}
				else
				{
			  		return $this->assignNumber($plan, $number);          
				}
		  	}
		}
  	}
  
  /**
   * Método que se ejecutará una vez identificado el usuario
   * número a portar
   */
	public function confirmPortability($plan, $number)
  	{
    	if($plan !== null && $number !== null)
		{
		  	$profile = Profile::getByPostString($plan);
		  	$user    = Session::get('user');
		  	$user    = User::find($user->getID());

      		// Notificamos por hipchat
			$msg = ":arrow_right: **PETICIÓN DE PORTABILIDAD** El cliente **" .
				"{$user->usr_usuario} (ID: {$user->id_cliente})** ha solicitado " . 
				"la portabilidad de su número **{$number}** " . 
				"con el plan **{$profile->perfil}**.";

      		$this->hipchat($msg,'rfghqo1saifkxnm1cjxy89ba9h');
      
      		// Notificamos por e-mail tanto al usuario como a Flash
      		$subject = "Has solicitado la portabilidad del número {$number}";
      		$subjectFlash = "El cliente con ID {$user->id_cliente} ha solicitado la portabilidad de su número {$number}";

      		$header = "Has solicitado la portabilidad del número <strong>{$number}</strong>";      
      		$headerFlash = "El cliente con ID {$user->id_cliente} ha solicitado la portabilidad del número {$number}";

      		$subheader = "Has escogido el perfil <strong>{$profile->perfil}</strong>";
      
			$body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
			  "</strong> has solicitado correctamente la portabilidad del número " .
			  "<strong>{$number}</strong>.</p>";
			$body.= "<p>Descarga desde el link: <a href='https://fmeuropa.com/wp-content/uploads/2013/05/portabilidad.pdf'>formulario de portabilidad</a>.
						<strong>Devuélvenoslo rellenado por favor</strong> junto con la última factura de tu operador actual.

						Cuando lo recibamos iniciamos el proceso y os informaremos de fecha/hora de la ventana de portabilidad.

						Un saludo y gracias por vuestra confianza.</p>
						<p>Si el link no te abre copia la ur en el navegador para descargar el formulario: https://fmeuropa.com/wp-content/uploads/2013/05/portabilidad.pdf</p>";
			  $body.= "<p>Para cualquier consulta o aclaración no dudes en ponerte en contacto " .
			  "con nosotros contestando a este mismo correo o llamándonos al teléfono " .
			  "gratuito <strong>".Session::get('telefono')."</strong>.</p><p>Gracias.</p><p>El equipo de ".Session::get('distribuidor').".</p>";
	  
      
      		$bodyFlash = "<p>Fecha de solicitud <strong>" . date('d/m/Y H:i') . "</strong></p>";
      		$bodyFlash.= "<p>Perfil: <strong>{$profile->perfil} ({$profile->id})</strong></p>";


      		$content = array(
				'header'    => $header,
				'subheader' => $subheader,
				'body'      => $body
			  );

      		if(Session::get('id_distribuidor') == 1){
				$this->flash->sendMail($user->mail_1, $subject, $content); 
			}else{
				$mail = new PHPMailer(true);
				$mail->IsSMTP();
				$mail->SMTPSecure = 'tls';
				$mail->Host = "email-smtp.us-east-1.amazonaws.com";
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				$mail->SMTPAuth = true;
				$mail->Port = 587;
				$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
				$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
				$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
				$mail->Subject = $subject;
				$mail->AddReplyTo = "no-reply@".Session::get('dominio');
				$mail->AddAddress($user->mail_1);
				$mail->AltBody = $header;
				$mail->msgHTML($body);
				$mail->send();
			}

      		$contentFlash = array(
				'header'    => $headerFlash,
				'subheader' => '',
				'body'      => $bodyFlash
			  );

      		$this->flash->sendMail('info@fmeuropa.com', $subjectFlash, $contentFlash);        
      
      		// Devolvemos a la vista de bienvenida de portabilidades
      		return View::make('alta.welcome_portability', array(
				'number'  => $number,
				'profile' => $profile
			  ));
    	}
	}
  
  /**
   * Método de confirmación para alta de cuenta SIP gratis
   */
  public function assignFreeSipAccount($plan)
  {
    $maxNumberOfFreeAccounts = 5;
    
    $profile = Profile::getByPostString($plan);
    $idUser  = Session::get('user')->getID();
    $user    = User::find($idUser);    
    
    // Comprobamos que no tenga contratadas ya el máximo
    // número de cuentas gratuitas que permitimos
    $freeSipAccounts = FreeSipAccounts::where('id_user', '=', $idUser)
                                      ->get();
    
    if(count($freeSipAccounts) < $maxNumberOfFreeAccounts) {
      // Creamos la cuenta SIP
      $sipAccount = SIPPeers::add($user, null, 1, 1, true, 'port,invite');
      
      // Y la metemos en la tabla de cuentas gratuitas
      $freeSipAccount = new FreeSipAccounts;
      
      $freeSipAccount->id_user = $idUser;
      $freeSipAccount->id_sippeer = $sipAccount->id_serial;
      $freeSipAccount->signup_date = date('Y-m-d H:i:s');
      $freeSipAccount->save();
      
      // Notificamos por HipChat
      $msg = " :heart: **NUEVA ALTA** El cliente **" .
        "{$user->usr_usuario} (ID: {$user->id_cliente})** ha dado " . 
        "de alta una **cuenta SIP gratuita**";      
      $this->hipchat($msg,'rfghqo1saifkxnm1cjxy89ba9h');
      
      // Generamos la tabla HTML con la información de las cuenta SIP para
      // el correo al usuario
	  if(Session::get('id_distribuidor') == 1){
      $sipAccountTable = '' . 
      '<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
        '<tr>' . 
          '<th>Host</th>' . 
          '<th>Usuario</th>' . 
          '<th>Contraseña</th>' . 
          '<th>Códecs válidos</th>' . 
          '<th>Softphone ya configurado</th>' . 
        '</tr>' .
        '<tr>' . 
          '<td>213.139.7.254</td>' . 
          "<td>{$sipAccount->defaultuser}</td>" . 
          "<td>{$sipAccount->secret}</td>" . 
          '<td/>G729, G711 (A-law), GSM</td>' . 
          '<td>' . 
            "<a href='https://www.zoiper.com/en/page/81cb7377842f6306ec688b4105efe6a9?u={$sipAccount->defaultuser}&h=sip.fmeuropa.com&p={$sipAccount->secret}&o=&t=&x=&a=&tr='" . 
              " title='Descargar Zoiper preconfigurado'>Zoiper</a>" .
          '</td>' .
        '</tr>' . 
      '</table>';
	  }else{
		$sipAccountTable = '' . 
      '<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
        '<tr>' . 
          '<th>Host</th>' . 
          '<th>Usuario</th>' . 
          '<th>Contraseña</th>' . 
          '<th>Códecs válidos</th>' . 
          '<th>Softphone</th>' . 
        '</tr>' .
        '<tr>' . 
          '<td>213.139.7.254</td>' . 
          "<td>{$sipAccount->defaultuser}</td>" . 
          "<td>{$sipAccount->secret}</td>" . 
          '<td/>G729, G711 (A-law), GSM</td>' . 
          '<td>' . 
            "<a href='http://www.zoiper.com/en/voip-softphone/download/zoiper3" . 
              " title='Descargar Zoiper'>Zoiper</a>" .
          '</td>' .
        '</tr>' . 
      '</table>';  
	  }
      // Notificamos por e-mail tanto al usuario como a Flash
      $subject = "Has dado de alta una cuenta SIP gratuita";

      $header  = "Has dado de alta <strong>una cuenta SIP gratuita</strong>";

      $subheader = 'Desde ahora puedes hacer y recibir llamadas con tu cuenta SIP';


			$enlaces_ayuda = Config::get('ayudas.configurarip');
			$tabla_ayuda = '';
			$tabla_ayuda.= '<p>';
			$tabla_ayuda.= 'Ayuda para configurar la cuenta SIP';
			$tabla_ayuda.= '<table>';
			$tabla_ayuda.= '<tbody>';
			foreach($enlaces_ayuda as $enlace)
			{
				$tabla_ayuda.= '<tr>';
					$tabla_ayuda.= '<td>';
						$tabla_ayuda.= '<a href="'.$enlace['url'].'" target="_blank">';
							$tabla_ayuda.= $enlace['title'];
						$tabla_ayuda.= '</a>';
					$tabla_ayuda.= '</td>';
				$tabla_ayuda.= '</tr>';
			}
			$tabla_ayuda.= '</tbody>';
			$tabla_ayuda.= '</table>';
			$tabla_ayuda.= '</p>';

      $body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
      "</strong> has dado de alta correctamente <strong>una cuenta SIP gratuita</strong>" . 
      " sin número asignado.</p>" . 
      "<p>Te dejamos el detalle de configuración de tu nueva <strong>cuenta SIP</strong> con la que podrás enviar y recibir llamadas:</p>" .
      $sipAccountTable . 
      "<p>Recuerda que puedes contratar numeración para tu cuenta SIP y recargar saldo para hacer" .
      " llamadas a números regulares desde la <a href='http://panel.".Session::get('dominio')."'>" . 
      "zona de cliente</a>.</p>" . 
	  $tabla_ayuda.
      "<p>Para cualquier consulta o aclaración no dudes en ponerte en contacto " .
      "con nosotros contestando a este mismo correo o llamándonos al teléfono " .
      "gratuito <strong>".Session::get('telefono')."</strong>.</p><p>Gracias.</p><p>El equipo de ".Session::get('distribuidor').".</p>";    

      $content = array(
        'header'    => $header,
        'subheader' => $subheader,
        'body'      => $body
      );

      if(Session::get('id_distribuidor') == 1){
				$this->flash->sendMail($user->mail_1, $subject, $content); 
			}else{
				$mail = new PHPMailer(true);
				$mail->IsSMTP();
				$mail->SMTPSecure = 'tls';
				$mail->Host = "email-smtp.us-east-1.amazonaws.com";
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				$mail->SMTPAuth = true;
				$mail->Port = 587;
				$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
				$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
				$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
				$mail->Subject = $subject;
				$mail->AddReplyTo = "no-reply@".Session::get('dominio');
				$mail->AddAddress($user->mail_1);
				$mail->AltBody = $header;
				$mail->msgHTML($body);
				$mail->send();
			}

      // Devolvemos a la vista de bienvenida para hacer la
      // configuración básica en caso de analog-fax    
      return View::make('alta.config.config_ip_pbx', array(
        'sipAccounts' => array($sipAccount)
      ));
    } else {
      $error = "Lo sentimos, ya tienes contratado el número máximo de cuentas SIP gratuitas.";
      
      return Redirect::to('alta')->with('error', $error);
    }
  }
  
  /**
   * Método que coordina la asignación del número al usuario
   */
  public function assignNumber($plan, $number)
  {    
    $profile = Profile::getByPostString($plan);
    $user    = Session::get('user');
    $user    = User::find($user->getID());
    $number  = Number::where('numero_visible', '=', $number)->first();
    $fNumber = $this->flash->number($number->numero_visible);
    
    $number->assignTo($user, $profile);           
    
    $number->pago_anual = Session::get('annual-billing');
    $number->save();
    
    // Notificamos por hipchat
    $msg = " :heart: **NUEVA ALTA** El cliente **" .
      "{$user->usr_usuario} (ID: {$user->id_cliente})** ha dado " . 
      "de alta el número **{$number->numero_visible}** " . 
      "con el plan **{$profile->perfil}**.";
      
    $this->hipchat($msg,'rfghqo1saifkxnm1cjxy89ba9h');
    
    // Notificamos por e-mail al usuario
    $subject = "Has dado de alta el número {$number->numero_visible}";
    
    $header  = "Hola! Has dado de alta el número <strong>{$number->numero_visible}</strong>";
    
    $subheader = 'Desde ahora puedes recibir ';
    $subheader.= $fNumber->getType() === 'fax' ? 'faxes' : 'llamadas';
    $subheader.= ' en tu número.';

    $body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
    "</strong> has dado de alta correctamente el número " .
    "<strong>{$number->numero_visible}</strong>.</p>" .
    "<p>Recuerda que puedes configurar tu número desde la <a href='http://panel.".Session::get('dominio')."'>" . 
    "zona de cliente</a> además de poder recargar saldo o ver las llamadas que has recibido en él.</p>" ;
	
	if($fNumber->getType() === 'fax')
	{
		$body.= "<p>
					<b>INSTRUCCIONES DE ENVÍO DE FAX</b>
					<ul>
						<li>Crea un nuevo correo para fax@enviofax.es</li>
						<li>En el Asunto pon el número de teléfono donde quieres enviar el fax</li>
						<li>Adjunta el archivo que quieres enviar por fax (lo ideal es que sea un PDF de resolución igual o inferior a 150 ppp)</li>
						<li>Te llegará un email diciendo si ha ido correcto o ha habido algún error</li>
					</ul>
				</p>";
	}
	
	$body.=    "<p>Para cualquier consulta o aclaración no dudes en ponerte en contacto " .
    "con nosotros contestando a este mismo correo o llamándonos al teléfono " .
    "gratuito <strong>".Session::get('telefono')."</strong>.</p><p>Gracias.</p><p>El equipo de ".Session::get('distribuidor').".</p>";    
    
    $content = array(
      'header'    => $header,
      'subheader' => $subheader,
      'body'      => $body
    );

    if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content); 
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor'));
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		} 
   
    // Devolvemos a la vista de bienvenida para hacer la
    // configuración básica en caso de analog-fax o info
    // de las cuentas SIP y resumen planes IP
	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
    return View::make('alta.config.config_' . $number->getType(), array(
      'number' => $number->numero_visible,
	  'distribuidor' => $results
    ));    
  }  

  /**
   * Método que coordina la asignación del número al usuario
   */
  	public function assignNumberIP($plan, $number)
  	{   
    	// <SUPERMEGAÑAPA>
    	$plan = $plan === 'voip_basico' ? 'IP_1_1_2_0' : $plan;
    	$plan = $plan === 'voip_simple' ? 'IP_1_1_2_0_200' : $plan;
    	$plan = $plan === 'voip_plus'   ? 'IP_1_2_4_0' : $plan;
    	$plan = $plan === 'centralita_basico' ? 'IP_1_2_4_1' : $plan;
    	$plan = $plan === 'centralita_plus' ? 'IP_1_4_8_1' : $plan;
    	// </SUPERMEGAÑAPA>

    	$profile = Profile::getIPPlanByText($plan);
    	$profileAux = Profile::find(200); // Perfil auxiliar para los numeros de centralita    
    	$infoIPPlan = $profile->getInfoIPPlan();
    	$user = Session::get('user');
    	$user = User::find($user->getID());    
    	$numbers = array();
    
    	//
    	// Asignamos los números al cliente
    	//
		$cuenta_sip_centralita = false;
		if(preg_match('/^[0-9,]+$/', $number))
		{
      		$arrStrNumber = explode(',', $number);

      		foreach($arrStrNumber as $i => $strNumber)
			{
        		$auxNumber = Number::where('numero_visible', '=', $strNumber)->first();                    
        		$auxNumber->assignTo($user, !$i ? $profile : $profileAux);
		
				if($profileAux->id == 200 && $i > 0)
				{
					//Numero satelite estableciendo el SIP asociado al numero principal
					$auxNumber->cuenta_sip = 'NSG/'.$arrStrNumber[0];
					$auxNumber->save();
					$cuenta_sip_centralita = true;
				}

        		$numbers[] = $auxNumber;
      		}
    	}
		else
		{
      		return false;
    	}
    
    	//
    	// Generamos sus cuentas SIP
    	//
    	$channels = $infoIPPlan->channels;
    	$sips = $infoIPPlan->sip_accounts;
    
    	// Intentamos repartir equitativamente el número de canales
    	// por líneas IP. Si no es un cociente entero, el resto se lo
    	// sumamos a la primera línea SIP
    	$channels_per_sip = floor($channels / $sips);
    	$channels_first_sip = $channels_per_sip + ($channels % $sips);    
    
    	$priority = 1;
    	for($i=0; $i<$infoIPPlan->sip_accounts; $i++,$priority++)
		{
			
      		SIPPeers::add($user, $numbers[0], $priority, $priority == 1 ? $channels_first_sip : $channels_per_sip);
    	}

    	// Recuperamos las cuentas SIP generadas
    	$sipAccounts = SIPPeers::getAccounts($user->id_cliente, $numbers[0], false);
     
    	// Para todos los números que ha dado de alta, desviamos las llamadas
    	// de forma simultánea a todas las cuentas SIP. Ya luego podrá el usuario
    	// modificar esto desde el panel
    	$strForwardSIP = '';

    	for($i=0; $i<count($sipAccounts); $i++)
		{
      		$strForwardSIP.= $i>0 ? 'SIP/' : '';
      		$strForwardSIP.= $sipAccounts[$i]->defaultuser;
      		$strForwardSIP.= ($i<(count($sipAccounts)-1)) ? '&' : '';
    	}

    	foreach($numbers as $key=>$auxNumber)
		{
			if($key > 0 && $cuenta_sip_centralita == false)
			{
				$auxNumber->cuenta_sip = $strForwardSIP;
			}
			elseif($key == 0)
			{
				if($profile->getType() == 'trunk')
				{
					$auxNumber->cuenta_sip = $strForwardSIP.'/'.$numbers[0]->numero_visible;
				}
				else
				{
					$auxNumber->cuenta_sip = $strForwardSIP;
				}
			}
      		
			$aux_annual_billing = Session::get('annual-billing');
      		$auxNumber->pago_anual = $aux_annual_billing?$aux_annual_billing:false;
      		$auxNumber->save();
    	} 

    	// Generamos la tabla HTML con la información de las cuentas SIP para
    	// el correo al usuario
		if(Session::get('id_distribuidor') == 1){
    	$sipAccountsTable = '' . 
    		'<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
      			'<tr>' . 
        			'<th>Host</th>' . 
        			'<th>Usuario</th>' . 
        			'<th>Contraseña</th>' . 
        			'<th>Códecs válidos</th>' . 
        			'<th>Softphone ya configurado</th>' . 
      			'</tr>';
		}else{
			$sipAccountsTable = '' . 
    		'<table border="0" cellpadding="5" cellspacing="5" width="100%">' . 
      			'<tr>' . 
        			'<th>Host</th>' . 
        			'<th>Usuario</th>' . 
        			'<th>Contraseña</th>' . 
        			'<th>Códecs válidos</th>' . 
        			'<th>Softphone</th>' . 
      			'</tr>';
		}
    		foreach($sipAccounts as $sipAccount)
			{
				if(Session::get('id_distribuidor') == 1){
       			$sipAccountsTable.= '' . 
        		'<tr>' . 
          			'<td>213.139.7.254</td>' . 
          			"<td>{$sipAccount->defaultuser}</td>" . 
          			"<td>{$sipAccount->secret}</td>" . 
          			'<td>G729, G711 (A-law), GSM</td>' . 
          			'<td>' . 
            			"<a href='https://www.zoiper.com/en/page/81cb7377842f6306ec688b4105efe6a9?u={$sipAccount->defaultuser}&h=sip.fmeuropa.com&p={$sipAccount->secret}&o=&t=&x=&a=&tr='" . 
             " title='Descargar Zoiper preconfigurado'>Zoiper</a>" .
          			'</td>' .
        		'</tr>'; 
				}else{
					$sipAccountsTable.= '' . 
        		'<tr>' . 
          			'<td>213.139.7.254</td>' . 
          			"<td>{$sipAccount->defaultuser}</td>" . 
          			"<td>{$sipAccount->secret}</td>" . 
          			'<td>G729, G711 (A-law), GSM</td>' . 
          			'<td>' . 
            			"<a href='http://www.zoiper.com/en/voip-softphone/download/zoiper3'" . 
             " title='Descargar Zoiper'>Zoiper</a>" .
          			'</td>' .
        		'</tr>'; 
				}
    		}

    		$sipAccountsTable.= '</table>';

    		// Notificamos por hipchat
    		$msg = " :heart: **NUEVA ALTA** El cliente **" .
      				"{$user->usr_usuario} (ID: {$user->id_cliente})** ha dado " . 
      				"de alta " . (count($numbers)>1 ? 'los' : 'el') . " número **" . 
     				"{$number}** con el plan **{$profile->perfil}**.";      
    		$this->hipchat($msg,'rfghqo1saifkxnm1cjxy89ba9h');
    
    		// Notificamos por e-mail al usuario
    		$subject = "Has dado de alta " . (count($numbers)>1 ? 'los números' : 'el número') . ' ' . $number;
    
    		$header  = "Has dado de alta " . (count($numbers)>1 ? 'los números' : 'el número') . ' ' . $number;
    
    		$subheader = 'Desde ahora puedes recibir llamadas';
    		$subheader.= ' en ' . (count($numbers)>1 ? 'tus números' : 'tu número');

			$enlaces_ayuda = Config::get('ayudas.configurarip');
			$tabla_ayuda = '';
			$tabla_ayuda.= '<p>';
			$tabla_ayuda.= 'Ayuda para configurar la cuenta SIP';
			$tabla_ayuda.= '<table>';
			$tabla_ayuda.= '<tbody>';
			foreach($enlaces_ayuda as $enlace)
			{
				$tabla_ayuda.= '<tr>';
					$tabla_ayuda.= '<td>';
						$tabla_ayuda.= '<a href="'.$enlace['url'].'" target="_blank">';
							$tabla_ayuda.= $enlace['title'];
						$tabla_ayuda.= '</a>';
					$tabla_ayuda.= '</td>';
				$tabla_ayuda.= '</tr>';
			}
			$tabla_ayuda.= '</tbody>';
			$tabla_ayuda.= '</table>';
			$tabla_ayuda.= '</p>';


   			$body   = "<p>Te confirmamos que con fecha <strong>" . date('d/m/Y H:i') . 
    "</strong> has dado de alta correctamente " . (count($numbers)>1 ? 'los números' : 'el número') . 
    " <strong>{$number}</strong>.</p>" .
    "<p>Te dejamos el detalle de tus cuentas SIP con las que podrás enviar y recibir llamadas:</p>" .
    $sipAccountsTable . 
    "<p>Recuerda que puedes configurar todos tus números desde la <a href='http://panel.".Session::get('dominio').".com'>" . 
    "zona de cliente</a> además de poder recargar saldo o ver las llamadas que has recibido en ellos.</p>" . 
	$tabla_ayuda.
	"Tenemos disponibles en nuestro blog manuales de configuración para multitud de teléfonos y softphones. Entra <a href='https://fmeuropa.com/category/configuracion-2/' target='_blank'>AQUÍ</a> para acceder a los manuales de configuración.".
    "<p>Para cualquier consulta o aclaración no dudes en ponerte en contacto " .
    "con nosotros contestando a este mismo correo o llamándonos al teléfono " .
    "gratuito <strong>".Session::get('telefono')."</strong>.</p><p>Gracias.</p><p>El equipo de ".Session::get('distribuidor').".</p>";    
    
    	$content = array(
      		'header'    => $header,
      		'subheader' => $subheader,
      		'body'      => $body
    	);

    	if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content); 
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor'));
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}   
    
    	// Devolvemos a la vista de bienvenida para hacer la
    	// configuración básica en caso de analog-fax    
    	return View::make('alta.config.config_ip_pbx', array(
      		'sipAccounts' => $sipAccounts,
     	 	'numbers'     => $numbers
    	));
  	}
  
  /**
   * Muestra la ventana de selección de número
   * una vez escogido servicio y plan
   * 
   * @param string $productType
   * @param string $plan
   */
  public function showNumbers($productType, $plan)
  {
    $prefix = null;
    $voip   = false;
    
    switch($productType) {
      case '902':
      case 'fax-902':
      case '902-movil':        
        $prefix = '902';
        break;
      case '900':
        $prefix = array('800', '900');
        break;
      case 'voip':
      case 'centralita':
        $voip = true;
        break;      
    }

    $plan = $productType . '_' . $plan;
	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();     
    $viewData = array(
      'title' => 'Alta - Elige el número que más te guste',
      'plan'  => $plan,
	  'distribuidor'=> $results
    );
    
    if($prefix !== null) {
      $numbers = Number::getAvailable($prefix);      
      $viewData['numbers'] = $numbers;
    } elseif($prefix === null && $voip) {
      $profile = Profile::getByPostString($plan);
      $infoIPPlan = $profile->getInfoIPPlan();
      
      if(!$infoIPPlan->num_dids) {
        return $this->showSummary($plan);
      } else {
        $viewData['customIP'] = false;
        $viewData['title']    = 'Elige el número que más te guste';

        // Numeración 902   
        $numbers = Number::getAvailable('902');      
        $viewData['numbers']['902'] = $numbers;    

        // Numeración 800-900
        $numbers = Number::getAvailable(array('800', '900'));      
        $viewData['numbers']['800-900'] = $numbers;                

        // Geos
        $map = App::make('SignUpMapController')->show($plan);      
        $viewData['map'] = $map;      

        return View::make('alta.custom_ip_number_selection', $viewData);      
      }
    } else {
      $map = App::make('SignUpMapController')->show($plan);      
      $viewData['map'] = $map;
    }   

    return View::make('alta.number_selection', $viewData);
  }
  
  /**
   * Método que se ejecuta una vez el usuario ha seleccionado
   * producto, plan y número
   */
 	public function postPlanNumber()
  	{
		$number      = Input::get('alta-number', null);
    	$plan        = Input::get('alta-plan', null);   
    	$portability = (bool)Input::get('portability', false);
        
    	if($number === null || $plan === null)
		{
      		return Redirect::to('alta');
    	}
		else
		{
      		$number_db = Number::where('numero_visible', '=', $number)->first();
	  		if($number_db == NULL)
	  		{
      			return $this->showSummary($plan, $number, $portability,false);
	  		}
	  		else
	  		{	  
	  			return $this->showSummary($plan, $number_db, $portability,true);
	  		}
    	}
  	}
  
  /**
   * Método hermano de postPlanNumber pero para peticiones
   * GET, que vendrán desde el WordPress para dar el alta
   * con número seleccionado
   */
  public function getPlanNumber($productType, $plan, $number)
  {  
    $portability = false;

    // Comprobamos que el número sea efectivamente de Flash    
    try {
      $fNumber = $this->flash->number($number);
      $idUser = $fNumber->getUser()->getID();
      $number = Number::find($fNumber->getID());
    } catch (\FlashTelecom\NotAFlashNumberException $ex) {
      return Redirect::to('alta');
    }
    
    return $idUser === 137
            ? $this->showSummary($productType.'_'.$plan, $number, $portability)
            : Redirect::to('alta');
  }
  
  /**
   * Método que se ejecuta una vez el usuario ha seleccionado
   * número de canales, cuentas SIP y opción de centralita en
   * plan a medida de IP
   */
	public function postPlanCustomIP()
  	{
    	$productType = Input::get('product-type', null);
    	$plan        = Input::get('profile', null);
    
    	if($productType !== null && $plan !== null)
		{
      		return $this->showNumbersIP($productType, $plan);
    	}
		else
		{
      		return Redirect::to('alta');
    	}
  	}
	
	public function postPlanCustomSipTrunks()
  	{
    	$productType = Input::get('product-type', null);
    	$plan        = Input::get('profile', null);
    
    	if($productType !== null && $plan !== null)
		{
      		return $this->showNumbersIP($productType, $plan);
    	}
		else
		{
      		return Redirect::to('alta');
    	}
  	}
  
  /**
   * Método que se ejecuta una vez el usuario ha configurado
   * plan y elegido números en la configuración IP a medida
   */
  	public function postPlanNumberIP()
  	{
    	$strDIDs = Input::get('chosen-number', null);
    	$plan    = Input::get('plan', null);
    
    	if($strDIDs !== null || $plan !== null)
		{
      		if(($user = Session::get('user', null)) === null)
			{      
        		return $this->showUserForm($plan, $strDIDs);
      		}
      
      		return $this->assignNumberIP($plan, $strDIDs);      
    	}
		else
		{
      		return Redirect::to('alta');
    	}
  	}
  
  /**
   * Método que muestra la ventana de configuración IP a medida
   */
  public function showCustomIP()
  {
    $pbx = (bool)Input::get('centralita', false);
	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
    return View::make('alta.custom_ip_setup', array(
      'pbx' => $pbx,
	  'distribuidor' => $results
    ));
  }
  
    public function showCustomSipTrunks()
  	{
    	$pbx = (bool)Input::get('centralita', false);
		$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
    	return View::make('alta.custom_sip_trunks_setup', array(
      		'pbx' => $pbx,
	  		'distribuidor' => $results
    	));
  	}
  
  /**
   * Método que muestra la ventana de selección de numeración
   * para planes IP
   * 
   * @param string $productType
   * @param string $plan
   */
  	public function showNumbersIP($productType, $plan)
  	{ 
    	$viewData['plan']    = $plan;    
    	$viewData['numbers'] = array();    
    	$viewData['title']   = 'Alta - Elige uno o varios números que te gusten';

    	// Recuperamos precio base del plan, sin número (-3 €)
    	$profile = Profile::getIPPlanByText($plan);
    	$viewData['basePrice'] = $profile->cuota - 3;
    
    	// Numeración 902   
    	$numbers = Number::getAvailable('902');      
    	$viewData['numbers']['902'] = $numbers;    
    
    	// Numeración 800-900
    	$numbers = Number::getAvailable(array('800', '900'));      
    	$viewData['numbers']['800-900'] = $numbers;        
    
    	// Mapa para numeración geográfica
    	$map = App::make('SignUpMapController')->show();      
    	$viewData['map'] = $map;    
    
    	$viewData['customIP'] = true;
    	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
		$viewData['distribuidor'] = $results;
    	return View::make('alta.custom_ip_number_selection', $viewData);
  	}
  
  /*
  Obtener cupon
  */
 	public function validateCupon($name_cupon)
  	{
		$cupon = Cupon::where('name', '=', $name_cupon)->first();
		
		if($cupon)
		{
			$aplicar_descuento = false;
			if($cupon->duration == NULL && $cupon->duration == '')
			{
				//tiempo ilimitado
				if($cupon->due_date <= date('Y-m-d',strtotime('now')))
				{
					$aplicar_descuento = true;
				}
			}
			else
			{
				//tiempo limitado
				//obtener lafecha limite con lasumade los dias a due_date
				$fecha_limite = $this->dateadd($cupon->due_date,$cupon->duration);
				$fecha_actual = date('Y-m-d',strtotime('now'));

				if($cupon->due_date <= $fecha_actual && $fecha_limite >= $fecha_actual)
				{
					$aplicar_descuento = true;
				}
			}
			if($aplicar_descuento == true)
			{
				print json_encode(array('result' => 't','data' => array('valor' => $cupon->value,'due_date' => $cupon->due_date,'duration' => $cupon->duration)));
			}
			else
			{
				print json_encode(array('result' => 'f','data' => array('msg' => 'Vencido')));
			}
		}
		else
		{
			print json_encode(array('result' => 'f','data' => array('msg' => 'no válido')));
		}
	}
  
  /**
   * Método para mostrar la vista de resumen del servicio a contratar
   * 
   * @param string $plan Cadena con el nombre compuesto del plan que usamos
   *                     en los posts
   * @param string|array $numbers Número o números a contratar
   * @param boolean $portability Es o no portabilidad
   */
 	public function showSummary($plan, $number = null, $portability = false,$numero_bd = true)
  	{
    	$profile = Profile::getByPostString($plan);
		$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get(); 
    	return View::make('alta.summary', array(
		  'title' => 'Alta - Resumen del servicio a contratar',        
			
		  'plan'        => $plan,
		  'public_plan' => $profile->perfil,
		  'cuota'       => $profile->cuota,
		  'cuota_anual' => $profile->cuota_anual,
		  'number'      => $number,
		  'portability' => $portability,
		  'numero_bd'   => $numero_bd,
		  'distribuidor' => $results
    	));
  	}
	private function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0)
	{  
		$date_r = getdate(strtotime($date));  
		$date_result = date("Y-m-d", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
		return $date_result; 
	}
}
