<?php

/**
 * Controlador para el mapa de provincias del alta
 */
class SignUpMapController extends BaseController
{
  /**
   * Muestra vista con el mapa
   * 
   * @return type
   */
  public function show($plan = null)
  {    
    $provincias = array(
      'Albacete' => 'albacete',
      'Alicante' => 'alicante',
      'Almería' => 'almeria',
      'Araba' => 'araba',
      'Asturias' => 'asturias',
      'Avila' => 'avila',
      'Badajoz' => 'badajoz',
      'Baleares' => 'baleares',
      'Barcelona' => 'barcelona',
      'Bizkaia' => 'bizkaia',
      'Burgos' => 'burgos',
      'Cáceres' => 'caceres',
      'Cadiz' => 'cadiz',
      'Cantabria' => 'cantabria',
      'Castellón' => 'castellon',
      'Ceuta' => 'ceuta',
      'Ciudad Real' => 'ciudad-real',
      'Córdoba' => 'cordoba',
      'La Coruña' => 'coruna',
      'Cuenca' => 'cuenca',
      'Gerona' => 'gerona',
      'Gipuzkoa' => 'gipuzkoa',
      'Granada' => 'granada',
      'Guadalajara' => 'guadalajara',
      'Huelva' => 'huelva',
      'Huesca' => 'huesca',
      'Jaén' => 'jaen',
      'La Rioja' => 'la-rioja',
      'Las Palmas' => 'las-palmas',
      'León' => 'leon',
      'Lérida' => 'lerida',
      'Lugo' => 'lugo',
      'Madrid' => 'madrid',
      'Málaga' => 'malaga',
      'Melilla' => 'melilla',
      'Murcia' => 'murcia',
      'Navarra' => 'navarra',
      'Orense' => 'orense',
      'Palencia' => 'palencia',
      'Pontevedra' => 'pontevedra',
      'Salamanca' => 'salamanca',
      'Segovia' => 'segovia',
      'Sevilla' => 'sevilla',
      'Soria' => 'soria',
      'Tarragona' => 'tarragona',
      'Tenerife' => 'tenerife',
      'Teruel' => 'teruel',
      'Toledo' => 'toledo',
      'Valencia' => 'valencia',
      'Valladolid' => 'valladolid',
      'Zamora' => 'zamora',
      'Zaragoza' => 'zaragoza'
    );    
    
    return View::make('alta.map', array(
      'provincias' => $provincias,
      'plan'       => $plan
    ));    
  }
  
  /**
   * Recupera los números libres de una provincia dada
   * 
   * @param string $provincia
   */
  public function getNumbers($provincia)
  {
    $provincias = array(
      'albacete' => array('_67'),
      'alicante' => array('_65', '_66'),
      'almeria' => array('_50'),
      'araba' => array('_45'),
      'asturias' => array('_84'),
      'avila' => array('_20'),
      'badajoz' => array('_24'),
      'baleares' => array('_71'),
      'barcelona' => array('_3'),
      'bizkaia' => array('_46', '_44'),
      'burgos' => array('_47'),
      'caceres' => array('_27'),
      'cadiz' => array('_56'),
      'cantabria' => array('_42'),
      'castellon' => array('_64'),
      'ceuta' => array('_56'),
      'ciudad-real' => array('_26'),
      'cordoba' => array('_57'),
      'coruna' => array('_81'),
      'cuenca' => array('_69'),
      'gerona' => array('_72'),
      'gipuzkoa' => array('_43'),
      'granada' => array('_58'),
      'guadalajara' => array('_49'),
      'huelva' => array('_59'),
      'huesca' => array('_74'),
      'jaen' => array('_53'),
      'la-rioja' => array('_41'),
      'las-palmas' => array('_28'),
      'leon' => array('_87'),
      'lerida' => array('_73'),
      'lugo' => array('_82'),
      'madrid' => array('_1'),
      'malaga' => array('_51', '_52'),
      'melilla' => array('_51', '_52'),
      'murcia' => array('_68'),
      'navarra' => array('_48'),
      'orense' => array('_88'),
      'palencia' => array('_79'),
      'pontevedra' => array('_86'),
      'salamanca' => array('_23'),
      'segovia' => array('_21'),
      'sevilla' => array('_54', '_55'),
      'soria' => array('_75'),
      'tarragona' => array('_77'),
      'tenerife' => array('_22'),
      'teruel' => array('_78'),
      'toledo' => array('_25'),
      'valencia' => array('_61', '_63'),
      'valladolid' => array('_83'),
      'zamora' => array('_80'),
      'zaragoza' => array('_76')
    );    
    
    $available = Number::getAvailable($provincias[$provincia], 12);    
    $onlyNumbers = array();
    
    foreach($available as $number) {
      $onlyNumbers[] = array(
        'pretty_number' => $number->pp_numero,
        'number' => $number->numero_visible
      );
    }
    
    return json_encode($onlyNumbers);
  }
}
