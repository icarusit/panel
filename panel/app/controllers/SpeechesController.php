<?php

class SpeechesController extends BaseController
{
  /**
   * Muestra la lista de locuciones del usuario
   * 
   * @return type
   */
  public function showSpeeches()
  {
    $dataView = array();
    
    //$type = Input::get('type', 'all');

    //if(in_array($type, array('speech', 'voicemail', 'menu'))) {
      //$dataView['speeches'] = $this->getUserSpeeches($type);
    //} else {
      $dataView['speeches'] = $this->getUserSpeeches();
    //}

    //$dataView['general_speeches'] = $this->getUserSpeeches('speech');
	$dataView['general_speeches'] = $this->getUserSpeeches();
    
    // De momento para ir más rápido, las locuciones de entrada
    // y espera las pillamos directamente desde la DDBB y no vía
    // la API ya que aún no están implementadas
    $results = DB::select('SELECT loc_entrada,loc_espera,loc_operador 
      FROM numero WHERE numero_visible = ? LIMIT 1', 
            array($this->number->getNumber()));
    
    if(is_array($results) && !empty($results)) {
      $dataView['welcomeSpeech']  = $results[0]->loc_entrada;
      $dataView['queueSpeech']    = $results[0]->loc_espera;      
      $dataView['operatorSpeech'] = $results[0]->loc_operador;
    }
    
    // Si es móvil, de momento, ocultamos la
    // opción de subir locuciones
    $dataView['isMobile'] = $this->mobileDetect->isMobile();    
    
    $dataView['typesText'] = array(
      'speech'    => 'Locución general',
      'voicemail' => 'Locución para buzón de voz',
      'menu'      => 'Locución para menú'
    );
       
    //$dataView['speechesType'] = $type;
    //$dataView['title'] = 'Locuciones';
    
    // Datos para las opciones de lo que hacer tras la locución
    // de espera
    $hasVoicemail = $this->number->hasVoicemail();
    $dataView['hasVoicemail'] = $hasVoicemail;    
    
    $numberSettings = NumberSettings::find($this->number->getID());
    
    $dataView['afterQueueVoicemailActive'] = $numberSettings->minutes_after_queue_speech_do_voicemail > 0;
    $dataView['minutesAfterQueueVoicemail'] = $numberSettings->minutes_after_queue_speech_do_voicemail;
    $dataView['queueSpeechMode'] = $numberSettings->queue_speech_mode;
    
    // Construimos la vista
    return View::make('content.speeches.speeches', $dataView);
  }
  
  /**
   * Método que devuelve las locuciones del usuario
   * 
   * @param  string $type Tipo de locuciones a devolver
   * @return array
   */
  public function getUserSpeeches($type = null)
  {
    $user = $this->number->getUser();
    
    return $user->speeches()->get();
  }
  
  /**
   * Método que devuelve el contenido para la ventana modal
   * de las locuciones
   */
  public function returnModal()
  {
    // Vemos si ya había una locución seleccionada para 
    // el servicio que estemos configurando
    $selectedSpeech = FALSE;
    
    if(!is_null($jsonAction = Input::get('jsonAction', NULL))) {
      $action = json_decode($jsonAction);

      if (!is_null($action)) {
        //if($action->type === 'speech') {
          $selectedSpeech = $action->data->speech->name;
        //}
      }
    }
    
    $viewData = array();                

    //$viewData['speeches'] = $this->getUserSpeeches('speech');    
	$viewData['speeches'] = $this->getUserSpeeches();
    $viewData['selectedSpeech'] = $selectedSpeech;
    $viewData['hideSaveButton'] = TRUE;
    
    return View::make('components.actions.modals.speech', $viewData);
  }

  /**
   * Sube una locución grabada desde el panel al sistema
   */
  public function uploadRecording()
  {
    // Validación cutre, refactor
    //if(($speechName = Input::get('speech_name', '')) !== '' &&
      //  ($speechType = Input::get('speech_type', '')) !== '') {
	  if(($speechName = Input::get('speech_name', '')) !== ''){
      $idUser = Session::get('user')->getID();
      $fileName = urldecode(Input::get('file_name'));
      $fileContent = Input::get('file_content');
      
      // Obtenemos y decodificamos de base64 el contenido del audio
      $fileContentEncoded = substr($fileContent, strpos($fileContent, ",") + 1);
      $fileContentDecoded = base64_decode($fileContentEncoded);
      
      // Esto es mu cutre pero no hay forma rápida de subirlo vía la API
      $path = $this->dataPath . $idUser . '/speeches';

      // Comprobamos que exista el directorio, si no, lo creamos
      if(!is_dir($path)) {
        mkdir($path, 0777, true);
      }  

      // Guardamos el fichero
      $fh = fopen($path . '/' . $fileName, 'wb');
      if(!fwrite($fh, $fileContentDecoded)) { echo "false"; return; }
      fclose($fh);
      
      // Y registramos la locución en la tabla
      $speech = new Speech();
      
      $speech->id_user     = $idUser;
      $speech->date_add    = (new \DateTime())->format('Y-m-d H:i:s'); 
      $speech->public_name = $speechName;
      $speech->filename    = $fileName;
      $speech->type        = '';
      $speech->size        = filesize($path . '/' . $fileName);
      
      $speech->original_filename = $fileName;
      
      // Sacamos el mimetype
      $finfo = finfo_open(FILEINFO_MIME_TYPE);                                                   
      $mimetype = finfo_file($finfo, $path . '/' . $fileName);      
      $speech->original_mimetype = $mimetype;
      
      echo $speech->save() ? "true" : "false";
      return;      
    } else {
      echo "false";
    }
  }
  
  /**
   * Sube una locución al sistema
   * 
   * @return type
   */
  public function upload()
  {
    // Validamos primero los datos introducidos por el usuario
    /*
	$rules = array(
      'speech_name' => 'required',
      'speech_type' => 'required'
//      'speech'      => 'required'
    );
    */
	$rules = array(
      'speech_name' => 'required',
//      'speech'      => 'required'
    );
    $messages = array(
      'required' => 'El campo :attribute es obligatorio.',
    );
    
    $validator = Validator::make(
      Input::only('speech_name', 'speech'),
      $rules,
      $messages
    );
    
    if ($validator->fails()) {
      return Redirect::to('speeches')->withErrors($validator);
    } else {    
      $user = $this->number->getUser();      
      $speeches = $user->speeches();

      if(Input::hasFile('speech')) {
        if($speeches->upload()) {
          $info = 'La locución <strong>' . Input::get('speech_name') . '</strong> 
            ha sido subida satisfactoriamente y nuestro sistema la está procesando.
            Espera unos minutos, actualiza esta página y comprueba que la locución
            esté ya procesada y podrás utilizarla en tus líneas.';

          return Redirect::to('speeches')->with('info', $info);
        } else {
          $error = 'Ha habido un problema al subir la locución <strong>' .
            Input::get('speech_name') . '</strong>. Por favor, vuelve a intentarlo
            tras unos minutos y si sigues teniendo problemas, ponte en contacto 
            con nosotros.';

          return Redirect::to('speeches')->with('error', $error);       
        }
      }
    }
  }
  
  /**
   * Actualiza la selección de locuciones de bienvenida y espera
   */
	public function update()
  	{
    	$waitingSpeech = Input::get('select-welcome-speech');
    	$queueSpeech   = Input::get('select-queue-speech');
    	$operatorSpeech   = Input::get('select-operator-speech');
   
    	if($waitingSpeech == "disabled")  { $waitingSpeech = null; }
    	if($queueSpeech == "disabled")    { $queueSpeech = null; }
    	if($operatorSpeech == "disabled") { $operatorSpeech = null; }
		
		//$numberSettings = NumberSettings::find($this->number->getID());
   
    	if($queueSpeech !== null)
		{
      		$checkQueueSpeechMode = (bool)Input::get('check_queue_speech_mode', false);      
      		$checkAfterQueue = (bool)Input::get('check_minutes_after_queue', false);
      
      		$numberSettings = NumberSettings::find($this->number->getID());

      		if($checkAfterQueue === true)
	  		{
        		$minutesAfterQueue = (int)Input::get('minutes_after_queue', 0);

        		if($minutesAfterQueue > 0)
				{
          			$numberSettings->minutes_after_queue_speech_do_voicemail = $minutesAfterQueue;
        		}
      		}
	  		else
	  		{
        		$numberSettings->minutes_after_queue_speech_do_voicemail = 0;
      		}
			$numberSettings->queue_speech_mode = $checkQueueSpeechMode;
    	    $numberSettings->save(); 
    	}
		
    	//$checkQueueSpeechMode="";
    	//$numberSettings->queue_speech_mode = $checkQueueSpeechMode;
    	//$numberSettings->save(); 
    
    	$sql = "UPDATE numero SET loc_entrada = ?, loc_espera = ?, 
            loc_operador = ? WHERE numero_visible = ?";
            
    	$affectedRows = DB::update($sql, array(
      		$waitingSpeech, 
      		$queueSpeech,
      		$operatorSpeech,        
      		$this->number->getNumber()
    	));
    
		$info = 'La configuración de locuciones del número ha sido actualizada.';
            
    	return Redirect::to('speeches')->with('info', $info);
	}
  
  /**
   * Descarga una locución
   * 
   * @param integer $idSpeech
   * 
   * @todo  De momento estamos metiendo a pelo la ruta donde estarán las locuciones.
   *        Esto debería hacerse con la API cuando estemos mejor de tiempo.
   */
  public function download($idSpeech)
  {
    $speech = Speech::find($idSpeech);
    
    $idUser = $this->number->getUser()->getID();    
    $idUser = $speech->id_user == 0 ? 0 : $idUser;
    $path = $this->dataPath . $idUser . '/speeches/';    
    
    if($speech->id_user == $idUser || $speech->id_user == 0) {
      $fh = fopen($path . $speech->filename, 'rb');

      header('Content-Type: audio/x-wav, audio/wav');
      header('Content-Description: File Transfer');
      header('Content-length: ' . filesize($path . $speech->filename));
      header('Content-Disposition: attachment;filename=' . $speech->original_filename);

      fpassthru($fh);
      exit;
    }
  }
  
  /**
   * Elimina locuciones del sistema
   * 
   * @param integer $idSpeech
   */
  public function remove($idSpeech)
  {
    $idUser = Session::get('user')->getID();
    
    $path = $this->dataPath . $idUser . '/speeches/';
    
    $speech = Speech::find($idSpeech);
    
    if($speech->id_user == $idUser) {
      $speechName = $speech->public_name;
      $speechFilename = $speech->filename;
      
      // Borramos la fila de la locución
      $speech->delete();
      
      // Y los ficheros de los distintos formatos de locución
      array_map('unlink', glob($path . $speechFilename . '*'));      
      
      $info = "La locución <strong>$speechName</strong> ha sido correctamente eliminada del sistema";
      return Redirect::to('speeches')->with('info', $info);
    }
  }

  /**
   * Método que genera una locución a partir de un texto
   *
   * @param  string $text
   * @param  string $gender
   *
   * @return string|boolean Ruta completa del fichero o false
   *                        si no se ha generado el fichero
   */
  public function text2speechGenerate($text = null, $gender = 'male')
  {
    if($text !== null) {
      $x = $gender === 'male' ? 'pa' : 'sf';
      $text = preg_replace('/[^\p{L}0-9 \,\.]/', '', utf8_decode($text));
      $file = '/tmp/text2speech_' . rand(10000, 99999) . '.wav';
      exec('echo "' . $text . '" | text2wave -eval "(voice_JuntaDeAndalucia_es_' . $x . '_diphone)" -o ' . $file);
      return file_exists($file) ? $file : false;
    } else {
      return false;
    }
  }

  /**
   * Método para reproducir una locución generada con Festival
   */
  public function text2speechPlay()
  {
    $text   = Input::get('text', null);
    $gender = Input::get('gender', 'male');

    if(Request::ajax() && $text !== null) {
      $file = $this->text2speechGenerate($text, $gender);
      if($file !== false) {
        $return = base64_encode(file_get_contents($file));
        unlink($file);
      } else {
        $return = "false";
      }

      return $return;
    }
  }

  /**
   * Método que genera una locución desde texto y la almacena como
   * locución del usuario
   */
  public function text2speechUpload()
  {
    // Validamos primero los datos introducidos por el usuario
    $rules = array(
      'speech_name'      => 'required',
      /*'speech_type'      => 'required',*/
      'speech_gender'    => 'required',
      'text2speech-text' => 'required'
    );
    
    $messages = array(
      'required' => 'El campo :attribute es obligatorio.',
    );
    
    $validator = Validator::make(
      Input::only('speech_name', /*'speech_type',*/ 'speech_gender', 'text2speech-text'),
      $rules,
      $messages
    );
    
    if ($validator->fails()) {
      return Redirect::to('speeches')->withErrors($validator);
    } else {    
      $idUser = Session::get('user')->getID();

      $speechName   = Input::get('speech_name');
      //$speechType   = Input::get('speech_type');
      $speechGender = Input::get('speech_gender');
      $speechText   = Input::get('text2speech-text');

      // Esto es mu cutre pero no hay forma rápida de subirlo vía la API
      $path = $this->dataPath . $idUser . '/speeches';      

      // Comprobamos que exista el directorio, si no, lo creamos
      if(!is_dir($path)) {
        mkdir($path, 0777, true);
      }
 
      $fileName = 'text2speech-' . Session::get('user')->getUsername() . '-' . date('U') . '.wav'; 
      
      $tempFile = $this->text2speechGenerate($speechText, $speechGender);  
      
      if($tempFile === false) {
        $return = false;
      } else {
        // Guardamos el fichero en su ruta definitiva
        if(!rename($tempFile, $path . '/' . $fileName)) {
          $return = false;
        } else {
          // Y registramos la locución en la tabla
          $speech = new Speech();
      
          $speech->id_user     = $idUser;
          $speech->date_add    = (new \DateTime())->format('Y-m-d H:i:s'); 
          $speech->public_name = $speechName;
          $speech->filename    = $fileName;
          $speech->type        = '';
          $speech->size        = filesize($path . '/' . $fileName);
      
          $speech->original_filename = $fileName;
      
          // Sacamos el mimetype
          $finfo = finfo_open(FILEINFO_MIME_TYPE);                                                   
          $mimetype = finfo_file($finfo, $path . '/' . $fileName);      
          $speech->original_mimetype = $mimetype;
      
          $return = $speech->save() ? true : false;
        }
      }   
     
      if($return) {
        $info = 'La locución <strong>' . $speechName . '</strong> ha sido correctamente generada. ' .
        'Espera unos minutos, actualiza esta página y comprueba que la locución ' .
        'esté ya procesada y podrás utilizarla en tus líneas.';

        return Redirect::to('speeches')->with('info', $info); 
      } else {
        $error = 'Ha habido algún problema generando tu locución desde texto. Ponte en contacto con nosotros.';
        return Redirect::to('speeches')->with('error', $error);
      }
    } 
  }
}

?>
