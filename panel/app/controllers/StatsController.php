<?php

class StatsController extends BaseController
{
	var $filtro_parametros = array();
  /**
   * Método que añade filtros a las estadísticas
   * 
   * @return boolean Devolverá false si no se ha podido
   *                 añadir el filtro
   */
	public function addFilter($filter, $value)
  	{
    	$inoutbound = Session::get('inoutbound');
    	//$userNumbers = Number::where('id_cliente', '=', $this->number->getUser()->getID())->get();       
    
    	// TODO: validar números
    	if($filter === 'caller' || $filter === 'called')
		{
      		if(trim($value) !== '' && !in_array($value, Session::get('filters.' . $filter, array())))
			{
        		Session::push('filters.' . $filter, $value);
      		}
    	}
		elseif($filter === 'start' || $filter === 'end')
		{
      		if(\DateTime::createFromFormat('d/m/Y', $value) !== false)
			{
        		Session::put('filters.' . $filter, array($value));
      		}
    	}
  	}
  
  /**
   * Método que hace el filtrado de las llamadas
   */
	public function filter()
  	{
    	// Dirección de la llamada
    	$inoutbound = Input::get('inoutbound');
    
    	if($inoutbound !== Session::get('inoutbound'))
		{
      		// Eliminamos los filtros si antes consultábamos las llamadas
      		// salientes y ahora las entrantes
      		if($inoutbound === 'inbound')
			{
        		Session::forget('filters.caller');
      		}
			else
			{
        		Session::forget('filters.called');        
      		}
    	}
    
    	Session::put('inoutbound', $inoutbound === 'outbound' ? 'outbound' : 'inbound');    
    
    	// Filtros
    	$filters = Input::only('caller', 'called', 'start', 'end');

    	foreach($filters as $filter => $value)
		{
			Session::forget('filters.'.$filter);
			if($filter == 'called' && $value == '' && $inoutbound == 'inbound')
			{
				Session::forget('filters.called');
				Session::push('filters.called', '');
			}
			else
			{
      			$this->addFilter($filter, $value);
			}
    	}    
    	return $this->showStats();
  	}

  /**
   * Elimina un filtro 
   * 
   * @param string $filter
   */
  public function removeFilter($filter, $value)
  {
    if($filter === 'caller' || $filter === 'called') {
      if(trim($value) !== '' && ($pos = array_search($value, Session::get('filters.' . $filter, array()))) !== FALSE) {          
        Session::forget('filters.' . $filter . '.' . $pos);
      }

      if(count(Session::get('filters.' . $filter)) === 0) Session::forget('filters.' . $filter);
    } elseif(($filter === 'start' || $filter === 'end') && $value !== NULL) {
      Session::forget('filters.' . $filter);
    }    
  }
  
  /** 
   * Elimina un filtro de las estadísticas
   */
  public function unfilter()
  {
    $filtersToDelete = Input::only('caller', 'called', 'start', 'end');
    
    foreach($filtersToDelete as $filter => $value) {
      $this->removeFilter($filter, $value);
    }

    return Redirect::to('stats');
  }
    
  /**
   * Recupera las estadísticas
   * 
   * @param  boolean $export
   * @return array
   */
	protected function getStats($export = false)
  	{
    	$filters = Session::get('filters');
    	$inoutbound = Session::get('inoutbound');

    	$queryParams = array();

    	// Tenemos que comprobar la fecha de alta de los números que están en los
    	// filtros, para así no mostrar llamadas cuya fecha sea anterior a la 
    	// fecha de alta
    	if(isset($filters['called']) || isset($filters['caller']))
		{
      		$called = (isset($filters['called']) && $filters['called'] != '') ? $filters['called'] : array();
      		$caller = isset($filters['caller']) ? $filters['caller'] : array();
			
			if(is_array($called) && sizeof($called) > 0)
			{
				foreach($called as $key => $valor)
				{
					if(trim($valor) == '')
					{
						unset($called[$key]);
					}
				}
			}    

	  		if((is_array($called) && sizeof($called) > 0) && (is_array($caller) && sizeof($caller) > 0))
	  		{
		  		$numbers = array();
	  		}
	  		else
	  		{
				if(is_array($called))
				{
      				$numbers = array_merge($called, $caller);
				}
				else
				{
					$numbers = array_merge(array(), $caller);
				}
	  		}
      		
			$numeros_cliente = array();			
	  		if(sizeof($numbers) == 0)
	  		{
				//Si no tiene numeros, obtengo los numeros del cliente
		 		$userNumbers = Number::where('id_cliente', '=', $this->number->getUser()->getID())->get();				
				foreach($userNumbers as $number)
				{
					$numbers[] = $number->numero_visible;
					$numeros_cliente[] = $number->numero_visible;
				}
	  		}
			
			//Filtro para la fecha de inicio
			$sqlStart = 'SELECT MIN(fecha_alta) as `date` FROM numero WHERE fecha_alta <> "0000-00-00" and numero_visible IN ('
        		. implode(', ', $numbers) . ')';      
      		$res = DB::select($sqlStart);

      		if(count($res) > 0)
			{        
        		//$queryParams[] = 'start >= "' . $res[0]->date . '"';
        		$minStartDate = \DateTime::createFromFormat('d/m/Y', $res[0]->date);        
      		}
      
      		if(isset($filters['start']))
			{
        		$dtStart = \DateTime::createFromFormat('d/m/Y', $filters['start'][0]);        
        		//Si la fecha del filtro es menor que la minima
        		if($dtStart < $minStartDate)
				{
          			$queryParams[] = 'start >= "' . $minStartDate->format('Y-m-d') . '"';                  
        		}
				else
				{
          			$queryParams[] = 'start >= "' . $dtStart->format('Y-m-d') . ' 00:00:00"';                            
        		}        
      		}
			else
			{
				$queryParams[] = 'start >= "' . $res[0]->date . '"';
			}
			
			//Filtro para la fecha fin
      		if(isset($filters['end']))
			{
        		$dateTime = \DateTime::createFromFormat('d/m/Y', $filters['end'][0]);
        		$queryParams[] = 'end <= "' . $dateTime->format('Y-m-d') . ' 23:59:59"';
      		}
			
			//Filtro para entrante
			if(isset($filters['called']))
			{
				//Si es entrante y no tiene numeros, les tomo los del cliente opcion TODOS
				if(sizeof($numbers) > 0)
				{
					$queryParams[] = 'called in (' . implode(', ', $numbers) . ')';
				}
				else
				{
					$queryParams[] = 'called in (' . implode(', ', $filters['called']) . ')';
				}
			}
			
			//Filtro para saliente
			if(isset($filters['caller']))
			{
				$queryParams[] = 'caller in (' . implode(', ', $filters['caller']) . ')';
			}  
    	}    	        

    	// Si no está puesto ni llamante ni llamado, fijamos el número llamado
    	// desde el número elegido para la configuración
    	if(!isset($filters['caller']) && !isset($filters['called']))
		{
      		$selectedNumber = $this->number->getNumber();
      		$viewData['selectedNumber'] = $selectedNumber;

      		Session::push('filters.called', $selectedNumber);
      		$filters = Session::get('filters');
      		$viewData['filters'] = $filters;  

      		$queryParams[] = 'called = "' . $selectedNumber . '"';
    	}
		
    	$this->filtro_parametros = $queryParams;
    	if($export)
		{
      		$calls = Call::whereRaw(implode(' and ', $queryParams))
              ->orderBy('start', 'desc')->get();        
    	}
		else
		{
      		$calls = Call::whereRaw(implode(' and ', $queryParams))
              ->orderBy('start', 'desc')->paginate(15);
    	}
    
    	return $calls;
	}  
  
  /**
   * Muestra la ventana de estadísticas
   */
  	public function showStats()
  	{
    	$viewData = array();
    	$viewData['title'] = 'Estadísticas';
    
    	$isMobile = Session::get('is_mobile');
    	$viewData['isMobile'] = $isMobile;
    
    	// ¿Entrante o saliente?
    	if(($inoutbound = Session::get('inoutbound', null)) === null)
		{
      		$viewData['inoutbound'] = 'inbound';          
      		Session::put('inoutbound', 'inbound');
    	}
		else
		{
      		$viewData['inoutbound'] = $inoutbound;          
    	}
    
    	//
    	// Filtros
    	//
    	// Si no hay filtros, meteremos el filtro de llamadas recibidas
    	// en el número que estemos configurando en ese momento
    	//
    	$filters = Session::get('filters', array());
    
    	if(count($filters) === 0)
		{
      		$this->addFilter('called', $this->number->getNumber());
			$filters = Session::get('filters');
      		//$filters = Session::get('filters', array());      
    	}
    
    	$viewData['filters'] = $filters;

    	// Texto público de los filtros
    	$viewData['filter_public'] = array(
      		'called' => 'Llamadas a ',
      		'caller' => 'Llamadas desde ',
      		'start'  => 'Fecha igual o posterior a',
      		'end'    => 'Fecha igual o anterior a'
    	);
    
    	// Números contratados del usuario
    	$userNumbers = Number::where('id_cliente', '=', $this->number->getUser()->getID())
                        ->where('suspension', '=', 0)                        
                        ->get();

    	$viewData['userNumbers'] = $userNumbers;    
    
    	$viewData['calls'] = $this->getStats();
	
		$viewData['totales'] = $this->getTotales();
    	
		if(Session::get('called') == '')
		{
			$viewData['selectedNumber'] = '';
		}
		else
		{
			$viewData['selectedNumber'] = $this->number->getNumber();
		}
    
    	return View::make('content.stats.stats', $viewData);      
  	}
  
  	public function getTotales()
  	{
		$total_minutos = 0;
		$total_costo = 0;    
    	$summary = Call::select(DB::raw('ROUND(SUM(duration)/60, 2) as total_minutos'),DB::raw('ROUND(SUM(cost_in) + SUM(cost_out),4) as total_costo'))->whereRaw(implode(' and ', $this->filtro_parametros))->get();
		if(sizeof($summary) > 0)
		{
			$summary = $summary[0];
			$total_minutos = $summary->total_minutos;
			$total_costo = $summary->total_costo; 
		}
		return array(
						'total_minutos' => $total_minutos,
						'total_costo' => $total_costo
					);
  	}
  
  /**
   * Método para exportar las estadísticas
   */
  public function export()
  {
    $calls = $this->getStats(true);    
    
    $content = "Comienzo\tOrigen\tDestino\tDuracion\tFinal\tEntrante (EUR)\tSaliente (EUR)\tSIP Entrante\tSIP Saliente\n";

    foreach($calls as $call) { 
      $start = (new DateTime($call->start))->format('d/m/Y H:i:s');
      $end   = (new DateTime($call->end))->format('d/m/Y H:i:s');

      $cost_in  = number_format($call->cost_in, 4,',',' ');                    
      $cost_out = number_format($call->cost_out, 4,',',' ');                    

      $content.= $start . "\t" . $call->ContactoCaller() . "\t" . $call->ContactoCalled() . "\t";
      $content.= gmdate('H:i:s', $call->duration) . "\t" . $end . "\t" . $cost_in;
      $content.= "\t" . $cost_out . "\t" . $call->who_picked_up . "\t" . $call->who_called . "\n";
    }

    $response = Response::make($content, 200);

    $response->header('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
    $response->header('Content-length', strlen($content));
    $response->header('Content-Disposition', "attachment;filename=flash_telecom_stats.xls");

    return $response;          
  }
  
  /**
   * Método que devuelve los datos para el resumen de tráfico entrante
   * 
   * @return array
   */
  protected function getSummaryDataIncoming()
  {    
    $summaryData = array();
    $selectedNumber = $this->number->getNumber();
   
    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(duration)/60, 2) AS `minutes` FROM calls WHERE
    called = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 DAY)";
    
    $summaryDay = DB::select($sql, array($this->number));    
    $summaryDay = reset($summaryDay);

    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(duration)/60, 2) AS `minutes` FROM calls WHERE
    called = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 WEEK)";
    
    $summaryWeek = DB::select($sql, array($this->number));    
    $summaryWeek = reset($summaryWeek);
    
    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(duration)/60, 2) AS `minutes` FROM calls WHERE
    called = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 MONTH)";
    
    $summaryMonth = DB::select($sql, array($this->number));    
    $summaryMonth = reset($summaryMonth);
    /* 
    $sql = "SELECT DATE_FORMAT(dates.date, '%d') AS 'day', (SELECT COUNT(*) FROM calls WHERE
     called = ? AND DATE_FORMAT(dates.date, '%Y-%m-%d') = DATE_FORMAT(`start`, '%Y-%m-%d'))
     AS 'total' FROM (SELECT DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL a + b DAY 'date'
     FROM (SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3 UNION 
     SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8
     UNION SELECT 9) d, (SELECT 0 b UNION SELECT 10 UNION SELECT 20 UNION 
     SELECT 30 UNION SELECT 40) m WHERE DATE_FORMAT(NOW(), '%Y-%m-01') + INTERVAL a + b DAY
     < LAST_DAY(NOW()) ORDER BY a + b) dates GROUP BY dates.date;"; 
    */
    $sql = "SELECT DATE_FORMAT(`start`, '%d') AS `day`, COUNT(*) AS `total` FROM calls
    WHERE called = ? AND DATE_FORMAT(`start`, '%Y-%m') = DATE_FORMAT(NOW(), '%Y-%m')
    GROUP BY DATE_FORMAT(`start`, '%d') ORDER BY `start` ASC";

    $monthCallsGroup = DB::select($sql, array($this->number));    

    $sql = "SELECT calls.caller, DATE_FORMAT(calls.`start`, '%d-%m-%Y %H:%i') AS `date`,
      TIME_FORMAT(SEC_TO_TIME(calls.duration),'%i:%s') AS `duration` FROM calls 
      JOIN numero n ON n.numero_visible = calls.called WHERE called = ? 
      AND calls.`start` >= n.fecha_alta ORDER BY calls.`start` DESC LIMIT 10";
    
    $latestCalls = DB::select($sql, array($this->number));
    
    $summaryData = (object)array(
      'day' => (object)array(
        'count'   => $summaryDay->count !== null ? $summaryDay->count : 0,
        'minutes' => $summaryDay->minutes !== null ? $summaryDay->minutes : 0
      ),
      'week' => (object)array(
        'count'   => $summaryWeek->count !== null ? $summaryWeek->count : 0,
        'minutes' => $summaryWeek->minutes !== null ? $summaryWeek->minutes : 0
      ),
      'month' => (object)array(
        'count'   => $summaryMonth->count !== null ? $summaryMonth->count : 0,
        'minutes' => $summaryMonth->minutes !== null ? $summaryMonth->minutes : 0,
        'calls'   => $monthCallsGroup
      ),
      'latestCalls' => $latestCalls
    );
    
    return json_encode($summaryData);
  }
  
  /**
   * Método que devuelve los datos para el resumen de tráfico saliente
   * 
   * @return array|bool
   */
  protected function getSummaryDataOutgoing()
  {    
    if(!in_array($this->number->getProfile()->getType(), array('voip', 'pbx', 'trunk')))
      return false;
        
    $summaryData = array();
    $selectedNumber = $this->number->getNumber();
   
    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(cost_out), 2) AS `cost` FROM calls WHERE
    caller = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 DAY)";
    
    $summaryDay = DB::select($sql, array($this->number));    
    $summaryDay = reset($summaryDay);

    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(cost_out), 2) AS `cost` FROM calls WHERE
    caller = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 WEEK)";
    
    $summaryWeek = DB::select($sql, array($this->number));    
    $summaryWeek = reset($summaryWeek);
    
    $sql = "SELECT COUNT(*) AS `count`, ROUND(SUM(cost_out), 2) AS `cost` FROM calls WHERE
    caller = ? AND `start` > DATE_SUB(NOW(), INTERVAL 1 MONTH)";
    
    $summaryMonth = DB::select($sql, array($this->number));    
    $summaryMonth = reset($summaryMonth);

    $sql = "SELECT DATE_FORMAT(`start`, '%d') AS `day`, COUNT(*) AS `total` FROM calls
    WHERE caller = ? AND DATE_FORMAT(`start`, '%Y-%m') = DATE_FORMAT(NOW(), '%Y-%m')
    GROUP BY DATE_FORMAT(`start`, '%d') ORDER BY `start` ASC";

    $monthCallsGroup = DB::select($sql, array($this->number));    

    $sql = "SELECT calls.called, DATE_FORMAT(calls.`start`, '%d-%m-%Y %H:%i') AS `date`,
      TIME_FORMAT(SEC_TO_TIME(calls.duration),'%H:%i:%s') AS `duration` FROM calls 
      JOIN numero n ON n.numero_visible = calls.caller WHERE caller = ? 
      AND calls.`start` >= n.fecha_alta ORDER BY calls.`start` DESC LIMIT 10";
    
    $latestCalls = DB::select($sql, array($this->number));    
    
    $summaryData = (object)array(
      'day' => (object)array(
        'count' => $summaryDay->count !== null ? $summaryDay->count : 0,
        'cost'  => $summaryDay->cost !== null ? $summaryDay->cost : 0
      ),
      'week' => (object)array(
        'count' => $summaryWeek->count !== null ? $summaryWeek->count : 0,
        'cost'  => $summaryWeek->cost !== null ? $summaryWeek->cost : 0
      ),
      'month' => (object)array(
        'count' => $summaryMonth->count !== null ? $summaryMonth->count : 0,
        'cost'  => $summaryMonth->cost !== null ? $summaryMonth->cost : 0,
        'calls' => $monthCallsGroup
      ),
      'latestCalls' => $latestCalls
    );
    
    return json_encode($summaryData);
  }
}

?>
