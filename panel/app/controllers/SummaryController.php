<?php

use Panel\Model\Queue;

class SummaryController extends BaseController
{
  /**
   * Muestra ventana del dashboard
   * 
   * @return type
   */
  public function show($mens = '')
  {
    $number = $this->number;    
    $viewData = array();
    if(isset($mens) && !empty($mens)){
		$viewData['mens'] = 'Tarifa plana eliminada con exito';
	}
    $viewData['title']     = 'Resumen de ' . $number;
    $viewData['number']    = $number;   
    $viewData['profile']   = $number->getProfile();
    $viewData['options']   = $number->getOptions();
    $viewData['plan']      = $number->getProfile()->getPlan();       
    $viewData['panelMode'] = Session::get('panel_mode');
    
    // Festivos nacionales
    $viewData['nationalHolidays'] = [/* d-m */'01-01', '06-01', '17-04', '18-04',
    '01-05', '15-08', '01-11', '06-12', '08-12', '25-12'];
    
    // Desvíos
    $viewData['mainForward'] = $number->getMainForward();
    
    $forwards = $number->getForwards();
    if(is_array($forwards)) $forwards = array_slice($forwards, 1);
    
    $viewData['forwards'] = $forwards;

    // Día, mes y tal en texto
    $wdays = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado');
    $months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');    
    
    $today = new \DateTime();
    $summaryDate = Input::get('summary-formatted-date', $today->format('Y-d-m'));
    $summaryDate = \DateTime::createFromFormat('Y-d-m', $summaryDate);
    
    $viewData['summaryDate']        = $summaryDate;
    $viewData['summaryDateWeekDay'] = (int)$summaryDate->format('N') - 1; 
    
    $viewData['today'] = $today;
    $viewData['todayWeekDay'] = (int)$today->format('N') - 1;
    
    // Cadena con la fecha seleccionada, por defecto, hoy
    $viewData['date'] = $wdays[date('w')] . ', ' . (date('d') < 10 ? date('d')[1] : date('d'))
      . ' de ' . $months[date('n') - 1] . ' de ' . date('Y');    
    
    // Tarifas planas
    $sql = 'SELECT fr.name, fr.description, nfr.minutes_left, fr.minutes, 
      nfr.since, nfr.id_flatrate FROM flatrates fr, number_flatrates nfr WHERE id_number = ? AND
      nfr.id_flatrate = fr.id_flatrate;';
    
    $flatrates = DB::select($sql, array($number->getID()));
    $viewData['flatrates'] = $flatrates;   
    if(in_array($number->getType(), array('pbx', 'voip', 'trunk'))){
		$viewData['tarifaplana'] = 'si';
	}else{
		$viewData['tarifaplana'] = 'no';
	}
    	// Si es IP-ish, pasamos la cola parseada
    	if(in_array($number->getType(), array('pbx', 'voip')))
		{
      		$numberRow = Number::find($number->getID());
      		$viewData['queue'] = Queue::parse($numberRow->cuenta_sip);
    	}
		$viewData['id_numero'] = $number->getID();
    
    return View::make('content.summary', $viewData);
  }
  
  public function deleteFlatrate($id_tarifa, $id_numero){
	  //DB::table('number_flatrates')->where('id_number', '=', ''.$id.'')->delete();
	  DB::table('number_flatrates')->where('id_flatrate', '=', $id_tarifa)->where('id_number', '=', $id_numero)->delete();
	  return $this->show("si");
  }
}

?>
