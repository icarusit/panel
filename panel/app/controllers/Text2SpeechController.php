<?php

class Text2SpeechController extends BaseController
{ 
  /**
   * Método que devuelve el contenido para la ventana modal
   * de texto a voz
   */
  public function returnModal()
  {
    $viewData = array();
    
    return View::make('components.actions.modals.text2speech', $viewData);
  }
}

?>
