<?php

class TrunkController extends BaseController
{
  /**
   * Muestra ventana de los trunks
   * 
   * @return View
   */
  public function showTrunks()
  {    
    $trunks = SIPPeers::where('id_cliente', '=', $this->user->getID())->get();

    $selectedTrunk = Number::find($this->number->getID())->cuenta_sip;
    $selectedTrunk = substr($selectedTrunk, 0, strpos($selectedTrunk, '/'));    
    
    return View::make('content.trunk', array(
      'title'  => 'Trunks',
      'trunks' => $trunks,
      'selectedTrunk' => $selectedTrunk
    ));
  }
  
  /**
   * Actualiza la configuración del trunk
   */
  public function update()
  {
    $postTrunk = Input::get('trunk');
    
    // Comprobamos que el trunk sea efectivamente del usuario
    $trunk = SIPPeers::where('id_cliente', '=', $this->user->getID())
                      ->where('defaultuser', '=', $postTrunk)
                      ->get();
    
    if(count($trunk) > 0) {
      $strNumber = $this->number->getNumber();
      
      $number = Number::find($this->number->getID());
      $number->cuenta_sip = $postTrunk . '/' . $strNumber;

      if($number->save()) {
        $info = "Se ha actualizado el trunk de <strong>$strNumber</strong>";
        return Redirect::to('trunk')->with('info', $info);
      } else {
        $error = "Ha habido un error actualizando el trunk.";
        return Redirect::to('trunk')->with('error', $error);
      }
    }
  }
}
