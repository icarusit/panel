<?php

class UserController extends BaseController
{
 	 public function profile()
  	{
    	$user = Session::get('user');
    	$idUser = $user->getID();   
        
    	$user = User::find($idUser);
    	$countries = Countries::get();
    
    	$userHasFilledOutProfile = $user->hasFilledOutProfile();
		
		$codigo_afiliacion = CodigoAfiliacion::where('id_cliente','=',$idUser)->first();

    	return View::make('content.profile', array(
			  'title'     => 'Mi perfil',
			   
			  'name'           => $user->nombre_fiscal,
			  'user_country'   => Countries::find($user->id_country),
			  'username'       => $user->usr_usuario,
			  'email'          => $user->mail_1,
			  'phone'          => $user->telefono_1,
			  'cnif'           => $user->NIF,
			  'fax'            => $user->fax,
			  'address'        => $user->direccion,
			  'city'           => $user->poblacion,
			  'legal_rep_name' => $user->rep_legal,
			  'legal_rep_nif'  => $user->rep_legal_dni,        
			  'is_company'     => !(trim($user->rep_legal) === '' && trim($user->rep_legal_dni) === ''),
			  'province'       => $user->provincia,
			  'post_code'      => $user->cp,
			  'signup_date'    => $user->fecha_alta,        
			  'new_client'     => $userHasFilledOutProfile,
			  'countries'      => $countries,
			  'whatsapp'	   => $user->whatsapp,
			  'codigo_afiliacion' => $codigo_afiliacion
    	));
  	}
  
  public function update()
  {
    $return = Input::get('return', 'me');
    $returnOnError = Input::get('return_on_error', 'me');

    $user = Session::get('user');
    $idUser = $user->getID();
    
    $user = User::find($idUser);    
    
    $userHadFilledOutProfile = $user->hasFilledOutProfile();
    
    $fields = (object)Input::only('name', 'address', 'city', 'province', 'post_code', 
            'phone', 'fax', 'email', 'legal_rep_name', 'legal_rep_nif', 'cnif','whatsapp');
			

    // Validamos NIF si ha seleccionado España como país
    if(Input::get('country') === 'ES') {
      $nif = new NIF($fields->cnif);

      if(!$nif->isValid()) {
        $error = "El NIF/NIE/CIF introducido no es válido.";      

        Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
        return Redirect::to($returnOnError)->with('error', $error);  
      }
    }
    
    // Comprobamos si el NIF del usuario ya está en otra ficha
    $rptUser = User::where('NIF', '=', $fields->cnif)->get();
    
    if($rptUser !== null && count($rptUser) > 0 && !$userHadFilledOutProfile) {
      $error = "El NIF/CIF introducido ya está en otra ficha de cliente en nuestra base de datos. ";
      $error.= "Es probable que ya te dieras de alta anteriormente con otro nombre de usuario. Llámanos al 902998901 ";
      $error.= "para solucionar la incidencia.";
      
      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
      return Redirect::to($returnOnError)->with('error', $error);   
    }
    
    // Si el nombre del cliente no tiene al menos un espacio, lo damos por incorrecto
    if(strpos(trim($fields->name), ' ') === false) {
      $error = "El nombre introducido no parece válido. Ten en cuenta que debes proporcionar tu nombre "
              . "completo o el nombre completo de tu empresa con su tipo societario.";

      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
      return Redirect::to($returnOnError)->with('error', $error);         
    }

    if(!(($country = Countries::where('iso', '=', Input::get('country'))->first()) instanceof Countries)) {
      $error = "El país seleccionado no es correcto.";
      
      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
      return Redirect::to($returnOnError)->with('error', $error);               
    }

    if(strlen(trim($fields->city)) < 4 && strlen(trim($fields->address)) < 4) {
      $error = "Debes rellenar la dirección y el nombre de la población con datos válidos.";

      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
      return Redirect::to($returnOnError)->with('error', $error);         
    }
    
    if(!is_numeric($fields->post_code)) {
      $error = "El código postal no parece válido.";

      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));
      return Redirect::to($returnOnError)->with('error', $error);         
    }
    
    $user->cliente_nuevo = 2;
    $user->id_country    = $country->id_country;
    $user->nombre_fiscal = $fields->name;
    $user->NIF           = $fields->cnif;
    $user->rep_legal     = $fields->legal_rep_name;
    $user->rep_legal_dni = $fields->legal_rep_nif;
    $user->direccion     = $fields->address;
    $user->provincia     = Input::get('country') === 'ES' ? $fields->province : null;
    $user->poblacion     = $fields->city;
    $user->cp            = $fields->post_code;
    $user->mail_1        = $fields->email;
    $user->mail_2        = $fields->email;    
    $user->telefono_1    = $fields->phone;
    $user->fax           = $fields->fax;
	$user->whatsapp		 = $fields->whatsapp;
    
    if($country->iso !== 'ES') {
      $user->IVA = 0;
      
      if($country->eu) {
        $user->impreso = "349";  
        $user->intracomunitario = "Si";
      } else {
        $user->extranjero = "Si";
      }
    } else {
      $user->impreso = "347";
      if(in_array($fields->province, array('Las Palmas', 'Santa Cruz de Tenerife'))) {
        $user->IVA = 0;  
      }
    }
    
    if((($password = Input::get('password', null)) !== null)
        && (($cfmPassword = Input::get('cfm-password', null)) !== null)
        && $password === $cfmPassword
        && trim($password) !== '') {
      $user->usr_pass = md5($password);
    }
    
    if($user->save()) {    
      $info = "La información de tu perfil ha sido correctamente actualizada.";    
      $this->refresh();

      return Redirect::to($return)->with('info', $info);    
    } else {
      $error = "Ha habido algún problema guardando la información de tu perfil. Inténtalo más tarde.";    
      
      Session::flash('formData', Input::except('_token', 'password', 'cfm-password'));      
      return Redirect::to($returnOnError)->with('error', $error);          
    }
  }
  
  /**
   * Método post de olvidar contraseña   * 
   */
  public function postForgotPassword()
  {
    // Validamos primero que el e-mail introducido corresponda
    // a una cuenta de cliente de Flash
    $rules = array(
      'email'     => 'required|email|exists:cliente,mail_1'
    );
    
    $messages = array(
      'email.email'    => 'La dirección de e-mail no es válida.',
      'email.exists'   => 'No existe ningún cliente en '.Session::get('distribuidor').' con esa cuenta de e-mail.',
      'email.required' => 'El campo es obligatorio'  
    );
    
    $validator = Validator::make(Input::all(), $rules, $messages);
    
    if ($validator->fails()) {
      return Redirect::to('forgot-password')->withErrors($validator);   
    } else {
      $email = Input::get('email');
      $user = User::where('mail_1', '=', $email)->firstOrFail();
      
      // Si es cliente, registramos el token solicitado
      $newPasswordRequest = new PasswordRestoreTempTokens;
      
      $newPasswordRequest->id_user    = $user->id_cliente;
      $newPasswordRequest->token      = PasswordRestoreTempTokens::generateStringToken();
      $newPasswordRequest->date_added = date('Y-m-d H:i:s');
      
      $newPasswordURL = URL::to('restore-password') . '?token=' . $newPasswordRequest->token;
      
      if($newPasswordRequest->save()) {
        // Notificamos por e-mail al cliente con el link para restablecer la password
        $subject = "Nueva contraseña en tu cuenta de ".Session::get('distribuidor');        
        $header  = "Has solicitado restablecer tu contraseña de ".Session::get('distribuidor');
        $subheader = "Haz click en <a href='$newPasswordURL'><strong>aquí</strong></a> para restablecer tu contraseña del " .
                "<a href='$newPasswordURL'>panel de cliente</a> de ".Session::get('distribuidor');

        $body = "<p>Haz click <a href='$newPasswordURL'>aquí</a> para restablecer tu contraseña del " . 
                "<a href='$newPasswordURL'>panel de cliente</a> de ".Session::get('distribuidor').".</p>" .
                '<p>Si no puedes hacer click en el link, copia y pega la siguiente ' . 
                'dirección en la barra de direcciones de tu navegador:</p>' . 
                "<p><a href='$newPasswordURL'>$newPasswordURL</a></p>";

        $content = array(
          'header'    => $header,
          'subheader' => $subheader,
          'body'      => $body
        );
		if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content); 
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}
        
        $info = "<p>Hemos mandado instrucciones a tu correo electrónico para restablecer "
                . "tu contraseña.</p><p>Ten en cuenta que puede tardar unos minutos en llegarte.</p>"
                . "<p>No olvides por favor comprobar también la carpeta de spam.</p>";
        
        return Redirect::to('forgot-password')->with('info', $info);
      } else {
        $error = "Ha habido algún error tratando de enviarte las instrucciones.";
        
        return Redirect::to('forgot-password')->with('error', $error);
      }
    }
  }
  
  /**
   * Método para restablecer la contraseña
   * 
   * Entraremos por aquí cuando hayamos enviado ya el correo al usuario con el 
   * token temporal
   */
  public function restorePassword()
  {
    $strToken = Input::get('token', null);
    $token = PasswordRestoreTempTokens::where('token', '=', $strToken)->first();
    $results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono','logo_alta','css','dominio_master')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
    if($token instanceof PasswordRestoreTempTokens && $token->isValid()) {
      // Y lo devolvemos a la ventana para que ponga su nueva contraseña
      return View::make('restore_password', array('token' => $strToken, 'distribuidor' => $results));
    } else {
      return Redirect::to('forgot-password');
    }
  }
  
  /**
   * Método que fija la nueva contraseña del usuario cuando la había olvidado.
   * 
   * @todo HASHEAR Y SALTEAR
   */
  public function postSetNewPassword()
  {            
    $strToken = Input::get('token', null);
    $token = PasswordRestoreTempTokens::where('token', '=', $strToken)->first();
    
    // Si el token ya no era válido o es una trampilla de alguien devolvemos
    // a pantalla inicial
    if(($token instanceof PasswordRestoreTempTokens && !$token->isValid())
        || $token === null) {
      return Redirect::to('forgot-password');
    }
    
    // Validamos primero los datos introducidos por el usuario
    $rules = array(
      'password'     => 'required|min:8',        
      'rpt-password' => 'required|same:password'
    );
    
    $messages = array(
      'min'       => 'La contraseña debe tener al menos 8 caracteres.',
      'same'      => 'Las contraseñas no coinciden.',
      'required'  => 'El campo es obligatorio.'
    );
    
    $validator = Validator::make(Input::all(), $rules, $messages);
    
    if ($validator->fails()) {
      return Redirect::to('restore-password?token=' . $strToken)
        ->withErrors($validator);
    } else {
      $user = User::find($token->id_user);
      
      if($user === null) 
        return Redirect::to('forgot-password');
      
      // CAMBIAR ESTO POR CLAVE HASHEADA Y SALTEADA, ESTO ES CRAZY
      $user->usr_pass = md5(Input::get('password'));
      
      if($user->save()) {
        $token->used = true;
        $token->save();
        $results = DB::table('distribuidores')->select('dominio')->where('id', '=', Session::get('id_distribuidor'))->get();
        // Mandamos mail al usuario confirmando cambio de contraseña
        $subject = "Has cambiado tu contraseña de ".Session::get('distribuidor');        
        $header  = $subject;
        $subheader = 'Usa estos datos para acceder al <a href="http://'.$results[0]->dominio.'">panel de clientes</a>';

        $body = '<table cellpadding="5" cellspacing="0" width="100%" border="0">' .
          '<tr>' . 
            '<th>Usuario</th>' . 
            '<th>Contraseña</th>' . 
          '</tr>' . 
          '<tr>' . 
            '<td>' . $user->usr_usuario . '</td>' .
            '<td>La contraseña que has escrito</td>' . 
          '</tr>' . 
        '</table>' .
        '<p>Para cualquier consulta no dudes en contactarnos contestando a este mismo correo ' .
        'o bien llamándonos al número gratuito 800 00 77 66</p>' .
        '<p>Gracias.</p>' . 
        '<p>Equipo de '.Session::get('distribuidor').'</p>'; 
        
        $content = array(
          'header'    => $header,
          'subheader' => $subheader,
          'body'      => $body
        );

        if(Session::get('id_distribuidor') == 1){
        	$this->flash->sendMail($user->mail_1, $subject, $content); 
		}else{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->SMTPSecure = 'tls';
			$mail->Host = "email-smtp.us-east-1.amazonaws.com";
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
			$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
			$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor')."");
			$mail->Subject = $subject;
			$mail->AddReplyTo = "no-reply@".Session::get('dominio');
			$mail->AddAddress($user->mail_1);
			$mail->AltBody = $header;
			$mail->msgHTML($body);
			$mail->send();
		}         
        
        $url = URL::to('/');
        
        $info = "<p>La contraseña ha sido correctamente actualizada.</p>";
        $info.= "<p>Ya puedes <a href='$url'>iniciar sesión</a>.</p>";
        
        return Redirect::to('forgot-password')->with('info', $info);
      } else {
        
      }
    }
  }
  
  /**
   * Método para loggear usuarios
   */
  public function login()
  { 
    // Validamos primero los datos introducidos por el usuario
    $rules = array(
      'user'     => 'required',
      'password' => 'required'
    );
    
    $messages = array(
      'required' => 'El campo es obligatorio.',
    );
    
    $validator = Validator::make(Input::all(), $rules, $messages);
    
    if ($validator->fails()) {
      return Redirect::to('alta/user')
        ->withErrors($validator)
        ->with('portability', Input::get('portability', false))
        ->with('alta-plan', Input::get('alta-plan'))
        ->with('alta-number', Input::get('alta-number'));              
    } else {
      $user_data = Input::only('user', 'password', 'redirect');
      
      $user = User::where('usr_usuario', '=', $user_data['user'])
                  ->where('usr_pass', '=', md5($user_data['password']))
                  ->first();

      if($user === null) {
        $error = "Usuario no válido o contraseña incorrecta.";
        
        return Redirect::to('alta/user')
          ->with('error', $error)
          ->with('portability', Input::get('portability', false))
          ->with('alta-plan', Input::get('alta-plan'))
          ->with('alta-number', Input::get('alta-number'));
      } else {
        $flashUser = $this->flash->user($user->id_cliente);
        Session::put('user', $flashUser);      

        $portability = (bool)Input::get('portability', false);
        $plan        = Input::get('alta-plan');
        $number      = Input::get('alta-number');       

        if(substr($plan, 0, 2) === 'IP' || substr($plan, 0, 2) === 'ST') {
          $profile = Profile::getIPPlanByText($plan);
        } else {
          $profile = Profile::getByPostString($plan);            
        }

        if(in_array($profile->getType(), array('voip', 'pbx','trunk'))) {
          $infoIPPlan = $profile->getInfoIPPlan();
        }

        if($portability) {
          return App::make('SignUpController')->confirmPortability($plan, $number);
        } else {
          if(in_array($profile->getType(), array('voip', 'pbx','trunk'))) {
            if(!$infoIPPlan->num_dids) {
              return App::make('SignUpController')->assignFreeSipAccount($plan);
            } else {
              return App::make('SignUpController')->assignNumberIP($plan, $number); 
            }
          } else {
            return App::make('SignUpController')->assignNumber($plan, $number);          
          }
        }
      }
    }
  }
  
  /**
   * Método por el que se crea un nuevo usuario 
   * 
   * @return integer|boolean Si ha ido todo bien devuelve
   *                         el id del nuevo usuario, si no
   *                         devolverá false.
   */
 	public function create()
  	{
		/*
		Validar que el ip del usuario no ha sido registrado
		*/
		$ip_cliente = Request::getClientIp();
		$registros_ip = LogAltas::where('ip','=',$ip_cliente)->get();
		if(sizeof($registros_ip) > 0)
		{
			Session::flash('formData', Input::except('_token', 'new-password', 'new-rpassword'));
      
      		return Redirect::to('alta/user')
          		->withErrors("Ya eres cliente nuestro. No puedes crear otra cuenta. Usa el login para acceder a tu cuenta")
          		->with('portability', Input::get('portability'))
          		->with('alta-plan', Input::get('alta-plan'))
          		->with('alta-number', Input::get('alta-number'));
		}
    	// Validamos primero los datos introducidos por el usuario
    	$rules = array(
      		'new-user'         => 'required|alpha_num|unique:cliente,usr_usuario',
      		'new-email'        => 'required|email',
      		'new-password'     => 'required|same:new-rpassword',        
      		'new-rpassword'    => 'required',
      		'new-phone'        => 'required',
      		'new-accept-terms' => 'accepted',
      		'g-recaptcha-response' => 'required|recaptcha'
    	);
    
    	$messages = array(
      		'email'     => 'La dirección de e-mail no es válida.',
      		'same'      => 'Las contraseñas no coinciden.',
      		'alpha_num' => 'El campo debe tener sólo valores alfanuméricos.',
      		'required'  => 'El campo es obligatorio.',
      		'g-recaptcha-response.required' => 'El reto del captcha es obligatorio.',
      		'unique'    => 'Ya existe un usuario con el nombre de usuario indicado.',
      		'accepted'  => 'Debes leer y aceptar los términos y condiciones',
      		'recaptcha' => 'Has introducido caracteres erróneos en el captcha'
    	);
    
    	$validator = Validator::make(Input::all(), $rules, $messages);
    
    	if ($validator->fails())
		{
      		Session::flash('formData', Input::except('_token', 'new-password', 'new-rpassword'));
      
      		return Redirect::to('alta/user')
          		->withErrors($validator)
          		->with('portability', Input::get('portability'))
          		->with('alta-plan', Input::get('alta-plan'))
          		->with('alta-number', Input::get('alta-number'));
    	}
		else
		{   
      		$userPostData = Input::only('new-email', 'new-user', 'new-password', 'new-phone');
      
      		$user = new User;
      
      		$user->usr_usuario = $userPostData['new-user'];
      		$user->usr_pass    = md5($userPostData['new-password']);
      		$user->mail_1      = $userPostData['new-email'];
      		$user->fecha_alta  = (new \DateTime())->format('Y-m-d H:i:s');
      		$user->telefono_1  = $userPostData['new-phone'];
      		$user->nuevo_panel = 1;
      		$user->userbalance = 0.1;
	  		$user->id_distribuidor = Session::get('id_distribuidor');
      
	  		/*
			Codigo de afiliacion
			*/
			$codigo_afiliacion = Session::get('codigo_afiliacion');
			if($codigo_afiliacion)
			{
				$afiliacion = CodigoAfiliacion::where('codigo','=',$codigo_afiliacion)->first();
				if($afiliacion)
				{
					$user->customer_affiliator = $afiliacion->id_cliente;
				}					
			}
	  
			if($user->save())
			{
				$codigo_afiliacion = Session::put('codigo_afiliacion',NULL);
								
			  	$logAlta = new LogAltas;
				$logAlta->nif = $user->id_cliente;
				$logAlta->numero = Input::get('alta-number');
				$logAlta->fecha = (new \DateTime())->format('Y-m-d');
				$logAlta->ip = Request::getClientIp();
				$logAlta->save();
				
				$flashUser = $this->flash->user($user->id_cliente);
				Session::put('user', $flashUser);      
				$results = DB::table('distribuidores')->select('dominio')->where('id', '=', Session::get('id_distribuidor'))->get();
				// Notificamos por e-mail al cliente con los datos de su cuenta
				$subject = "Te damos la bienvenida a ".Session::get('distribuidor');        
				$header  = $subject;        
				$subheader = 'Adjuntamos tus datos de acceso al <a href="http://'.$results[0]->dominio.'">panel de clientes</a>';
				//$user->usr_pass
				$body = '<p>Podrás configurar toda tu numeración desde el ' .
						'<a href="http://'.$results[0]->dominio.'">panel de cliente</a> utilizando ' .
						'los credenciales que usaste en el momento del alta:</p>' . 
						'<table cellpadding="5" cellspacing="0" width="100%" border="0">' .
						'<tr>' . 
						'<th>Usuario</th>' . 
						'<th>Contraseña</th>' . 
						'</tr>' . 
						'<tr>' . 
						'<td>' . $user->usr_usuario . '</td>' .
						'<td>La que has puesto al darte de alta</td>' . 
						'</tr>' . 
						'</table>' .
						'<p>Para cualquier consulta no dudes en contactarnos contestando a este mismo correo ' .
						'o bien llamándonos al número gratuito '.Session::get('telefono').'</p>' .
						'<p>Gracias.</p>' . 
						'<p>Equipo de '.Session::get('distribuidor').'</p>'; 
			
				$content = array(
						'header'    => $header,
						'subheader' => $subheader,
						'body'      => $body
					);
	
				//	$this->flash->sendMail($user->mail_1, $subject, $content); 
				
				$portability = (bool)Input::get('portability', false);
				$plan        = Input::get('alta-plan');
				$number      = Input::get('alta-number');    
		   
				if(Session::get('id_distribuidor') == 1)
				{
					$this->flash->sendMail($user->mail_1, $subject, $content); 
				}
				else
				{
					$mail = new PHPMailer(true);
					$mail->IsSMTP();
					$mail->SMTPSecure = 'tls';
					$mail->Host = "email-smtp.us-east-1.amazonaws.com";
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->SMTPAuth = true;
					$mail->Port = 587;
					$mail->Username = "AKIAJMW3J6EK2X3LTAFA";
					$mail->Password = "AlczdNxeOowrdzvcVLjYSjq0HsVUoEE0GftjStKeDi03";
					$mail->SetFrom ("no-reply@".Session::get('dominio'), "Servicio de cuentas ".Session::get('distribuidor'));
					$mail->Subject = $subject;
					$mail->AddReplyTo = "no-reply@".Session::get('dominio');
					$mail->AddAddress($user->mail_1);
					$mail->AltBody = $header;
					$mail->msgHTML($body);
					$mail->send();
				} 
			
				if(substr($plan, 0, 2) === 'IP')
				{
					$profile = Profile::getIPPlanByText($plan);
				}
				else
				{
					$profile = Profile::getByPostString($plan);  
				}
	
				if(in_array($profile->getType(), array('voip', 'pbx')))
				{
					$infoIPPlan = $profile->getInfoIPPlan();
				}
		
				if($portability)
				{
					return App::make('SignUpController')->confirmPortability($plan, $number);
				}
				else
				{
					if(in_array($profile->getType(), array('voip', 'pbx')))
					{
						if(!$infoIPPlan->num_dids)
						{
							return App::make('SignUpController')->assignFreeSipAccount($plan);
						}
						else
						{
							return App::make('SignUpController')->assignNumberIP($plan, $number); 
						}
					}
					else
					{
						return App::make('SignUpController')->assignNumber($plan, $number);          
					}
				}        
			}
			else
			{
				return false;
			}
		}
	}
	
	public function generateCodigoAfiliacion()
	{
		$return = 'me';
		$user = Session::get('user');
    	$idUser = $user->getID();
		
		$codigo_afiliacion = CodigoAfiliacion::where('id_cliente','=',$idUser)->first();
		
		if(!$codigo_afiliacion)
		{
			$codigo_afiliacion =  new CodigoAfiliacion;
			$codigo_afiliacion->id_cliente = $idUser;
			$codigo_afiliacion->codigo = $this->generaCodigo($idUser);
			
			if($codigo_afiliacion->save())
			{
				$info = 'Se ha generado el código de afiliación.';      		      
      			return Redirect::to($return)->with('info', $info);
			}
			else
			{
				$error = 'Error inesperado, intente nuevamente.';
      			return Redirect::to($return)->with('error', $error);
			}
		}
		else
		{
			$error = 'Ya existe generado eun código de afiliación.';
      		return Redirect::to($return)->with('error', $error);
		}
	}
	
	private function generaCodigo($id_cliente)
	{
		$palabra="";
		$posible="0123456789abcdefghijklmnpqrstuwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i=0;
		$cantidad = 5;
		while($i<$cantidad)
		{
			$char=substr($posible,mt_rand(0,strlen($posible)),1);
				
			if(!strstr($palabra,$char))
			{
				$palabra.=$char;
				$i++;
			}
		}
		return $id_cliente.$palabra;
	}
}

?>
