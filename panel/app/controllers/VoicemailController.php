<?php

class VoicemailController extends BaseController
{
  /**
   * Muestra configuración del buzón de voz
   * 
   * @return type
   */
  	public function showVoicemail()
  	{
    	$viewData = array();
    
    	$fifteenDaysAgoDate = new \DateTime('15 days ago');
    	$strFifteenDaysAgoDate = $fifteenDaysAgoDate->format('Y-m-d H:i:s');    
    
    	$vmMessages = VoicemailMessages
            ::where('id_number', '=', $this->number->getID())
            ->where('date', '>', $strFifteenDaysAgoDate)
            ->orderBy('date', 'desc')
            ->paginate(10);
    
    	$viewData['vmMessages'] = $vmMessages;
    
    	$hasVoicemail = $this->number->hasVoicemail();
    	$viewData['hasVoicemail'] = $hasVoicemail;
    	$viewData['title'] = 'Buzón de voz';    
    
    	// Recuperamos las locuciones del usuario
    	// TODO: Filtrar sólo las locuciones de buzón
    	// de voz.
    	$user = $this->number->getUser();
    	$speeches = $user->speeches()->get();    

    	$viewData['speeches'] = $speeches;

    	// Recuperamos la dirección de correo para
    	// recibir los buzones y la locución elegida
    	// TODO: Hacer esto vía la API
    	$number = Number::find($this->number->getID());
	
		/*
		Configuración de Notificación whatsapp
		*/
		//-------------------------------------------
		$accion = 'buzon';
		$idUser = Session::get('user')->getID();
		$infoNumberWP = NumeroWhatsApp::where('idUser','=',$idUser)->where('numero_visible','=',$this->number->getNumber())->where('accion','=',$accion)->first();
		if($infoNumberWP)
		{
			$viewData['wp']   = $infoNumberWP->numerowp;
			$viewData['wp_activo']   = $infoNumberWP->activo;
			$viewData['wp_estado_api']   = $infoNumberWP->activo_api;
		}
		else
		{
			$viewData['wp']   = '';
			$viewData['wp_activo']   = 0;
		}
		$viewData['return']   = 'voicemail';
		$viewData['accion']   = $accion;
		//-------------------------------------------

    	$viewData['email']          = $number->mailvoz;
    	$viewData['selectedSpeech'] = $number->locucion_buz;
    
    	return View::make('content.voicemail.voicemail', $viewData);
  	}
  
  /**
   * Método que devuelve la ventana modal de los buzones de voz
   */
  public function returnModal()
  {
    // Vemos si ya había una locución seleccionada para 
    // el buzón de voz del servicio que estemos configurando
    $selectedSpeech = FALSE;
    
    if(!is_null($jsonAction = Input::get('jsonAction', NULL))) {
      $action = json_decode($jsonAction);
      
      if (!is_null($action) && isset($action->type)) {
        if($action->type === 'voicemail') {
          $selectedSpeech = $action->data->speech->name;
        }
      }
    }
    
    $user = $this->number->getUser();
   
    $viewData['selectedSpeech'] = $selectedSpeech;
    $viewData['speeches']       = $user->speeches()->get();
    $viewData['hideSaveButton'] = TRUE;    
    
    return View::make('components.actions.modals.voicemail', $viewData);
  }
  
  /**
   * Activa o desactiva el buzón de voz
   */
  public function setVoicemail()
  {
    $voicemail = Input::get('voicemail-toggle');
    $return = Input::get('return', 'voicemail');
    
    if($voicemail) {
      // Si los campos de correo y locucion de buzón no están seleccionados
      // no dejamos al usuario activar el servicio de buzón de voz      
      $number = Number::find($this->number->getID());

      $email          = $number->mailvoz;
      $selectedSpeech = $number->locucion_buz;
      
      if($email == null || $selectedSpeech == null) {
        $error = 'Debes configurar una locución y un correo de recepción de los
          mensajes de buzón de voz antes de activar el servicio.';
        return Redirect::to($return)->with('error', $error);
      }
    }
    
    $this->number->setVoicemail($voicemail ? TRUE : FALSE);
    
    if(!Voicemail::existsFor($this->number->getNumber())) {
      Voicemail::createFor($this->number->getNumber());
    }

    $this->refresh();
    
    $info = 'El servicio de buzón de voz ha sido correctamente <strong>';
    $info.= $voicemail ? 'activado' : 'desactivado';
    $info.= '</strong>.';
    
    return Redirect::to($return)->with('info', $info);
  }
  
  /**
   * Modifica la configuración del buzón de voz general
   * 
   * @todo validar
   */
  public function setConfig()
  {
    $postSpeech = Input::get('voicemail-speech');
    $postEmail  = Input::get('voicemail-email');        
    
    $number = Number::find($this->number->getID());
    
    $number->locucion_buz = $postSpeech;    
    $number->mailvoz      = $postEmail;

    if(!Voicemail::existsFor($this->number->getNumber())) {
      Voicemail::createFor($this->number->getNumber());
    }
    
    $voicemail = Voicemail::where('mailbox', '=', $this->number->getNumber())->firstOrFail();
    $voicemail->email = $postEmail;    
    $voicemail->save();
    
    if($number->save()) {
      $this->refresh();
      
      $info = 'La configuración del buzón de voz ha sido actualizada.';

      return Redirect::to('voicemail')->with('info', $info);
    } else {
      $error = 'Ha habido un error al actualizar la configuración del buzón de voz.';
      
      return Redirect::to('voicemail')->with('error', $error);
    }
  }

  /**
   * Elimina un mensaje de voz
   * 
   * @param integer $idMessage ID del mensaje de voz
   */
  public function remove($idMessage)
  {
    $number = Session::get('number');
    $idNumber = $number->getID();
    $idUser = $number->getUser()->getID();
    
    $vmMessage = VoicemailMessages::find($idMessage);
    
    if($vmMessage !== null && $vmMessage->id_number == $idNumber) {
      //$path = '/home/flash/data/' . $idUser . '/voicemail/' . $number . '/'; 
      $path = '/mnt/nfs_asterisk/data/' . $idUser . '/voicemail/' . $number . '/'; 
      $fileName = $number . '_' . $vmMessage->caller . '_' . 
                  (new \DateTime($vmMessage->date))->format('YmdHis') . '.wav';
      $fullPath = $path . $fileName;
      
      if(file_exists($fullPath)) {
        unlink($fullPath); 
        $caller = $vmMessage->caller;
        $vmMessage->delete();
        
        $info = "El mensaje de voz del número <strong>{$caller}</strong> ha sido eliminado.";
        return Redirect::to('voicemail')->with('info', $info);
      } else {
        return Redirect::to('voicemail');
      }      
    } else {
      return Redirect::to('voicemail');
    }  
  }
  
  /**
   * Descarga un mensaje de voz
   * 
   * @param integer $idMessage ID del mensaje de voz
   */
  public function download($idMessage)
  {
    $number = Session::get('number');
    $idNumber = $number->getID();
    $idUser = $number->getUser()->getID();
    
    $vmMessage = VoicemailMessages::find($idMessage);
    
    if($vmMessage !== null && $vmMessage->id_number == $idNumber) {
      //$path = '/home/flash/data/' . $idUser . '/voicemail/' . $number . '/'; 
      $path = '/mnt/nfs_asterisk/data/' . $idUser . '/voicemail/' . $number . '/'; 
      $fileName = $number . '_' . $vmMessage->caller . '_' . 
                  (new \DateTime($vmMessage->date))->format('YmdHis') . '.wav';
      $fullPath = $path . $fileName;
      
      $content = file_get_contents($fullPath);
      $response = Response::make($content, 200);

      $response->header('Content-Type', 'audio/x-wav, audio/wav');
      $response->header('Content-Description', 'File Transfer');
      $response->header('Content-length', strlen($content));
      $response->header('Content-Disposition', "attachment;filename=vm_" . $fileName);

      return $response;
    } else {
      return Redirect::to('voicemail');
    }
  }    
}
