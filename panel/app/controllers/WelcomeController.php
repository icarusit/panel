<?php

class WelcomeController extends BaseController
{
  /**
   * Método que se ejecuta cuando un cliente nuevo
   * hace el alta, dándole la bienvenida e invitándole
   * a que rellene la ficha de cliente por primera
   * vez
   */
  public function showFirst()
  {
	$results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();  
    return View::make('content.welcome.welcome', array('distribuidor'=>$results));
  }
  
  /**
   * Método que muestra la ficha de cliente para cliente
   * nuevo
   */
  public function showNewCustomerForm()
  { 
  
    $results = DB::table('distribuidores')->select('logo', 'id', 'nombre', 'dominio','telefono')->where('dominio', '=', ''.$_SERVER['HTTP_HOST'].'')->get();
	
    return View::make('content.welcome.new-customer-form', array(
      'new_client'   => false,
      'is_company'   => false,
      'province'     => '',
      'user_country' => null,
      'countries'    => Countries::get(),
	  'distribuidor' =>$results
    ));
  }
  
  /**
   * Método que muestra la ventana de selección de forma de pago
   */
  public function showPaymentMethods()
  {
    return View::make('content.welcome.payment-method', array(
      
    ));
  }
}

?>
