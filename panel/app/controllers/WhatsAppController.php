<?php
/**
 * Controlador para la administración de numeros de notificacion de whatsapp
 */
class WhatsAppController extends BaseController
{	
	/*
	Activar o desactivar el servicio de whatsapp
	*/
	public function setWhatsAppActive()
	{
		$enabled = Input::get('numero-toggle-wp');  
		$accion = Input::get('accion');  
    	$return = Input::get('return');
		
		$enabled = $enabled ? 1 : 0;
		
		$idUser = Session::get('user')->getID();
		$infoNumberWP = NumeroWhatsApp::where('idUser','=',$idUser)->where('numero_visible','=',$this->number->getNumber())->where('accion','=',$accion)->first();
		$resultado = false;
		if($infoNumberWP)
		{
			//Actualizar
			$infoNumberWP->activo = $enabled;
			$resultado = $infoNumberWP->save();
		}
		if($resultado)
		{
			$info = 'Has <strong>' . ($enabled ? 'activado' : 'desactivado') . '</strong> el servicio de notificación de whatsapp.';
      		$this->refresh();        
      		return Redirect::to($return)->with('info', $info);
		}
		else
		{
			$error = 'Ha habido un problema cambiando la configuración del servicio.';
      		return Redirect::to($return)->with('error', $error);
		}
	}
	/*
	Ingresar el número de whatsapp
	*/
	public function setWhatsAppNumber()
	{
		$numerowp = Input::get('numero-wp');
		$numerowp = str_replace ("+" ,"", $numerowp);

		$accion = Input::get('accion');
		$return = Input::get('return');
		
		if($numerowp == null || $numerowp == '')
		{
        	$error = 'Debes ingresar el número whatsapp para recibir las notificaciones';        
        	return Redirect::to($return)->with('error', $error);
      	}
		
		$idUser = Session::get('user')->getID();
		$infoNumberWP = NumeroWhatsApp::where('idUser','=',$idUser)->where('numero_visible','=',$this->number->getNumber())->where('accion','=',$accion)->first();
		if($infoNumberWP)
		{
			//Actualizar
			$infoNumberWP->numerowp = $numerowp;
			$resultado = $infoNumberWP->save();
		}
		else
		{
			//Insertar
			$nuevo = new NumeroWhatsApp;
			$nuevo->idUser = $idUser;
			$nuevo->numerowp = $numerowp;
			$nuevo->accion = $accion;
			$nuevo->numero_visible = $this->number->getNumber();
			$nuevo->activo = 1;
			$nuevo->activo_api = 0;
			$resultado = $nuevo->save();
		}
		if($resultado)
		{
			$info = 'Número whatsapp <strong>' . ($numerowp) . '</strong> adicionado correctamente al servicio de notificación';
      		$this->refresh();        
      		return Redirect::to($return)->with('info', $info);
		}
		else
		{
			$error = 'Ha habido un problema cambiando la configuración del servicio.';
      		return Redirect::to($return)->with('error', $error);
		}
	}
}