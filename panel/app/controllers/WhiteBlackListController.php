<?php

class WhiteBlackListController extends BaseController
{
  public function addToWhitelist()
  {
    $this->number->getWhitelist()->add(trim(Input::get('number')));

    return Redirect::to('whiteblacklist');    
  }
  
  public function addToBlacklist()
  {   
    $this->number->getBlacklist()->add(trim(Input::get('number')));
    $this->refresh();

    return Redirect::to('whiteblacklist');
  }
  
  public function showWhiteBlacklist()
  {
    $whitelist = $this->number->getWhitelist()->getArrayWhitelist();
    $blacklist = $this->number->getBlacklist()->getArrayBlacklist();    
    $settings = NumberSettings::find($this->number->getID());
    
    //var_dump($settings); exit;
    
    return View::make('content.whiteblacklist.whiteblacklist', array(
      'title'     => 'Lista blanca-negra',
      'whitelist' => $whitelist,
      'blacklist' => $blacklist,
      'settings'  => $settings
    ));
  }
  
  public function removeFromBlacklist($number)
  {
    $this->number->getBlacklist()->remove($number);
    $this->refresh();

    return Redirect::to('whiteblacklist');
  }
  
  public function removeFromWhitelist($number)
  {
    $this->number->getWhitelist()->remove(trim($number));
    $this->refresh();

    return Redirect::to('whiteblacklist');    
  }
  
  /**
   * Método que actualiza los parámetros de bloqueo de llamadas entrantes
   * 
   * @return
   */
  public function settings()
  {
    $idNumber = $this->number->getID();
    
    $settings = NumberSettings::find($idNumber);
   
    if($settings === NULL) {
      $settings = new NumberSettings;
      $settings->id_number = $idNumber;
    }
    
    $inputs = Input::except(array('_token'));
    
    foreach($inputs as &$input) {
      $input = $input == "false" ? FALSE : TRUE;
    }
    
    $settings->fill($inputs);
    
    if($settings->save()) {
      $info = 'El bloqueo de llamadas ha sido actualizado correctamente.';
    
      return Redirect::to('whiteblacklist')->with('info', $info);
    } else {
      $error = 'Ha ocurrido un error actualizando el bloqueo de llamadas.';
    
      return Redirect::to('whiteblacklist')->with('error', $error);      
    }
  }
}

?>
