<?php
$user   = Session::get('user', NULL);
$number = Session::get('number', NULL);

App::before(function($request)
{
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
    header('Access-Control-Allow-Credentials: true');
});

Route::filter('flash-auth', function() use($user) {
  $request = Request::path();
  if($user === NULL) { return Redirect::to('login?redirect=' . $request); }
});

Route::filter('user-has-filled-out-profile', function() use($user) {
  $user = User::find($user->getID());
  if(!$user->hasFilledOutProfile()) { return Redirect::to('welcome'); }
});

Route::filter('number-selected', function() use($number) {
  if($number === NULL) { return Redirect::to('numbers'); }
});

Route::filter('number-suspended', function() use($number) {
  if($number->isSuspended()) return View::make('suspended', array('number' => $number));
});

Route::filter('plan-allows-routing', function() use($number) {
  if($number !== NULL) {
    $plan = $number->getProfile()->getPlan();
    
    if (!in_array($plan, array('plus', 'premium'))) {
      return View::make('no_routing', array(
        'number' => $number,
        'plan'   => $plan
      ));
    }
  }
});

/**
 * Filtro para sólo permitir determinadas URL a las IPs de RedSys
 */
Route::filter('is-redsys', function() {
  if(!in_array($_SERVER['REMOTE_ADDR'], array('195.76.9.187', '195.76.9.222'))) {
    return Redirect::to('/');
  }
});

Route::filter('is-not-fax', function() use ($number) {
  if($number !== NULL && $number->getProfile()->getType() === 'fax')
    return Redirect::to('numbers');
});

Route::filter('is-fax', function() use($number) {
  if($number !== NULL && $number->getProfile()->getType() !== 'fax')
    return Redirect::to('numbers');
});

Route::filter('is-trunk', function() use($number) {
  if($number !== NULL && $number->getProfile()->getType() !== 'trunk')
    return Redirect::to('numbers');
});

Route::filter('is-voip', function() use($number) {
  if($number !== NULL && $number->getProfile()->getType() !== 'voip')
    return Redirect::to('numbers');
});

Route::filter('is-pbx', function() use($number) {
  if($number !== NULL && $number->getProfile()->getType() !== 'pbx')
    return Redirect::to('numbers');
});

Route::filter('number-is-voip-generic', function() use($number) {
  if($number !== NULL && 
     !in_array($number->getProfile()->getType(), array('voip', 'pbx','trunk')))
    return Redirect::to('numbers');  
});

Route::filter('number-is-analog', function() use($number) {
  if($number !== NULL && 
     $number->getProfile()->getType() !== 'analog')
    return Redirect::to('numbers');  
});

Route::filter('plan-is-plus', function() use($number) {
  // Si el plan no es plus aquí será premium, ya que antes de este filtro
  // pasó el filtro de plan-allows-routing, que sólo permite pasar a 
  // premium y plus
  if($number !== NULL && $number->getProfile()->getPlan() !== 'plus') {
    return View::make('no_routing', array(
      'number' => $number,
      'plan'   => 'premium'
    ));  
  }
});

Route::filter('must-be-ajax-request', function() {
  if (!Request::ajax()) App::abort('404');
  
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
