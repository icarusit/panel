<?php

class NIF 
{
  const NIF = 1;
  const CIF = 2;
  const NIE = 3;
  
  private $nif;
  
  /**
   * Constructor 
   * 
   * @param type $nif
   */
  public function __construct($nif)
  {
    $this->nif = $nif;
  }
  
  /**
   * Devuelve si el documento introducido es o no válido
   * 
   * @return boolean
   */
  public function isValid()
  {
    return $this->getDocumentType() !== false ? true : false;
  }
  
  /**
   * Devuelve el tipo de documento introducido
   * 
   * Fuente original:
   * http://da-software.blogspot.com.es/2011/12/comprobar-cif-nif-nie-con-php.html
   * 
   * @return int|false Tipo de documento o false si no es válido
   */
  public function getDocumentType()
  {
    $nif = strtoupper($this->nif);
  
    for($i=0; $i<9; $i++){
      $num[$i] = substr($nif, $i, 1);
    }

    // Si no tiene un formato valido devuelve
    if(!preg_match('/^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$|^[0-9]{8}[A-Z]{1}$/', $nif)) {
      return false;
    }    

    // Comprobación de NIFs estándar 
    if(preg_match('/^[0-9]{8}[A-Z]{1}$/', $nif)) {
      if($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', 
         substr($nif, 0, 8) % 23, 1)) {
        return self::NIF;
      } else {
        return false;
      }
    }
    
    // Comprobación de códigos tipo CIF
    $suma = $num[2] + $num[4] + $num[6];
    for ($i=1; $i<8; $i+=2){
      $suma += substr((2 * $num[$i]), 0, 1) + 
               substr((2 * $num[$i]), 1, 1);
    }
    $n = 10 - substr($suma, strlen($suma) - 1, 1);
    
    // Comprobación de NIFs especiales (se calculan como CIFs)
    if(preg_match('/^[KLM]{1}/', $nif)) {
      if($num[8] == chr(64 + $n)){
        return self::NIF;
      } else {
        return false;
      }
    }
    
    // Comprobación de CIFs
    if(preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $nif)) {
      if($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1)) {
        return self::CIF;
      } else {
        return false;
      }
    }
    
    // Comprobación de NIEs
    // - T
    if(preg_match('/^[T]{1}/', $nif)) {
      if($num[8] == preg_match('^[T]{1}[A-Z0-9]{8}$', $nif)){
        return self::NIE;
      } else {
        return false;
      }
    }
    
    // - XYZ
    if(preg_match('/^[XYZ]{1}/', $nif)) {
      if($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', 
         substr(str_replace(array('X','Y','Z'), 
         array('0','1','2'), $nif), 0, 8) % 23, 1)) {
        return self::NIE;
      } else {
        return false;
      }
    }
    
    // Si llegamos aquí y no tenemos si el doc es válido
    return false;
  }     
}