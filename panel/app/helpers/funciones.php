<?php
function color_celda($tipo,$anterior,$actual)
{
	$color_igual = '#FDFDC1';
	if($tipo == 'fecha_alta')
	{		
		$color_menor = '#FF6862';
		$color_mayor = '#B0D238';
	}
	else
	{
		$color_menor = '#B0D238';
		$color_mayor = '#FF6862';
	}
	if($anterior == $actual)
	{
		return $color_igual;
	}
	elseif($anterior > $actual)
	{
		return $color_menor;
	}
	elseif($anterior < $actual)
	{
		return $color_mayor;
	}
}
?>