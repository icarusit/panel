<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Factura Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'factura' => 'Invoice',
	'direccion_1' => 'VAT Number: ESB50979491',
	'direccion_2' => 'C/Lorente, 22 Local',
	'direccion_3' => '50005 * Zaragoza - SPAIN',
	'factura_numero' => 'Invoice #',
	'fecha' => 'Date',
	'concepto' => 'Concept',
	'importe' => 'Amount',
	'base_imponible' => 'Subtotal',
	'iva' => 'VAT',
	'total' => 'Total',
	'cuenta_cargo' => 'Client bank account',
	'transferencias' => 'Banck account for wire transfers',
	'ing' => 'ING',
	'ing_numero' => 'ES89 1465 0100 9519 0016 4115',
	'texto_pie_1' => 'En cumplimiento del artículo 5 de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de',
	'texto_pie_2' => 'Carácter Personal, se le informa que sus datos personales están incluidos en un fichero llamado "clientes" (código',
	'texto_pie_3' => 'de inscripción 2051120255), que el responsable de su tratamiento es Flash Media Europa, S.L., y que Vd. tiene',
	'texto_pie_4' => 'derecho a acceder a sus datos, a rectificarlos, a cancelarlos y a oponerse a su tratamiento, mediante solicitud',
	'texto_pie_5' => 'por escrito dirigida a Flash Media Europa, S.L.,C/Lorente, 22 Local 50005 Zaragoza. Email: info@fmeuropa.com',
	'texto_izquierda' => 'Flash Media Europa, S.L. B-50979491 Inscrita en el Registro Mercantil de Zaragoza Tomo: 2976 Folio: 43 Hoja: Z-34208 Inscripción: 1ª',

);