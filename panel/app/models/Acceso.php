<?php
class Acceso extends Eloquent 
{
  protected $table      = 'accesos';
  protected $primaryKey = 'id';  
  public    $timestamps = FALSE;
}
