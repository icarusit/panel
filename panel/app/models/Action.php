<?php
/**
 * Clase Action
 * 
 * La hacemos así de momento para salir del paso. Falta hacer Facade y que 
 * resuelva en el contenedor IoC.
 */
class Action
{
  /**
   * Acción
   * 
   * @var stdClass
   */
  protected $action;
  
  /**
   * Constructor
   * 
   * @param stdClass $action
   */
  public function __construct(stdClass $action = null)
  {
    $this->action = $action;
  }
  
  /**
   * Devuelve el resumen de una acción en palabras. Vale para los resúmenes
   * cuando un usuario ha escogido y configurado una acción.
   * 
   * @return string|FALSE
   */
  public function parse()
  {
    $action = $this->action;
    $panelMode = Session::get('panel_mode');

    if(!isset($action->type) || !isset($action->data)) {      
      return false;
    } else {
      if(!($action->data instanceof stdClass) || !is_string($action->type)) {
        return false;
      }
    }       
    
    switch($action->type) {
      case 'speech':
        $str = 'Reproducir locución "<strong>' . $action->data->speech->name . '</strong>".';
        break;
      case 'forward':
        $forwards = $action->data->forwards;
        $strForwards = '';
        $arrForwards = array();
        
        for($i=0; $i<count($forwards); $i++) {
          $arrForwards[] = '<strong>' . $forwards[$i]->forward . '</strong>' .
           ($panelMode === 'advanced' ? ' (' . $forwards[$i]->timeout . ' segundos)' : '');
        }
        
        $strForwards = implode(', ', $arrForwards);
        
        $str = 'Desviado a ' . $strForwards;
        break;
      case 'voicemail':
        $str = 'Desviado a buzón de voz con locución "<strong>' .
              $action->data->speech->name . '</strong>"';
        break;
      case 'text2speech':
        $str = 'Convertir texto "' . $action->data->text . '" a voz';
        break;
      default:
        return FALSE;
        break;
    }
  
    return $str;    
  }
  
  /**
   * Comprueba si una acción es o no válida
   * 
   * @return boolean
   */
  public function isValid()
  {
    $action = $this->action;

    if(!isset($action->type) || !isset($action->data)) {      
      return false;
    } else {
      if(!($action->data instanceof stdClass) || !is_string($action->type)) {
        return false;
      }
    }
    
    return true;
  }
}
