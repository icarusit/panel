<?php
class Call extends Eloquent 
{
  protected $table      = 'calls';  
  protected $primaryKey = 'id_call';    
  public    $timestamps = FALSE;
  	public function ContactoCaller()
  	{
	  	$contacto = ContactAgenda::where('number','=',$this->caller)->where('id_client','=',Session::get('user')->getID())->first();
	  	if($contacto)
	  	{
		 	 return $contacto->name;
	  	}
	  	else
	  	{
		 	 return $this->caller;
	  	}
  	}
	public function ContactoCalled()
  	{
	  	$contacto = ContactAgenda::where('number','=',$this->called)->where('id_client','=',Session::get('user')->getID())->first();
	  	if($contacto)
	  	{
		 	 return $contacto->name;
	  	}
	  	else
	  	{
		 	 return $this->called;
	  	}
  	}
}
