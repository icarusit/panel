<?php

class Cancellation extends Eloquent 
{
  protected $primaryKey = 'id_cancellation';    
  public    $timestamps = false;  
}
