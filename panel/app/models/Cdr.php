<?php
class Cdr extends Eloquent 
{
  	protected $table      = 'cdr';  
 	protected $primaryKey = 'id';    
  	public    $timestamps = FALSE;
	public function ContactoCaller()
  	{
	  	$contacto = ContactAgenda::where('number','=',$this->channel)->where('id_client','=',Session::get('user')->getID())->first();
	  	if($contacto)
	  	{
		 	 return $contacto->name;
	  	}
	  	else
	  	{
		 	 return $this->channel;
	  	}
  	}
	public function ContactoCalled()
  	{
	  	$contacto = ContactAgenda::where('number','=',$this->dst)->where('id_client','=',Session::get('user')->getID())->first();
	  	if($contacto)
	  	{
		 	 return $contacto->name;
	  	}
	  	else
	  	{
		 	 return $this->dst;
	  	}
  	}
	public function CuentaSip()
	{
		$cuenta_sip = str_ireplace('SIP/','',$this->channel);
		$desgloce_cuenta_sip = explode('-',$cuenta_sip);
		if(sizeof($desgloce_cuenta_sip) > 0)
		{
			return $desgloce_cuenta_sip[0];
		}
		else
		{
			return $cuenta_sip;
		}
	}
}
