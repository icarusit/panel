<?php
class ClienteNotificacion extends Eloquent 
{
  	protected $table      = 'cliente_notificacion';  
  	protected $primaryKey = 'id';    
 	public    $timestamps = FALSE;
}
