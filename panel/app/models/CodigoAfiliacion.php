<?php
class CodigoAfiliacion extends Eloquent 
{
  	protected $table      = 'code_afiliacion';  
  	protected $primaryKey = 'id';    
 	public    $timestamps = FALSE;
}
