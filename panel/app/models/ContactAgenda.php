<?php
class ContactAgenda extends Eloquent 
{
  	protected $table      = 'contact_agenda';  
  	protected $primaryKey = 'id';    
 	public    $timestamps = FALSE;
}
