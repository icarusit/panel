<?php
class FacturaConcepto extends Eloquent 
{
  	protected $table      = 'factura_concepto';  
  	protected $primaryKey = 'id';    
 	public    $timestamps = FALSE;
}
