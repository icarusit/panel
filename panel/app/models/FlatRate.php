<?php
class FlatRate extends Eloquent 
{
  protected $table      = 'flatrates';  
  protected $primaryKey = 'id_flatrate';    
  public    $timestamps = FALSE;  
}
