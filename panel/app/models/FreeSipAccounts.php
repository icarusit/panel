<?php
class FreeSipAccounts extends Eloquent 
{
  protected $table        = 'free_sippeers';  
  protected $primaryKey   = ['id_user', 'id_sippeer'];
  public    $incrementing = false;
  public    $timestamps   = false;
}
