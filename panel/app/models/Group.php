<?php
class Group extends Eloquent 
{
  	protected $table      = 'group';  
  	protected $primaryKey = 'id';    
 	public    $timestamps = FALSE;
	
	public function TotalMiembros()
	{
		if($this->contacts == NULL || trim($this->contacts) == '')
		{
			return 0;
		}
		else
		{
			$miembros = explode('&',$this->contacts);
			return sizeof($miembros);
		}
	}
	
	public function Miembros()
	{
		if($this->contacts == NULL || trim($this->contacts) == '')
		{
			return array();
		}
		else
		{
			$miembros = explode('&',$this->contacts);
			$miembros_limpios = array();
			foreach($miembros as  $miembro)
			{
				$reemplazo = str_ireplace("SIP/",'',$miembro);
				$reemplazo = str_ireplace("NSG/",'',$reemplazo);
				$miembros_limpios[] = $reemplazo;
			}
			return $miembros_limpios;
		}
	}
	
	public function MiembrosToolTip()
	{
		$miembros = $this->Miembros();
		return implode(', ',$miembros);
	}
}
