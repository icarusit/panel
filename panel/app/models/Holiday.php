<?php
class Holiday extends Eloquent 
{
  protected $primaryKey = 'id_holiday';
  public    $timestamps = false;

  /**
   * Método que comprueba si un festivo
   * es válido
   *
   * @return boolean
   */
  public function isValid()
  {
    try {
      $from = \DateTime::createFromFormat('Y-m-d', $this->from);    
    
      $until = null;
   
      if(!is_null($this->until)) { 
        $until = \DateTime::createFromFormat('Y-m-d', $this->until);      
      }
    } catch (\Exception $ex) {
      return false;
    }    
    
    if(!is_null($until)) {
      if($until < $from) {
        return false;
      }
    }

    $f_from  = $from->format('Y-m-d');

    if(is_null($until)) {
      $overlappedHolidays = Holiday
        ::where('id_number', '=', $this->id_number)
        ->whereRaw("`from` <= CAST('$f_from' AS DATE)
        AND `until` >= CAST('$f_from' AS DATE)")->get();
    } else {
      $f_until = $until->format('Y-m-d');

      $overlappedHolidays = Holiday
        ::where('id_number', '=', $this->id_number)
        ->where('active', '=', 1)
        ->whereRaw("((`from` < CAST('$f_from' AS DATE) AND 
        `until` > CAST('$f_from' AS DATE)) OR
        (`from` < CAST('$f_until' AS DATE) AND 
        `until` > CAST('$f_until' AS DATE)) OR
        (`from` > CAST('$f_from' AS DATE) AND 
        `until` < CAST('$f_until' AS DATE)))")
        ->whereRaw($this->id_holiday === null ? '1' : 'id_holiday != ' . $this->id_holiday)
        ->get(); 
    }

    return !count($overlappedHolidays);
  }
}
