<?php
class Number extends Eloquent 
{
  protected $table      = 'numero';  
  protected $primaryKey = 'id_numero';    
  public    $timestamps = FALSE;
  
  /**
   * Recupera los números en libres
   * 
   * @param string|array $prefixes Prefijos de los números a localizar
   * @param integer      $limit    Número máximo de números que devuelve
   *                               la consulta
   * 
   * @return type
   */
  public static function getAvailable($prefixes = null, $limit = 36)
  {  
    if($prefixes !== null) {
      $prefixesQuery = array();
      
      if(!is_array($prefixes)) {
        $prefixes = array($prefixes);
      }
      
      foreach($prefixes as &$prefix) {
        $prefixesQuery[] = 'numero_visible LIKE ?';
        $prefix = $prefix . '%';
      }
    }
    
    return Number::where('id_cliente', '=', 137)
          ->whereRaw($prefixes === null ? '1' : '(' . implode(' OR ', $prefixesQuery) . ')', $prefixes)
          ->take($limit)
          ->orderBy(DB::raw('RAND()'))
          ->get();
  }
  
  /**
   * Método que asigna un número a un usuario
   */
  public function assignTo(User $user, Profile $profile)
  {
    // Comprobamos que el número esté efectivamente libre
    if($this->id_cliente == 137) {
      $this->fecha_alta   = (new \DateTime())->format('Y-m-d');
      $this->facturable   = 'Si';
      $this->id_perfil    = $profile->id;
      $this->saldo        = 0.01;
      $this->numero_nuevo = 1;
      $this->id_cliente   = $user->id_cliente;

      if(strtolower($profile->tipo) === 'fax') {
        $this->tipo = 'FAX';
      }
      
      $this->save();
    }
  }
  
  /**
   * Método que recupera el tipo de un número
   */
  public function getType()
  {
    $profile = Profile::find($this->id_perfil);
    
    if($profile->tipo === null) {
      return 'analog';
    } else {
      return strtolower($profile->tipo);
    }
  }
  
  	public function minutos_recibidos()
	{
		$total_minutos = Call::select(DB::raw('ROUND(SUM(duration)/60, 2) as total_minutos'))->where('called','=',$this->numero_visible)->first();
		if($total_minutos)
		{
			return $total_minutos->total_minutos;
		}
		return 0;
	}
	
	public function perfilTipo()
	{
		$perfil = Profile::find($this->id_perfil);
		if($perfil)
		{
			return strtoupper($perfil->tipo);
		}
		return '';
	}
}
