<?php
class NumberFlatRate extends Eloquent 
{
  protected $table        = 'number_flatrates';  
  protected $primaryKey   = ['id_number', 'id_flatrate'];
  public    $incrementing = false;
  public    $timestamps   = false;
  
  public function flatrate()
  {
    return $this->hasOne('FlatRate', 'id_flatrate', 'id_flatrate');
  }
}
