<?php
class NumberSettings extends Eloquent 
{
  protected $table      = 'number_settings';
  protected $primaryKey = 'id_number';  
  protected $guarded    = array('id_number');
  public    $timestamps = FALSE;  
}
