<?php
class OriginAction extends Eloquent 
{
  protected $table      = 'prefixes_action';
  protected $primaryKey = 'id_prefix_action';  
  public    $timestamps = FALSE;
}
