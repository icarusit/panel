<?php
class PasswordRestoreTempTokens extends Eloquent 
{
  protected $table      = 'password_restore_temp_tokens';
  protected $primaryKey = 'id_token';
  public    $timestamps = false;  
  
  /**
   * Genera una cadena aleatoria de 128 caracteres para el token temporal
   * 
   * @return string
   */
  public static function generateStringToken()
  {
    $tokenLength = 128;
    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    
    $string = '';    
    for ($i = 0; $i < $tokenLength; $i++) {
         $string .= $characters[rand(0, strlen($characters) - 1)];
    }
    
    return $string;    
  }
  
  /**
   * Comprueba si el token es válido, es decir, si aún no ha expirado
   * 
   * @return boolean
   */
  public function isValid()
  {
    $expiration = '+1 day';
    
    // Comprobamos primero si ya ha sido utilizado
    if(!$this->used) {
      // Y ahora que el token no haya expirado
      $dateTokenCreated = new DateTime($this->date_added);
      $dateExpiration   = $dateTokenCreated->add(DateInterval::createFromDateString($expiration));      
      $now = new DateTime();
      
      return $now < $dateExpiration ? true : false;
    } else {
      return false;
    }
  }
}
