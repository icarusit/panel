<?php

class Payment extends Eloquent
{ 
  protected $table      = 'recargas';
  public    $timestamps = FALSE;
  
  /* Parámetros RedSys */
  protected static $merchantCode    = '46095089';
  protected static $currency        = '978';
  protected static $transactionType = '0';
  protected static $randomKey       = 'BXxVJGLRWCV2uljFK5OG0bfrAMVFtVX2';
  //protected static $randomKey       = 'iasjkahsdjahd8585585';
  
	/******  Array de DatosEntrada ******/
	protected static $version       = 'HMAC_SHA256_V1';
  
  /**
   * Marca un pago como completado
   * 
   * @return boolean TRUE si ha ido todo bien, FALSE si no
   */
  public function markAsPaid()
  {
    if($this->resultado === 'NA') {
      $this->resultado = 'OK';
      
      return $this->save();
    } else {
      return FALSE;
    }
  }
  
  /**
   * Devuelve la clave secreta
   * 
   * @return string
   */
  public static function getRandomKey()
  {
    return self::$randomKey;
  }
  
  /**
   * Devuelve los parámetros necesarios para calcular la firma del gateway
   * 
   * @param float  $amount   Importe total
   * @param string $order    Referencia transacción
   * 
   * @return array
   */
  public static function getParams($amount, $order) 
  {
    return array(
      'Ds_Merchant_Amount'          => $amount,
      'Ds_Merchant_Order'           => $order,
      'Ds_Merchant_MerchantCode'    => self::$merchantCode,
      'Ds_Merchant_Currency'        => self::$currency,
      'Ds_Merchant_TransactionType' => self::$transactionType,
      'Ds_Merchant_MerchantURL'     => URL::to('payment/callback')
    );    
  }
  
  /**
   * Genera firma
   * 
   * @param  array $params Parámetros base (self::getParams())
   * @return string
   */
  public static function generateSignature(array $params)
  {
    return strtoupper(sha1(implode($params) . self::$randomKey));
  }
  
  public static function RedsysAPI()
  {
	 $obj_api = new RedsysAPI;
	 return $obj_api; 
  }
  
  /**
   * Prepara los datos de la pasarela de pago y genera
   * el HTML del que se hará 'echo' para redirigir al 
   * usuario a la pasarela de pago
   * 
   * @param float  $amount Cantidad del pago
   * @param string $order  Referencia del pago
   * @param string $urlOK  Redirección cuando el pago ha sido
   *                       correcto
   * @param string $urlKO  Redirección cuando el pago ha sido
   *                       incorrecto
   * @return string HTML con la redirección a la pasarela
   */
  public static function redirectToGateway($amount, $order, $urlOK, $urlKO)
  {
    // Preparamos los parámetros para la pasarela de pago
    $amount  = str_replace(',', '.', $amount) * 100; 
    //$gateway = 'https://sis-d.redsys.es/sis/realizarPago';
	$gateway = 'https://sis.redsys.es/sis/realizarPago';
	
	$obj_api = new RedsysAPI;
	//PASO 1
	// Se Rellenan los campos
	$obj_api->setParameter("DS_MERCHANT_AMOUNT",$amount);
	$obj_api->setParameter("DS_MERCHANT_ORDER",strval($order));
	$obj_api->setParameter("DS_MERCHANT_MERCHANTCODE",self::$merchantCode);
	$obj_api->setParameter("DS_MERCHANT_CURRENCY",self::$currency);
	$obj_api->setParameter("DS_MERCHANT_TRANSACTIONTYPE",self::$transactionType);
	$obj_api->setParameter("DS_MERCHANT_TERMINAL",'001');
	$obj_api->setParameter("DS_MERCHANT_MERCHANTURL",URL::to('payment/callback'));
	$obj_api->setParameter("DS_MERCHANT_URLOK",$urlOK);		
	$obj_api->setParameter("DS_MERCHANT_URLKO",$urlKO);
	
	//PASO 2
	//Datos de configuración
	$version = self::$version;// "HMAC_SHA256_V1";
	$kc = self::$randomKey;//Clave recuperada de CANALES
	
	//PASO 3
	$params = $obj_api->createMerchantParameters();
	$signature = $obj_api->createMerchantSignature($kc);
	
	//PASO 4    
    // Generamos y devolvemos el salto a la pasarela de pago
    $html = "<html><body><form id='form' action='$gateway' method='post'>";

		$html.= "<input type='hidden' name='Ds_SignatureVersion' value='$version' />";
		$html.= "<input type='hidden' name='Ds_MerchantParameters' value='$params' />";
		$html.= "<input type='hidden' name='Ds_Signature' value='$signature' />";
    
    $html.= "</form><script>document.getElementById('form').submit();</script>";
    $html.= "</body></html>";  
   
    return $html;
  }
  
  public static function redirectToGateway_old($amount, $order, $urlOK, $urlKO)
  {
    // Preparamos los parámetros para la pasarela de pago
    $amount  = str_replace(',', '.', $amount) * 100; 
    $gateway = 'https://sis.redsys.es/sis/realizarPago';
    
    $params = self::getParams($amount, $order);
    $signature = self::generateSignature($params);

    $params['Ds_Merchant_Terminal'] = '001';
    $params['Ds_Merchant_MerchantSignature'] = $signature; 
    $params['Ds_Merchant_UrlOK'] = $urlOK;
    $params['Ds_Merchant_UrlKO'] = $urlKO;
    
    // Generamos y devolvemos el salto a la pasarela de pago
    $html = "<html><body><form id='form' action='$gateway' method='post'>";
    
    foreach($params as $param => $value) {
     $html.= "<input type='hidden' name='$param' value='$value' />";
    } 
    
    $html.= "</form><script>document.getElementById('form').submit();</script>";
    $html.= "</body></html>";  
   
    return $html;
  }
  
  /**
   * Método que registra un pago
   * 
   * @param float   $amount
   * @param string  $order     Referencia del pago
   * @param string  $type      'saldo' || 'factura'
   * @param integer $idInvoice Caso de que sea un pago de factura, el ID de la
   *                           factura
   */
  public static function register($amount, $order, $type = 'saldo', $idInvoice = null,$forma_pago = 'redsys')
  {
    // Contador del proceso de pago
    $contador = ((int)DB::table('recargas')->max('contador')) + 1;
    
    // ¡APAÑO ALERT!
    // 
    // Recuperamos el id_numero de alguno de sus números para no romper
    // el programa de gestión y seguir pudiendo ver desde ahí las recargas
    // del usuario
    //
    $user = Session::get('user');
    $idUser = $user->getID();
   
    if(Session::get('number', null) !== null) {
      $idNumber = Session::get('number')->getID();
    } else { 
      $idNumber = Number::where('id_cliente', '=', $idUser)->pluck('id_numero');    
    }
   
    // ¡APAÑO ALERT!
    //
    // Si el cliente no tiene números contratados, no se recuperará ningún
    // id_numero, por lo que metemos cualquier dato
    $idNumber = $idNumber !== null ? $idNumber : 111111;
 
    // Insertamos un registro en la tabla de recargas con el intento
    $row = new self;
    
    if($type === 'factura') {
      $row->id_factura = $idInvoice;
    }
    
    $row->id_cliente  = $idUser;
    $row->id_numero   = $idNumber;
    $row->fecha       = date('Y-m-d H:i:s', time());    
    $row->tipo        = in_array($type, array('saldo', 'factura', 'tplana')) ? $type : 'factura';    
    $row->importe     = str_replace(',', '.', $amount);
    $row->referencia  = $order;
    $row->contador    = $contador;
    $row->resultado   = 'NA';
	$row->formapago   = $forma_pago;
    
    // Si es una recarga, almacenamos el saldo que tenía anteriormente
    if($type === 'saldo') {      
      $row->saldo_antes = $user->getBalance();
    }
    
    return $row->save();
  }
  
	public static function my_bcmod( $x, $y ) 
	{ 
		// how many numbers to take at once? carefull not to exceed (int) 
		$take = 5;     
		$mod = ''; 
	
		do 
		{ 
			$a = (int)$mod.substr( $x, 0, $take ); 
			$x = substr( $x, $take ); 
			$mod = $a % $y;    
		} 
		while ( strlen($x) ); 
	
		return (int)$mod; 
	} 
  	public static function checkIBAN($iban)
	{
		if(strlen($iban) == 24 || strlen($iban) == 20)
		{
			$iban = strtolower(str_replace(' ','',$iban));
			$Countries = array('al'=>28,'ad'=>24,'at'=>20,'az'=>28,'bh'=>22,'be'=>16,'ba'=>20,'br'=>29,'bg'=>22,'cr'=>21,'hr'=>21,'cy'=>28,'cz'=>24,'dk'=>18,'do'=>28,'ee'=>20,'fo'=>18,'fi'=>18,'fr'=>27,'ge'=>22,'de'=>22,'gi'=>23,'gr'=>27,'gl'=>18,'gt'=>28,'hu'=>28,'is'=>26,'ie'=>22,'il'=>23,'it'=>27,'jo'=>30,'kz'=>20,'kw'=>30,'lv'=>21,'lb'=>28,'li'=>21,'lt'=>20,'lu'=>20,'mk'=>19,'mt'=>31,'mr'=>27,'mu'=>30,'mc'=>27,'md'=>24,'me'=>22,'nl'=>18,'no'=>15,'pk'=>24,'ps'=>29,'pl'=>28,'pt'=>25,'qa'=>29,'ro'=>24,'sm'=>27,'sa'=>24,'rs'=>22,'sk'=>24,'si'=>19,'es'=>24,'se'=>24,'ch'=>21,'tn'=>24,'tr'=>26,'ae'=>23,'gb'=>22,'vg'=>24);
			$Chars = array('a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15,'g'=>16,'h'=>17,'i'=>18,'j'=>19,'k'=>20,'l'=>21,'m'=>22,'n'=>23,'o'=>24,'p'=>25,'q'=>26,'r'=>27,'s'=>28,'t'=>29,'u'=>30,'v'=>31,'w'=>32,'x'=>33,'y'=>34,'z'=>35);
		
			if(strlen($iban) == $Countries[substr($iban,0,2)])
			{
		
				$MovedChar = substr($iban, 4).substr($iban,0,4);
				$MovedCharArray = str_split($MovedChar);
				$NewString = "";
		
				foreach($MovedCharArray AS $key => $value)
				{
					if(!is_numeric($MovedCharArray[$key]))
					{
						$MovedCharArray[$key] = $Chars[$MovedCharArray[$key]];
					}
					$NewString .= $MovedCharArray[$key];
				}
		
				if(self::my_bcmod($NewString, '97') == 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
  /**
   * Valida un número de cuenta
   * 
   * Copypasteado en plan cutre desde 
   * http://www.neleste.com/validar-ccc-con-php/
   * 
   * @param string $bankAccountNumber Número de cuenta bancaria (CCC)
   */
  public static function isBankAccountNumberValid($bankAccountNumber) {
    if(!preg_match('/^\d{20}$/', $bankAccountNumber)) { return false; }
    
    $ccc = str_replace(' ', '', $bankAccountNumber);
    $valido = true;

    ///////////////////////////////////////////////////
    //    Dígito de control de la entidad y sucursal:
    //Se multiplica cada dígito por su factor de peso
    ///////////////////////////////////////////////////
    $suma = 0;
    $suma += $ccc[0] * 4;
    $suma += $ccc[1] * 8;
    $suma += $ccc[2] * 5;
    $suma += $ccc[3] * 10;
    $suma += $ccc[4] * 9;
    $suma += $ccc[5] * 7;
    $suma += $ccc[6] * 3;
    $suma += $ccc[7] * 6;

    $division = floor($suma/11);
    $resto    = $suma - ($division  * 11);
    $primer_digito_control = 11 - $resto;
    if($primer_digito_control == 11)
        $primer_digito_control = 0;

    if($primer_digito_control == 10)
        $primer_digito_control = 1;

    if($primer_digito_control != $ccc[8])
        $valido = false;

    ///////////////////////////////////////////////////
    //            Dígito de control de la cuenta:
    ///////////////////////////////////////////////////
    $suma = 0;
    $suma += $ccc[10] * 1;
    $suma += $ccc[11] * 2;
    $suma += $ccc[12] * 4;
    $suma += $ccc[13] * 8;
    $suma += $ccc[14] * 5;
    $suma += $ccc[15] * 10;
    $suma += $ccc[16] * 9;
    $suma += $ccc[17] * 7;
    $suma += $ccc[18] * 3;
    $suma += $ccc[19] * 6;

    $division = floor($suma/11);
    $resto = $suma-($division  * 11);
    $segundo_digito_control = 11- $resto;

    if($segundo_digito_control == 11)
        $segundo_digito_control = 0;
    if($segundo_digito_control == 10)
        $segundo_digito_control = 1;

    if($segundo_digito_control != $ccc[9])
        $valido = false;

    return $valido;
  } 
}

/*
API
*/
class RedsysAPI{

	/******  Array de DatosEntrada ******/
    var $vars_pay = array();
	
	/******  Set parameter ******/
	function setParameter($key,$value){
		$this->vars_pay[$key]=$value;
	}

	/******  Get parameter ******/
	function getParameter($key){
		return $this->vars_pay[$key];
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	////////////					FUNCIONES AUXILIARES:							  ////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	

	/******  3DES Function  ******/
	function encrypt_3DES($message, $key){
		// Se establece un IV por defecto
		$bytes = array(0,0,0,0,0,0,0,0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
		$iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2

		// Se cifra
		$ciphertext = mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
		return $ciphertext;
	}

	/******  Base64 Functions  ******/
	function base64_url_encode($input){
		return strtr(base64_encode($input), '+/', '-_');
	}
	function encodeBase64($data){
		$data = base64_encode($data);
		return $data;
	}
	function base64_url_decode($input){
		return base64_decode(strtr($input, '-_', '+/'));
	}
	function decodeBase64($data){
		$data = base64_decode($data);
		return $data;
	}

	/******  MAC Function ******/
	function mac256($ent,$key){
		$res = hash_hmac('sha256', $ent, $key, true);//(PHP 5 >= 5.1.2)
		return $res;
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	////////////	   FUNCIONES PARA LA GENERACIÓN DEL FORMULARIO DE PAGO:			  ////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	/******  Obtener Número de pedido ******/
	function getOrder(){
		$numPedido = "";
		if(empty($this->vars_pay['DS_MERCHANT_ORDER'])){
			$numPedido = $this->vars_pay['Ds_Merchant_Order'];
		} else {
			$numPedido = $this->vars_pay['DS_MERCHANT_ORDER'];
		}
		return $numPedido;
	}
	/******  Convertir Array en Objeto JSON ******/
	function arrayToJson(){
		$json = json_encode($this->vars_pay); //(PHP 5 >= 5.2.0)
		return $json;
	}
	function createMerchantParameters(){
		// Se transforma el array de datos en un objeto Json
		$json = $this->arrayToJson();
		// Se codifican los datos Base64
		return $this->encodeBase64($json);
	}
	function createMerchantSignature($key){
		// Se decodifica la clave Base64
		$key = $this->decodeBase64($key);
		// Se genera el parámetro Ds_MerchantParameters
		$ent = $this->createMerchantParameters();
		// Se diversifica la clave con el Número de Pedido
		$key = $this->encrypt_3DES($this->getOrder(), $key);
		// MAC256 del parámetro Ds_MerchantParameters
		$res = $this->mac256($ent, $key);
		// Se codifican los datos Base64
		return $this->encodeBase64($res);
	}
	


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////// FUNCIONES PARA LA RECEPCIÓN DE DATOS DE PAGO (Notif, URLOK y URLKO): ////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////

	/******  Obtener Número de pedido ******/
	function getOrderNotif(){
		$numPedido = "";
		if(empty($this->vars_pay['Ds_Order'])){
			$numPedido = $this->vars_pay['DS_ORDER'];
		} else {
			$numPedido = $this->vars_pay['Ds_Order'];
		}
		return $numPedido;
	}

	function getOrderNotifSOAP($datos){
		$posPedidoIni = strrpos($datos, "<Ds_Order>");
		$tamPedidoIni = strlen("<Ds_Order>");
		$posPedidoFin = strrpos($datos, "</Ds_Order>");
		return substr($datos,$posPedidoIni + $tamPedidoIni,$posPedidoFin - ($posPedidoIni + $tamPedidoIni));
	}
	function getRequestNotifSOAP($datos){
		$posReqIni = strrpos($datos, "<Request");
		$posReqFin = strrpos($datos, "</Request>");
		$tamReqFin = strlen("</Request>");
		return substr($datos,$posReqIni,($posReqFin + $tamReqFin) - $posReqIni);
	}
	function getResponseNotifSOAP($datos){
		$posReqIni = strrpos($datos, "<Response");
		$posReqFin = strrpos($datos, "</Response>");
		$tamReqFin = strlen("</Response>");
		return substr($datos,$posReqIni,($posReqFin + $tamReqFin) - $posReqIni);
	}
	/******  Convertir String en Array ******/
	function stringToArray($datosDecod){
		$this->vars_pay = json_decode($datosDecod, true); //(PHP 5 >= 5.2.0)
	}
	function decodeMerchantParameters($datos){
		// Se decodifican los datos Base64
		$decodec = $this->base64_url_decode($datos);
		return $decodec;	
	}
	function createMerchantSignatureNotif($key, $datos){
		// Se decodifica la clave Base64
		$key = $this->decodeBase64($key);
		// Se decodifican los datos Base64
		$decodec = $this->base64_url_decode($datos);
		// Los datos decodificados se pasan al array de datos
		$this->stringToArray($decodec);
		// Se diversifica la clave con el Número de Pedido
		$key = $this->encrypt_3DES($this->getOrderNotif(), $key);
		// MAC256 del parámetro Ds_Parameters que envía Redsys
		$res = $this->mac256($datos, $key);
		// Se codifican los datos Base64
		return $this->base64_url_encode($res);	
	}
	/******  Notificaciones SOAP ENTRADA ******/
	function createMerchantSignatureNotifSOAPRequest($key, $datos){
		// Se decodifica la clave Base64
		$key = $this->decodeBase64($key);
		// Se obtienen los datos del Request
		$datos = $this->getRequestNotifSOAP($datos);
		// Se diversifica la clave con el Número de Pedido
		$key = $this->encrypt_3DES($this->getOrderNotifSOAP($datos), $key);
		// MAC256 del parámetro Ds_Parameters que envía Redsys
		$res = $this->mac256($datos, $key);
		// Se codifican los datos Base64
		return $this->encodeBase64($res);	
	}
	/******  Notificaciones SOAP SALIDA ******/
	function createMerchantSignatureNotifSOAPResponse($key, $datos, $numPedido){
		// Se decodifica la clave Base64
		$key = $this->decodeBase64($key);
		// Se obtienen los datos del Request
		$datos = $this->getResponseNotifSOAP($datos);
		// Se diversifica la clave con el Número de Pedido
		$key = $this->encrypt_3DES($numPedido, $key);
		// MAC256 del parámetro Ds_Parameters que envía Redsys
		$res = $this->mac256($datos, $key);
		// Se codifican los datos Base64
		return $this->encodeBase64($res);	
	}
}


?>
