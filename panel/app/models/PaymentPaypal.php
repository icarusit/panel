<?php

class PaymentPaypal extends Eloquent
{ 
  protected $table      = 'recargas';
  public    $timestamps = FALSE;
  
  /* Parámetros RedSys */
  protected static $cmd        = '_xclick';
  protected static $business   = 'info@fmeuropa.com';
  protected static $currency   = 'EUR';
  
  /**
   * Marca un pago como completado
   * 
   * @return boolean TRUE si ha ido todo bien, FALSE si no
   */
	public function markAsPaid()
  	{
    	if($this->resultado === 'NA')
		{
      		$this->resultado = 'OK';
      
      		return $this->save();
    	}
		else
		{
      		return FALSE;
    	}
  	}  
  
	  /**
	   * Prepara los datos de la pasarela de pago y genera
	   * el HTML del que se hará 'echo' para redirigir al 
	   * usuario a la pasarela de pago
	   * 
	   * @param float  $amount Cantidad del pago
	   * @param string $order  Referencia del pago
	   * @param string $urlOK  Redirección cuando el pago ha sido
	   *                       correcto
	   * @param string $urlKO  Redirección cuando el pago ha sido
	   *                       incorrecto
	   * @return string HTML con la redirección a la pasarela
	   */
 	public static function redirectToGateway($amount, $order, $urlOK, $urlFinal,$producto = 'Recarga')
  	{
    	// Preparamos los parámetros para la pasarela de pago
    	$amount  = str_replace(',', '.', $amount); 
    	//$gateway = 'https://sis-d.redsys.es/sis/realizarPago';
		$gateway = 'https://www.paypal.com/cgi-bin/webscr';

    	// Generamos y devolvemos el salto a la pasarela de pago
    	$html = "<html>";
			$html.= "<body>";
				$html.= "<form id='form' action='$gateway' method='post'>";
					$html.= "<input type='hidden' name='cmd' value='".self::$cmd."' />";
					$html.= "<input type='hidden' name='no_note' value='1' />";
					$html.= "<input type='hidden' name='currency_code' value='".self::$currency."' />";
					$html.= "<input type='hidden' name='amount' value='".$amount."' />";
					$html.= "<input type='hidden' name='item_name' value='".$producto."' />";
					$html.= "<input type='hidden' name='item_number' value='".$amount."' />";
					$html.= "<input type='hidden' name='custom' value='".$order."' />";
					$html.= "<input type='hidden' name='option_index' value='0' />";
    				$html.= "<input type='hidden' name='quantity' value='1' />";
					$html.= "<input type='hidden' name='business' value='".self::$business."' />";
					$html.= "<input type='hidden' name='notify_url' value='".$urlOK."' />";
					$html.= "<input type='hidden' name='return' value='".$urlFinal."' />";
    			$html.= "</form>";
				$html.= "<script>document.getElementById('form').submit();</script>";
    		$html.= "</body>";
		$html.= "</html>";  
   
    	return $html;
  	}

	  /**
	   * Método que registra un pago
	   * 
	   * @param float   $amount
	   * @param string  $order     Referencia del pago
	   * @param string  $type      'saldo' || 'factura'
	   * @param integer $idInvoice Caso de que sea un pago de factura, el ID de la
	   *                           factura
	   */
 	public static function register($amount, $order, $type = 'saldo', $idInvoice = null)
  	{
    	// Contador del proceso de pago
    	$contador = ((int)DB::table('recargas')->max('contador')) + 1;
    
    	// ¡APAÑO ALERT!
    	// 
    	// Recuperamos el id_numero de alguno de sus números para no romper
    	// el programa de gestión y seguir pudiendo ver desde ahí las recargas
    	// del usuario
    	//
    	$user = Session::get('user');
    	$idUser = $user->getID();
   
    	if(Session::get('number', null) !== null)
		{
      		$idNumber = Session::get('number')->getID();
    	}
		else
		{ 
      		$idNumber = Number::where('id_cliente', '=', $idUser)->pluck('id_numero');    
    	}
   
    	// ¡APAÑO ALERT!
    	//
    	// Si el cliente no tiene números contratados, no se recuperará ningún
    	// id_numero, por lo que metemos cualquier dato
    	$idNumber = $idNumber !== null ? $idNumber : 111111;
 
    	// Insertamos un registro en la tabla de recargas con el intento
    	$row = new self;
    
    	if($type === 'factura')
		{
      		$row->id_factura = $idInvoice;
   	 	}
    
    	$row->id_cliente  = $idUser;
    	$row->id_numero   = $idNumber;
    	$row->fecha       = date('Y-m-d H:i:s', time());    
    	$row->tipo        = in_array($type, array('saldo', 'factura', 'tplana')) ? $type : 'factura';    
    	$row->importe     = str_replace(',', '.', $amount);
    	$row->referencia  = $order;
    	$row->contador    = $contador;
    	$row->resultado   = 'NA';
    
    	// Si es una recarga, almacenamos el saldo que tenía anteriormente
    	if($type === 'saldo')
		{      
      		$row->saldo_antes = $user->getBalance();
    	}
    
    	return $row->save();
  	}
}
?>