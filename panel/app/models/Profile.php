<?php
class Profile extends Eloquent 
{
  protected $table      = 'perfiles';  
  public    $timestamps = FALSE;
  
  /**
   * Recupera un listado de perfiles por tipo de servicio
   * 
   * @param  string $productType Tipo de servicio
   * @return array
   */
  public static function getByProductType($productType)
  {
    return self::where('product_type', '=', $productType)
        ->where('can_order_online', '=', true)
        ->orderBy('order')
        ->get();
  }
  
  /**
   * Recupera las ventajas de un plan
   * 
   * @return array
   */
  public function getPros() 
  {
    $sql = 'SELECT p.pro, p.tooltip FROM pros p, pros_plans pp WHERE p.id_pro = pp.id_pro
            AND pp.id_profile = ' . $this->id;
    
    return DB::select($sql);           
  }
  
  /**
   * Recupera las diferencias entre ventajas del plan
   * y un plan que se pasa como argumento
   * 
   * Los perfiles deberían tener el mismo product_type
   * 
   * @param Profile $profile Perfil con el que se va
   *                         a comparar
   * 
   * @return array
   */
  public function getDiffPros(Profile $profile) 
  {
    $pros = array();   

    $sqlPlus = "SELECT p.pro, p.tooltip FROM pros p INNER JOIN pros_plans pp
      ON p.id_pro = pp.id_pro WHERE (p.id_pro NOT IN (SELECT id_pro FROM 
      pros_plans pp WHERE id_profile = {$this->id})) AND 
      pp.id_profile = {$profile->id}";
    
    $pros['plus'] = DB::select($sqlPlus);      
    
    $sqlMinus = "SELECT p.pro, p.tooltip FROM pros p INNER JOIN pros_plans pp
      ON p.id_pro = pp.id_pro WHERE (p.id_pro NOT IN (SELECT id_pro FROM 
      pros_plans pp WHERE id_profile = {$profile->id})) AND 
      pp.id_profile = {$this->id}";
    
    $pros['minus'] = DB::select($sqlMinus);  
    
    return $pros;
  }  
  
  /**
   * Devuelve un perfil de acuerdo con el nombre compuesto de servicio + plan
   * que usamos para el alta
   * 
   * 902_plus -> "902 Plus"
   * geografico_basico -> "Geográfico Básico"
   * ...
   * 
   * @param  string $plan 
   * @return Profile|null
   */
  public static function getByPostString($plan)
  {
    if(strpos($plan, '_') === false) return null;
    
    $arrPlan = explode('_', $plan);
    
    $productType = $arrPlan[0];
    $plan = $arrPlan[1];
    
    return self::where('product_type', '=', $productType)
        ->where('plan', '=', $plan)
        ->where('can_order_online', '=', true)
        ->first();
  }
  
  /**
   * Método que recupera un plan IP-pbx en función de
   * la cadena de observaciones que tiene el siguiente
   * formato:
   * 
   * "IP_{num-did}_{num-cuentas-sip}_{num-canales}_{0->sin cent, 1->con cent}"
   * 
   * @param string $text Cadena de observaciones
   * 
   * @return Profile|null Devuelve null si la cadena es
   *                      errónea o si el plan no existe
   */
  public static function getIPPlanByText($text)
  {
    return self::where('observaciones', '=', $text)
        ->first();    
  }
  
  /**
   * Si se llama a este método y el plan es un plan IP/PBX
   * se devuelve en un objeto de stdClass la información
   * del plan, número de DIDs, canales, IP y flag de si 
   * tiene o no centralita.
   * 
   * @return stdClass|false Falso si no es plan IP-PBX
   */
	public function getInfoIPPlan()
  	{    
    	if(in_array($this->getType(), array('voip', 'pbx','trunk')))
		{
      		if(substr($this->observaciones, 0, 3) !== 'IP_' && substr($this->observaciones, 0, 3) !== 'ST_')
			{
        		return false;
      		}
      
      		$rawInfo = explode('_', $this->observaciones);
     		
      		if(isset($rawInfo[5]))
			{
        		$limit = (int)$rawInfo[5];
      		}
			else
			{
        		$limit = 0;
      		}
 
      		return (object)array(
				'num_dids'     => (int)$rawInfo[1],
				'sip_accounts' => (int)$rawInfo[2],
				'channels'     => (int)$rawInfo[3],        
				'pbx'          => $rawInfo[4] == 1 ? true : false,
				'limit'        => $limit
      		);      
    	}
		else
		{
      		return false;
    	}
	}
  
  /**
   * Devuelve el tipo de perfil (voip, analog, etc.)
   * 
   * @return string
   */
  public function getType()
  {
    return is_null($this->tipo)
      ? 'analog'
      : strtolower($this->tipo);
  }
}
