<?php

namespace Panel\Model;

class Queue
{
  	public static function parse($str)
  	{
    	if(trim($str) !== '')
		{
			$queue = str_replace('SIP/NSG/', '', $str);
			$queue = str_replace('SIP/', '', $queue);
			$queue = explode(',', $queue);
    
      		foreach($queue as &$row)
			{
				$row = explode('&', $row);
			}
    	}
		else
		{
      		$queue = false;
    	}

    	return $queue;  
  	}
}
