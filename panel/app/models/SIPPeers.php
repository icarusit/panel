<?php
class SIPPeers extends Eloquent 
{
  protected $table      = 'sippeers';
  protected $primaryKey = 'id_serial';  
  public    $timestamps = FALSE;
  
  /**
   * Método que recupera los peers para un usuario dado y opcionalmente
   * un número dado
   * 
   * @param integer $idUser
   * @param Number  $number
   * @param boolean $namesOnly Apaño. Si se pasa 'true', devolverá únicamente
   *                           un array con los nombres de las cuentas, a 'false'
   *                           devolverá un array con los objetos completos de 
   *                           las cuentas SIP
   * @return array
   */
  public static function getAccounts(
    $idUser, 
    Number $number = null,
    $namesOnly = true
  ) {
    $sipPeers = self::where('id_cliente', '=', $idUser);
        
    if($number === null) {
      $sipPeers = $sipPeers->get();
    } else {
      $sipPeers = $sipPeers
              ->where('callerid', '=', $number->numero_visible)
              ->get();
    }

    if(!$namesOnly) {
      return $sipPeers;
    } else {
      $accounts = array();

      foreach($sipPeers as $row)
      {
        $accounts[] = $row->defaultuser;
      }
      
      return $accounts;      
    }
  }
  
  /**
   * Método que añade una nueva cuenta SIP para un cliente y número
   * dado
   * 
   * @param User    $user
   * @param Number  $number
   * @param integer $priority  Prioridad como extensión
   * @param integer $callLimit Número tope de llamadas (núm canales)
   * @param boolean $returnNewAccount Si TRUE devuelve el objeto de la cuenta
   *                                  Si FALSE devuelve si se pudo o no crear
   * @return SIPPeer|boolean
   */
  public static function add(
    $user, 
    $number = null,
    $priority = 1, 
    $callLimit = 2,
    $returnNewAccount = false,
    $insecure = 'port,invite'
  ) {
    $peer = new self;
    
    // Generamos nombre de usuario aleatorio y comprobamos que no 
    // exista ya
    do {
      $username = $user->usr_usuario . '_' . rand(10000, 99999);

      	$peer->name        = $username;      
      	$peer->defaultuser = $username;

    } while(count(SIPPeers::where('defaultuser', '=', $username)->get()) > 0);       
            
    // generación de password: http://stackoverflow.com/a/13809110
    $peer->secret  = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 12);
    $peer->insecure = $insecure;
    $peer->mailbox        = $user->mail_1;    
    
    if($number instanceof Number) {
      $peer->callerid = $number->numero_visible; 
    } else {
      $peer->callerid = '000000000';
    }
    
    $peer->{'call-limit'} = $callLimit;
    $peer->extension      = $user->id_cliente . str_pad($priority, 2, '0', STR_PAD_LEFT);
    $peer->id_cliente     = $user->id_cliente;
    $peer->context        = 'prepago';
    
    $saved = $peer->save();
    
    return $returnNewAccount ? $peer : $saved;
  }  
}
