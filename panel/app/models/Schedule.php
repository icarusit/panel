<?php
class Schedule extends Eloquent 
{
  protected $primaryKey = 'id_schedule';  
  public    $timestamps = FALSE;
  
  /**
   * Método que comprueba si un horario es válido
   * 
   * @return boolean
   */
  public function isValid()
  {
    $numOverlapped = 0;
    
    try {
      $from = \DateTime::createFromFormat('H:i', $this->from);      
      $until = \DateTime::createFromFormat('H:i', $this->until);      
    } catch (\Exception $ex) {
      return false;
    }    
    
    if($until <= $from) {
      return false;
    }       

    $overlappedSchedules = Schedule      
      ::where('id_number', '=', $this->id_number)
      ->where('week_day', '=', $this->week_day)
      ->whereRaw("((`from` <= CAST('{$this->from}' AS TIME) AND "
      . "`until` > CAST('{$this->from}' AS TIME)) OR "
      . "(`from` < CAST('{$this->until}' AS TIME) AND "
      . "`until` >= CAST('{$this->until}' AS TIME)))")
      ->whereRaw($this->id_schedule === null ? '1' : 'id_schedule != ' . $this->id_schedule)
      ->get();

    return !count($overlappedSchedules);
  }
}
