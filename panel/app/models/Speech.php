<?php
class Speech extends Eloquent 
{
  protected $table      = 'speeches';  
  protected $primaryKey = 'id_speech';  
  public    $timestamps = FALSE;
}