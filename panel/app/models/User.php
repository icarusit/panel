<?php
class User extends Eloquent 
{
  protected $table      = 'cliente';
  protected $primaryKey = 'id_cliente';  
  public    $timestamps = FALSE;  
  
  /**
   * Método que comprueba si existe un usuario
   * con el nombre de usuario $username
   * 
   * @param string $username
   * @return boolean
   */
  public static function exists($username)
  {
    $user = User::where('usr_usuario', '=', $username)
                ->get();
    
    return count($user) > 0;
  }
  
  /**
   * Método que comprueba si el usuario es dueño de un
   * número dado
   * 
   * @param string $number Número
   */
  public function hasNumber($number)
  {
    $x = Number::where('id_cliente', '=', $this->id_cliente)
            ->where('numero_visible', '=', $number)
            ->get();
    
    return count($x) > 0;
  }

  /**
   * Método que comprueba si el usuario ha rellenado su ficha
   * 
   * @return type
   */
  public function hasFilledOutProfile()
  {
    // <ÑAPA>    
    $filled_out = $this->cliente_nuevo !== 1;
    $filled_out = $filled_out && (trim($this->nombre_fiscal) === '' || 
            trim($this->nombre_fiscal) === '\'');    
    $filled_out = $filled_out && (trim($this->NIF) === '' || 
            trim($this->NIF) === '\'');   
    $filled_out = $filled_out && (trim($this->ccc_pagos) === '' || 
            trim($this->ccc_pagos) === '\'');   
    $filled_out = $filled_out && (trim($this->rep_pagos) === '' || 
            trim($this->rep_pagos) === '\'');   
    $filled_out = $filled_out && (trim($this->rep_legal) === '' || 
            trim($this->rep_legal) === '\'');   
    $filled_out = $filled_out && (trim($this->rep_legal_dni) === '' || 
            trim($this->rep_legal_dni) === '\'');
    // </ÑAPA>   
    
    return !$filled_out;
  }
}
