<?php
class Voicemail extends Eloquent 
{
  protected $table      = 'voicemail';  
  protected $primaryKey = 'uniqueid';    
  public    $timestamps = FALSE;
  
  /**
   * Método que comprueba si la fila de buzón de voz existe
   * para un número dado
   * 
   * @param string $number
   */
  public static function existsFor($number)
  {
    // Comprobamos si el registro de buzón de voz está creado. 
    // Si no, lo creamos
    $rvoicemail = Voicemail::where('mailbox', '=', $number)
                           ->get();
    
    return count($rvoicemail) > 0;
  }
  
  /**
   * Método que crea la fila de buzón de voz para un número dado
   * 
   * @param string $number
   */
  public static function createFor($number)
  {
    if(!self::existsFor($number)) {
      $rvoicemail = new Voicemail;
      $rvoicemail->mailbox = $number;
      $rvoicemail->context = 'default';
      
      return $rvoicemail->save();
    }
    
    return false;
  }
}
