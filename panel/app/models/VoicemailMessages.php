<?php
class VoicemailMessages extends Eloquent 
{
  protected $table      = 'voicemail_messages';  
  protected $primaryKey = 'id_message';    
  public    $timestamps = false;
}
