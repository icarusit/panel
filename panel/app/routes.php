<?php
//////////////////////////
///// RUTAS PUBLICAS /////
//////////////////////////

//
// Home
//
Route::get('/', 'HomeController@init');
Route::get('login', 'HomeController@showLogin');
Route::get('logout', 'HomeController@logout');
Route::post('/', 'HomeController@postLogin');

// Notificacion al navegador
Route::post('notificacion','NotificacionController@enviarNotificacion');

//validar mail
Route::get('validar-mail/{mail}', 'SignUpController@validar_mail');


Route::get('hipchat-b64/{msg}/{sala}', 'BaseController@hipchat_base64'); 
Route::get('hipchat-b64/{msg}', 'BaseController@hipchat_base64');

Route::get('hipchat/{msg}/{sala}', 'BaseController@hipchat'); 
Route::get('hipchat/{msg}', 'BaseController@hipchat');

Route::get('descargar-factura/{key}/{idioma}', 'InvoicesController@descargar_modelo_online');
Route::get('descargar-factura/{key}', 'InvoicesController@descargar_modelo_online');

Route::get('enviar-prefacturas', 'PrefacturaController@enviar');


//
// Regeneración de password
//
Route::get('forgot-password', 'HomeController@forgotPassword');
Route::post('forgot-password', 'UserController@postForgotPassword');
Route::get('restore-password', 'UserController@restorePassword');
Route::post('set-new-password', 'UserController@postSetNewPassword');

//
// Callback pago TPV
//
/*Route::group(array('before' => 'is-redsys'), function() {  
  Route::post('payment/callback', 'PaymentController@callback');
});*/
Route::post('payment/callback', 'PaymentController@callback');

Route::get('invoices-paypal/resultado', 'InvoicesController@GetresultadoPaypal');
Route::post('invoices-paypal/resultado', 'InvoicesController@resultadoPaypal');
Route::post('balance-paypal/resultado', 'BalanceController@resultadoPaypal');

//////////
// ALTA //
//////////
Route::get('alta/cupon/{name_cupon}','SignUpController@validateCupon');

Route::get('alta', 'SignUpController@showProductSelection');
Route::get('alta/codigo/{codigo_afiliacion}', 'SignUpController@altaAfiliacion');
Route::get('alta/all-set', 'SignUpController@allSet');
Route::get('alta/product-select', 'SignUpController@showProductSelection');
Route::get('alta/user', 'SignUpController@showUserForm');
Route::post('alta/checkout', 'SignUpController@proceedToCheckout');
Route::post('alta/portability', 'SignUpController@transferNumber');
Route::post('alta/ip-a-medida', 'SignUpController@postPlanCustomIP');
Route::post('alta/ip-a-medida-num', 'SignUpController@postPlanNumberIP');
Route::get('alta/ip-a-medida', 'SignUpController@showCustomIP');
Route::get('alta/sip-trunks', 'SignUpController@showCustomSipTrunks');
Route::post('alta/sip-trunks', 'SignUpController@postPlanCustomSipTrunks');
Route::post('alta/ip-a-medida-num', 'SignUpController@postPlanNumberIP');
Route::get('alta/summary', 'SignUpControler@showSummary');
Route::get('alta/summary-ip', 'SignUpControler@showSummaryIP');
Route::group(array('before' => 'must-be-ajax-request'), function() {
  Route::get('alta/available/{provincia}', 'SignUpMapController@getNumbers');
});
Route::get('alta/{productType}', 'SignUpController@showPlans');
Route::get('alta/{productType}/{plan}/{number}', 'SignUpController@getPlanNumber');
Route::get('alta/{productType}/{plan}', 'SignUpController@showNumbers')
     ->where('plan', '[A-Za-z]+');
Route::get('alta/{productType}/{number}', 'SignUpController@showPlans')
     ->where('plan', '[0-9]+');
Route::post('alta', 'SignUpController@postPlanNumber');
Route::post('alta/user', 'UserController@create');
Route::post('alta/login', 'UserController@login');




Route::get('api-rates', 'RatesController@portalRates'); 
//
// Rutas de configuración inicial tras el alta
//
Route::post('alta/fax/main-mail-fax', 'FaxController@saveMainMailfax');
Route::post('alta/analog/main-forward', 'ForwardsController@setMainForwardSignUp');

///////////////////////////////////////
///// RUTAS PRIVADAS PARA USUARIO /////
///////////////////////////////////////
Route::group(array('before' => 'flash-auth'), function() {  
  //
  // Rutas para nuevos clientes (clientes que aún no han 
  // rellenado su ficha)
  //
  Route::get('welcome', 'WelcomeController@showFirst');  
  Route::get('welcome/new-customer-form', 'WelcomeController@showNewCustomerForm'); 
  Route::post('welcome/payment-method', 'PaymentController@update');
  Route::post('me', 'UserController@update'); 
  
  Route::post('generarcodigoafiliacion', 'UserController@generateCodigoAfiliacion');      
  //
  // Carga AJAX de números de la barra superior
  //
  Route::get('numbers.json', 'NumbersController@returnTopBarNumbers');  
  //
  // Rutas protegidas para clientes que no tienen
  // la ficha rellena
  //
  Route::group(array('before' => 'user-has-filled-out-profile'), function() {
    //
    // Rutas de bienvenida pero ya el usuario debería haber rellenado
    // su ficha
    //
    Route::get('welcome/payment-method', 'WelcomeController@showPaymentMethods'); 
	
	// Notificacion al navegador
	Route::post('setsubscriptor','NotificacionController@setSubscriptorNotificacion'); 
    //
    // Números
    //
    Route::get('numbers/{page?}', 'NumbersController@showNumbers');
    Route::get('number/{number}', 'NumbersController@setNumber');
	//
	// Clientes
	//
	Route::get('misclientes/{page?}', 'ClientesController@showClientes');
    Route::get('cliente/{numbers}/{page?}', 'ClientesController@setCliente');
	
	Route::get('clientes/{page?}', 'AdministracionController@showClientes');
	Route::get('simular-cliente/{id_cliente}/{id_administrador}', 'AdministracionController@setCliente');
	Route::get('parar-simulacion/{id_administrador}', 'AdministracionController@regresarSimular');
	
	//
	// Reportes
	//
	Route::get('reporte-anual', 'ReportsController@show');
	Route::get('reporte-anual/{anno}/{tipo}', 'ReportsController@show');
	Route::post('reporte-anual', 'ReportsController@show');
	Route::get('reporte-mensual/{anno}/{mes}/{campo}', 'ReportsController@showMensual');
	
    //
    // Fijar modo de uso
    //
    Route::get('mode/{mode}', 'BaseController@setPanelMode');    
    //
    // Facturas
    // 
    Route::get('invoices', 'InvoicesController@showInvoices');
	Route::get('invoices/pdf/{invoice}', 'InvoicesController@pdf');
    Route::get('invoices/download/{invoice}', 'InvoicesController@download');
    Route::get('invoices/pay/{invoice}', 'InvoicesController@pay');  
    Route::get('invoices/success', 'InvoicesController@showSuccess');
    Route::get('invoices/error', 'InvoicesController@showError');      
    Route::get('invoices/unpaid', 'InvoicesController@getNoOfOutstandingInvoices');
	
	Route::post('invoices/pay/{invoice}', 'InvoicesController@pay'); 
	
    Route::post('invoices-paypal/retorno', 'InvoicesController@retornoPaypal'); 
    //
    // Recarga de saldo
    // 
    Route::get('balance', 'BalanceController@show');
    Route::get('balance/success', 'BalanceController@showSuccess');
    Route::get('balance/error', 'BalanceController@showError');  
    Route::post('balance', 'BalanceController@pay');
	
	
    Route::post('balance-paypal/retorno', 'BalanceController@retornoPaypal');  
    //
    // Ficha
    //
    Route::get('me', 'UserController@profile');
    //
    // Cambio de plan sin número seleccionado
    // 
    Route::get('change-plan/{number}', 'ChangePlanController@show');
    //
    // Baja sin número seleccionado
    // 
    Route::get('cancel/{number}', 'CancelController@show');
  });
  
  //
  // Estadísticas
  //
  Route::get('free-stats', 'FreeStatsController@showStats');
  Route::get('free-stats/export', 'FreeStatsController@export');    
  Route::post('free-stats', 'FreeStatsController@filter');
	
  Route::group(array('before' => 'user-has-filled-out-profile|number-selected|number-suspended'), function() {
    Route::get('stats', 'StatsController@showStats');
    Route::get('stats/export', 'StatsController@export');    
    Route::post('stats', 'StatsController@filter');
    Route::get('stats/filter/remove', 'StatsController@unfilter');
    
    Route::group(array('before' => 'must-be-ajax-request'), function() {
      Route::get('stats/summary/incoming', 'StatsController@getSummaryDataIncoming');      
      Route::get('stats/summary/outgoing', 'StatsController@getSummaryDataOutgoing');            
    });
  });
  
  ////////////////////////////////////////////////////////////  
  // RUTAS PRIVADAS ACCESIBLES SOLO CON NÚMERO SELECCIONADO //
  // SIN TENER EN CUENTA NI TIPO DE NUMERO, NI PLAN         //
  ////////////////////////////////////////////////////////////  
  Route::group(array('before' => 'user-has-filled-out-profile|number-selected'), function() {
    //
    // Cambio de plan
    // 
    Route::get('change-plan', 'ChangePlanController@show');
    Route::post('change-plan', 'ChangePlanController@postChange');
    Route::post('change-plan-custom-ip', 'ChangePlanController@postPlanCustomIP');
    //
    // Resumen + Configuraciones
    //
    Route::get('summary', 'SummaryController@show');
    Route::post('summary', 'SummaryController@show');
	Route::get('summary/del/{id_tarifa}/{id_numero}', 'SummaryController@deleteFlatrate');
    //
    // Baja del servicio
    //
    Route::get('cancel', 'CancelController@show');    
    Route::post('cancel', 'CancelController@cancel');    
  });  
  
  //////////////////////////////////////////////////////
  // RUTAS PRIVADAS ACCESIBLES SOLO CON PLANES DE VOZ //
  // VALIDAS PARA ANALOG + VOIP                       //
  //////////////////////////////////////////////////////  
  Route::group(array('before' => 'user-has-filled-out-profile|number-selected|is-not-fax|plan-allows-routing'), function() {
    //
    // Locuciones
    //
    Route::get('speeches', 'SpeechesController@showSpeeches');    
    Route::post('speeches/upload', 'SpeechesController@upload');
    Route::post('speeches/upload-recording', 'SpeechesController@uploadRecording');    
    Route::post('speeches/upload-text2speech', 'SpeechesController@text2speechUpload');
    Route::post('speeches/update', 'SpeechesController@update');
    Route::post('speeches/text2speech_play', 'SpeechesController@text2speechPlay');
    Route::get('speeches/download/{idSpeech}', 'SpeechesController@download');    
    Route::get('speeches/remove/{idSpeech}', 'SpeechesController@remove');        
    //
    // Opciones adicionales
    //
    Route::get('misc', 'MiscController@showMisc');   
    
    /////////////////////////////////////////
    // RUTAS ACCESIBLES SOLO CON PLAN PLUS //
    /////////////////////////////////////////    
    Route::group(array('before' => 'plan-is-plus'), function() {  
      //
      // Hooks
      //
      Route::get('hooks', 'HooksController@showHooks'); 
      Route::post('hooks', 'HooksController@add');
      Route::get('hooks/remove/{idHook}', 'HooksController@remove'); 
      //
      // Grabación de llamadas
      // 
      Route::get('callrecording', 'CallRecordingController@show');
      Route::post('callrecording', 'CallRecordingController@setCallRecording');
      Route::post('callrecording/download-bulk', 'CallRecordingController@downloadBulk');
      Route::get('callrecording/download/{recording}', 'CallRecordingController@download');
      Route::get('callrecording/remove/{recording}', 'CallRecordingController@remove');         
    });
  });
  
  /////////////////////////////////////////////////////
  // RUTAS PRIVADAS ACCESIBLES SOLO CON PLANES VOIP  //
  /////////////////////////////////////////////////////
  Route::group(array('before' => 'user-has-filled-out-profile|number-suspended|number-selected|number-is-voip-generic|is-trunk'), function() {
    //
    // Cuentas SIP
    //
    Route::get('sip-accounts', 'SIPAccountsController@showSIPAccounts');    
    Route::get('sip-accounts/{id_sip_account}', 'SIPAccountsController@edit');        
    Route::post('sip-accounts', 'SIPAccountsController@update');        
    //
    // Tarifas
    //
    Route::get('rates', 'RatesController@showRates');     
    Route::get('rates/export', 'RatesController@export');     
    //
    // Tarifas planas
    //
    Route::get('flatrates', 'FlatRatesController@show');
    Route::get('flatrates/order', 'FlatRatesController@order');
    Route::post('flatrates/order', 'FlatRatesController@postOrder');
    Route::get('flatrates/order/success', 'FlatRatesController@showSuccess');
    Route::get('flatrates/order/error', 'FlatRatesController@showError');       
    //
    // Opciones adicionales
    //
    Route::post('misc/speech-credit', 'MiscController@setSpeechCredit');    
    Route::post('misc/settings', 'MiscController@settings');    
    
    /////////////////////////////////////////
    // RUTAS ACCESIBLES SOLO CON PLAN PLUS //
    /////////////////////////////////////////    
    Route::group(array('before' => 'plan-is-plus'), function() {     
      //
      // Queues
      //
      Route::get('queue', 'QueueController@showQueues');    
      Route::post('queue/order', 'QueueController@order');    
    });
  });

  ////////////////////////////////////////////////////////////
  // RUTAS PRIVADAS ACCESIBLES SOLO CON NUMERO SELECCIONADO //
  // Y CUYO PLAN SEA O BIEN PREMIUM Ó PLUS                  //
  ////////////////////////////////////////////////////////////
  Route::group(array('before' => 'user-has-filled-out-profile|number-suspended|number-selected|plan-allows-routing'), function() {
    //
    // Horarios
    //
    Route::get('schedules', 'SchedulesController@showSchedules');
    Route::post('schedules', 'SchedulesController@save');
    Route::get('schedules/remove/{idSchedule}', 'SchedulesController@remove');
    Route::get('schedules/repeat/{weekDay}', 'SchedulesController@repeat');
    //
    // Festivos y vacaciones
    //
    Route::get('holidays', 'HolidaysController@show');
    Route::post('holidays/national-action', 'HolidaysController@saveNationalHolidays');
    Route::post('holidays/save', 'HolidaysController@save');
    Route::get('holidays/remove/{idHoliday}', 'HolidaysController@remove');
    //
    // Desvíos por provincia
    // 
    Route::get('origin', 'OriginController@showOrigin');
    Route::post('origin/add', 'OriginController@add');
    Route::get('origin/remove/{idAction}', 'OriginController@remove');
    //
    // Buzón de voz
    //
    Route::get('voicemail', 'VoicemailController@showVoicemail');
    Route::post('voicemail', 'VoicemailController@setVoicemail');
    Route::post('voicemail/config', 'VoicemailController@setConfig');
    Route::get('voicemail/download/{recording}', 'VoicemailController@download');
    Route::get('voicemail/remove/{recording}', 'VoicemailController@remove');         
    //
    // Llamadas salientes
    //    
    Route::get('outgoing-calls', 'OutgoingCallsController@show'); 
    Route::post('outgoing-calls', 'OutgoingCallsController@add');
    Route::get('outgoing-calls/remove/{idNumber}', 'OutgoingCallsController@remove');     
    Route::post('outgoing-calls/toggle', 'OutgoingCallsController@toggle');
	
	//
	// Agenda de contacto
	//
	Route::get('contact-agenda', 'ContactAgendaController@show'); 
	Route::post('contact-agenda', 'ContactAgendaController@add');
	Route::get('contact-agenda/remove/{idNumber}', 'ContactAgendaController@remove');
	Route::get('contact-agenda/update/{idNumber}', 'ContactAgendaController@formUpdate');
	Route::post('contact-agenda/update', 'ContactAgendaController@update');
	
	//
	// Grupos
	//
	Route::get('groups', 'GroupController@show'); 
	Route::post('group', 'GroupController@add');
	Route::get('group/remove/{idGroup}', 'GroupController@remove');
	Route::get('group/update/{idGroup}', 'GroupController@formUpdate');
	Route::post('group/update', 'GroupController@update');
	Route::post('group/addtelefono', 'GroupController@addtelefono');
	Route::post('group/addsip', 'GroupController@addsip');
	Route::get('group/removemiembro/{idGroup}/{miembro}', 'GroupController@removemiembro');
    
    ////////////////////////////////////////////////////////
    // RUTAS ACCESIBLES SOLO CON PLAN PREMIUM-PLUS ANALOG //
    ////////////////////////////////////////////////////////
    Route::group(array('before' => 'number-is-analog|plan-allows-routing'), function() {  
      //
      // Desvíos
      //
      Route::get('forwards', 'ForwardsController@showForwards');  
      Route::get('forwards/remove/{number}', 'ForwardsController@remove');    
      Route::post('forwards/main', 'ForwardsController@setMainForward');
      Route::post('forwards/forwards', 'ForwardsController@setForwards');         
    });     
    
    /////////////////////////////////////////
    // RUTAS ACCESIBLES SOLO CON PLAN PLUS //
    /////////////////////////////////////////    
    Route::group(array('before' => 'plan-is-plus'), function() {  
      //
      // Menús
      //
      Route::get('menus', 'MenusController@showMenus');
      Route::get('menus/remove/{dtmf}', 'MenusController@removeOption');
      Route::post('menus', 'MenusController@setConfig');
      Route::post('menus/options', 'MenusController@setOptions');
      Route::post('menus/speech', 'MenusController@setSpeech');
      //
      // Lista blanca/negra
      //
      Route::get('whiteblacklist', 'WhiteBlackListController@showWhiteBlacklist');
      Route::post('blacklist/add', 'WhiteBlackListController@addToBlacklist');
      Route::get(
        'blacklist/remove/{number}',
        'WhiteBlackListController@removeFromBlacklist'
      );
      Route::post('whitelist/add', 'WhiteBlackListController@addToWhitelist');
      Route::get(
        'whitelist/remove/{number}',
        'WhiteBlackListController@removeFromWhitelist'
      );
      Route::post('whitelist/settings', 'WhiteBlackListController@settings');  
      //
      // Servicio de aviso llamadas perdidas
      // 
      Route::get('missed-calls', 'MissedCallsController@show');
      Route::post('missed-calls', 'MissedCallsController@setMissedCalls');      
      Route::post('missed-calls.email', 'MissedCallsController@setEmail'); 
	  
	  Route::post('whatsapp.active', 'WhatsAppController@setWhatsAppActive'); 
	  Route::post('whatsapp.numero', 'WhatsAppController@setWhatsAppNumber');            
      //
      // Modo "No molestar"
      //
      Route::get('dnd', 'DoNotDisturbController@showDND');
      Route::post('dnd', 'DoNotDisturbController@setDND');
      //
      // Opciones adicionales
      //
      Route::post('misc/displaycallerid', 'MiscController@setDisplayCallerID');
    });
  });
  
  //////////////////
  // RUTAS DE FAX //
  //////////////////
  Route::group(array('before' => 'user-has-filled-out-profile|number-suspended|number-selected|is-fax'), function() {
    //
    // Faxes recibidos
    //
    Route::get('fax/received', 'FaxController@showReceivedFaxes');  
    Route::get('fax/received/download/{idFax}', 'FaxController@downloadReceivedFax');
    //
    // Faxes enviados
    //
    Route::get('fax/sent', 'FaxController@showSentFaxes');      
    Route::get('fax/sent/download/{idFax}', 'FaxController@downloadSentFax');
    Route::get('fax/sent/certificate/{idFax}', 'FaxController@downloadCertificate');    
    //
    // Configuración de Fax
    //
    Route::get('fax/config', 'FaxController@showConfig');   
    Route::post('fax/config', 'FaxController@saveConfig');
    //
    // Rutas AJAX para reenviar los faxes por correo electrónico
    //
    Route::group(array('before' => 'must-be-ajax-request'), function() {    
      Route::get('fax/received/send/{idFax}', 'FaxController@sendReceivedFaxByEmail');
      Route::get('fax/sent/send/{idFax}', 'FaxController@sendSentFaxByEmail');
    });
  });
  
  ////////////////////////////////////////////////////////////  
  // RUTAS PRIVADAS ACCESIBLES SOLO CON NÚMERO SELECCIONADO //
  // Y PLAN TRUNK                                           //
  ////////////////////////////////////////////////////////////  
  Route::group(array('before' => 'user-has-filled-out-profile|number-suspended|number-selected|is-trunk'), function() {
    Route::get('trunk', 'TrunkController@showTrunks');
    Route::post('trunk', 'TrunkController@update');
  });
    
});

//////////////////////////////////////
// RUTAS QUE USAMOS INTERNAMENTE    //
// VIA AJAX PARA CARGAR PLANTILLAS  //
// Y COSAS POR EL ESTILO            // 
//////////////////////////////////////

Route::group(array('before' => 'must-be-ajax-request|flash-auth'), function() {
  //
  // Ruta AJAX para la calculadora de tarifas
  //
  Route::post('rate/{destination}', 'RatesController@calculate');
  //
  // Rutas de carga de modales
  //
  Route::post('component/actions.modals.{which}', function($which) {
    // Chapucilla, hacer mejor cuando los días sean de 90horas.
    if($which === 'speech') { $controller = 'SpeechesController'; }
    elseif($which === 'voicemail') { $controller = 'VoicemailController'; }
    elseif($which === 'text2speech') { $controller = 'Text2SpeechController'; }
    else { $controller = 'ForwardsController'; }

    return App::make($controller)->returnModal();
  });

  //
  // Rutas de carga lazy de componentes
  //  
  Route::get('component/{which}', function($which) {
    $which = str_replace('.', '/', $which);
    return View::make('components/' . $which, Input::all());
  });
}); 
