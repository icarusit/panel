<?php

class NIFTest extends TestCase {

  /**
   * Test para tipo CIF
   *
   * @return void
   */
  public function testGetTypeCIF()
  {
    $validCIF = 'B50979491';
    
    $nif = new NIF($validCIF);   

    $this->assertTrue($nif->getDocumentType() === NIF::CIF);
  }    

  /**
   * Test para tipo DNI
   *
   * @return void
   */
  public function testGetTypeDNI()
  {
    $validDNI = '78499072A';
    
    $nif = new NIF($validDNI);   

    $this->assertTrue($nif->getDocumentType() === NIF::NIF);
  }
    
  /**
   * Test para tipo NIE
   *
   * @return void
   */
  public function testGetTypeNIE()
  {
    $validNIE = 'X8674236Q';
    
    $nif = new NIF($validNIE);   

    $this->assertTrue($nif->getDocumentType() === NIF::NIE);
  }  
  
  /**
   * Test para CIF
   *
   * @return void
   */
  public function testValidCIF()
  {
    $validCIF = 'B50979491';
    
    $nif = new NIF($validCIF);   

    $this->assertTrue($nif->isValid());
  }  
  
  /**
   * Test para DNI
   *
   * @return void
   */
  public function testValidDNI()
  {
    $validDNI = '78499072A';
    
    $nif = new NIF($validDNI);   

    $this->assertTrue($nif->isValid());
  }  
  
  /**
   * Test para NIE
   *
   * @return void
   */
  public function testValidNIE()
  {
    $validNIE = 'X8674236Q';
    
    $nif = new NIF($validNIE);   

    $this->assertTrue($nif->isValid());
  }  
}