@extends('alta.master')
@section('content')

<div class="row">
  <div class="col-md-12">
    <h1>¡Todo listo!</h1>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <p>Ya tienes tu número operativo. Adelante, haz unas pruebas.</p>
    
    @if(Session::get('user')->isNewClient())
    <p>Lo siguiente es rellenar tu ficha de cliente desde el panel de cliente
      y decirnos cómo quieres hacer los pagos de las cuotas.</p>
    @endif
    
    <p>Desde la zona de cliente también podrás configurar tu nueva numeración
      a tu gusto, ver las estadísticas y recargar saldo en tu cuenta.</p>
    <p><a href="{{ URL::to('/') }}" class="btn btn-primary">Continuar al panel de cliente</a></p>
  </div>
</div>
@stop