<ol class="breadcrumb"> 
  @if(Request::is('alta'))
  <li class="active">1. Producto</li>  
  @else
  <li><a href="{{ URL::to('alta') }}">1. Producto</a></li>  
  @endif
  
  @if(Request::is('alta/*') && Request::segment(2) !== null &&
      Request::segment(3) === null)
  <li class="active">2. Plan</li>
  @else
  <li><a href="#">2. Plan</a></li>  
  @endif

  @if(Request::is('alta/*') && Request::segment(3) !== null)
  <li class="active">3. Numeración</li>  
  @else
  <li><a href="#">3. Numeración</a></li>  
  @endif

  @if(Request::is('alta/summary'))
  <li class="active">4. Resumen</li>  
  @else
  <li><a href="#">4. Resumen</a></li>  
  @endif  

  @if(Request::is('alta/user'))
  <li class="active">5. Tus datos</li>  
  @else
  <li><a href="#">5. Tus datos</a></li>
  @endif    
  
  <li><a href="#">6. Configuración</a></li>      
</ol>