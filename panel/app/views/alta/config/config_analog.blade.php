@extends('alta.welcome')
@section('config')

<h2>Sólo una cosa más para terminar de dar de alta tu número de voz</h2>

<p>Índicanos por favor el número de teléfono donde quieres que te desviemos las
  llamadas que recibas en el número <strong>{{ $number }}</strong>.</p>

<p>No te preocupes, podrás cambiarlo más adelante desde el panel de clientes.</p>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Desvío principal</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'ForwardsController@setMainForwardSignUp',
          'class'  => 'form-horizontal'
        )) }}
        
        <input type="hidden" name="redirect" value="alta/all-set">
        <input type="hidden" name="number" value="{{ $number }}">
        
        <div class="form-group">
          <label class="col-sm-4 control-label" for="main-forward">Teléfono</label>
          <div class="col-sm-8">  
            <input type="text" class="form-control phone-number" name="main-forward" id="main-forward" 
                   value="" />
          </div>
        </div>     
        
        <input class="btn btn-success pull-right" type="submit" value="Guardar">
        {{ Form::close() }}
      </div>
    </div>    
  </div>
</div>

@stop