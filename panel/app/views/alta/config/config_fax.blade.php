@extends('alta.welcome')
@section('content')
<h3>Sólo una cosa más para terminar de dar de alta tu número de <strong>Fax</strong></h3>

<p>Índicanos por favor la dirección principal de correo electrónico donde quieres
  recibir los faxes que te envíen a <strong>{{ $number }}</strong>.</p>

<p>No te preocupes, podrás cambiarlo más adelante desde el panel de clientes.</p>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Correo electrónico donde recibir los faxes</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'FaxController@saveMainMailfax',
          'class'  => 'form-horizontal'
        )) }}

        <input type="hidden" name="redirect" value="alta/all-set">
        <input type="hidden" name="number" value="{{ $number }}">
        
        <div class="form-group">
          <label class="col-sm-4 control-label" for="mailfax">E-mail</label>
          <div class="col-sm-8">  
            <input type="text" class="form-control email" name="mailfax" id="mailfax" 
                   value="" />
          </div>
        </div>        

        <input class="btn btn-success pull-right" type="submit" value="Guardar">
        {{ Form::close() }}
      </div>
    </div>    
  </div>
</div>

@stop