@extends('alta.welcome')
@section('content')
<h1>¡Todo listo!</h1>

<p>Estos son los parámetros de tu nueva configuración VoIP (también te la estamos
  enviando a la dirección de correo que usaste para el alta):</p>

<h4 class="margin-30">Numeración</h4>

@if(isset($numbers))
  @if(!count($numbers))
  <p>Este es tu número VoIP</p>
  @else
  <p>Este es tu número VoIP principal:</p>
  @endif

  <p class="main-number">{{ $numbers[0]->numero_visible }}</p>

  @if(count($numbers) > 1)
  <p><strong>Tus otros números: </strong></p>
    <?php array_shift($numbers); ?>

    <ul>
    @foreach($numbers as $number)
      <li>{{ $number->numero_visible }}</li>
    @endforeach
    </ul>
  @endif
@else
  <p class="main-number">Sin número asignado</p>
@endif

<h4 class="margin-30">Cuentas SIP</h4>

<div class="table-responsive">
  <table class="table table-striped">
    <thead>
    @if(Session::get('id_distribuidor') == 1)
      <tr>
        <th>Host</th>
        <th>Usuario</th>
        <th>Contraseña</th>
        <th>Códecs válidos</th>
        <th>Clientes preconfigurados</th>
      </tr>
    @else
      <tr>
        <th>Host</th>
        <th>Usuario</th>
        <th>Contraseña</th>
        <th>Códecs válidos</th>
        <th>Clientes preconfigurados</th>
      </tr>
    @endif
    </thead>
    <tbody>
    @foreach($sipAccounts as $sipAccount)
    @if(Session::get('id_distribuidor') == 1)
      <tr>
        <td>213.139.7.254</td>
        <td>{{ $sipAccount->defaultuser }}</td>
        <td>{{ $sipAccount->secret }}</td>
        <td>G729, G711 (A-law), GSM</td>
        <td>
          <a href="https://www.zoiper.com/en/page/81cb7377842f6306ec688b4105efe6a9?u={{ $sipAccount->defaultuser }}&h=sip.fmeuropa.com&p={{ $sipAccount->secret }}&o=&t=&x=&a=&tr=" 
             title="Descargar Zoiper preconfigurado">
            <img src="{{ asset('img/voip_clients/zoiper.jpg') }}"
                 alt="Zoiper"
                 title="Descargar Zoiper preconfigurado">
          </a>
        </td>
      </tr>
      @else
      <tr>
        <td>213.139.7.254</td>
        <td>{{ $sipAccount->defaultuser }}</td>
        <td>{{ $sipAccount->secret }}</td>
        <td>G729, G711 (A-law), GSM</td>
        <td>
          <a href="http://www.zoiper.com/en/voip-softphone/download/zoiper3" 
             title="Descargar Zoiper preconfigurado">
            <img src="{{ asset('img/voip_clients/zoiper.jpg') }}"
                 alt="Zoiper"
                 title="Descargar Zoiper">
          </a>
        </td>
      </tr>
      @endif
    @endforeach      
    </tbody>
  </table>
</div>

@if(Session::get('user')->isNewClient())
<p>Lo siguiente es rellenar tu ficha de cliente desde el panel de cliente
  y decirnos cómo quieres hacer los pagos de las cuotas.</p>
@endif

<p>Desde la zona de cliente también podrás configurar tu nueva numeración
  a tu gusto, ver las estadísticas, recargar saldo en tu cuenta y ver si
  tus extensiones están correctamente registradas.</p>
<p><a href="{{ URL::to('/') }}" class="btn btn-primary">Continuar al panel de cliente</a></p>

@stop