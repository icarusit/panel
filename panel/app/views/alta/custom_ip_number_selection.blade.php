@extends('alta.master')

@section('content')

<h1>{{ isset($title) ? $title : 'Elige uno o varios números que te gusten' }}</h1>

<div class="row number-selection-content">
  <div class="col-md-12">    
    <ul class="nav nav-pills">
      <li class="active">
        <a href="#alta-tab-content-num-geo" role="tab" data-toggle="tab">
          Numeración geográfica
        </a>
      </li>
      <li>
        <a href="#alta-tab-content-902" role="tab" data-toggle="tab">        
          Números 902
        </a>
      </li>
      <li>
        <a href="#alta-tab-content-800-900" role="tab" data-toggle="tab">
          Números 900-800
        </a>
      </li>
    </ul>
    
    @if($customIP)
    <div class="row">
      <div class="col-md-12">
        {{ Form::open(array(
            'action' => 'SignUpController@postPlanNumberIP',
            'name'   => 'alta-custom-ip-continue',
            'method' => 'post'
        )) }}          
        <div class="selected-numbers-row row">
          <div class="col-md-9">
            <p>Números elegidos:</p>

            <input type="hidden" name="plan" id="plan" value="{{ $plan }}">
            <input type="hidden" name="geos" id="geos" value="0">
            <input type="hidden" name="chosen-number" id="chosen-numbers">            
            
            <div class="selected-numbers-list-wrapper">
              <ul class="selected-numbers-list"></ul>
            </div>
            
            <p id="you-can-add-more-dids" style="clear: both;">Si quieres puedes añadir más números</p>  
          </div>           
          <div class="col-md-3">
            <p>Cuota total: <span class="alta-custom-ip-price"><span id="price">{{ $basePrice }}</span>€/mes</span></p>
            <input type="submit" class="btn btn-success alta-custom-ip-continue" value="Continuar">
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
    @endif
    
    <div class="tab-content">
      <div class="tab-pane active" id="alta-tab-content-num-geo">
        @if(isset($map))
        {{ $map }}
        @endif        
      </div>
      @foreach(array('902', '800-900') as $tipo)
        <div class="tab-pane" id="alta-tab-content-{{ $tipo }}">
          <ul class="alta-number-selection">
            @foreach($numbers[$tipo] as $number)
              <li class="col-md-3">
                {{ Form::open(array(
                    'action' => 'SignUpController@postPlanNumber',
                    'method' => 'post',
                    'class'  => 'form-number-select'
                )) }}    
                <input type="hidden" name="alta-plan" value="{{ $plan }}">
                <input type="hidden" name="alta-number" value="{{ $number->numero_visible }}">                
                <input type="submit" class="btn btn-primary btn-huge btn-full-width alta-btn-number"
                       data-number="{{ $number->numero_visible }}"
                       value="{{ $number->pp_numero === '' || $number->pp_numero === null ? $number->numero_visible : $number->pp_numero }}">
                {{ Form::close() }}          
              </li>
            @endforeach
          </ul>
        </div>
      @endforeach   
    </div>
  </div>
</div>

@if(!Request::is('alta/ip-a-medida'))
  @include('alta.portability')
@endif

@if($customIP)
<script type="text/javascript">
var tlfs = new Array();
var auxid = 0;
var price_did = 3;
var base_price = {{ $basePrice }};

$(document).on('submit', 'form[class="form-number-select"]', function(event) {
  console.log($(this));
    event.preventDefault();
  
  var selectedPrettyNumber = $('input[type="submit"]', this).val();
  var selectedNumber = $('input[type="submit"]', this).data('number');
  
  $('.selected-numbers-row').show(300);
  
  add_number_to_cart(selectedNumber);
});  

/**
 * add_number_to_cart
 *
 * Función que añade números al carrito en el configurador
 * IP
 *
 * @param string number Número
 */
function add_number_to_cart(number)
{
  if(tlfs.length === 10) {
    alert("La cantidad máxima de números a contratar es de 10 números. ¿Necesitas más numeración? Llámanos al 800007766 o escríbenos a info@fmeuropa.com");
    return;
  }  
  
  number = number.toString();
  
  // Comprobamos que no exista ya en la lista de teléfonos elegidos
  if ($.inArray(number, tlfs) === -1) {
    tlfs.push(number);

    if(tlfs.length < 10) {
      $('#you-can-add-more-dids').show();
    } else {
      $('#you-can-add-more-dids').hide();      
    }

    var no_geos = parseInt($("#geos").val()) + 1;
    $("#geos").prop('value', no_geos);

    var id_item = 'num' + auxid;
    auxid++;    

    var li_item = '<li id="' + id_item + '"><span>' + number + '</span>';
    li_item+= ' <a href="#" class="remove-item-from-cart" ' + 
            'title="Eliminar este número de tu carrito">' + 
            '<span class="glyphicon glyphicon-remove"></span>' + 
            '</a></li>';

    $('ul.selected-numbers-list').append(li_item);
    $('#' + id_item).fadeIn('slow');

    $("#" + id_item + " a").click(function() {
        remove_number_from_cart($(this).parent().attr('id'));
        return false;        
    });

    $('ul.selected-numbers-list').scrollLeft($('ul.selected-numbers-list')[0].scrollWidth);    

    update_plan();
    price_reload();        
    update_hidden_numbers();
  }
  else {
    alert("El número " + number + " ya está en tu selección de números");
  }
}

/**
 * remove_number_from_cart
 *
 * Función que elimina un número del carrito del configurador
 * IP
 *
 * @param integer ID del número a borrar
 */
function remove_number_from_cart(id)
{
  if(tlfs.length === 1) {
    alert("Debes tener al menos un número en el carrito. Si quieres eliminar este, añade otro número y vuelve a pulsar el botón de eliminar en el primero.");
    return;
  }
  
  var tlf = $('ul.selected-numbers-list #' + id + ' span').html();
  var index_tlf = $.inArray(tlf, tlfs);
  if (index_tlf > -1) {
    tlfs.splice(index_tlf, 1);

    if(tlfs.length < 10) {
      $('#you-can-add-more-dids').show();
    } else {
      $('#you-can-add-more-dids').hide();      
    }

    var no_geos = parseInt($("#geos").val()) - 1;
    $("#geos").prop('value', no_geos);  

    $('#' + id).fadeOut('slow');
    $('ul.selected-numbers-list #' + id).remove(); 

    update_plan();
    price_reload();      
  }

  update_hidden_numbers();
}

function update_hidden_numbers()
{
  $("#chosen-numbers").attr("value", tlfs);
}

function price_reload()
{
  var no_geos = parseInt($("#geos").val());
  $('#price').html(base_price + (no_geos * price_did));
}

function update_plan()
{
  var current_plan = $("#plan").val();
  var arr_plan = current_plan.split('_');  
  var no_geos = parseInt($("#geos").val());  
  if(arr_plan.length < 5) return false;
  arr_plan[1] = no_geos;
  $("#plan").prop("value", arr_plan.join('_'));
}
</script>
@endif

@stop