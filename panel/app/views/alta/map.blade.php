<p>Haz click sobre una provincia en el mapa o elige una del desplegable a tu
  derecha para ver los números disponibles.</p>
  
<p>Una vez hayas elegido el número que más te guste, haz click en él para seguir
  con el proceso de alta.</p>
  
<div class="col-md-6 number-selection-col-map">  
  <img src="{{ asset('img/mapa.gif') }}" usemap="#mapa" style="display: block; float: left; width: 376px; height: 346px;">
    
  <map name="mapa" id="mapa">
    <area shape="poly" coords="229,111,241,99,250,104,258,95,267,98,279,105,286,94,274,92,264,86,256,77,248,65,244,46,232,56,235,68,230,77,223,74,218,89,214,94,214,103" href="#zaragoza" alt="Zaragoza" title="Zaragoza" />
    <area shape="poly" coords="232,111,227,134,249,148,262,132,265,119,280,116,282,108,262,99,246,108" href="#teruel" alt="Teruel" title="Teruel" />
    <area shape="poly" coords="1,26,10,17,30,17,36,7,45,3,46,9,38,34,12,43,8,35" href="#coruna" alt="La Coru&ntilde;a" title="La Coru&ntilde;a" />
    <area shape="poly" coords="21,44,12,54,11,65,11,70,28,66,29,48,40,48,41,38" href="#pontevedra" alt="Pontevedra" title="Pontevedra" />
    <area shape="poly" coords="51,2,50,16,44,36,46,50,65,52,70,37,67,29,62,12" href="#lugo" alt="Lugo" title="Lugo" />
    <area shape="poly" coords="34,52,31,78,60,76,73,59,68,55,42,53,38,50" href="#orense" alt="Orense" title="Orense" />
    <area shape="poly" coords="69,12,67,18,78,37,91,29,106,32,139,20,105,11" href="#asturias" alt="Asturias" title="Asturias" />
    <area shape="poly" coords="163,14,131,26,136,32,144,32,154,40,158,32,176,19" href="#cantabria" alt="Cantabria" title="Cantabria" />
    <area shape="poly" coords="130,31,74,40,69,48,81,61,111,65,122,57" href="#leon" alt="Leon" title="Leon" />
    <area shape="poly" coords="133,36,130,49,129,67,156,78,145,64,150,42,143,38" href="#palencia" alt="Palencia" title="Palencia" />
    <area shape="poly" coords="78,64,115,73,114,99,89,93,82,81,69,70" href="#zamora" alt="Zamora" title="Zamora" />
    <area shape="poly" coords="116,66,118,86,118,101,133,105,141,90,155,89,149,81,141,77,131,74,123,65" href="#valladolid" alt="Valladolid" title="Valladolid" />
    <area shape="poly" coords="160,41,153,45,150,61,160,71,157,88,169,89,182,74,183,50,178,43,180,33,173,29,163,32" href="#burgos" alt="Burgos" title="Burgos" />
    <area shape="poly" coords="82,99,71,109,72,137,88,128,98,137,120,109,104,100" href="#salamanca" alt="Salamanca" title="Salamanca" />
    <area shape="poly" coords="51,167,69,152,69,142,85,134,109,145,120,170,101,180,89,181,73,173,65,167" href="#caceres" alt="C&aacute;ceres" title="C&aacute;ceres" />
    <area shape="poly" coords="57,170,91,188,124,174,131,177,121,190,102,216,86,227,59,217,52,202,63,186,56,175" href="#badajoz" alt="Badajoz" title="Badajoz" />
    <area shape="poly" coords="59,224,82,231,74,238,76,269,57,255,44,251,45,239" href="#huelva" alt="Huelva" title="Huelva" />
    <area shape="poly" coords="77,274,86,295,103,304,108,294,102,285,114,268,87,274" href="#cadiz" alt="C&aacute;diz" title="C&aacute;diz" />
    <area shape="poly" coords="109,282,124,263,141,259,156,274,135,279,131,283,114,292" href="#malaga" alt="M&aacute;laga" title="M&aacute;laga" />
    <area shape="poly" coords="100,222,108,243,121,243,126,257,117,261,90,272,80,265,80,239,88,232" href="#sevilla" alt="Sevilla" title="Sevilla" />
    <area shape="poly" coords="152,255,168,250,189,244,200,229,208,231,198,250,187,260,184,272,169,275,148,261" href="#granada" alt="Granada" title="Granada" />
    <area shape="poly" coords="208,247,194,261,188,274,200,271,215,273,229,250,220,245,214,233" href="#almeria" alt="Almer&iacute;a" title="Almer&iacute;a" />
    <area shape="poly" coords="211,226,232,214,243,200,249,218,255,237,242,240,232,248" href="#murcia" alt="Murcia" title="Murcia" />
    <area shape="poly" coords="252,198,257,230,270,206,286,197,277,191" href="#alicante" alt="Alicante" title="Alicante" />
    <area shape="poly" coords="274,187,256,195,245,186,245,181,243,173,233,167,244,153,269,159,265,171" href="#valencia" alt="Valencia" title="Valencia" />
    <area shape="poly" coords="254,149,269,132,269,120,291,124,272,155" href="#castellon" alt="Castell&oacute;n" title="Castell&oacute;n" />
    <area shape="poly" coords="291,122,285,115,288,98,302,96,315,83,323,96,301,105,301,120" href="#tarragona" alt="Tarragona" title="Tarragona" />
    <area shape="poly" coords="326,96,320,79,325,73,330,53,338,64,344,63,342,74,355,77,340,88" href="#barcelona" alt="Barcelona" title="Barcelona" />
    <area shape="poly" coords="339,48,352,51,359,46,367,51,366,57,368,64,358,74,348,69,350,59,335,56" href="#gerona" alt="Gerona" title="Gerona" />
    <area shape="poly" coords="293,37,312,46,317,44,324,49,321,69,313,79,298,93,288,89,294,74,300,57" href="#lerida" alt="L&eacute;rida" title="L&eacute;rida" />
    <area shape="poly" coords="250,38,262,41,277,45,291,44,292,70,283,77,286,87,276,89,256,71,249,54" href="#huesca" alt="Huesca" title="Huesca" />
    <area shape="poly" coords="183,148,192,135,209,127,217,129,225,140,240,150,228,171,209,176,190,173" href="#cuenca" alt="Cuenca" title="Cuenca" />
    <area shape="poly" coords="186,76,192,75,207,69,217,78,212,88,208,105,198,98,183,96,174,90" href="#soria" alt="Soria" title="Soria" />
    <area shape="poly" coords="181,101,192,100,201,109,216,108,227,113,223,127,204,123,193,129,185,138,173,119,170,106" href="#guadalajara" alt="Guadalajara" title="Guadalajara" />
    <area shape="poly" coords="138,140,167,106,168,117,175,131,180,140,170,148,156,138" href="#madrid" alt="Madrid" title="Madrid" />
    <area shape="poly" coords="154,95,145,95,137,105,146,124,158,109,174,99,166,93" href="#segovia" alt="Segovia" title="Segovia" />
    <area shape="poly" coords="125,109,133,109,143,129,134,137,116,145,110,137" href="#avila" alt="&Aacute;vila" title="&Aacute;vila" />
    <area shape="poly" coords="116,150,152,142,160,153,177,152,186,167,157,175,154,167,154,163,144,166,125,168" href="#toledo" alt="Toledo" title="Toledo" />
    <area shape="poly" coords="122,200,145,216,143,224,149,251,143,254,130,255,127,236,115,239,108,224,108,210" href="#cordoba" alt="C&oacute;rdoba" title="C&oacute;rdoba" />
    <area shape="poly" coords="138,171,147,175,163,180,179,173,191,176,188,188,194,200,187,209,163,208,144,210,126,198" href="#ciudad-real" alt="Ciudad Real" title="Ciudad Real" />
    <area shape="poly" coords="197,179,193,190,200,201,202,208,208,225,218,212,232,211,235,198,249,194,240,185,235,173,216,179" href="#albacete" alt="Albacete" title="Albacete" />
    <area shape="poly" coords="153,215,197,211,202,219,188,234,182,238,156,250,148,231" href="#jaen" alt="Ja&eacute;n" title="Ja&eacute;n" />
    <area shape="poly" coords="224,22,209,41,207,52,220,59,229,73,229,56,246,36" href="#navarra" alt="Navarra" title="Navarra" />
    <area shape="poly" coords="208,56,219,65,215,72,199,66,186,69,188,51" href="#la-rioja" alt="La Rioja" title="La Rioja" />
    <area shape="poly" coords="186,39,184,29,204,39,202,51,187,45" href="#araba" alt="Araba" title="Araba" />
    <area shape="poly" coords="175,24,193,18,202,20,197,30,189,27" href="#bizkaia" alt="Bizkaia" title="Bizkaia" />
    <area shape="poly" coords="205,21,219,20,216,27,208,36,199,33" href="#gipuzkoa" alt="Gipuzkoa" title="Gipuzkoa" />
    <area shape="poly" coords="306,185,319,176,329,171,343,158,367,150,375,153,367,155,348,160,355,163,346,175,336,171,314,185,323,193" href="#baleares" alt="Islas Baleares" title="Islas Baleares" />
    <area shape="poly" coords="287,302,278,320,270,311,263,317,239,329,232,324,243,322,248,323,260,312,244,306,239,295,249,294,246,303,263,312,280,305" href="#tenerife" alt="Santa Cruz de Tenerife" title="Santa Cruz de Tenerife" />
    <area shape="poly" coords="294,320,306,311,309,317,338,312,352,283,362,288,350,299,348,314,331,316,310,323,303,326" href="#las-palmas" alt="Las Palmas" title="Las Palmas" />
    <area shape="poly" coords="98,314,109,314,101,321" href="#ceuta" alt="Ceuta" title="Ceuta" />
    <area shape="poly" coords="190,326,199,326,196,332,189,333" href="#melilla" alt="Melilla" title="Melilla" />  
  </map>

  <input type="hidden" id="prefix" name="prefix" value=""/>
  <input type="hidden" name="plan" value=""/>
</div>  
<div class="col-md-6">
  <div class="row">
    <div class="col-md-12 province-selector-container">      
      <select class="form-control form-control-huge" id="provincia" name="provincia" style="display: inline-block; width: 100%; padding: 5px;">
        <option disabled selected>Escoge una provincia</option>
        @foreach($provincias as $provincia => $key_provincia)
          <option id="{{ $key_provincia }}" value="{{ $key_provincia }}">{{ $provincia }}</option>
        @endforeach
      </select>
    </div>
  </div>  
  
  <div class="row">
    <div class="col-md-12" id="available-numbers-wrapper"> 
      <button class="btn btn-primary geo-fetch-more-numbers" data-province="" type="button">Cargar otros números de esta provincia</button>  
      <?php
	  if(Session::get('id_distribuidor') == 1){
		  $loader = 'flash-loading-100.gif';
	  }else{
		  $loader = 'loading.gif';
	  }
	  ?>
      <img src="{{ asset('img/'.$loader) }}" id="available-numbers-loading" style="display: none; margin: 20px auto;" />
      <ul class="alta-number-selection" id="available-numbers"></ul>
    </div>
  </div>
</div>
    
<div style="clear: both;"></div>

<script>
  
function fetch_available_numbers_by_province(province) 
{      
  $('.geo-fetch-more-numbers').hide();
  $("#available-numbers-loading").css('display', 'block');    
  $("#available-numbers").empty();

  $.getJSON("{{ URL::to('alta/available') }}/" + province, function(data) {     
    var html = '';
    if(data.length > 0) {
      for(i in data) {
        html+= '<li class="col-md-4">' + 
          '{{ Form::open(array('action' => 'SignUpController@postPlanNumber', 'class' => 'form-number-select', 'method' => 'post')) }}' + 
          '<input type="hidden" name="alta-plan" value="{{ $plan }}">' +
          '<input type="hidden" name="alta-number" value="' + data[i].number + '">' +                           
          '<input class="btn btn-primary btn-huge btn-full-width alta-btn-number" ' +
             'data-number="' + data[i].number + '" ' + 
             'type="submit" ' + 
             'value="' + (data[i].pretty_number === null ? data[i].number : data[i].pretty_number) + '">' +
          '{{ Form::close() }}' +    
        '</li>';
      }
      $("#available-numbers-loading").hide();
      $("#available-numbers").append(html);
      $('.geo-fetch-more-numbers').data('province', province);
      $('.geo-fetch-more-numbers').css('display', 'block');
    } else {
      $("#available-numbers").html('<div style="margin-top: 10px;" class="alert alert-warning">No hay números disponibles.</div>');
	  $("#available-numbers-loading").hide();
    }
  });
}
  
$(document).ready(function() {
  $('.geo-fetch-more-numbers').click(function() {
    var prov = $(this).data('province');
    fetch_available_numbers_by_province(prov);
  });
  
  $("#mapa area").click(function() {
      var prov = jQuery(this).prop("href");
      prov = prov.substr(prov.indexOf("#") + 1, prov.length);
      $("select#provincia option#" + prov).prop("selected", true);
      fetch_available_numbers_by_province(prov);
  });
  
  $("select#provincia").change(function() {
	  $("select#provincia").each(function() {
        var prov = jQuery(this).val();
		fetch_available_numbers_by_province(prov);
	    return false;
    });
  });  
});  
</script>