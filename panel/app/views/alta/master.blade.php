<?php
if(!Session::has('id_distribuidor') || isset($distribuidor)){
	$dominio = explode(".",$distribuidor[0]->dominio);
	Session::put('id_distribuidor', $distribuidor[0]->id);
	Session::put('logo', $distribuidor[0]->logo);
	Session::put('distribuidor', $distribuidor[0]->nombre);
	Session::put('dominio', $dominio[1].".".$dominio[2]);
	Session::put('telefono', $distribuidor[0]->telefono);
	Session::put('logo-alta',$distribuidor[0]->logo_alta);
	if(Session::get('dominio') != 'fmeuropa.com'){
		Session::put('ocultar','si');
	}else{
		Session::put('ocultar','no');
	}
	Session::put('css',$distribuidor[0]->css);
	Session::put('dominio_master',$distribuidor[0]->dominio_master);
	Session::put('prefijo',$dominio[1]);
}else{
	Session::put('id_distribuidor', 1);
	Session::put('logo', 'logo-in-logo.png');
	Session::put('distribuidor', 'Flash Telecom');
	Session::put('dominio', 'fmeuropa.com');
	Session::put('telefono', '800007766');
	Session::put('logo-alta','logo-web.png');
	Session::put('ocultar','no');
	Session::put('css','common.css');
	Session::put('dominio_master','http://www.fmeuropa.com');
	Session::put('prefijo','');
}
$logo_alta = Session::get('logo-alta');
$css = Session::get('css');
$idDist = Session::get('id_distribuidor');
$prefijo = Session::get('prefijo');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ES">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">

    <title>{{ isset($title) ? $title . ' - ' . Session::get('distribuidor') : Session::get('distribuidor') }}</title>

    <link rel="icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />

    <link rel="shortcut icon" href="{{ asset('img/'.$prefijo.'favicon.ico') }}?{{ filemtime(public_path() . '/img/'.$prefijo.'favicon.ico') }}">    
    
    <link media="all" href="{{ asset('css/bootstrap.css') }}?{{ filemtime(public_path() . '/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/selectize.bootstrap3.css') }}?{{ filemtime(public_path() . '/css/selectize.bootstrap3.css') }}" rel="stylesheet" type="text/css">            
    <link media="all" href="{{ asset('css/'.$css) }}?{{ filemtime(public_path() . '/css/'.$css) }}" rel="stylesheet" type="text/css">
    <link media="all and (min-width: 768px)" href="{{ asset('css/screen.css') }}?{{ filemtime(public_path() . '/css/screen.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/bootstrap-switch.min.css') }}?{{ filemtime(public_path() . '/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css">      
    <link media="all" href="{{ asset('css/jquery-ui.min.css') }}?{{ filemtime(public_path() . '/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">      
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    <script type="text/javascript" src="{{ asset('js/jquery-1.9.1.min.js') }}?{{ filemtime(public_path() . '/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}?{{ filemtime(public_path() . '/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/selectize.min.js') }}?{{ filemtime(public_path() . '/js/selectize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-switch.min.js') }}?{{ filemtime(public_path() . '/js/bootstrap-switch.min.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}?{{ filemtime(public_path() . '/js/jquery-ui.min.js') }}"></script>
    
    <script type="text/javascript">
    @if(Session::get('is_mobile') === false && Session::get('id_distribuidor') == 1)
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
    $.src='//v2.zopim.com/?z3vnvWNM1LX5TAXWUyJBLzLAoXX2mOv3';z.t=+new Date;$.
    type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

    @if(!is_null($user = Session::get('user', NULL)) && Session::get('id_distribuidor') == 1)
    $zopim(function() {
      $zopim.livechat.setName('{{ $user->getUsername() }}');
      $zopim.livechat.setEmail('{{ $user->getEmail() }}');
      $zopim.livechat.setNotes('ID cliente: {{ $user->getID() }}');          
    });
    @endif
    @endif
    </script>
  </head>

  <body class="alta-body">
    <div class="alta-container">    
      <div class="row">
        <div class="col-md-6">
          <a href="{{ URL::to('alta') }}">
            <img src="{{ asset('img/'.$logo_alta) }}" border=0 alt="Logo de <?php echo Session::get('distribuidor'); ?>">            
          </a>                  
        </div>
        @if(($user = Session::get('user', null)) !== null)
        <div class="col-md-6">
          <div class="alta-user-box pull-right">
            <span class="glyphicon glyphicon-user"></span>
            <a href="{{ URL::to('me') }}">
              {{ (trim($user->getName()) == '' || trim($user->getName()) == "'") ? $user->getUsername() : $user->getName() }}
            </a>          
          </div>          
        </div>
        @endif        
      </div>      
      
      {{-- @include('alta.breadcrumb') --}}
        
      @yield('content')        
    </div>

    @include('footer_legal_links')
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-49619351-2', 'auto');
      ga('send', 'pageview');
      
      $(document).on('keypress', '.phone-number', function(event) {
        if(!$.isNumeric(String.fromCharCode(event.which))) {
          event.preventDefault();
        } 
      });      
      
      $(document).on('keyup', '.phone-number', function() {
        var re = /^(?:8|9)[1-9][0-9]{7}$|^6[0-9]{8}$|^7[1-9][0-9]{7}$|^00\d+$/;

        if(!re.test($(this).val())) {
          $(this).removeClass('input-text-success');
          $(this).addClass('input-text-error');
          $(this).data('error', true);
          $(this).prop('title', 'El número es inválido.');
        } else {        
          $(this).removeClass('input-text-error');
          $(this).addClass('input-text-success');
          var $that = $(this);
          setTimeout(function() {
            $that.removeClass('input-text-success', 30);
          }, 1000);
          $(this).data('error', false);
          $(this).prop('title', '');
        }      
      });   

      $(document).on('keyup', '.email', function() {      
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(!re.test($(this).val())) {
          $(this).removeClass('input-text-success');
          $(this).addClass('input-text-error');        
          $(this).data('error', true);
          $(this).prop('title', 'La dirección de e-mail no es válida.');
        } else {        
          $(this).removeClass('input-text-error');
          $(this).addClass('input-text-success');
          var $that = $(this);
          setTimeout(function() {
            $that.removeClass('input-text-success', 30);
          }, 1000);
          $(this).data('error', false);
          $(this).prop('title', '');
        }      
      });

    </script>
  </body>
</html>
