@extends('alta.master')
@section('content')

<div class="row">
  <div class="col-md-12">
    <h1>Elige el número que más te guste</h1>
  </div>
</div>

<div class="row number-selection-content">
  <div class="col-md-12">
    <div class="tab-content">
        @if(isset($map))
        {{ $map }}
        @endif        
  
        @if(isset($numbers))
        <ul class="alta-number-selection">
          @foreach($numbers as $number)
            <li class="col-md-3">
              {{ Form::open(array(
                'action' => 'SignUpController@postPlanNumber',
                'method' => 'post',
                'class' => 'form-number-select'                  
              )) }}    

              <input type="hidden" name="alta-plan" value="{{ $plan }}">
              <input type="hidden" name="alta-number" value="{{ $number->numero_visible }}">

              <input type="submit" class="btn btn-primary btn-huge btn-full-width alta-btn-number"
                     value="{{ $number->pp_numero === '' || $number->pp_numero === null ? $number->numero_visible : $number->pp_numero }}">
              {{ Form::close() }}          
            </li>
          @endforeach
        </ul>
        @endif
    </div>
  </div>
</div>

@include('alta.portability')

@stop