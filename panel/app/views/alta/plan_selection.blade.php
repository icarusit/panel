@extends('alta.master')

@section('content')

@if(isset($number))
<div class="row">
  <div class="col-md-12">
    <h1>Has elegido el número</h1>
    
    <p class="main-number">{{ $number->pp_numero }}</p>
  </div>
</div>
@endif

<div class="row">
  <div class="col-md-12">
    <h1>Elige el plan que más se adecúa a tus necesidades</h1>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <h3>Planes</h3>

    <ul class="alta-product-selection">
      @foreach($plans as $plan)
        <li>
          <a class="btn btn-primary btn-huge btn-full-width alta-btn-plan" 
             @if(isset($number))
             href="{{ URL::to('alta/' . $plan->product_type . '/' . $plan->plan) . '/' . $number->numero_visible }}"
             @else
             href="{{ URL::to('alta/' . $plan->product_type . '/' . $plan->plan) }}"             
             @endif
             data-plan="{{ $plan->product_type . '_' . $plan->plan }}"
             data-perfil="{{ $plan->perfil }}">            
            <span>{{ $plan->perfil }}</span>
            <span class="alta-btn-plan-price">{{ number_format($plan->cuota, 2) }} €/mes</span>
            @if($plan->cuota_anual !== null && $plan->plan !== 'gratis')
            <br>o <span class="alta-btn-plan-price">{{ number_format($plan->cuota_anual / 12, 2) }} €/mes</span> <span class="label label-alta">Contratando un año</span>
            @endif
          </a>
        </li>
      @endforeach
      @if(in_array($plan->product_type, array('voip', 'centralita')))
        <li>
          <a class="btn btn-primary btn-huge btn-full-width alta-btn-plan" 
             href="{{ URL::to('alta/ip-a-medida') }}{{ $plan->product_type === 'centralita' ? '?centralita=1' : '' }}"
             data-plan="ip-a-medida"
             data-perfil="IP a medida">
              <span>Configurar a medida</span>
              <span class="alta-btn-plan-price">??? €/mes</span>
          </a>
        </li>      
      @endif
    </ul>
  </div>
  <div class="col-md-6">
    <h3 id="alta-features-title">Características</h3>
    
    <p><strong>Desplaza el ratón sobre los planes</strong> a tu izquierda para
      descubrir las características del plan.</p>    
        
    @if(in_array($plan->product_type, array('voip', 'centralita')))    
    <ul class="alta-pros" id="pros-ip-a-medida">
      <li>
        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Diseña tu plan a medida. El número de canales, DID y cuentas SIP que quieras.">
          El número de canales, DID y cuentas SIP que tú elijas. 
        </a>
      </li>
    </ul>  
    @endif    
    
    @foreach($plansPros as $plan => $planPros)
    <ul class="alta-pros" id="pros-{{ $plan }}">
      @foreach($planPros as $pro)
      <li>
        <a href="#"  data-toggle="tooltip" data-placement="top" title="{{ $pro->tooltip }}">
          {{ $pro->pro }}
        </a>
      </li>
      @endforeach
    </ul>
    @endforeach
  </div>    
</div>

<script>
$(document).ready(function() {
  $('.alta-btn-plan').hover(function() {
    var plan = $(this).data('plan');
    $('.alta-pros').hide();
    $('#pros-' + plan).show();
    $('#pros-' + plan).scrollTop($('#pros-' + plan)[0].scrollHeight);    
    $('#alta-features-title').html('Características <strong>' + $(this).data('perfil') + '</strong>');
  });
  
  $('[data-toggle="tooltip"]').tooltip();
});  
</script>
  

@stop
