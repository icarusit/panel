<?php
if(Session::get('ocultar') == 'no'){
?>
<div id="portar" class="row">
  <div class="col-md-12">
    <h1>O bien porta tu número actual con <?php echo Session::get('distribuidor'); ?></h1>
  </div>
</div>

<div class="row margin-15">
  <div class="col-md-6">
    {{ Form::open(array(
      'action' => 'SignUpController@postPlanNumber',
      'class'  => 'form-horizontal',
      'name'   => 'form-transfer'
    )) }}
      <div class="form-group">
        <label class="col-sm-4 control-label" for="number-to-transfer">Número a portar</label>
        <div class="col-sm-8">  
          <input type="hidden" name="portability" value="true">                    
          <input type="hidden" name="alta-plan" value="{{ $plan }}">          
          <input type="text" class="form-control" name="alta-number" id="number-to-transfer" 
                 value="" />
        </div>
      </div>     

      <input class="btn btn-success pull-right" type="submit" value="Continuar">
    {{ Form::close() }}
  </div>
</div>
<?php
}
?>