@extends('alta.master')
@section('content')
<h1>Elige el servicio que estás buscando</h1>

@if($link_alta == true)    
    
    <div class="row">
      <div class="col-md-4">
        <h3>Voz</h3>
        
        <ul class="alta-product-selection">
          <li>
            <a class="btn btn-primary btn-huge btn-full-width"
               href="{{ URL::to('alta/902') }}">902</a>
          </li>
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/902-movil') }}">902 móvil</a>
          </li>      
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/900') }}">900/800</a>
          </li>      
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/geografico') }}">Geográficos</a>
          </li>            
        </ul>
      </div>
      <div class="col-md-4">
        <h3>Voz IP</h3>    
    
        <ul class="alta-product-selection">
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/voip') }}">Línea IP</a>
          </li>
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/centralita') }}">Centralita</a>
          </li>      
          <!--
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="">Trunk</a>
          </li>      
          -->
          <li>
            <a class="btn btn-primary btn-huge btn-full-width"
               href="{{ URL::to('alta/ip-a-medida') }}">Configurar a medida</a>
          </li>
          <li>
            <a class="btn btn-primary btn-huge btn-full-width"
               href="{{ URL::to('alta/sip-trunks') }}">Sip trunks</a>
          </li>           
        </ul>    
      </div>
      <div class="col-md-4">
        <h3>Fax</h3>    
        
        <ul class="alta-product-selection">
          <li>
            <a class="btn btn-primary btn-huge btn-full-width"
               href="{{ URL::to('alta/fax') }}">Fax geográfico</a>
          </li>
          <li>
            <a class="btn btn-primary btn-huge btn-full-width" 
               href="{{ URL::to('alta/fax-902') }}">Fax 902</a>
          </li>      
        </ul>    
      </div>
    </div>
    <?php
    if(Session::get('ocultar') == 'no'){
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-warning">
          <p>Si quieres contratar <a target="_blank" href="http://fmeuropa.com/lineas-internacionales/">numeración internacional</a>
            o los servicios de <a target="_blank" href="http://fmeuropa.com/sms">envío masivo de SMS</a>
            ponte en <a target="_blank" href="http://fmeuropa.com/contacto">contacto</a> con nosotros.</p>
        </div>    
      </div>
    </div>
    <?php
    }
    ?>
@else
	<div class="alert alert-danger">
         El estado de su ficha no permite nuevas altas
    </div>
@endif
@stop
