@extends('alta.master')
@section('content')

<h1>Resumen del servicio a contratar</h1>

{{ Form::open(array(
  'action' => 'SignUpController@proceedToCheckout',
  'method' => 'post'
)) }}
<div class="row">
  <div class="col-md-6">
    @if($number !== null)
    	<h3>Número elegido{{ $portability ? ' (<strong>PORTADO</strong>)' : '' }}</h3>
    	@if($numero_bd == true)    		
    		<p class="alta-chosen-number">{{ $number->pp_numero }}</p>
        @endif
        @if($numero_bd == false)
        	<p class="alta-chosen-number">{{ $number }}</p>
        @endif
    @endif
    
    <h3>Plan escogido</h3>
    <p class="alta-chosen-plan">{{ $public_plan }}</p>
    
    <br/>
    <h3>Cupón</h3>
	<div class="row">
    	<div class="col-md-8">
      		<input class="form-control" type="text" id="name_cupon" name="cupon"/>
      	</div>
      	<div class="col-md-4">
      		<input type="button" value="Validar" id="validar_cupon" class="btn btn-primary"/>
      	</div>
    </div>
    	
  </div>
  
  <div class="col-md-6">
    <div class="row">
      <div class="col-md-6">
        <h3>Alta</h3>
        <p class="alta-chosen-number">0 €</p>
      </div>

      <div class="col-md-6">
        <h3>Cuota</h3>
        @if($cuota_anual == null)
        <p class="alta-chosen-number">{{ $cuota }} €/mes</p>
        @else        
        <div class="radio">
          <label>
            <input type="radio" class="radio-payment-cycle" 
                   name="annual-billing" value="false" checked>
            <strong>{{ number_format($cuota, 2) }} €/mes</strong>
          </label>
        </div> 

        <div class="radio">
          <label>
            <input type="radio" class="radio-payment-cycle" 
                   name="annual-billing" value="true">
            <strong>{{ number_format($cuota_anual / 12, 2) }} €/mes</strong> (contratando un año completo)
          </label>
        </div>              
        @endif
      </div>
    </div>
    <br/>
    <br/>
    <input type="hidden" name="portability" value="{{ $portability }}">
    <input type="hidden" name="alta-plan" value="{{ $plan }}">
    @if($number !== null)
    	@if($numero_bd == true)
    		<input type="hidden" name="alta-number" value="{{ $number->numero_visible }}">
        @endif
        @if($numero_bd == false)
    		<input type="hidden" name="alta-number" value="{{ $number }}">
        @endif
    @endif
    <input type="submit"
           class="btn btn-primary alta-btn-confirm"
           value="Es correcto. Continuar">
    
  </div>    
</div>
{{ Form::close() }}
<script language="javascript" type="text/javascript">
$('#validar_cupon').click(function(){
	$.getJSON("{{ URL::to('alta/cupon') }}/" + $('#name_cupon').val(), function(data) { 
		if(data.result == 't')
		{
			alert('El cupón aplica un descuento de un valor de '+data.data.valor);
		}
		else
		{
			alert('Cupón '+data.data.msg);
		}
	});
});
</script>
@stop

