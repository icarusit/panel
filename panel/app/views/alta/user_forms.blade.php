@extends('alta.master')

@section('content')

<h1>Tus datos</h1>

@include('errors')

<div class="row">  
  <div class="col-md-6">
    <h3>Aún no soy cliente</h3>
    
    {{ Form::open(array(
        'action' => 'UserController@create',
        'class'  => 'form-horizontal',
        'role'   => 'form',
        'method' => 'post',
        'id' => 'form_alta'
    )) }}    

    <?php $formData = Session::get('formData', null); ?>
    
    <input type="hidden" name="portability" value="{{ $portability }}">    
    <input type="hidden" name="alta-plan" value="{{ $plan }}">
    <input type="hidden" name="alta-number" value="{{ $number }}">    

    <?php $err = (bool)($errors->first('new-email')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">      
      <label class="col-sm-4 control-label" for="new-email">
        @if($err)
        {{ $errors->first('new-email') }}
        @else
        E-mail
        @endif
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control email" name="new-email" id="new-email"
               value="{{ $formData !== null ? $formData['new-email'] : '' }}" placeholder="Tu correo electrónico" />
      </div>
    </div>      

    <?php $err = (bool)($errors->first('new-user')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">      
      <label class="col-sm-4 control-label" for="new-user">
        @if($err)
        {{ $errors->first('new-user') }}
        @else
        Usuario
        @endif
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="new-user" id="new-user" 
               value="{{ $formData !== null ? $formData['new-user'] : '' }}" placeholder="Nombre de usuario" />
      </div>
    </div>        

    <?php $err = (bool)($errors->first('new-password')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">      
      <label class="col-sm-4 control-label" for="new-password">
        @if($err)        
        {{ $errors->first('new-password') }}
        @else
        Contraseña
        @endif
      </label>      
      <div class="col-sm-8">
        <input type="password" class="form-control" name="new-password" 
               id="new-password" value="" placeholder="Elige una contraseña" />
      </div>
    </div>        

    <?php $err = (bool)($errors->first('new-rpassword')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">      
      <label class="col-sm-4 control-label" for="new-rpassword">
      @if ($err)
      {{ $errors->first('new-rpassword') }}
      @else
      Repite la contraseña
      @endif
      </label>
      <div class="col-sm-8">
        <input type="password" class="form-control" name="new-rpassword" id="new-rpassword" 
               value="" placeholder="Vuelve a escribir la contraseña" />
      </div>
    </div>              

    <?php $err = (bool)($errors->first('new-phone')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">      
      <label class="col-sm-4 control-label" for="new-phone">
      @if ($err)
      {{ $errors->first('new-phone') }}
      @else
      Teléfono de contacto
      @endif
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control phone-number" name="new-phone" id="new-phone" 
               value="{{ $formData !== null ? $formData['new-phone'] : '' }}" placeholder="Un teléfono para hablar contigo" />
      </div>
    </div>       
    
    <div class="form-group">
      <div class="col-md-11 col-md-offset-1">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="new-accept-terms" value="1"> He leído y acepto los @if(Session::get('id_distribuidor') == 1)<a target="_blank" href="https://fmeuropa.com/aviso-legal/">términos y condiciones de uso</a>. @else términos y condiciones de uso. @endif
          </label>
        </div>          
      </div>    
    </div>

    <div class="form-group">
      <div class="col-sm-4 col-sm-offset-1">
        {{ Form::captcha() }}
      </div>    
    </div>
        
    <input type="button" id="enviar_form_alta" value="Continuar" class="btn btn-primary btn-login">
      
  {{ Form::close() }}   
  </div>
  <div class="col-md-6">
    <h3>Ya soy cliente</h3>    
    
    {{ Form::open(array(
        'action' => 'UserController@login',
        'class'  => 'form-horizontal',
        'role'   => 'form',
        'method' => 'post'  
    )) }}   
    
    <input type="hidden" name="redirect" value="{{ URL::to('alta') }}">
    <input type="hidden" name="portability" value="{{ $portability }}">        
    <input type="hidden" name="alta-plan" value="{{ $plan }}">
    <input type="hidden" name="alta-number" value="{{ $number }}">        
    
    <?php $err = (bool)($errors->first('user')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">            
      <label class="col-sm-4 control-label" for="user">
        @if($err)
        {{ $errors->first('user') }}
        @else
        Usuario
        @endif
      </label>
      <div class="col-sm-8">
        <input type="text" placeholder="Nombre de usuario" id="user" 
             name="user" class="form-control error" autocapitalize="off">        
      </div>
    </div>

    <?php $err = (bool)($errors->first('password')); ?>
    <div class="form-group {{ $err ? 'has-error' : '' }}">   
      <label class="col-sm-4 control-label" for="password">      
        @if($err)
        {{ $errors->first('password') }}
        @else
        Contraseña
        @endif
      </label>        
      <div class="col-sm-8">      
        <input type="password" placeholder="Contraseña" id="password"
               name="password" class="form-control">
      </div>
    </div>        

    <input type="submit" class="btn btn-primary btn-login" value="Acceder">
    {{ Form::close() }}       
  </div>
</div>
<script language="javascript" type="text/javascript">
$("#enviar_form_alta").click(function(e){
	var email = $("#new-email").val();
	$.ajax({
		url: "{{ URL::to('validar-mail') }}/"+email,
	 
		// The name of the callback parameter, as specified by the YQL service
	 
		// Tell jQuery we're expecting JSONP
		dataType: "json",
	 
		// Tell YQL what we want and that we want JSON
		data: {        
		},
	 
		// Work with the response
		success: function( response ) {
			if(response.valid == true)
			{
				$("#form_alta").submit();
			}
			else
			{
				alert(response.message);
			}
		}
	});
});



</script>

@stop
