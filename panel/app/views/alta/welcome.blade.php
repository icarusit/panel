@extends('alta.master')
@section('content')

@if(isset($number))
<h1>¡Has contratado tu número <strong>{{ $number }}</strong>!</h1>
@else
<h1>¡Ya tienes tu numeración operativa!</h1>
@endif

@yield('config')        
@stop