@extends('alta.master')
@section('content')
<h1>¡Has solicitado la portabilidad de tu número <strong>{{ $number }}</strong>!</h1>

<p>El plan a contratar es <strong>{{ $profile->perfil }}</strong> con un coste de
  alta de <strong>0 €</strong> y una cuota de mantenimiento de <strong>{{ $profile->cuota }} €/mes</strong>.</p>

<p>Descarga desde aqui el <a href='https://fmeuropa.com/wp-content/uploads/2013/05/portabilidad.pdf'>formulario de portabilidad</a>.</p>
<p>Devuélvenoslo rellenado por favor junto con la última factura de tu operador actual. Cuando lo recibamos iniciamos el proceso y os informaremos de fecha/hora de la ventana de portabilidad.</p>
<p>Un saludo y gracias por vuestra confianza.</p>

<p>Si tienes alguna duda, llámanos al número gratuito <strong>800 00 77 66</strong>
  de lunes a viernes de 9 a 18h o escríbenos a <a href="mailto:info@fmeuropa.com">info@fmeuropa.com</a>.</p>

<p>Gracias por confiar en <strong><?php echo Session::get('distribuidor'); ?></strong>.</p>

<a href="http://www.fmeuropa.com" class="btn btn-primary">Volver a nuestra web</a>
@stop