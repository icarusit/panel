@extends('main')

@section('subpage')
<h1>Se ha solicitado el cambio de plan</h1>

<p>Uno de nuestros comerciales se pondrá en contacto contigo para ayudarte a 
  elegir el plan más adecuado para tus necesidades.</p>

<p>Gracias por tu confianza.</p>
@stop