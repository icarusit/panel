{{-----------}}
{{-- MODAL --}}
{{-----------}}
<div class="modal-dialog">
  <div class="modal-content">      
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title" id="action-selector-modal-title">
        <span>Configurar acción
          <span class="label label-default">
            @yield('modal-title')
          </span>          
        </span>
      </h4>
    </div>
    <div class="modal-body">
      @yield('modal-content')
      
      <input type="hidden" id="modal-trigger" value="">
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      @if(!isset($hideSaveButton))
      <button type="button" id="modal-btn-save" class="btn btn-primary">Aceptar</button>
      @endif
    </div>
  </div>
</div>

