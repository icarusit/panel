@extends('components.actions.modals.base')

@section('modal-title') Desvíos @stop

@section('modal-content')
<p>Puedes añadir más de un desvío. Ten en cuenta que si lo haces, éstos se 
  ejecutarán en cascada. Es decir, uno después de otro.</p>

<form method="post" id="modal-forwards-form" class="form-inline">
  <div class="modal-forward-forwards">
    <p><a href="#" 
          class="btn btn-primary modal-forward-add-new-forward"
          data-loading-text="Espera...">
      <span class="glyphicon glyphicon-plus"></span>
      Añadir un nuevo desvío en cascada
    </a></p>
    
    @if($forwards === FALSE)
      <div class="modal-forward-forward-container">
        <h4>Desvío #1</h4>
        @include('components.forward')
      </div>
    @elseif(is_array($forwards))
      @foreach($forwards as $index => $stdForward)
        <div class="modal-forward-forward-container">
          <h4>Desvío #{{ $index }}</h4>  
          @include('components.forward', array('forward' => $stdForward, 'sipAccounts' => $sipAccounts))
          <a href="#" class="btn btn-primary remove-forward" title="Eliminar desvío secundario">
            <span class="glyphicon glyphicon-remove"></span> Eliminar
          </a>
        </div>
      @endforeach
    @endif
    <div class="row margin-30">
      <div class="col-md-12">      
        <h>Locución al operador</h5>

        <div class="form-group">
          <div class="col-md-8">
            <select class="form-control" name="select-forward-operator-speech" id="select-forward-operator-speech" 
              <?php if(count($speeches) == 0):
                $title = "Sube primero alguna locución de tipo general para poderla elegir como locución al operador";
              ?>
              title="{{ $title }}" disabled
              <?php endif; ?>>
              <option value="disabled" {{ $operatorSpeechSelected === false ? ' selected' : '' }}>Desactivada</option>

              @if($speeches !== false)
              @foreach($speeches as $speech)
              <option value="{{ $speech->id_speech }}" 
                      data-sample="{{ $speech->sample }}" 
                      data-speech-filename="{{ $speech->filename }}"
                      {{ $operatorSpeechSelected === $speech->public_name ? ' selected' : '' }}>{{ $speech->public_name }}</option>
              @endforeach
              @endif              
            </select>
          </div>          
        </div>        
      </div>
    </div>    
  </div>
</form>

<script>
$(document).ready(function() {
  var numForwards = 1;
  var panelMode = '{{ Session::get('panel_mode') }}';
  var sipAccounts = {{ json_encode($sipAccounts) }};

  $('.modal-forward-add-new-forward').click(function() {
    var $that = $(this);    
    $(this).button('loading');
    
    $.get('{{ url() }}/component/forward', {
      sipAccounts: sipAccounts
    }, function(data) {
      numForwards++;

      var html = '<div class="modal-forward-forward-container">';
      html+= '<h4>Desvío #' + numForwards + '</h4>';
      html+= data;
      html+= ' <a href="#" class="btn btn-primary remove-forward" title="Eliminar desvío">';
      html+= '<span class="glyphicon glyphicon-remove"></span> Eliminar desvío';
      html+= '</a></div>'; 
      
      $('.modal-forward-forward-container:last').after(html);
      $that.button('reset');
    });
  });
  
  $(document).on('keyup', '#modal-forwards-form .phone-number', function() {
	  	//Expresion regular de patrones que no debe tener el numero
      	var re = /^(?:8|9)[1-9][0-9]{7}$|^6[0-9]{8}$|^7[1-9][0-9]{7}$|^00\d+$/;
		
		var error_prefijo = false;
		
		<?php
			$prefijos_no = Config::get('prefijos.no_validos');
			$prefijos_no_re = "";
			foreach($prefijos_no as $keypn => $pn)
			{
				$prefijos_no[$keypn] = "^".$pn."\d+$";
			}
			$prefijos_no_re = implode('|',$prefijos_no);
			print 'var re_no = /'.$prefijos_no_re.'/;';
		?>
		
		
		if(re_no.test($(this).val()))
		{
			error_prefijo = true;  
		}
		else
		{
			if(!re.test($(this).val()))
		   	{			   
			  	error_prefijo = true; 
		   	}
		   	else
		   	{
			 	$(this).removeClass('input-text-error');
				$(this).addClass('input-text-success');
				var $that = $(this);
				setTimeout(function() {
				  $that.removeClass('input-text-success', 30);
				}, 1000);
				$(this).data('error', false);
				$(this).prop('title', '');
			}
		}
		  
		  
		 if(error_prefijo == true)
		 {        
			$(this).removeClass('input-text-success');
			$(this).addClass('input-text-error');
			$(this).data('error', true);
			$(this).prop('title', 'El número es inválido.');
		 }      
    });
  
  $('#modal-btn-save').click(function() {
	  
	  if($('#modal-forwards-form .select-forward-type').val() == 'phone-number')
	  {
		  var error = false;
		  $('#modal-forwards-form').find('.phone-number').each(function() {
				if($(this).data('error') === true)
				{
					alert('Por favor, revisa el formato de los desvíos introducidos. Alguno es erróneo.');
					error = true;
					return false;
				}
		  });
		  if(error == true)
		  {
			  return false;
		  }
	  }
	  
    var modalTrigger = $('#modal-trigger').val();
    var forwards = new Array();
    var strForwards = '';    
    
    $('.forward:not(:disabled)').each(function(i, v) {
      forwards.push({
        forward: v.value,
        timeout: panelMode === 'advanced'
          ? $('.forward-timeout')[i].value
          : 10
      });
    });
    
    var operatorSpeech = $('#select-forward-operator-speech').val() !== 'disabled'
      ? $('#select-forward-operator-speech').val()
      : false;
    
    var $operatorSpeechSel = $('#select-forward-operator-speech option:selected');
    
    var action = {
      type: 'forward',
      data: {
        forwards: forwards,
        operatorSpeech: {
          name: $operatorSpeechSel.html(),
          filename: $operatorSpeechSel.data('speech-filename'),
          sample: $operatorSpeechSel.data('sample') ? true : false
        }
      }
    };       

    $('input[id="' + modalTrigger + '"]').val(JSON.stringify(action));       
    
    $('#selector_' + modalTrigger + ' .selector-select').hide();
    
    for(i in forwards) {
      strForwards+= '<strong>' + forwards[i].forward + '</strong>';
      strForwards+= panelMode === 'advanced' ? ' (' + forwards[i].timeout + ' segundos)' : '';
      strForwards+= (i < forwards.length - 1 ? ', ' : '');
    }
    
    $('#selector_' + modalTrigger + ' .action-summary .action-config-button').attr('href', '{{ URL::to('component/actions.modals.forward') }}');      
    $('#selector_' + modalTrigger + ' .action-summary p').html('Desviado a ' + strForwards);    
    $('#selector_' + modalTrigger + ' .action-summary').show();
    
    $('#action-selector-modal').modal('hide');
    
    return false;
  });

  $(document).on('click', ".remove-forward", function() {
    $parent = $(this).parent();

    $parent.hide('slow', function() { $parent.remove(); });
  });
});  
</script>

@stop
