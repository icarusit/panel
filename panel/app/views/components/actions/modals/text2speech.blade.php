@extends('components.actions.modals.base')

@section('modal-title') Texto a voz @stop

@section('modal-content')
<p>Escribe el texto que quieres convertir a voz.</p>

<form method="post" id="modal-forwards-form" class="form-inline">
  <textarea id="text2speech-text" rows="6" class="form-control" style="resize: none;"></textarea>
  
  <a href="#" class="margin-15 btn btn-primary">Reproducir</a>
</form>

<script>
$(document).ready(function() {
  $('#modal-btn-save').click(function() {
    var modalTrigger = $('#modal-trigger').val();
   
    var action = {
      type: 'text2speech',
      data: {
        text: $('#text2speech-text').html()
      }
    };       

    $('input[name="' + modalTrigger + '"]').val(JSON.stringify(action)); 
    
    $('#action-selector-modal').modal('hide');
  });
});  
</script>

@stop
