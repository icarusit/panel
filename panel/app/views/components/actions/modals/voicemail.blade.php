@extends('components.actions.modals.base')

@section('modal-title') Buzón de voz @stop

@section('modal-content')

@if($speeches === FALSE)
  <p>No tienes locuciones disponibles para <strong>buzón de voz</strong>. <a href="{{ URL::to('speeches') }}">
      Sube una primero</a> para usarla en el sistema.</p>
@else
  <p>Elige una de tus locuciones de <strong>buzón de voz</strong> y haz click sobre ella.</p>
  <p>Administra <a href="{{ URL::to('speeches') }}">aquí</a> tus locuciones.</p>
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th></th>
          <th>Título</th>        
          <th>Añadida</th>
          <th>Procesada</th>
        </tr>
      </thead>
      <tbody>
      @foreach($speeches as $speech)
        <tr{{ $selectedSpeech === $speech->public_name ? ' class="active"' : '' }}
           @if($speech->sample) style="background-color: #f3f3f3 !important;" @endif>        
          <td>
            <a href="{{ URL::to('speeches/download/' . $speech->id_speech) }}"
               title="Escuchar la locución"
               class="speech-play">
              <span class="glyphicon glyphicon-play"></span>           
            </a>          
          </td>           
          <td><a class="speech-select" href="#" data-sample="{{ $speech->sample }}" data-speech-filename="{{ $speech->filename }}">{{ $speech->public_name }}</a></td>        
          <td>{{ $speech->date_added }}</td>
          <td>{{ $speech->converted ? "Sí" : "No" }}</td>        
        </tr>
      @endforeach
      <tbody>  
    </table>
  </div>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('.speech-play').click(function() {
      jBeep($(this).prop('href'));
      return false;
    });    
        
    $('.speech-select').click(function() {
      var modalTrigger = $('#modal-trigger').val();

      var action = {
        type: 'voicemail',
        data: {
          speech: {
            name: $(this).html(),
            filename: $(this).data('speech-filename'),
            sample: $(this).data('sample') ? true : false
          }
        }
      };       

      $('#selector_' + modalTrigger + ' .selector-select').hide();     
      $('#selector_' + modalTrigger + ' .action-summary .action-config-button').attr('href', '{{ URL::to('component/actions.modals.voicemail') }}');
      $('#selector_' + modalTrigger + ' .action-summary p').html('Desviado a buzón de voz con locución "<strong>' + action.data.speech.name + '</strong>".');    
      $('#selector_' + modalTrigger + ' .action-summary').show();     

      $('input[id="' + modalTrigger + '"]').val(JSON.stringify(action)); 

      $('#action-selector-modal').modal('hide');
      
      return false;
    });
  });  
  </script>
@endif

@stop
