<?php $disabled = isset($disabled) ? $disabled : FALSE; ?>

<div class="form-group selector {{ isset($action) && isset($action['class']) ? $action['class'] : '' }}"
     id="selector_{{ isset($action) ? $action['name'] : 'action_data' }}">  
  <div class="action-summary" @if(!isset($action['value'])) style="display: none;" @endif>
    <p>
      @if(isset($action['value'])) 
      {{ (new Action($action['value']))->parse() }} 
      @endif
    </p>
    
    <a class="btn btn-primary action-config-button"
       @if(isset($action['value']))
        href="{{ url() }}/component/actions.modals.{{ $action['value']->type }}"
       @else
        href="{{ url() }}/component/actions.modals.forward"
       @endif
       data-toggle="modal"
       data-target="#action-selector-modal"
       data-loading-text="Espera..."       
       data-modal-trigger="{{ isset($action) ? $action['name'] : 'action_data' }}">Editar</a>  
    <a class="btn btn-primary action-switch" href="#"
       style="margin-left: 5px;">Cambiar tipo acción</a>
  </div>
    
  <input type="hidden"
         name="{{ isset($action) ? $action['name'] : 'action_data' }}" 
         id="{{ isset($action) ? $action['name'] : 'action_data' }}"         
         value='{{ isset($action["value"]) ? json_encode($action["value"]) : "" }}'>      
  
  <div class="selector-select" @if(isset($action['value'])) style="display: none;" @endif>
    <select class="form-control action-type"
            name="action">
      <option value="" disabled @if(!isset($action['value'])) selected @endif>Elige un tipo de acción</option>
      <option value="forward" @if(isset($action['value']) && $action['value']->type === "forward") selected @endif>Desvíos</option>
      <option value="speech" @if(isset($action['value']) && $action['value']->type === "speech") selected @endif>Locución</option>    
      <option value="voicemail" @if(isset($action['value']) && $action['value']->type === "voicemail") selected @endif>Buzón de voz</option>
      {{-- <option value="text2speech" @if(isset($action['value']) && $action['value']->type === "text2speech") selected @endif>Texto a voz</option>--}}
    </select>

    <a class="btn btn-primary action-config-button @if(!isset($action)) disabled @endif"
        @if(isset($action['value']))
         href="{{ url() }}/component/actions.modals.{{ $action['value']->type }}"
        @else
         href="{{ url() }}/component/actions.modals.forward"
        @endif
        data-toggle="modal"
        data-target="#action-selector-modal"
        data-modal-trigger="{{ isset($action) ? $action['name'] : 'action_data' }}"
        data-loading-text="Espera..."
        {{ $disabled ? 'disabled' : '' }}>Configurar</a>
  </div>
</div>
