<div class="row">
  <div class="col-sm-8">   
    <div class="alta-custom-ip-block-wrapper">
      <h3>Número de <strong>líneas IP</strong> <small>cuentas SIP (mínimo 1)</small></h3>
      <div id="slider-ip-lines" class="alta-custom-ip-slider alta-custom-ip-slider-ip-lines"></div>      
    </div>

    <div class="alta-custom-ip-block-wrapper">
      <h3>Número de <strong>canales</strong> <small>llamadas simultáneas (mínimo 2)</small></h3>
      <div id="slider-channels" class="alta-custom-ip-slider alta-custom-ip-slider-channels"></div>
    </div>

    <div class="alta-custom-ip-block-wrapper">
      <h3>Servicio de centralita</h3>
      <p id="alta-custom-ip-err-centralita">Para el servicio de centralita son necesarias al menos dos líneas IP</p>
      
      <input type="checkbox" 
             class="alta-custom-ip-pbx-switch"
             id="alta-custom-ip-pbx" 
             name="alta-custom-ip-pbx"
             value="1"
             {{ $infoPlan->pbx ? 'checked' : '' }}>      
    </div>
  </div>
  <div class="col-sm-4">
    <div class="cart-custom-ip">
      <h3>Resumen plan</h3>

      <p class="cart-item">Números (<span id="geos">{{ $infoPlan->num_dids }}</span>)</p>
      <p class="cart-item">Líneas IP (<span id="ips">{{ $infoPlan->sip_accounts }}</span>)</p>
      <p class="cart-item">Canales (<span id="chns">{{ $infoPlan->channels }}</span>)</p>
      <p class="cart-item">Centralita (<span id="cent">{{ $infoPlan->pbx ? 'SÍ' : 'NO' }}</span>)</p>
      <hr />
      <p class="cart-item cart-prices">Precio final del plan:</p>
      <p class="cart-item cart-price cart-prices"><span id="price_ip">{{ number_format((float)$currentProfilePrice, 2) }}</span>€</p>   
      <p class="cart-item cart-prices">/mes</p>   

      {{ Form::open(array(
        'action' => 'ChangePlanController@postPlanCustomIP',
        'method' => 'post',
        'id'     => 'form-change-plan'
      )) }}    
        <input type="hidden" id="product-type" name="product-type" value="voip" />
        <input type="hidden" id="profile" name="profile" value="{{ $observaciones }}" />
        <input type="submit" id="confirm-change-plan" class="btn btn-success btn-huge btn-full-width" value="Cambiar plan" />
      {{ Form::close() }}
    </div>
  </div>
</div>

<script>
var centralita = "La centralita permite gestionar las llamadas recibidas (primero a una línea y luego a la otra, a ambas líneas a la vez, etc), transferir llamadas entre las líneas, poner llamadas en espera, etc. Exactamente igual que las centralitas tradicionales, pero sin aparatos físicos.";
var desc_lineas = "<p>Podemos entender las líneas como puestos de trabajo. En una centralita tradicional serían 'extensiones'.</p><p>Utiliza la barra de desplazamiento para variar el número de líneas que deseas contratar.</p>";
var desc_canales = "<p>Los canales de voz son el número de llamadas a la vez que se pueden realizar o recibir. En una centralita tradicional serían número de 'líneas'.</p><p>El número de canales debe ser como mínimo el doble que el número de líneas IP.</p><p>Utiliza la barra de desplazamiento para variar el número de canales que deseas contratar.</p>";

var auxid = 0;
var displaymap = false;
var planstr = "";
var tlfs = new Array();

function price_reload()
{
  var pgeo = 3;
  var plip = 1;
  var pchn = 0.5;
  var pcent = 7;

  var no_geos = $("#geos").html();
  var no_ips = $("#ips").html();
  var no_chns = $("#chns").html();
  var cent = $("#cent").html() === "SÍ" ? 1 : 0;

  var price = no_geos*pgeo + no_ips*plip + no_chns*pchn + pcent*cent;

  planstr = 'IP_' + no_geos + '_' + no_ips + '_' + no_chns + '_' + cent;
  
  $("#profile").attr("value", planstr);
  $("#product-type").attr("value", cent === 1 ? 'pbx' : 'voip');
  $("#price_ip").html(price.toFixed(2));
}

$(document).ready(function() {
  $("#slider-ip-lines").slider({
    <?php
    // ÑAPA
    switch($infoPlan->sip_accounts) {
      case 1:  $sliderValue = 1; break;
      case 2:  $sliderValue = 2; break;      
      case 4:  $sliderValue = 3; break;
      case 6:  $sliderValue = 4; break;
      case 8:  $sliderValue = 5; break;
      case 10: $sliderValue = 6; break;      
    }
    ?>
    value: {{ $sliderValue }},
    min: 1,
    max: 6,
    step: 1,
    slide: function(event, ui) {
      var val = ui.value;

      switch(val) {
        case 1: 
          val = 1;
          var cent = $("#cent").html() === "SÍ" ? true : false;

          if (cent) {
            $("#alta-custom-ip-err-centralita").show();
            $('.alta-custom-ip-pbx-switch').bootstrapSwitch('state', false);
            $("#slider_cent").slider("value", 0);
          }
          break;
        case 2: val = 2; break;      
        case 3: val = 4; break;
        case 4: val = 6; break;
        case 5: val = 8; break;
        case 6: val = 10; break;
      }

      $("#marcador-ips").html(val);                        
      $("#ips").html(val);            
      var no_chns = 2*val;
      $("#marcador-chns").html(no_chns);            
      $("#chns").html(no_chns);            
      $("#slider-channels").slider("value", no_chns);
      price_reload();
    }
  });

  $("#slider-channels").slider({ 
    value: {{ $infoPlan->channels }},    
    min: 2,
    max: 20,
    step: 2,
    slide: function(event, ui) {  
      var val = ui.value;
      var no_ips = parseInt($("#ips").html());
      $("#marcador-chns").html(val);
      $("#chns").html(val);
      $("#err-centralita").hide();

      if (val < (2*no_ips)) {
        val = 2*no_ips;
        $("#marcador-chns").html(val);                
        $("#chns").html(val);
        return false;
      }

      price_reload();      
    }
  });    

  $('#alta-custom-ip-pbx').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      if ($("#ips").html() > 1) {
        $("#err-centralita").hide();
        $("#cent").html(state ? 'SÍ' : 'NO');
        price_reload();
      }
      else {
        $("#err-centralita").show();
        return false;
      }
    }
  });  
  
  $('#alta-custom-ip-pbx').addClass('bs-switch');  
  
  $('#confirm-change-plan').click(function(e) {  
    if(confirm("Se procederá al cambio de plan. ¿Quieres continuar?")) {
      $('#form-change-plan').submit();
    } else {
      e.preventDefault();      
    }
  });  
});
</script>