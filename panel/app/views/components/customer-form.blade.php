<?php 
$formData = Session::get('formData', null); 

if($formData !== null) extract($formData);
?>

<div class="form-group">
  <label class="col-sm-5 control-label">¿Es empresa?</label>
  <div class="col-sm-7">  
    <input type="checkbox" class="bs-switch" name="is_company" value="1" 
           {{ isset($is_company) && $is_company ? ' checked' : '' }} {{ $new_client ? 'disabled' : '' }}>
  </div>
</div>

<div class="form-group">
  <label id="label-name" class="col-sm-5 control-label">{{ $is_company ? 'Razón social' : 'Nombre' }}</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="name" id="name" 
           value="{{ isset($name) ? $name :'' }}" {{ $new_client ? 'readonly' : '' }} />
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label" id="label-cnif">
    {{ $is_company ? 'CIF' : 'NIF/NIE' }}
  </label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="cnif" id="cnif" 
           value="{{ isset($cnif) ? $cnif :'' }}"  {{ $new_client ? 'readonly' : '' }} />
  </div>
</div>

<div class="form-group form-group-legal" style="{{ $is_company ? '' : 'display: none;' }}">
  <label class="col-sm-5 control-label">Representante legal</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="legal_rep_name" id="legal_rep_name" 
           value="{{ isset($legal_rep_name) ? $legal_rep_name :'' }}" {{ $new_client ? 'readonly' : '' }}/>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">País</label>
  <div class="col-sm-7">
    <select class="form-control" name="country" id="country" {{ $new_client ? 'readonly' : '' }}>
      @foreach($countries as $country)
      <?php 
      $selected = '';
      
      if($user_country === null && $country->iso === 'ES') {
        $selected = 'selected';
      } elseif($country == $user_country) {
        $selected = 'selected';
      }
      ?>
      <option value="{{ $country->iso }}"{{ $selected }}>{{ $country->country_es }}</option>
      @endforeach
    </select>    
  </div>
</div>

<div class="form-group form-group-legal" style="{{ $is_company ? '' : 'display: none;' }}">
  <label class="col-sm-5 control-label">NIF rep. legal</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="legal_rep_nif" id="legal_rep_nif" 
           value="{{ isset($legal_rep_nif) ? $legal_rep_nif :'' }}" {{ $new_client ? 'readonly' : '' }}/>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">Dirección</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="address" id="address" 
           value="{{ isset($address) ? $address : '' }}" {{ $new_client ? 'readonly' : '' }}/>
  </div>
</div>


<div class="form-group">
  <label class="col-sm-5 control-label">Población</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="city" id="city"
           value="{{ isset($city) ? $city : '' }}" {{ $new_client ? 'readonly' : '' }}/>
  </div>
</div>

<div class="form-group block-province">
  <label class="col-sm-5 control-label">Provincia</label>
  <div class="col-sm-7">
    <select class="form-control" name="province" style="display: {{ $user_country === null ? 'block' : ($user_country->iso === 'ES' ? 'block' : 'none') }};"
            id="province-select" {{ $new_client ? 'readonly' : '' }}>
      <option value="Álava" {{ $province === 'Álava' ? ' selected' : ''}}>Álava</option>
      <option value="Albacete" {{ $province === 'Albacete' ? ' selected' : ''}}>Albacete</option>
      <option value="Alicante" {{ $province === 'Alicante' ? ' selected' : ''}}>Alicante</option>
      <option value="Almería" {{ $province === 'Almería' ? ' selected' : ''}}>Almería</option>
      <option value="Asturias" {{ $province === 'Asturias' ? ' selected' : ''}}>Asturias</option>
      <option value="Ávila" {{ $province === 'Ávila' ? ' selected' : ''}}>Ávila</option>
      <option value="Badajoz" {{ $province === 'Badajoz' ? ' selected' : ''}}>Badajoz</option>
      <option value="Baleares" {{ $province === 'Baleares' ? ' selected' : ''}}>Baleares</option>
      <option value="Barcelona" {{ $province === 'Barcelona' ? ' selected' : ''}}>Barcelona</option>
      <option value="Burgos" {{ $province === 'Burgos' ? ' selected' : ''}}>Burgos</option>
      <option value="Cáceres" {{ $province === 'Cáceres' ? ' selected' : ''}}>Cáceres</option>
      <option value="Cádiz" {{ $province === 'Cádiz' ? ' selected' : ''}}>Cádiz</option>
      <option value="Cantabria" {{ $province === 'Cantabria' ? ' selected' : ''}}>Cantabria</option>
      <option value="Castellón de la Plana" {{ $province === 'Castellón de la Plana' ? ' selected' : ''}}>Castellón de la Plana</option>
      <option value="Ciudad Real" {{ $province === 'Ciudad Real' ? ' selected' : ''}}>Ciudad Real</option>
      <option value="Córdoba" {{ $province === 'Córdoba' ? ' selected' : ''}}>Córdoba</option>
      <option value="Coruña" {{ $province === 'Coruña' ? ' selected' : ''}}>Coruña</option>
      <option value="Cuenca" {{ $province === 'Cuenca' ? ' selected' : ''}}>Cuenca</option>
      <option value="Girona" {{ $province === 'Girona' ? ' selected' : ''}}>Girona</option>
      <option value="Granada" {{ $province === 'Granada' ? ' selected' : ''}}>Granada</option>
      <option value="Guadalajara" {{ $province === 'Guadalajara' ? ' selected' : ''}}>Guadalajara</option>
      <option value="Guipúzcoa" {{ $province === 'Guipúzcoa' ? ' selected' : ''}}>Guipúzcoa</option>
      <option value="Huelva" {{ $province === 'Huelva' ? ' selected' : ''}}>Huelva</option>
      <option value="Huesca" {{ $province === 'Huesca' ? ' selected' : ''}}>Huesca</option>
      <option value="Jaén" {{ $province === 'Jaén' ? ' selected' : ''}}>Jaén</option>
      <option value="La Rioja" {{ $province === 'La Rioja' ? ' selected' : ''}}>La Rioja</option>
      <option value="Las Palmas" {{ $province === 'Las Palmas' ? ' selected' : ''}}>Las Palmas</option>
      <option value="León" {{ $province === 'León' ? ' selected' : ''}}>León</option>
      <option value="Lleida" {{ $province === 'Lleida' ? ' selected' : ''}}>Lleida</option>
      <option value="Lugo" {{ $province === 'Lugo' ? ' selected' : ''}}>Lugo</option>
      <option value="Madrid" {{ $province === 'Madrid' ? ' selected' : ''}}>Madrid</option>
      <option value="Málaga" {{ $province === 'Málaga' ? ' selected' : ''}}>Málaga</option>
      <option value="Murcia" {{ $province === 'Murcia' ? ' selected' : ''}}>Murcia</option>
      <option value="Navarra" {{ $province === 'Navarra' ? ' selected' : ''}}>Navarra</option>
      <option value="Orense" {{ $province === 'Orense' ? ' selected' : ''}}>Orense</option>
      <option value="Palencia" {{ $province === 'Palencia' ? ' selected' : ''}}>Palencia</option>
      <option value="Pontevedra" {{ $province === 'Pontevedra' ? ' selected' : ''}}>Pontevedra</option>
      <option value="Salamanca" {{ $province === 'Salamanca' ? ' selected' : ''}}>Salamanca</option>
      <option value="Santa Cruz de Tenerife" {{ $province === 'Santa Cruz de Tenerife' ? ' selected' : ''}}>Santa Cruz de Tenerife</option>
      <option value="Segovia" {{ $province === 'Segovia' ? ' selected' : ''}}>Segovia</option>
      <option value="Sevilla" {{ $province === 'Sevilla' ? ' selected' : ''}}>Sevilla</option>
      <option value="Soria" {{ $province === 'Soria' ? ' selected' : ''}}>Soria</option>
      <option value="Tarragona" {{ $province === 'Tarragona' ? ' selected' : ''}}>Tarragona</option>
      <option value="Teruel" {{ $province === 'Teruel' ? ' selected' : ''}}>Teruel</option>
      <option value="Toledo" {{ $province === 'Toledo' ? ' selected' : ''}}>Toledo</option>
      <option value="Valencia" {{ $province === 'Valencia' ? ' selected' : ''}}>Valencia</option>
      <option value="Valladolid" {{ $province === 'Valladolid' ? ' selected' : ''}}>Valladolid</option>
      <option value="Vizcaya" {{ $province === 'Vizcaya' ? ' selected' : ''}}>Vizcaya</option>
      <option value="Zamora" {{ $province === 'Zamora' ? ' selected' : ''}}>Zamora</option>
      <option value="Zaragoza" {{ $province === 'Zaragoza' ? ' selected' : ''}}>Zaragoza</option>  
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">Código postal</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="post_code" id="post_code"
           value="{{ isset($post_code) ? $post_code : '' }}" {{ $new_client ? 'readonly' : '' }}/>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">E-mail</label>
  <div class="col-sm-7">
    <input type="text" class="form-control email" name="email" id="email"
           value="{{ isset($email) ? $email :'' }}" />
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">Teléfono</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="phone" id="phone"
           value="{{ isset($phone) ? $phone :'' }}" />
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">Fax</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="fax" id="fax"
           value="{{ isset($fax) ? $fax :'' }}" />
  </div>
</div>

<div class="form-group">
  <label class="col-sm-5 control-label">WhatsApp</label>
  <div class="col-sm-7">
    <input type="text" class="form-control" name="whatsapp" id="whatsapp"
           value="{{ isset($whatsapp) ? $whatsapp :'' }}" />
  </div>
</div>


<script type="text/javascript">
$(document).ready(function() { 
  $('.bs-switch').on('switchChange.bootstrapSwitch', function(event, state) {
    if(state === true) {
      // Es empresa 
      $('.form-group-legal').show();
      $('#label-cnif').html('CIF');
      $('#label-name').html('Razón social');
    } else {
      $('.form-group-legal').hide();    
      $('#label-cnif').html('NIF/NIE');    
      $('#label-name').html('Nombre');
    }
  });

  $('#country').change(function() {
    if($(this).val() !== 'ES') {
      $('.block-province').hide();
    } else {
      $('.block-province').show();
    }
  });
})
</script>
