<?php
$isSipAccount = false;

if(isset($forward)) {
  $isSipAccount = !is_numeric($forward->forward);
  if($isSipAccount) $selectedSipAccount = $forward->forward;
}

$number = Session::get('number');
?>

@if($number->getType() === 'analog')
<div class="form-group">
  <select class="form-control select-forward-type"          
    @if(!isset($sipAccounts) || !count($sipAccounts)) title="¿Quieres hacer desvío a IP? Contrata una cuenta SIP desde nuestra web y automáticamente te aparecerá disponible en el menú de desvíos" disabled @endif>
    <option value="phone-number"{{ !$isSipAccount ? ' selected' : '' }}>Teléfono</option>
    <option value="sip-account"{{ $isSipAccount ? ' selected' : '' }}>SIP</option>
  </select>
</div>
@endif
<div class="form-group">
  @if($number->getType() === 'analog')
  <input class="form-control forward phone-number" type="text" 
         autocomplete="off"
         id="{{ isset($name) ? $name : 'forward[]' }}" 
         value="{{ (isset($forward) && !$isSipAccount) ? $forward->forward : '' }}"
         name="{{ isset($name) ? $name : 'forward[]' }}"
         @if($isSipAccount)
         style="display: none;"
         disabled
         @endif
         >
  @endif
  
  @if((isset($sipAccounts) && count($sipAccounts) > 0) || $number->getType() !== 'analog')
  <select class="form-control forward-sip-accounts forward"
          name="{{ isset($name) ? $name : 'forward[]' }}"
          @if($isSipAccount || $number->getType() !== 'analog')
          style="display: inline-block;"
          @else
          disabled
          @endif
          >          

    @foreach($sipAccounts as $sipAccount)
    <option val="{{ $sipAccount }}"
    {{ (isset($forward) && $isSipAccount && $sipAccount == $selectedSipAccount) ? ' selected' : '' }}>{{ $sipAccount }}</option>
    @endforeach
  </select>
  @endif
</div>

@if(Session::get('panel_mode') === 'advanced')
<div class="form-group">
  <input class="form-control forward-timeout" type="text"  autocomplete="off"
         id="" value="{{ isset($forward) ? $forward->timeout : 60 }}" 
         name="{{ isset($name) ? $name . '-timeout' : 'forward-timeout[]' }}"
         placeholder="Tiempo" title="Tiempo durante el que llamaremos al desvío (timeout)">  
</div>
@endif

