<div class="recorder">
  <audio id="speech-recording-audio"></audio>
  <ul>
    <li>
      <a href="#" class="speech-recording-counter">
        <span class="glyphicon glyphicon-time"></span>
        <span id="speech-recording-counter-text">00:00</span>
      </a>     
    </li>    
    <li class="rounded-right">
      <a href="#" class="speech-recording-record">
        <span class="glyphicon glyphicon-record"></span>
        <span id="speech-recording-record-text">Grabar</span>
      </a>     
    </li>
    <li class="rounded-right">
      <a href="#" class="speech-recording-stop">
        <span class="glyphicon glyphicon-stop"></span> Detener
      </a>     
    </li> 
    <li>
      <a href="#" class="speech-recording-download" id="save">
        <span class="glyphicon glyphicon-download"></span> Descargar
      </a>
    </li>
    <!--<li class="disabled">
      <a href="#" class="speech-recording-play">
        <span class="glyphicon glyphicon-play"></span> Reproducir
      </a>     
    </li>-->    
  </ul>
</div>