<select class="form-control<?=isset($class) ? ' ' . $class : ''?>" name="<?=$name?>">
  <?php for($h=0; $h<24; $h++): ?>  
    <?php for($m=0; $m<60; $m=$m+15): ?>
      <?php $optVal = ($h<10 ? '0'.$h : $h) . ':' . ($m<10 ? '0'.$m : $m); ?>
      <option<?=$value==$optVal ? ' selected' : ''?>><?=$optVal?></option>
    <?php endfor; ?>
  <?php endfor; ?>
  <option<?=$value=='23:59' ? ' selected' : ''?>>23:59</option>
</select>
