{{ Form::open(array(
  'action' => 'BalanceController@pay',
  'class'  => 'form-horizontal'
)) }}
  <div class="form-group">
    <label class="col-md-7 control-label">
      Cantidad a recargar
    </label>
    <div class="col-md-5">
      <select class="form-control" name="amount">
        <option value="5">5 €</option>
        <option value="10">10 €</option>
        <option value="15">15 €</option>
        <option value="20" selected>20 €</option>
        <option value="25">25 €</option>
        <option value="30">30 €</option>
        <option value="40">40 €</option>
        <option value="50">50 €</option>
        <option value="70">70 €</option>
        <option value="100">100 €</option>
        <option value="150">150 €</option>
        <option value="200">200 €</option>            
      </select>
    </div>
  </div>
  <div class="form-group">
  	<label class="col-md-7 control-label">
      Forma de pago
    </label>
    <div class="col-md-5">
    	<select class="form-control" name="forma_pago">
        	<option selected="selected" value="redsys">Tarjeta</option>
            <option value="paypal">Paypal</option>
        </select>
    </div>
  </div>

  <input type="submit" class="btn btn-primary pull-right" 
         value="Iniciar proceso de recarga"
         id="balance-submit"
         data-loading-text="Conectando con el banco, por favor, espera...">
{{ Form::close() }}
