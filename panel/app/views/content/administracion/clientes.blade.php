@extends('main')

@section('subpage')
<h1>{{$title}}</h1>

@include('errors')

<?php if(count($clientes) == 0): ?>
<div class="row margin-15">
  <div class="col-md-12">
    <p>No tienes clientes aún.</p>
  </div>
</div>
<?php else: ?>
	<script type="text/javascript">
                $(function() {
                    $('#numeros').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "aoColumnDefs": [{ "sType": "date-uk", "aTargets": [2]}],	
                        "columnDefs": [ { "targets": 5, "orderable": false }]	,
                         "aoColumns": [
                            { "sType": "numeric-html" },
                            { "sType": "string" },
                            { "sType": "date-uk" },
                            { "sType": "numeric-html" }					
                        ]			
                    });
    
                });
            </script>
    <table class="table table-hover table-responsive table-striped margin-15" id="numeros">
        <thead>        
            <tr>
                <th class="numbers-list-col-number">Cliente</th>
                <th class="numbers-list-col-plan">Correo</th>
                <th class="numbers-list-col-sign-up-date">Fecha alta</th>
                <th class="numbers-list-col-reversion">Saldo</th>
            </tr>
         </thead>
        <tbody data-link="row" data-provides="rowlink">
    
            <?php
                $glyphicon = array(
                  'fax'    => 'print',
                  'analog' => 'phone-alt',
                  'voip'   => 'earphone',
                  'pbx'    => 'transfer',
                  'trunk'  => 'random'
                );
            ?>
       
            <?php foreach($clientes as $cliente): ?>      
                <tr<?php if ($selectedCliente != NULL && $selectedCliente->getNumber() == $cliente->id_cliente): ?> class="active"<?php endif; ?>>
                    <td class="numbers-list-col-number">
                        <a href="<?=URL::to('simular-cliente/' . $cliente->id_cliente.'/'.$id_administrador)?>">
                            <?=$cliente->nombre_fiscal?>
                        </a>
                    </td>
                    <td class="numbers-list-col-plan"><?=$cliente->mail_1?></td>
                    <?php $date = new \DateTime($cliente->fecha_alta); ?>
                    <td class="numbers-list-col-sign-up-date"><?=$date->format('d/m/Y')?></td>
                    <td class="numbers-list-col-pricing">
                        <span title="<?=$cliente->userbalance >= '0'?>"><?=$cliente->userbalance?></span>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        
    </table>
<?php endif; ?>
@stop