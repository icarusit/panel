@extends('main')

@section('subpage')
<h1>Saldo</h1>

@include('help', array('content' => 'balance'))
@include('errors')

<div class="row">
  <div class="col-md-5">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Recargar el saldo con tarjeta crédito/débito</h3>
      </div>
      <div class="panel-body">
        @include('components.top-up-credit')
      </div>
    </div>    
  </div>
</div>

<script type="text/javascript">
$('form').submit(function() {
  $('#balance-submit').button('loading');
});
</script>
@stop
