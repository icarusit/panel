@extends('main')

@section('subpage')
<h1>Grabación de llamadas</h1>

@include('help', array('content' => 'callrecording'))
@include('errors')

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Estado del servicio de grabación de llamadas</h3>
  </div>
  <div class="panel-body">
    {{ Form::open(array(
      'action' => 'CallRecordingController@setCallRecording',
      'class'  => 'form-horizontal'
    )) }}
      @if(!$lopdContract)
      <div class="alert alert-info" role="alert"
        <p>Escríbenos a <a href="mailto:info@<?php echo Session::get('dominio'); ?>">info@<?php echo Session::get('dominio'); ?></a> para
        que te remitamos el contrato exigido para la protección de datos y activar
        el servicio de grabación de llamadas en tu número.</p>
      </div>
      @endif

      <div class="form-group">
        <label class="col-sm-3 control-label">Grabación de llamadas</label>
        <div class="col-sm-4">
          <input type="hidden" name="callrecording"
                 value="{{ $isCallRecordingActive != TRUE ? 'enable' : 'disable' }}">
          <p class="form-control-static orange-hightlight">
            {{ ($isCallRecordingActive == TRUE && $lopdContract) ? 'activado' : 'desactivado' }}
            @if($lopdContract)
            <button class="btn btn-primary" style="margin-left: 10px;">
              {{ $isCallRecordingActive == TRUE ? 'Desactivar' : 'Activar' }}
            </button>
            @endif
          </p>
        </div>
      </div>
    {{ Form::close() }}
  </div>
</div>

@if($isCallRecordingActive == TRUE && $lopdContract)
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
      Registro de llamadas grabadas desde el día <strong>{{ $fifteenDaysAgoDate }}</strong>
    </h3>
  </div>
  <div class="panel-body">
    @if(count($recordings) > 0)
      {{ Form::open(array(
        'id'     => 'form-download-bulk',
        'action' => 'CallRecordingController@downloadBulk',
        'class'  => 'form-horizontal'
      )) }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-sm-5 control-label">Descarga en bloque:</label>
            <div class="col-sm-7">
              <div class="row">
                <select name="period" id="recording-download-period" class="form-control">
                  <option value="today">Grabaciones de hoy</option>
                  <option value="yesterday">Grabaciones de ayer</option>
                  <option value="all">Grabaciones últimos 15 días</option>                
                  <option value="custom">Un periodo determinado</option>
                </select>              
              </div>
              <div class="row recording-custom-period">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Desde:</label>                
                    <input type="text" 
                           name="period-from"
                           class="form-control recording-download-date"
                           id="recording-download-date-from">
                  </div>    
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Hasta:</label>                
                    <input type="text"
                           name="period-until"
                           class="form-control recording-download-date"
                           id="recording-download-date-until">
                  </div>
                </div>            
              </div>
            </div>
          </div>
        </div>          
      </div>        
      <div class="row">
        <div class="form-group">
          <div class="col-md-6">
            <input type="submit" class="btn btn-primary pull-right" value="Descargar">
          </div>
        </div>      
      </div>        
      {{ Form::close() }}
      
      {{ Form::open(array(
        'id'     => 'form-filtro',
        'action' => 'CallRecordingController@show',
        'class'  => 'form-horizontal',
        'method' => 'GET'
      )) }}
		<div class="row">        
        	<div class="col-md-6">
            	<div class="form-group">
                	<div class="col-sm-6">
                    </div>
                    <div class="col-sm-4" style="text-align:right !important;">
                        <input type="text" class="form-control input-sm" style="margin-left:10px;margin-top:1px;" name="numero_filtro" value="{{ $numero_filtro }}"/> 
                    </div>
                    <div class="col-sm-2">
                        <input type="submit" class="btn btn-primary pull-right" value="Buscar">
                    </div>
                 </div>
          	</div>     
		</div>
      {{ Form::close() }}
      
      <div class="table-responsive">
        <table class="table table-stretch table-striped table-hover">
          <thead>
            <tr>
              <th></th>
              <th>Llamante</th>
              <th>Fecha-hora</th>
            </tr>
          </thead>

          <tbody>
            @foreach($recordings as $recording)
            <tr>
              <td>
                <a href="{{ URL::to('callrecording/download/' . $recording->file) }}"
                   title="Escuchar la grabación"
                   class="recording-play">
                  <span class="glyphicon glyphicon-play"></span>
                </a>
                <a href="{{ URL::to('callrecording/download/' . $recording->file) }}"
                   title="Descargar la grabación">
                  <span class="glyphicon glyphicon-download-alt"></span>
                </a>
                <a href="{{ URL::to('callrecording/remove/' . $recording->file) }}"
                  title="Eliminar la grabación"
                  class="recording-remove">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>
              </td>
              <td>{{ in_array($recording->caller, ['unknown', '']) ? 'Sin identificar' : $recording->caller }}</td>
              <?php $dt = new \DateTime($recording->datetime); ?>
              <td>{{ $dt->format('d-m-Y H:i') }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    	{{ $paginator->appends(array('numero_filtro' => $numero_filtro))->links() }}
    @else
      <p>No tienes aún grabaciones disponibles.</p>

      <p>Si has recibido llamadas en los últimos minutos las grabaciones se están
        propagando entre nuestros servidores por lo que es posible que no
        aparezcan aún en el listado.</p>
    @endif
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('.recording-download-date').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '{{ (new \DateTime('20 days ago'))->format('d/m/Y') }}',
    endDate: '{{ (new DateTime())->format('d/m/Y') }}',
    weekStart: 1,
    autoclose: true,
    language: 'es'
  }).on('changeDate', function(e) {
    if($(this)[0].id === 'recording-download-date-from') {
      d = e.date;
      // http://stackoverflow.com/a/13459946/340494
      function pad(s) { return (s < 10) ? '0' + s : s; }
      var fmtDate = [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
      $('#recording-download-date-until').datepicker('setStartDate', fmtDate);
    }
  });

  $('.recording-play').click(function() {
    jBeep($(this).prop('href'));
    return false;
  });

  $('#recording-download-period').change(function() {
    if($(this).val() === 'custom') {
      $('.recording-custom-period').fadeIn();
    } else {
      $('.recording-custom-period').hide();    
    }
  })

  $('.recording-remove').click(function() {
    return confirm("Si eliminas la grabación no habrá forma de recuperarla de nuevo. Te recomendamos que antes la descargues a tu ordenador. ¿Deseas continuar?");
  });  
})
</script>
@endif

@stop
