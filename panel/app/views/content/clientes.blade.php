@extends('main')

@section('subpage')
<h1>Mis clientes</h1>

@include('errors')

<?php if(count($clientes) == 0): ?>
<div class="row margin-15">
  <div class="col-md-12">
    <p>No tienes números clientes aún.</p>
  </div>
</div>
<?php else: ?>
<script type="text/javascript">
            $(function() {
                $('#numeros').dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false,
					"aoColumnDefs": [{ "sType": "date-uk", "aTargets": [2]}],	
					"columnDefs": [ { "targets": 5, "orderable": false }]	,
					 "aoColumns": [
						{ "sType": "numeric-html" },
						{ "sType": "string" },
						{ "sType": "date-uk" },
						{ "sType": "numeric-html" },
						{ "sType": "string" },
						{ "sType": "numeric-html" },
						null						
					]			
                });

            });
        </script>
<table class="table table-hover table-responsive table-striped margin-15" id="numeros">
  <thead>        
    <tr>
      <th class="numbers-list-col-number">Cliente</th>
      <th class="numbers-list-col-plan">Correo</th>
      <th class="numbers-list-col-sign-up-date">Fecha alta</th>
      <th class="numbers-list-col-reversion">Saldo</th>
    </tr>
  </thead>
  <tbody data-link="row" data-provides="rowlink">

    <?php
    $glyphicon = array(
      'fax'    => 'print',
      'analog' => 'phone-alt',
      'voip'   => 'earphone',
      'pbx'    => 'transfer',
      'trunk'  => 'random'
    );
    ?>
   
    <?php foreach($clientes as $cliente): ?>      
    <?php
    // Ñapa para no modificar la API, se debería meter el dato del tipo de pago
    // en la respuesta number de la API y añadirla a la respuesta del wrapper
    //$rowNumber = Number::where('numero_visible', '=', $number->number)->first();
    //$rowProfile = Profile::find($rowNumber->id_perfil);
    ?>
    <tr<?php if ($selectedCliente != NULL && $selectedCliente->getNumber() == $cliente->id_cliente): ?> class="active"<?php endif; ?>>
      <td class="numbers-list-col-number">
        <a href="<?=URL::to('cliente/' . $cliente->usr_usuario)?>">
          <?=$cliente->nombre_fiscal?>
        </a>
      </td>
      <td class="numbers-list-col-plan"><?=$cliente->mail_1?></td>
      <?php $date = new \DateTime($cliente->fecha_alta); ?>
      <td class="numbers-list-col-sign-up-date"><?=$date->format('d/m/Y')?></td>
      <td class="numbers-list-col-pricing">
        <span title="<?=$cliente->userbalance >= '0'?>"><?=$cliente->userbalance?></span>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
    
</table>
<?php endif; ?>

<?php if(($numberOfPages) > 1): ?>
<ul class="pagination">
  <?php if($currentPage > 1): ?><li><a href="<?=URL::to('misclientes')?>/<?=$currentPage - 1?>">&laquo;</a></li><?php endif; ?>

  <?php for($i=1; $i<=$numberOfPages; $i++): ?>
  <li<?php if($currentPage == $i): ?> class="active"<?php endif;?>><a href="<?=URL::to('misclientes')?>/<?=$i?>"><?=$i?></a></li>
  <?php endfor; ?>

  <?php if($currentPage < $numberOfPages): ?><li><a href="<?=URL::to('misclientes')?>/<?=$currentPage + 1?>">&raquo;</a></li><?php endif; ?>
</ul>
<?php endif; ?>
<input type="hidden" id="id_subscriber" />
<script type="text/javascript">
/*
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/aa6a36f5b30966dc6e5edcce898d61b0.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
		
		p._pcq.push(['APIReady', callbackFunction]);
    })(window,document);
	function callbackFunction()
	{
		document.getElementById('id_subscriber').value = (pushcrew.subscriberId);
	}
	*/
</script>
@stop
