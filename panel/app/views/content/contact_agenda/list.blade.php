@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-user"></span> Agenda de contacto</h1>

@include('help', array('content' => 'contact_agenda', 'number' => $number))
@include('errors')

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Añadir nuevo contacto</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
      		{{ Form::open(array(
        		'action' => 'ContactAgendaController@add',
        		'class'  => 'form-horizontal col-md-5'
      		)) }}
 
       			<div class="form-group">
         			<label for="number" class="col-md-2 control-label">Número</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="number" value="" />
         			</div>               
       			</div>
                <div class="form-group">
         			<label for="name" class="col-md-2 control-label">Nombre</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="name" value="" />
         			</div>
                </div>
       			<input type="submit" class="btn btn-primary" value="Guardar" />
     		{{ Form::close() }}
    	</div>
  	</div>
</div>

<div class="panel panel-default">
  	<div class="panel-heading">
    	<h3 class="panel-title">Contactos</h3>
  	</div>
  	<div class="panel-body">
    	@if(count($contactos) > 0)
    		<table class="table" style="width: auto;">
      			<thead>
        			<tr>
          				<th>Nombre del contacto</th>
          				<th>Número</th>
          				<th>Operaciones</th>
        			</tr>
      			</thead>
      			<tbody>
                    @foreach($contactos as $contacto)
                        <tr>
                            <td>{{ $contacto->name }}</td>
                            <td>{{ $contacto->number }}</td>
                            <td>
                            	<a href="{{ URL::to('contact-agenda/update/' . $contacto->id) }}"
                                   class="btn btn-primary update-contact"
                                   title="Actualizar este contacto">
                                  <span class="glyphicon glyphicon-edit"></span> Actualizar
                                </a>
                                
                                <a href="{{ URL::to('contact-agenda/remove/' . $contacto->id) }}"
                                   class="btn btn-primary remove-contact"
                                   title="Eliminar este contacto de la lista">
                                  <span class="glyphicon glyphicon-remove"></span> Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach      
      			</tbody>
    		</table>
    	@else
    		<p>No has añadido aún contactos.</p>
    	@endif
  	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
  	$('.remove-contact').click(function() {
    	return confirm("Se eliminará el número de la lista de orígenes permitidos. ¿Quieres continuar?");
  	});
  
});
</script>
@stop
