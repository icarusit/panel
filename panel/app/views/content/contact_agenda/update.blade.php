@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-user"></span> Actualizar contacto de la agenda</h1>

@include('help', array('content' => 'contact_agenda', 'number' => $number))
@include('errors')

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Actualizar contacto</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
      		{{ Form::open(array(
        		'action' => 'ContactAgendaController@update',
        		'class'  => 'form-horizontal col-md-5'
      		)) }}
 
       			<div class="form-group">
         			<label for="number" class="col-md-2 control-label">Número</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="number" value="{{ $contacto->number }}" />
         			</div>               
       			</div>
                <div class="form-group">
         			<label for="name" class="col-md-2 control-label">Nombre</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="name" value="{{ $contacto->name }}" />
         			</div>
                </div>
                <input class="form-control" type="hidden" name="id" value="{{ $contacto->id }}" />
       			<input type="submit" class="btn btn-primary" value="Guardar" />
     		{{ Form::close() }}
    	</div>
  	</div>
</div>

<script type="text/javascript">

</script>
@stop
