@extends('main')

@section('subpage')
<h1>Modo no molestar</h1>

@include('help', array('content' => 'dnd'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Estado del servicio "No molestar"</h3>
  </div>
  <div class="panel-body">
    {{ Form::open(array(
      'action' => 'DoNotDisturbController@setDND',
      'class'  => 'form-horizontal'
    )) }}
      <div class="form-group">
        <label class="col-sm-3 control-label">Modo "No molestar"</label>
        <div class="col-sm-4">
          <input type="hidden" name="dndmode"
                 value="{{ $isDNDActive != TRUE ? 'enable' : 'disable' }}">
          <p class="form-control-static orange-hightlight">
            {{ $isDNDActive == TRUE ? 'activado' : 'desactivado' }}            
            <button class="btn btn-primary" style="margin-left: 10px;">
              {{ $isDNDActive == TRUE ? 'Desactivar' : 'Activar' }}              
            </button>
          </p>
        </div>
      </div>
    {{ Form::close() }}
  </div>
</div>

@stop
