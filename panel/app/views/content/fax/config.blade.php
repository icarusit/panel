@extends('main')

@section('subpage')
<h1>Configuración Fax</h1>

@include('help', array('content' => 'fax-config'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif

<div class="row">
  <div class="col-md-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configuración de los e-mails para enviar/recibir faxes</h3>        
      </div>
      <div class="panel-body">        
        {{ Form::open(array('action' => 'FaxController@saveConfig')) }}
        <table class="table table-hover table-striped" style="width: auto;">
          <thead>
            <tr>
              <th width="60%">E-mail</th>
              <th>Envío</th>
              <th>Recepción</th>
            </tr>
          </thead>
          <tbody>
            @for($i=0; $i<5; $i++)
            <tr>
              <td>
                <input type="text" class="form-control" name="mailfax[{{ $i }}][mail]" value="{{ isset($mailfax[$i]['mail']) ? $mailfax[$i]['mail'] : '' }}">
              </td>
              <td>
                <input type="checkbox" class="bs-switch" name="mailfax[{{ $i }}][send]" value="1"
                {{ isset($mailfax[$i]['send']) && $mailfax[$i]['send'] == true ? 'checked' : '' }}>
              </td>
              <td>
                <input type="checkbox" class="bs-switch" name="mailfax[{{ $i }}][rx]" value="1"
                {{ isset($mailfax[$i]['rx']) && $mailfax[$i]['rx'] == true ? 'checked' : '' }}>
              </td>              
            </tr>   
            @endfor
          </tbody>
        </table>
        
        <input type="submit" class="btn btn-primary" value="Guardar">        
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

@stop
