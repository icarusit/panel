@extends('main')

@section('subpage')
<h1>Faxes recibidos</h1>

@include('help', array('content' => 'fax-received'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if($faxes === FALSE)
<p>No has recibido aún faxes en tu número.</p>
@else
<div class="table-responsive">
  <table class="table table-hover table-striped" style="width: auto;">
    <thead>
      <tr>
        <th>Opciones</th>        
        <th>Fecha-hora</th>
        <th>Enviado por</th>
        <th>Núm. páginas</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($faxes as $fax)
      <?php $status = ($fax->fax_status === 'SUCCESS' && $fax->nro_paginas >= 1) ? TRUE : FALSE; ?>
      <tr>
        <td>
          <a href="{{ $status ? URL::to('fax/received/download/' . $fax->id) : '#' }}"
             title="Descargar fax">
            <span class="glyphicon glyphicon-download-alt"></span>
          </a>
          <a href="{{ $status ? URL::to('fax/received/send/' . $fax->id) : '#' }}"
             title="Enviar fax por correo electrónico"
             class="send-fax-mail {{ $status ? '' : 'disabled' }}">
            <span class="glyphicon glyphicon-envelope"></span>
          </a>
        </td>
        <td>{{ (new DateTime($fax->fecha))->format('d/m/Y H:i') }}</td>
        <td>{{ $fax->callerid === 'unknown' ? 'Sin identificar' : $fax->callerid }}</td>
        <td>{{ $fax->nro_paginas }}</td>
        <td style='color: {{ $status ? 'green' : 'red' }}'>{{ $status ? 'OK' : 'Incompleto' }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
  
{{ $faxes->links() }}

<script>
function validateEmail(email) { 
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

$(document).ready(function() {
  $('.send-fax-mail').click(function() {
    if(!$(this).hasClass('disabled')) {
      var email = prompt('Introduce la direccion de correo electronico adonde quieres reenviar el fax:');

      if (!validateEmail(email)) {
        alert("La direccion de correo electronico que has introducido no es valida.");
      } else {
        $.get($(this).attr('href'), {email: email}, function(data) {
          alert(data);
        });
      }     
    }
    
    return false;    
  });
});
</script>

@endif

@stop
