@extends('main')

@section('subpage')
<h1>Faxes enviados</h1>

@include('help', array('content' => 'fax-sent'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if($faxes === FALSE)
<p>No has enviado aún faxes desde tu número.</p>
@else
<div class="table-responsive">
  <table class="table table-hover table-striped" style="width: auto;">
    <thead>
      <tr>
        <th>Opciones</th>        
        <th>Fecha-hora</th>
        <th>Enviado por</th>
        <th>Enviado a</th>
        <th>Núm. páginas</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($faxes as $fax)
      <?php $status = ($fax->status == 1 && $fax->no_of_pages >= 1) ? TRUE : FALSE; ?>
      <tr>
        <td>
          <a href="{{ URL::to('fax/sent/download/' . $fax->id) }}"
             title="Descargar fax">
            <span class="glyphicon glyphicon-download-alt"></span>
          </a>
          <a href="{{ $status ? URL::to('fax/sent/certificate/' . $fax->id) : '#' }}"
             title="Descargar certificado envío fax">
            <span class="glyphicon glyphicon-ok"></span>   
          </a>
          <a href="#"
             title="Enviar fax por correo electrónico">
            <span class="glyphicon glyphicon-envelope"></span>
          </a>
        </td>
        <td>{{ (new DateTime($fax->date))->format('d/m/Y H:i') }}</td>
        <td>{{ $fax->from }}</td>
        <td>{{ $fax->to }}</td>
        <td>{{ $fax->no_of_pages }}</td>
        <td style='color: {{ $status ? 'green' : 'red' }}'>{{ $status ? 'OK' : 'Incompleto' }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endif

@stop
