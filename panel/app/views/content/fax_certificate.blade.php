<style type="text/css">
h1 {
  color: #ff5d28;
  text-align: right;
}

table {
  width: 100%;
}

td.label {
  font-weight: bold;
  color: #ff5d28;
}

p.footer {
  font-size: 50%;
}
</style>

<page>
  <table>
    <tr>
      <td style="width: 50%;">
        <img src="{{ asset('img/logo-web.png') }}">
      </td>
      <td style="width: 50%;">
        <h1>Certificado envío fax</h1>      
      </td>    
    </tr>
  </table>

  <table style="margin-top: 80px;">
    <tr>
      <td style="width: 20%;"></td>
      <td style="width: 60%;">
        <p>Certifico que desde nuestra plataforma de envío de faxes se ha
        realizado correctamente el siguiente envío, recibiendo respuesta
        afirmativa desde el fax de destino:</p>
      </td>    
      <td style="width: 20%;"></td>    
    </tr>
  </table>

  <table style="margin-top: 30px;">
    <tr>
      <td style="width: 20%;"></td>
      <td style="width: 60%;">
        <table style="width: 100%;">
          <tr>
            <td class="label">Fecha:</td>
            <td>{{ $date }}</td>
          </tr>
          <tr>
            <td class="label">Hora:</td>
            <td>{{ $time }}</td>
          </tr>          
          <tr>
            <td class="label">Origen:</td>
            <td>{{ $from }}</td>
          </tr>                  
          <tr>
            <td class="label">Destino:</td>
            <td>{{ $to }}</td>
          </tr>                   
          <tr>
            <td class="label">Nº págs:</td>
            <td>{{ $pages }}</td>
          </tr>                    
        </table>
      </td>    
      <td style="width: 20%;"></td>    
    </tr>
  </table>
  
  <table style="margin-top: 20px;">
    <tr>
      <td style="width: 20%;"></td>
      <td style="width: 60%;">
        <img src="{{ asset('img/firma.jpg') }}">
        
        <p class="footer">Flash Media Europa, S.L. B-50979491 Inscrita en el
        Registro Mercantil de Zaragoza Tomo: 2976 Folio: 43 Hoja: Z-34208
        Inscripción: 1ª</p>
      </td>    
      <td style="width: 20%;"></td>    
    </tr>
  </table>  
  
</page>