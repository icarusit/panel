@extends('main')

@section('subpage')
<h1><i class="fa fa-sort-numeric-asc"></i> Contratar tarifa plana</h1>

@include('help', array('content' => 'flatrates'))
@include('errors')

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Tarifas planas disponibles</h3>
  </div>
  <div class="panel-body">
    @if(empty($flatrates))
    <p>No hay tarifas planas disponibles para contratar.</p>
    @else
    <p>Estas son las tarifas planas disponibles para contratar:</p>
    
    <div class="table-responsive">
      <table id="flatrates-available" class="table table-striped table-hover" style="width: auto;">
        <thead>
          <tr>
            <th width="14%">Contratar</th>
            <th width="25%">Nombre</th>
            <th width="40%">Descripción</th>
            <th width="8%">Minutos</th>            
            <th width="13%">Cuota mensual</th>
          </tr>
        </thead>
        <tbody>
          @foreach($flatrates as $flatrate)
            <tr>
              <td>
                {{ Form::open(array(
                  'action' => 'FlatRatesController@postOrder',
                  'class'  => 'form-inline'
                )) }}
                <input type="hidden" name="flatrate-id" value="{{ $flatrate->id_flatrate }}">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-shopping-cart"></i> Contratar
                </button>
                {{ Form::close() }}
              </td>
              <td>{{ $flatrate->name }}</td>
              <td>{{ $flatrate->description }}</td>
              <td><strong>{{ $flatrate->minutes }}</strong></td>
              <td><strong>{{ $flatrate->monthly_fee }} &euro; + <span title="{{ $cliente->IVA }}%, Total a pagar {{ ($flatrate->monthly_fee + $flatrate->monthly_fee*$cliente->IVA/100) }}">IVA</span></strong></td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @endif
  </div>
</div>
@stop
