@extends('main')

@section('subpage')
<h1><span class="glyphicon glyphicon-share-alt"></span> Desvíos</h1>

@include('help', array('content' => 'forwards'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if(($error = Session::get('error', NULL)) !== NULL)
<div class="alert alert-danger">{{ $error }}</div>
@endif

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Desvío principal <span class="badge">1</span></h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'ForwardsController@setMainForward',
          'class'  => 'form-inline'
        )) }}

        <label>Desviar las llamadas a&nbsp;&nbsp;&nbsp;</label>
        
        <?=View::make('components.forward', array(
          'name'        => 'main-forward',
          'forward'     => $mainForward,
          'sipAccounts' => $sipAccounts
        ))?>
        
        <input class="btn btn-success" type="submit" value="Guardar">
        {{ Form::close() }}
      </div>
    </div>    
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Desvíos secundarios</h3>
      </div>
      <div id="forwards" class="panel-body">
        {{ Form::open(array(
          'action' => 'ForwardsController@setForwards',
          'class'  => 'form-inline',
          'name'   => 'form-forwards',
          'id'     => 'form-forwards'
        )) }}        
        
        @if(is_array($forwards) && count($forwards) > 0)
          <?php $i = 1; ?>
          @foreach($forwards as $forward)
          <div class="margin-15">
              <label>Si <span class="badge">{{ $i }}</span> comunica o no contesta, 
                desviar a <span class="badge">{{ $i+1 }}</span>&nbsp;&nbsp;&nbsp;</label>

              <?=View::make('components.forward', array(
                'forward'     => $forward,
                'sipAccounts' => $sipAccounts  
              ))?>
            
              <?php
              // <ÑAPA>
              $forwardRemove = substr($forward->forward, 0, strlen('SIP/')) === 'SIP/'
                ? substr($forward->forward, strlen('SIP/'), strlen($forward->forward))
                : $forward->forward;
              // </ÑAPA>
              ?>
              
              <div class="form-group">
                <a href="{{ URL::to('forwards/remove/' . $forwardRemove) }}"
                   class="btn btn-primary forward-remove"
                   title="Eliminar desvío secundario">
                  <span class="glyphicon glyphicon-remove"></span> Eliminar
                </a>
              </div>
          </div>
          <?php $i++; ?>
          @endforeach
          <input id="submit-forwards" class="btn btn-success margin-15" 
                 type="submit" value="Guardar">
        @else
          <p id="no-forwards-text">
            No tienes aún desvíos secundarios añadidos. 
          </p>
        @endif
        {{ Form::close() }}
        <a class="btn btn-primary add-new-forward margin-15" href="#"
           data-loading-text="Espera...">Añadir nuevo desvío
          secundario</a>
      </div>
    </div>    
  </div>
</div>

<script>
$(document).ready(function() {
  var numForwards = {{ count($forwards) > 0 ? count($forwards) : 0 }};
  var sipAccounts = {{ json_encode($sipAccounts) }};
  var html;
  
  $('.add-new-forward').click(function() {
    var $that = $(this);
    
    $(this).button('loading');
    
    if ($('#no-forwards-text').css('display') === 'block')
      $('#no-forwards-text').hide();

    $.get('{{ url() }}/component/forward', {
      sipAccounts: sipAccounts
    }, function(data) {
      numForwards++;
      html = '<div class="margin-15">';
      html+= '<label>Si <span class="badge">' + numForwards  + '</span> ' +
              'comunica o no contesta, desviar a <span class="badge">' +
              (numForwards + 1) + '</span>&nbsp;&nbsp;&nbsp;</label>'      
      html+= data;
      html+= '</div>';

      if ($('#submit-forwards').length > 0) {
        $(html).insertBefore('#submit-forwards');
      } else {
        html+= '<input id="submit-forwards" class="btn btn-success margin-15"' + 
               'type="submit" value="Guardar">';       
        $('#form-forwards').append(html);
      }
      
      $that.button('reset');
      
      return false;
    });   
   
    return false; 
  });
  
  $('.forward-remove').click(function() {
    return confirm("Se eliminará el desvío. ¿Quieres continuar?");
  });  
});  
</script>

@stop
