@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-user"></span> Grupos</h1>

@include('help', array('content' => 'groups', 'number' => $number))
@include('errors')

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Añadir nuevo grupo</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
      		{{ Form::open(array(
        		'action' => 'GroupController@add',
        		'class'  => 'form-horizontal col-md-5'
      		)) }}
 
                <div class="form-group">
         			<label for="name" class="col-md-2 control-label">Nombre</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="name" value="" required/>
         			</div>
                </div>
       			<input type="submit" class="btn btn-primary" value="Guardar" />
     		{{ Form::close() }}
    	</div>
  	</div>
</div>

<div class="panel panel-default">
  	<div class="panel-heading">
    	<h3 class="panel-title">Grupos</h3>
  	</div>
  	<div class="panel-body">
    	@if(count($grupos) > 0)
    		<table class="table" style="width: auto;">
      			<thead>
        			<tr>
          				<th>Nombre</th>
                        <th>Miembros configurados</th>
          				<th>Operaciones</th>
        			</tr>
      			</thead>
      			<tbody>
                    @foreach($grupos as $grupo)
                        <tr>
                            <td>{{ $grupo->name }}</td>
                            <td>{{ $grupo->TotalMiembros() }}</td>
                            <td>
                            	<a href="{{ URL::to('group/update/' . $grupo->id) }}"
                                   class="btn btn-primary"
                                   title="Actualizar este grupo">
                                  <span class="glyphicon glyphicon-edit"></span> Actualizar
                                </a>
                                
                                <a href="{{ URL::to('group/remove/' . $grupo->id) }}"
                                   class="btn btn-primary remove-group"
                                   title="Eliminar este grupo de la lista">
                                  <span class="glyphicon glyphicon-remove"></span> Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach      
      			</tbody>
    		</table>
    	@else
    		<p>No has añadido aún grupos.</p>
    	@endif
  	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
  	$('.remove-group').click(function() {
    	return confirm("Se eliminará el grupo de la lista con todos los contactos. ¿Quieres continuar?");
  	});
  
});
</script>
@stop
