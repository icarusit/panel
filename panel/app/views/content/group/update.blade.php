@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-user"></span> Grupo</h1>

@include('help', array('content' => 'groups', 'number' => $number))
@include('errors')

<div class="row">
	<div class="col-md-6">
    	<a class="btn btn-primary" href="<?php print URL::to('groups'); ?>">
            Regresar al listado de grupos
        </a>
    </div>
</div>

<div class="margin-30"></div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">{{$grupo->name}}</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
      		{{ Form::open(array(
        		'action' => 'GroupController@update',
        		'class'  => 'form-horizontal col-md-5'
      		)) }}
            <input class="form-control" type="hidden" name="id" value="{{$grupo->id}}" required/>
 
                <div class="form-group">
         			<label for="name" class="col-md-2 control-label">Nombre</label>
         			<div class="col-md-10">
           				<input class="form-control" type="text" name="name" value="{{$grupo->name}}" required/>
         			</div>
                </div>
       			<input type="submit" class="btn btn-primary" value="Guardar" />
     		{{ Form::close() }}
    	</div>
  	</div>
</div>

<div class="margin-30"></div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Añadir</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
        	
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Sip
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                        
                         	{{ Form::open(array(
                                'action' => 'GroupController@addsip',
                                'class'  => 'form-horizontal col-sm-12'
                            )) }}
                            	<input class="form-control" type="hidden" name="id" value="{{$grupo->id}}" required/>
                            	<div class="form-group">
                                    <label for="name" class="col-md-2 control-label">SIP</label>
                                    <div class="col-md-10">
                                    	<select name="sip" required>
                                        	@foreach($cuentas_sip as $sipAccount)
                                                <option value="{{ $sipAccount->defaultuser }}">{{ $sipAccount->defaultuser }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            	<button type="submit" class="btn btn-primary">
                                	<span class="glyphicon glyphicon-plus"></span>
                                    Añadir
                                </button>                            
                            {{ Form::close() }}
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                        	<span class="glyphicon glyphicon-phone-alt"></span> 
                            Teléfono
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            
                            {{ Form::open(array(
                                'action' => 'GroupController@addtelefono',
                                'class'  => 'form-horizontal col-sm-12'
                            )) }}
                            	<input class="form-control" type="hidden" name="id" value="{{$grupo->id}}" required/>
                            	<div class="form-group">
                                    <label for="name" class="col-md-2 control-label">Número</label>
                                    <div class="col-md-10">
                                        <input class="form-control phone-number" type="text" name="numero" value="" required/>
                                    </div>
                                </div>
                            	<button type="submit" class="btn btn-primary">
                                	<span class="glyphicon glyphicon-plus"></span>
                                    Añadir
                                </button>                             
                            {{ Form::close() }}
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="margin-30"></div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">
        	<span class="glyphicon glyphicon-list"></span> 
            Miembros del grupo
        </h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
        	@if(count($miembros) > 0)
                <table class="table col-sm-6" style="width: auto;">
                    <thead>
                        <tr>
                            <th>SIP/Número</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($miembros as $miembro)
                            <tr>
                                <td>
                                	<?php
                                    	$reemplazo = str_ireplace("SIP/",'',$miembro);
										$reemplazo = str_ireplace("NSG/",'',$reemplazo);
										print $reemplazo;
									?>
                                </td>
                                <td>                                    
                                    <a href="{{URL::to('group/removemiembro/'.$grupo->id)}}/<?php print str_ireplace("/",'-',$miembro); ?>"
                                       class="btn btn-primary remove-miembro"
                                       title="Eliminar este miembro del grupo">
                                      <span class="glyphicon glyphicon-remove"></span> Eliminar
                                    </a>
                                </td>
                            </tr>
                        @endforeach      
                    </tbody>
                </table>
            @else
                <p>No has añadido aún miembros al grupo.</p>
            @endif
        </div>
    </div>
</div>

@stop
