@extends('main')
@section('subpage')

<h1>Festivos y vacaciones</h1>

@include('help', array('content' => 'holidays'))
@include('errors')

@if(is_array($holidays))
  @include('content.holidays.list_holidays')
  @include('content.holidays.national_holidays')  
  @include('content.holidays.new_holiday')
@else
  @include('content.holidays.new_holiday')
  @include('content.holidays.national_holidays')    
  @include('content.holidays.list_holidays')
@endif

@include('components.actions.modals.include')


<script>
$(document).ready(function() {
  $('.date-picker').datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy',
    weekStart: 1
  });
  
  $('#national-holidays-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-national-holidays').submit();
    }
  });        

  $('.date-picker').prop('readonly', 'readonly');

  $('.holiday-remove').click(function() {
    return confirm("Se eliminará el festivo. ¿Quieres continuar?");
  }); 

  $('.holiday-edit').click(function() {
    $(this).hide();    
    $(this).siblings('.holiday-remove').hide();    
    $(this).siblings('.holiday-edit-save').show();
    $(this).siblings('.holiday-edit-cancel').show().css('display', 'inline-block');  
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('holiday-date')) {
          $(this).hide();
          $(this).siblings('.holiday-edit-input').show();
        } else if($(this).hasClass('holiday-summary')) {
          $(this).hide();
          $(this).siblings('.selector').show();
        }
      });
    });
    
    return false;
  });  
  
  $('.holiday-edit-cancel').click(function() {
    $(this).hide();
    $(this).siblings('.holiday-edit-save').hide();    
    $(this).siblings('.holiday-remove').show();
    $(this).siblings('.holiday-edit').show();        
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('holiday-edit-input')) {
          $(this).hide();
          $(this).siblings('.holiday-date').show();
        } else if($(this).hasClass('selector')) {
          $(this).hide();
          $(this).siblings('.holiday-summary').show();
        }
      });
    });
    
    return false;    
  });
});
</script>

@stop
