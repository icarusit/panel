<div class="row">
  <div class="col-md-10">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Días y períodos festivos configurados</h3>
      </div>
      <div class="panel-body">
        @if(is_array($holidays))
        <div class="table-responsive">
          <table class="table table-striped" id="table-holidays">
            <thead>
              <tr>
                <th width="13%">Desde</th>
                <th width="13%">Hasta</th>                  
                <th width="38%">Acción a realizar</th>
                <th width="35%">Operaciones</th>
              </tr>
            </thead>
            <tbody>           
              @foreach($holidays as $holiday)
              {{ Form::open(array('action' => 'HolidaysController@save')) }}              
                <tr class="holiday-row">
                  <td>
                    <span class="holiday-date">
                      {{ $holiday->getFrom()->format('d/m/Y') }}  
                    </span>
                    
                    <input type="text" name="from" id="from"
                       class="form-control date-picker holiday-edit-input"
                       placeholder="Desde"
                       value="{{ $holiday->getFrom()->format('d/m/Y') }}">
                    
                    <input type="hidden" name="holiday_id" value="{{ $holiday->getID() }}">                    
                  </td>
                  <td>
                    <span class="holiday-date">
                      {{ is_null($holiday->getUntil()) ? '' : $holiday->getUntil()->format('d/m/Y') }}
                    </span>
                    
                    <input type="text" name="until" id="until"
                       class="form-control date-picker holiday-edit-input" 
                       @if($holiday->getUntil() !== null)
                       value="{{ $holiday->getUntil()->format('d/m/Y') }}"
                       @endif
                       placeholder="Hasta">                    
                  </td>
                  <td>
                    <span class="holiday-summary">
                      {{ (new Action($holiday->getAction()))->parse() }}
                    </span>
                    
                    @include('components.actions.selector', array('action' => array('name' => 'action_data_' . $holiday->getID(), 'value' => $holiday->getAction(), 'class' => 'holiday-edit-action')))                    
                  </td>
                  <td>
                    <a href="#"
                       class="btn btn-primary holiday-edit"
                       title="Editar este festivo">
                      <span class="glyphicon glyphicon-edit"></span>
                      Editar
                    </a>                        

                    <input type="submit" 
                           class="btn btn-success holiday-edit-save"
                           title="Guardar festivo editado"
                           value="Guardar">

                    <a href="#"
                       class="btn btn-primary holiday-edit-cancel"
                       title="Cancelar edición de festivo">
                      <span class="glyphicon glyphicon-remove"></span>                          
                      Cancelar
                    </a>
                    
                    <a href="{{ URL::to('holidays/remove/' . $holiday->getID()) }}"
                       class="btn btn-primary holiday-remove">
                      <span class="glyphicon glyphicon-remove"></span> Eliminar festivo
                    </a>
                  </td>
                </tr>
                {{ Form::close() }}
              @endforeach            
            </tbody>
          </table>
        </div>  
        @else
          {{-------------------------}}
          {{-- NO HAY FESTIVOS AÚN --}}
          {{-------------------------}}            
          <div class="media col-md-12 no-holidays">
            <span class="pull-left glyphicon glyphicon-calendar panel-icon"></span>
            <div class="media-body">
              <h4 class="media-heading">No has añadido aún días o períodos festivos</h4>
            </div>
          </div>
          @endif
      </div>
    </div>
  </div>
</div>
