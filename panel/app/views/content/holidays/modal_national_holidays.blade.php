<div class="modal" id="modal-national-holidays" tabindex="-1" role="dialog"
     aria-labelledby="modalNationalHolidaysLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalNationalHolidaysLabel">Festivos nacionales para <strong><?=date('Y')?></strong></h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Fecha (día/mes)</th>
              <th>Motivo</th>
            </tr>                  
          </thead>
          <tbody>
            @foreach($nationalHolidaysCollection as $natHoliday)
            <tr>
              <td><?=$natHoliday->date?></td>
              <td><?=$natHoliday->detail?></td>
            </tr>
            @endforeach
          </tbody>                    
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>        
