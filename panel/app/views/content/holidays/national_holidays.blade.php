<div class="row">
  <div class="col-md-10">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Festivos nacionales</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          {{ Form::open(array(
            'action' => 'HolidaysController@saveNationalHolidays',
            'class'  => 'form-inline',
            'id'     => 'form-national-holidays'
          )) }}
          
          <div class="form-group col-md-9">
            <label for="action" class="col-md-3 control-label">Acción</label>
            <div class="col-md-9">
              <?php              
              $action = array(
                'name'  => 'national-holidays-action'
              );

              if($nationalHolidaysAction instanceof stdClass) {
                $action['value'] = $nationalHolidaysAction;
              }

              $actionSelectorData = array(
                'disabled' => !$nationalHolidays,
                'action' => $action
              );
              ?>

              @include('components.actions.selector')             
            </div>
          </div>                  
          
          <div class="form-group col-md-9 margin-15">
            <label class="col-sm-3 control-label">Servicio activo</label>
            <div class="col-sm-3">  
              <input type="checkbox" id="national-holidays-toggle" 
                     name="national-holidays-toggle" 
                     value="1" {{ $nationalHolidays ? ' checked' : '' }}>
            </div>           
          </div>
        </div>
          
        {{ Form::close() }}   
        
        <div class="row margin-15">
          <div class="col-md-12">
            <a target="_blank" href="#" data-toggle="modal" data-target="#modal-national-holidays">
              <span class="glyphicon glyphicon-calendar"></span>
              Ver calendario de festivos nacionales
            </a>        
          </div>
        </div>
        
        @include('content.holidays.modal_national_holidays')        
      </div>
    </div>
  </div>
</div>
