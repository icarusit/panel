<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Agregar un nuevo día o período festivo</h3>
      </div>
      <div class="panel-body">
        <div class="alert alert-info">
          <p>Si quieres configurar un período de vacaciones, por ejemplo, cinco
          días seguidos en agosto, debes rellenar tanto la fecha <strong>'Desde'</strong>
          como <strong>'Hasta'</strong>.</p>
          <p>Si quieres marcar un día puntual como festivo, sólo hace falta que
          rellenes el campo <strong>'Desde'</strong>.</p>
        </div>
        <div class="row">
         {{ Form::open(array(
            'action' => 'HolidaysController@save',
            'class'  => 'form-horizontal col-md-6'
          )) }}
           <div class="form-group">
             <label for="from" class="col-md-2 control-label">Desde</label>
             <div class="col-md-4">
                <input type="text" name="from" id="from"
                       class="form-control date-picker" placeholder="Desde">         
             </div>
           </div>

           <div class="form-group">
             <label for="until" class="col-md-2 control-label">Hasta</label>
             <div class="col-md-4">
                <input type="text" name="until" id="until"
                       class="form-control date-picker" placeholder="Hasta">         
             </div>
           </div>

           <div class="form-group">
             <label for="action" class="col-md-2 control-label">Acción</label>
             <div class="col-md-10">
                @include('components.actions.selector')
             </div>
           </div>                              
           
           <input type="submit" class="btn btn-success pull-left" value="Guardar" /> 
         {{ Form::close() }}
        </div>        
      </div>
    </div>
  </div>
</div>





