@extends('main')
@section('subpage')

<h1>Hooks</h1>

@include('help', array('content' => 'hooks'))
@include('errors')

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Añadir nuevo hook</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      {{ Form::open(array(
        'action' => 'HooksController@add',
        'class'  => 'form-horizontal col-md-5'
      )) }}

       <div class="form-group">
         <label for="event" class="col-md-2 control-label">Hook</label>
         <div class="col-md-10">
           <select class="form-control" name="event">
           @foreach($hookTypes as $hook => $hookText)
             <option value="{{ $hook }}">{{ $hookText }}</option>
           @endforeach
           </select>
         </div>
       </div>
 
       <div class="form-group">
         <label for="url" class="col-md-2 control-label">URL</label>
         <div class="col-md-10">
           <input class="form-control" type="text" name="url" value="http://" />
         </div>
       </div>     

       <input type="submit" class="btn btn-primary" value="Guardar" /> 
     {{ Form::close() }}
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Hooks disponibles</h3>
  </div>
  <div class="panel-body">
    @if(count($hooks) > 0)
    <table class="table" style="width: auto;">
      <thead>
        <tr>
          <th>Evento</th>
          <th>URL para notificación</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @foreach($hooks as $hook)
        <tr>
          <td>{{ $hookTypes[$hook->event] }}</td>            
          <td><a href="{{ $hook->action }}" target="_blank">{{ $hook->action }}</a></td>	
          <td><a title="Eliminar este hook" href="{{ URL::to('hooks/remove/' . $hook->id_hook) }}"><span class="glyphicon glyphicon-remove"></span></a></td>
        </tr>
      @endforeach      
      </tbody>
    </table>
    @else
    <p>No tienes aún hooks configurados</p>
    @endif
  </div>
</div>



@stop
