@extends('main')
@section('subpage')

<h1><span class="fa fa-frown-o"></span> Baja del servicio</h1>

@include('errors')
 
<div class="frame frame-danger">
  <p>Estás a punto de solicitar la baja definitiva de tu número
    <strong>{{ Session::get('number') }}</strong>.</p>

  <p>La baja supone perder el número y todos los servicios asociados. Así como la
    configuración que hayas podido hacer en él. Este proceso es irreversible.</p>

  <p>La baja es inmediata, una vez la confirmes, dejarás de recibir llamadas en él
    y dejará de aparecer en tu área de cliente.</p>    
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Motivo de la baja (<strong>obligatorio</strong>)</h3>
  </div>
  <div class="panel-body">
    {{ Form::open(array(
      'action' => 'CancelController@cancel',
      'class'  => 'form-horizontal col-md-8'
    )) }}           
    <div class="row">
      <p>En <?php echo Session::get('distribuidor'); ?> trabajamos diariamente para mejorar nuestro servicio.</p>
      <p>Creemos firmemente que tenemos el sistema más avanzado de gestión de
        llamadas. Sin embargo, podemos estar equivocados, por lo que agradecemos
        que nos ayudes a mejorar aún más.</p>
      <p>Indícanos por favor qué hemos hecho mal o en qué podemos trabajar más para
        mejorar el servicio.</p>      
    </div>
    
    <div class="form-group">
      <textarea class="form-control" rows="10" id="cancel_reasons" name="cancel_reasons"
                placeholder="Introduce aquí por favor los motivos para la baja."></textarea>        
    </div>

    <div class="form-group">    
      <input type="submit" class="btn btn-primary" value="Confirmar baja" /> 
    </div>
    
    {{ Form::close() }}
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('form').submit(function() {
    if($.trim($('#cancel_reasons').val()) === '' || $('#cancel_reasons').val().length < 10) {
      alert("Para tramitar la baja online es OBLIGATORIO que escribas tus motivos de la baja.\n\rEsto nos ayudará a saber qué estamos haciendo mal y cómo podemos mejorarlo en un futuro.\n\rGracias");
      return false;
    } else {
      return confirm('Al continuar darás de baja definitiva tu número {{ Session::get('number') }}. ¿Estás seguro de querer continuar?');
    }
  });
});
</script>

@stop
