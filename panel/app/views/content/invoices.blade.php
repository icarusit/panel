@extends('main')

@section('subpage')
<h1>Mis facturas</h1>

@include('help', array('content' => 'invoices'))
@include('errors')

<div class="form-group">
  <label for="balance">Balance: </label>
  <p style="font-weight: bold; color: #ff5d28;">{{ $balance }} &euro;</p>
</div>

@if(count($invoices) == 0)
<p>No se te ha generado aún ningúna factura.</p>
@else
<div class="table-responsive">
  <table class="table table-hover table-striped" style="width: auto;">
    <thead>
      <tr>
        <th></th>
        <th># de factura</th>
        <th>Fecha</th>
        <th>Subtotal</th>
        <th>Total</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody data-provides="rowlink">
      @foreach($invoices as $invoice)
      <tr>
        <td>
          <a href="{{ URL::to('invoices/pdf/' . $invoice->num_factura) }}"
             title="Descargar la factura {{ $invoice->num_factura }}">
            <span class="glyphicon glyphicon-download-alt"></span>
          </a>
        </td>      
        <td>{{ $invoice->num_factura }}</td>
        <?php $dt = new \DateTime($invoice->fecha); ?>
        <td>{{ $dt->format('d/m/Y') }}</td>
        <td>{{ number_format((float)str_replace(',', '.', $invoice->base_imponible), 2) }} &euro;</td>
        <td>{{ number_format((float)str_replace(',', '.', $invoice->total), 2) }} &euro;</td>
        <td>
          <?php
          $status = array(
            'pagada'        => 'success',
            'devolucion'    => 'primary',
            'pendiente'     => 'danger',
            'compensada'    => 'warning',
            'tarjeta'       => 'info',
            'transferencia' => 'info',
            'en curso'      => 'info'
          );
          ?>
          @if($invoice->estado !== NULL)
          <span class="label label-{{ isset($status[strtolower($invoice->estado)]) ? $status[strtolower($invoice->estado)] : 'default' }}">
            {{ $invoice->estado }}
          </span>

          @if(in_array(strtolower($invoice->estado), ['tarjeta', 'transferencia']))
          <form method="post" action="{{ URL::to('invoices/pay/' . $invoice->id) }}">
          	<select name="forma_pago">
            	<option value="redsys">Tarjeta</option>
                <option value="paypal">Paypal</option>
            </select>
          	<input class="" type="submit" value="Pagar ahora &raquo;"/>
          </form>
          <!--
          <span class="label label-flash">
            <a href="{{ URL::to('invoices/pay/' . $invoice->id) }}">Pagar ahora &raquo;</a>
          </span>
          -->
          @endif

          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{{ $invoices->links() }}

@endif

@stop
