<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Configuración del menú</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-8">
        <p>Pulsa sobre los botones numéricos para configurar las 
          <a href="#">acciones</a> a realizar</p>

        <div class="menu">          
          <?php $x = 1; ?>
          @for($i=1; $i<4; $i++)
          <ul class="menu-row">
            @for($j=1; $j<4; $j++,$x++)
            <li class="">
              <a href="#" class="btn menu-option-button">{{ $x }}</a>
            </li>
            @endfor
          </ul>
          
          <div class="clearfix"></div>
          @endfor

          <ul class="menu-row">
            <li class="">
              <a href="#" 
                 class="btn menu-option-button"
                 disabled="disabled">*</a>
            </li>

            <li class="">
              <a href="#" class="btn menu-option-button">0</a>
            </li>

            <li class="">
              <a href="#"
                 class="btn menu-option-button"
                 disabled="disabled">#</a>
            </li>
          </ul>
        </div>
        
        <div id="menu-option-config">
          {{ Form::open(array(
            'action' => 'MenusController@setOptions',
            'class'  => 'form-horizontal'
          )) }}
          <div class="row">
            <p id="menu-option-config-help">Al pulsar el X hacer lo siguiente</p>
          </div>
          
          <div class="row">
            @include('components.actions.selector')
          </div>

          <div class="row margin-15">          
            <input type="hidden" name="dtmf" id="dtmf" value="" />
            <input type="submit" class="form-control btn btn-success" value="Guardar cambios">
          </div>          
          {{ Form::close() }}
        </div>
        @include('components.actions.modals.include')
        
      </div>      
    </div>   
  </div>
</div>
