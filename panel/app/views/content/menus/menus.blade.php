@extends('main')

@section('subpage')
<h1>Menús</h1>

@include('help', array('content' => 'menus'))

@include('errors')

{{-- Activar desactivar servicio --}}
@include('content.menus.toggle')

{{-- Configurar locución del menú --}}
@include('content.menus.speech')

{{-- Configurar teclas del menú --}}
@include('content.menus.add_options')

{{-- Resumen de la configuración del menú --}}
@include('content.menus.summary')

<script type="text/javascript">
$(document).ready(function() {
  $('#menu-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-toggle').submit();
    }
  });      
  
  $('.menu-option-button').click(function() {
    var option = $(this).html();

    $('.menu-option-button').removeClass('menu-option-button-active');
    $(this).addClass('menu-option-button-active');

    $('#dtmf').prop('value', option);

    $('#menu-option-config').hide();
    $('#menu-option-config-help').html('Al pulsar <span class="badge">' + option + '</span> hacer lo siguiente:');
    $('#menu-option-config').fadeIn();

    return false;
  });

  $('#menu-config-action-type-select').change(function() {
    var actionType = $('option:selected', this).attr('val');

    $('.menu-option-action-config').hide();

    $('.menu-option-action-config-' + actionType).fadeIn();  
  });  
  
  $('.menu-option-remove').click(function() {
    return confirm('Se eliminará la opción del menú, ¿quieres continuar?');
  });
  
  $('.menu-option-edit').click(function() {
    $(this).hide();    
    $(this).siblings('.menu-option-remove').hide();    
    $(this).siblings('.menu-option-edit-save').show();
    $(this).siblings('.menu-option-edit-cancel').show().css('display', 'inline-block');  
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('menu-option-summary')) {
          $(this).hide();
          $(this).siblings('.selector').show();
        }
      });
    });
    
    return false;
  });  
  
  $('.menu-option-edit-cancel').click(function() {
    $(this).hide();
    $(this).siblings('.menu-option-edit-save').hide();    
    $(this).siblings('.menu-option-remove').show();
    $(this).siblings('.menu-option-edit').show();        
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('selector')) {
          $(this).hide();
          $(this).siblings('.menu-option-summary').show();
        }
      });
    });
    
    return false;    
  });  
});


</script>

@stop
