<div class="row">
  <div class="col-md-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configurar locución del menú</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'MenusController@setSpeech',
          'class' => 'form-horizontal'
        )) }}
        <div class="form-group">
          <label for="select-menu-speech" 
                 class="col-md-4 control-label">
            Locución del menú (<a href="{{ URL::to('speeches') }}">administrar mis locuciones</a>)
          </label>          
          
          <div class="col-md-8">            
            <select class="form-control" name="select-menu-speech" 
                    id="select-menu-speech" @if($speeches === false) disabled @endif>                    
              @if(!is_array($speeches))
              <option value="disabled">No tienes aún locuciones de menú</option>
              @else
                @if(is_array($speeches))
                  @if($selectedSpeech === FALSE)
                  <option disabled selected>No has seleccionado ninguna aún</option>
                  @endif
                
                  @foreach($speeches as $speech)
                  <option value="{{ $speech->id_speech }}"
                          @if($selectedSpeech !== FALSE && $selectedSpeech->name === $speech->public_name) selected @endif>
                    {{ $speech->public_name }}
                  </option>
                  @endforeach
                @endif
              @endif
            </select>
          </div>
        </div>
                
        <input type="submit" value="Actualizar locución" 
               class="btn btn-primary pull-right @if($speeches === false) disabled @endif">
        
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>