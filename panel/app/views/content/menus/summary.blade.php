<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Resumen de la configuración</h3>
  </div>
  <div class="panel-body">
  <?php /* ÑAPA */ $arrOptions = (array)$options; ?>
  @if($options === false || empty($arrOptions)) 
  <p>No has hecho aún la configuración del menú</p>
  @else
  <table class="table" style="width: auto;">
    <thead>
      <th>Dígito</th>
      <th>Acción</th>      
      <th>Operaciones</th>
    </thead>
    <tbody>
      @foreach($options as $dtmf => $action)
      {{ Form::open(array('action' => 'MenusController@setOptions')) }}      
      <tr>
        <td>
          <span class="label-retro-digits" title="{{ $dtmf }}">
            {{ $dtmf }}  
          </span>
          
          <input type="hidden" name="dtmf" value="{{ $dtmf }}">
        </td>
        <td>
          <span class="menu-option-summary">
            {{ $action->summary }}
          </span>

          @include('components.actions.selector', array('action' => array('name' => 'action_data_' . $dtmf, 'value' => $action, 'class' => 'menu-option-edit-action')))             
        </td>          
        <td>
          <a href="#"
             class="btn btn-primary menu-option-edit"
             title="Editar esta opción">
            <span class="glyphicon glyphicon-edit"></span>
            Editar
          </a>

          <input type="submit" 
                 class="btn btn-success menu-option-edit-save"
                 title="Guardar opción de menú editada"
                 value="Guardar">

          <a href="#"
             class="btn btn-primary menu-option-edit-cancel"
             title="Cancelar edición de opción de menú">
            <span class="glyphicon glyphicon-remove"></span>                          
            Cancelar
          </a>          
          
          <a href="{{ URL::to('menus/remove/' . $dtmf) }}"
             class="btn btn-primary menu-option-remove"
             title="Eliminar la acción configurada al pulsar {{ $dtmf }}">
            <span class="glyphicon glyphicon-remove"></span>
            Eliminar
          </a>
        </td>        
      </tr>
      {{ Form::close() }}
      @endforeach
    </tbody>
  </table>
  @endif
  </div>
</div>