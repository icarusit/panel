<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Activación/desactivación del servicio de menú</h3>
  </div>
  <div class="panel-body">
    {{ Form::open(array(
      'action' => 'MenusController@setConfig',
      'class'  => 'form-horizontal',
      'id'     => 'form-toggle'
    )) }}    
    <div class="form-group">
      <div class="row">
        <div class="col-sm-9">
          <div class="form-group">
            <label class="col-sm-3 control-label">Menú activo</label>
            <div class="col-sm-3">  
              <input type="hidden" id="menu-toggle-hidden" 
                     name="menu-toggle" value="0">                 
              <input type="checkbox" id="menu-toggle" 
                     name="menu-toggle" value="1" {{ $enabled ? ' checked' : '' }}>                 
            </div>
          </div>          
        </div>
      </div>
    </div>
    {{ Form::close() }}
  </div>
</div>