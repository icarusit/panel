<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
      Mostrar <strong>{{ $number }}</strong> en las llamadas recibidas
    </h3>
  </div>
  <div class="panel-body">
    {{ Form::open(array(
      'action' => 'MiscController@setDisplayCallerID',
      'class'  => 'form-horizontal'
    )) }}
      <div class="form-group">
        <label class="col-sm-2 control-label">Mostrar</label>
        <div class="col-sm-5">
          <input type="hidden" name="displayCallerID"
                 value="{{ $displayCallerID != TRUE ? 'enable' : 'disable' }}">          
          <p class="form-control-static orange-hightlight">
            {{ $displayCallerID == TRUE ? 'activado' : 'desactivado' }}            
            <button class="btn btn-primary" style="margin-left: 10px;">
              {{ $displayCallerID == TRUE ? 'Desactivar' : 'Activar' }}
            </button>
          </p>
        </div>
      </div>
    {{ Form::close() }}
  </div>
</div>