@extends('main')

@section('subpage')
<h1>Más opciones</h1>

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if($number->getProfile()->getType() === 'analog' && $number->getProfile()->getPlan() === 'plus')
  @include('content.misc.display_own_did')
@endif

@if(in_array($number->getProfile()->getType(), array('voip', 'pbx')))
  @include('content.misc.speech_credit')
@endif

@if(in_array($number->getProfile()->getType(), array('voip', 'pbx')))
  @include('content.misc.inbound_block')
@endif

@if(in_array($number->getProfile()->getType(), array('voip', 'pbx')))
  @include('content.misc.outbound_block')
@endif

@stop
