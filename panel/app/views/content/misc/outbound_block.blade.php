<div class="row"> 
  <div class="col-md-10">
    <div class="panel panel-default">
      <div class="panel-heading" id="block_outbound_calls">
        <h3 class="panel-title">Bloqueo de llamadas salientes</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'MiscController@settings'
        )) }}
        <div class="checkbox">
          <label>
            <input type="hidden" name="block_international_outbound_calls" value="false">
            <input type="checkbox" name="block_international_outbound_calls" 
            value="true"{{ ($settings !== NULL && $settings->block_international_outbound_calls) ? ' checked ' : '' }}>
            Bloquear llamadas hacia números <strong>internacionales</strong>
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="hidden" name="block_mobile_outbound_calls" value="false">            
            <input type="checkbox" name="block_mobile_outbound_calls"
            value="true"{{ ($settings !== NULL && $settings->block_mobile_outbound_calls) ? ' checked ' : '' }}>
            Bloquear llamadas hacia números <strong>móviles</strong>
          </label>
        </div>  
        <div class="checkbox">
          <label>
            <input type="hidden" name="block_vas_outbound_calls" value="false">
            <input type="checkbox" name="block_vas_outbound_calls"
            value="true"{{ ($settings !== NULL && $settings->block_vas_outbound_calls) ? ' checked ' : '' }}>
            Bloquear llamadas hacia números de <strong>tarificación especial</strong>
          </label>
        </div>

        <div class="form-group margin-15">
          <input type="submit" class="btn btn-primary" value="Guardar preferencias" />  
        </div>  

        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>