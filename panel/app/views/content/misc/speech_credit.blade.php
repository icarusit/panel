<div class="row">
  <div class="col-md-7">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Locución de saldo en llamadas salientes desde <strong>{{ $number->getNumber() }}</strong></h3>
      </div>
      <div class="panel-body">
        <p>Cuando llamas desde el número <strong>{{ $number->getNumber() }}</strong> y esta opción
          está activada, si tienes poco saldo o está el saldo agotado, reproducimos una locución
          antes de ejecutar la llamada para avisarte. Desactiva esta opción si no quieres
          que reproduzcamos la locución de aviso.</p>
        
        {{ Form::open(array(
          'action' => 'MiscController@setSpeechCredit',
          'class'  => 'form-horizontal',
          'id'     => 'form-toggle'
        )) }}
          <div class="form-group margin-30">
            <label class="col-md-5 control-label">Servicio activo</label>
            <div class="col-sm-3">  
              <input type="checkbox" id="speech-credit-toggle" 
                     name="speech-credit-toggle" 
                     value="1" {{ $hasSpeechCredit ? ' checked' : '' }}>
            </div>
          </div>
        {{ Form::close() }}        
      </div>
    </div>    
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#speech-credit-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-toggle').submit();
    }
  });
});
</script>
