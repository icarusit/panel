@extends('main')

@section('subpage')
<h1>Llamadas perdidas</h1>

@include('help', array('content' => 'missedcalls'))
@include('errors')

@include('content.missedcalls.toggle')
@include('content.missedcalls.settings')
@include('content.whatsapp.whatsapp')

<div class="row">
	<div class="col-md-6">
    	<div class="panel panel-default">
      		<div class="panel-heading">
        		<h3 class="panel-title">Llamadas perdidas</h3>
      		</div>
            
      		<div class="panel-body">
                @if(count($listmissedcalls) > 0)
                    {{ Form::open(array(
                        'id'     => 'form-filtro',
                        'action' => 'MissedCallsController@show',
                        'class'  => 'form-horizontal',
                        'method' => 'GET'
                      )) }}
                    <div class="row">        
                        <div class="col-md-12">
                             <div class="form-group">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-4" style="text-align:right !important;">
                                    <input type="text" class="form-control input-sm" style="margin-left:10px;margin-top:1px;" name="numero_filtro" value="{{ $numero_filtro }}"/> 
                                </div>
                                <div class="col-sm-2">
                                    <input type="submit" class="btn btn-primary pull-right" value="Buscar">
                                 </div>
                            </div>
                        </div>     
                    </div>
                    {{ Form::close() }}
                    
                    <div class="table-responsive">
                        <table style="width:100%" class="table table-stretch table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Llamante</th>
                                    <th>Fecha-hora</th>
                                </tr>
                            </thead>
                
                            <tbody>
                            @foreach($listmissedcalls as $call)
                                <tr>
                                    <td>{{ $call->caller }}</td>
                                    <?php $dt = new \DateTime($call->date_time); ?>
                                    <td>{{ $dt->format('d-m-Y H:i') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $listmissedcalls->appends(array('numero_filtro' => $numero_filtro))->links() }}
                @else
                    <p>No tienes llamadas perdidas.</p>
                @endif                
                
    		</div>
            
  		</div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#missed-calls-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-missed-calls-toggle').submit();
    }
  });      
});
</script>

@stop