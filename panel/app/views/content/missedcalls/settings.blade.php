<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configuración llamadas perdidas</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'MissedCallsController@setEmail',
          'class'  => 'form-horizontal'
        )) }}
        <div class="form-group">
          <label for="missedcalls-email" class="col-md-5 control-label">
            Email recepción notificaciones
          </label>
          <div class="col-md-6">
            <input id="missedcalls-email" class="form-control email" type="text" name="missedcalls-email"
                   value="{{ isset($email) ? $email : '' }}">
          </div>
        </div>
        <div class="form-group">
          <label for="missedcalls-email2" class="col-md-5 control-label">
            Email recepción notificaciones 2
          </label>
			<div class="col-md-6">
				<input id="missedcalls-email2" class="form-control email norequired" type="text" name="missedcalls-email2" value="{{ isset($email2) ? $email2 : '' }}"/>
          	</div>
        </div>
        <input type="submit" class="btn btn-primary pull-right" value="Guardar" />
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>