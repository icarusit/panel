<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Servicio de llamadas perdidas</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'MissedCallsController@setMissedCalls',
          'class'  => 'form-horizontal',
          'id'     => 'form-missed-calls-toggle'
        )) }}
          <div class="form-group">
            <label class="col-md-5 control-label">Servicio activo</label>
            <div class="col-sm-3">  
              <input type="checkbox" id="missed-calls-toggle" 
                     name="missed-calls-toggle" 
                     value="1" {{ $enabled ? ' checked' : '' }}>
            </div>    
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>