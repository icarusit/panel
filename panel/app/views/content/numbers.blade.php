@extends('main')

@section('subpage')
<h1>Mis números</h1>

@include('errors')


<div class="row">
  	<div class="col-md-12">
        @if($link_alta == true)
            <a class="btn btn-primary" href="{{ URL::to('alta') }}">
              <span class="glyphicon glyphicon-plus"></span> Contratar o portar nuevo número
            </a>
        @else
            <div class="alert alert-danger">
                El estado de su ficha no permite nuevas altas
            </div>
        @endif
  	</div>
</div>


<?php if(count($numbers) == 0): ?>
<div class="row margin-15">
  <div class="col-md-12">
    <p>No tienes números contratados aún. <a href="{{ URL::to('alta') }}">Consigue
        uno ahora</a>.</p>
  </div>
</div>
<?php else: ?>
<script type="text/javascript">
            $(function() {
                $('#numeros').dataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false,
					"aoColumnDefs": [{ "sType": "date-uk", "aTargets": [2]}],	
					"columnDefs": [ { "targets": 5, "orderable": false }]	,
					 "aoColumns": [
						{ "sType": "numeric-html" },
						{ "sType": "string" },
						{ "sType": "date-uk" },
						{ "sType": "numeric-html" },
						{ "sType": "string" },
						{ "sType": "numeric-html" },
						{ "sType": "numeric-html" },
						null						
					]			
                });

            });
        </script>
<table class="table table-hover table-responsive table-striped margin-15" id="numeros">
  <thead>        
    <tr>
      <th class="numbers-list-col-number">Número</th>
      <th class="numbers-list-col-plan">Plan</th>
      <th class="numbers-list-col-sign-up-date">Fecha alta</th>
      <th class="numbers-list-col-pricing">Cuota</th>
      <th class="numbers-list-col-reversion">Reversión</th>
      <th class="numbers-list-col-reversion">Saldo</th>
      <th class="numbers-list-col-reversion">Minutos recibidos</th>
      <th class="numbers-list-col-options no-sort">Opciones</th>
    </tr>
  </thead>
  <tbody data-link="row" data-provides="rowlink">

    <?php
    $glyphicon = array(
      'fax'    => 'print',
      'analog' => 'phone-alt',
      'voip'   => 'earphone',
      'pbx'    => 'transfer',
      'trunk'  => 'random'
    );
    ?>
   
    <?php foreach($numbers as $number): ?>      
    <?php
    // Ñapa para no modificar la API, se debería meter el dato del tipo de pago
    // en la respuesta number de la API y añadirla a la respuesta del wrapper
    $rowNumber = Number::where('numero_visible', '=', $number->number)->first();
    $rowProfile = Profile::find($rowNumber->id_perfil);
    ?>
    <tr<?php if ($selectedNumber != NULL && $selectedNumber->getNumber() == $number->number): ?> class="active"<?php endif; ?>>
      <td class="numbers-list-col-number">
      	<!--<?=$number->number?>-->
        <span class="glyphicon glyphicon-<?=$glyphicon[$number->type]?>"></span>
        <a href="<?=URL::to('number/' . $number->number)?>">
          <?=$number->pretty_number?>
        </a>
        @if($number->suspended)
        <span class="label label-danger" style="margin-left: 10px; text-transform: uppercase;">Suspendido</span>
        @endif        
      </td>
      <td class="numbers-list-col-plan"><?=$number->profile?></td>
      <?php $date = new \DateTime($number->signup_date); ?>
      <td class="numbers-list-col-sign-up-date"><?=$date->format('d/m/Y')?></td>
      <td class="numbers-list-col-pricing">
      	<!--<?=$number->price_per_month?>-->
        <?php
        switch($rowNumber->pago_anual) {
          // cuota mensual
          case 0:
            $spanTitle = 'Pago mensual';
            $price = $number->price_per_month < 0.1 ? 'Sin cuota' : (number_format($number->price_per_month, 2, '.', ',') . ' &euro;');
            break;
          // cuota anual
          case 1:
            $spanTitle = 'Pago anual';
            $price = $rowProfile->cuota_anual < 0.1 ? 'Sin cuota' : (number_format($rowProfile->cuota_anual, 2, '.', ',') . ' &euro;');
            break;
          // cuota semestral
          case 2:
            $spanTitle = 'Pago semestral';
            $price = $number->price_per_month < 0.1 ? 'Sin cuota' : (number_format(6 * $number->price_per_month, 2, '.', ',') . ' &euro;');
            break;
        }
        ?>
        <span title="<?=$price !== 'Sin cuota' ? $spanTitle : ''?>"><?=$price?></span>
      </td>
      <td class="numbers-list-col-reversion"><?=$number->reversion == 0 ? 'Sin reversión' : number_format($number->reversion, 3, '.', ',') . ' &euro;'?></td>
      <td>
      	<?php
			if($rowNumber->perfilTipo() == 'FAX')
			{
				print number_format(round($rowNumber->saldo,2),2).' &euro;';
			}
			else
			{

			}
		?>
      </td>
      <td class="numbers-list-col-options">
        <?php
			if($rowNumber->id_perfil == '1351')
			{
        		print $rowNumber->minutos_recibidos();
			}
		?>       
      </td>
      <td class="numbers-list-col-options">
        <a class="btn btn-primary" href="{{ URL::to('change-plan/' . $number->number) }}">
          Cambiar plan
        </a>        
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
    
</table>
<?php endif; ?>

<?php if(($numberOfPages) > 1): ?>
<ul class="pagination">
  <?php if($currentPage > 1): ?><li><a href="<?=URL::to('numbers')?>/<?=$currentPage - 1?>">&laquo;</a></li><?php endif; ?>

  <?php for($i=1; $i<=$numberOfPages; $i++): ?>
  <li<?php if($currentPage == $i): ?> class="active"<?php endif;?>><a href="<?=URL::to('numbers')?>/<?=$i?>"><?=$i?></a></li>
  <?php endfor; ?>

  <?php if($currentPage < $numberOfPages): ?><li><a href="<?=URL::to('numbers')?>/<?=$currentPage + 1?>">&raquo;</a></li><?php endif; ?>
</ul>
<?php endif; ?>
<input type="hidden" id="id_subscriber" />
<script type="text/javascript">
/*
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/aa6a36f5b30966dc6e5edcce898d61b0.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
		
		p._pcq.push(['APIReady', callbackFunction]);
    })(window,document);
	function callbackFunction()
	{
		document.getElementById('id_subscriber').value = (pushcrew.subscriberId);
	}
	*/
</script>
@stop
