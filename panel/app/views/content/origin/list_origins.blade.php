<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Acciones configuradas por provincia</h3>
      </div>
      <div class="panel-body">
        @if($numberPrefixesActions !== FALSE)
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Provincia</th>
                <th>Acción</th>   
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
              @foreach($numberPrefixesActions as $numberPrefixesAction)
              {{ Form::open(array('action' => 'OriginController@add' )) }}              
              <tr>
                <td>
                  <span class="origin-detail">
                    {{ $numberPrefixesAction->detail }}                    
                  </span>
                  
                  <select name="province" class="province-select edit-input">
                    @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->id_prefix }}"
                    {{ $numberPrefixesAction->id_prefix === $prefix->id_prefix ? 'selected' : '' }}>
                      {{ $prefix->detail }}
                    </option>
                    @endforeach
                    <option value="-1">
                    	Internacional
                    </option>
                  </select>
                  
                  <input type="hidden" name="id_prefix_action" value="{{ $numberPrefixesAction->id_prefix_action }}">                  
                </td>
                <td>
                  <span class="origin-summary">
                    {{ $numberPrefixesAction->summary }}  
                  </span>
                  
                  @include('components.actions.selector', array('action' => array('name' => 'action_data_' . $numberPrefixesAction->id_prefix_action, 'value' => $numberPrefixesAction->action, 'class' => 'edit-action')))
                </td>
                <td>
                  <a href="#"
                     class="btn btn-primary edit"
                     title="Editar esta acción por origen">
                    <span class="glyphicon glyphicon-edit"></span>
                    Editar
                  </a>                        

                  <input type="submit" 
                         class="btn btn-success edit-save"
                         title="Guardar acción por origen editada"
                         value="Guardar">

                  <a href="#"
                     class="btn btn-primary edit-cancel"
                     title="Cancelar edición acción por origen">
                    <span class="glyphicon glyphicon-remove"></span>                          
                    Cancelar
                  </a>
                  
                  <a href="{{ URL::to('origin/remove/' . $numberPrefixesAction->id_prefix_action) }}"
                     class="btn btn-primary remove-origin-action"
                     title="Eliminar acción por origen">
                    <span class="glyphicon glyphicon-remove"></span>
                    Eliminar
                  </a>
                </td>
              </tr>
              {{ Form::close() }}
              @endforeach
            </tbody>
          </table>
        </div>
        @else
        <div class="media col-md-12">
          <span class="pull-left glyphicon glyphicon-globe panel-icon"></span>
          <div class="media-body">
            <h4 class="media-heading">No tienes aún acciones configuradas por provincia.</h4>
          </div>
        </div>
        @endif
      </div>  
    </div>    
  </div>
</div>


