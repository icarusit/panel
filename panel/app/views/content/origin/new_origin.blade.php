<div class="row">
  <div class="col-md-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Añadir una nueva acción según provincia</h3>
      </div>
      <div class="panel-body">
        <p>
          {{ Form::open(array(
            'action' => 'OriginController@add',
            'class'  => 'form-horizontal col-md-10'
          )) }}
          
          <div class="form-group">
            <label for="province" class="col-md-3 control-label">Provincia</label>

            <div class="col-md-6">
              <select name="province" id="province" class="province-select">
                @foreach($prefixes as $prefix)
                <option value="{{ $prefix->id_prefix }}">{{ $prefix->detail }}</option>
                @endforeach
                <option value="-1">
                    Internacional
                </option>
              </select>
            </div>
          </div>          
          
          <div class="form-group"> 
            <label for="action" class="col-md-3 control-label">Acción</label>
            <div class="col-md-9">
              @include('components.actions.selector')
            </div>
          </div>

          <input type="submit" id="origin-submit" class="btn btn-success" value="Guardar">
          
          {{ Form::close() }}        
                 
          @include('components.actions.modals.include')
      </div>
    </div>    
  </div>
</div>


