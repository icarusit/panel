@extends('main')

@section('subpage')
<h1>Desvíos por provincia</h1>

@if(!Session::get('is_mobile'))
	@include('help', array('content' => 'origin'))
@endif

@include('errors')

@if($numberPrefixesActions !== false)
  @include('content.origin.list_origins')
  @include('content.origin.new_origin')
@else
  @include('content.origin.new_origin')
  @include('content.origin.list_origins')
@endif

<script type="text/javascript">
$(document).ready(function() {
  $('.province-select').selectize({
    create: true,
    sortField: 'text'
  });
  
  $('.edit').click(function() {
    $(this).hide();    
    $(this).siblings('.remove-origin-action').hide();    
    $(this).siblings('.edit-save').show();
    $(this).siblings('.edit-cancel').show().css('display', 'inline-block');  
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('origin-detail')) {
          $(this).hide();
          $(this).siblings('div.edit-input').show();
        } else if($(this).hasClass('origin-summary')) {
          $(this).hide();
          $(this).siblings('.selector').show();
        }
      });
    });
    
    return false;
  });  
  
  $('.edit-cancel').click(function() {
    $(this).hide();
    $(this).siblings('.edit-save').hide();    
    $(this).siblings('.remove-origin-action').show();
    $(this).siblings('.edit').show();        
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('edit-input')) {
          $(this).hide();
          $(this).siblings('.origin-detail').show();
        } else if($(this).hasClass('selector')) {
          $(this).hide();
          $(this).siblings('.origin-summary').show();
        }
      });
    });
    
    return false;    
  });  
});

$(document).on('click', '.remove-origin-action', function() {
  return confirm("Se eliminará la acción. ¿Desea continuar?");
});
</script>
@stop
