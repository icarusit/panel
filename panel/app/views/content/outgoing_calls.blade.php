@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-phone-alt"></span> Llamadas salientes</h1>

@include('help', array('content' => 'outgoing_calls', 'number' => $number))
@include('errors')

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Añadir nuevo teléfono de origen</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      {{ Form::open(array(
        'action' => 'OutgoingCallsController@add',
        'class'  => 'form-horizontal col-md-5'
      )) }}
 
       <div class="form-group">
         <label for="number" class="col-md-2 control-label">Número</label>
         <div class="col-md-10">
           <input class="form-control" type="text" name="number" value="" />
         </div>
       </div>     

       <input type="submit" class="btn btn-primary" value="Guardar" /> 
     {{ Form::close() }}
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Números desde los que se pueden hacer llamadas con origen <strong>{{ $number }}</strong></h3>
  </div>
  <div class="panel-body">
    @if(count($numbers) > 0)
    <table class="table" style="width: auto;">
      <thead>
        <tr>
          <th>Número de origen</th>
          <th>¿Activo?</th>
          <th>Operaciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($numbers as $number)
        <tr>
          <td>{{ $number->caller }}</td>            
          <td>
            <input type="checkbox" id="number-toggle" 
                   class="number-toggle" 
                   data-id="{{ $number->id }}"
                   value="1" {{ $number->active ? ' checked' : '' }}>
          </td>	
          <td>
            <a href="{{ URL::to('outgoing-calls/remove/' . $number->id) }}"
               class="btn btn-primary remove-number"
               title="Eliminar este número de la lista">
              <span class="glyphicon glyphicon-remove"></span> Eliminar
            </a>
          </td>
        </tr>
      @endforeach      
      </tbody>
    </table>
    @else
    <p>No has añadido aún números.</p>
    @endif
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('.remove-number').click(function() {
    return confirm("Se eliminará el número de la lista de orígenes permitidos. ¿Quieres continuar?");
  });
  
  $('.number-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      var id = $(this).data('id');
      
      $.post("{{ URL::to('outgoing-calls/toggle') }}", {
        idNumber: id
      }, function(data) {
        console.log(data);
      });
    }
  });
});
</script>

@stop
