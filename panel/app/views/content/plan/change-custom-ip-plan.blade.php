@extends('main')

@section('subpage')

<h1>Modifica tu plan IP personalizado</h1>

@include('errors')

@include('components.custom-ip-plan-setup')

@stop