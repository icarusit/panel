@extends('main')

@section('subpage')

<h1>Cambio de plan</h1>

@include('errors')

<p>El plan que tienes actualmente contratado para el número <strong>{{ $number->numero_visible }}</strong>
  es <strong>{{ $currentProfile->perfil }}</strong>.

@if(!$canChangePlan)
<p>Lo sentimos, hiciste un cambio de plan el día <strong>{{ $changePlanDate->format('d/m/Y') }}</strong>.
  Sólo permitimos un cambio de plan al mes.</p>

<p>Debes ponerte en contacto con nosotros en nuestros números de teléfono o mediante la dirección
  de correo <a href="mailto:info@fmeuropa.com">info@fmeuropa.com</a> para solicitar un nuevo cambio
  de plan o esperar al día <strong>{{ $nextPossibleChangePlanDate->format('d/m/Y') }}</strong> para
  solicitarlo desde la web.</p>

<p>Gracias.</p>
@else
  @if($number->a_medida == "Si")
  <p>Al estar tu número <strong>configurado a medida</strong> no puedes hacer cambio de plan online por
    lo que debes escribirnos a <a href="mailto:info@fmeuropa.com">info@fmeuropa.com</a>
    para que te hagamos el cambio de plan.</p>

  <p>Gracias.</p>
  @elseif($currentProfile->can_order_online == false)
  <p>El plan que tienes contratado es un <strong>plan especial</strong> que no puede ser contratado/cambiado
    online. Ponte en contacto con nosotros en la dirección de correo <a href="mailto:info@fmeuropa.com">info@fmeuropa.com</a>
    para que te hagamos el cambio de plan.</p>

  <p>Gracias.</p>
  @else
    @if(count($canOrderProfiles) > 1)
    <p>Elige el plan que más se ajusta a lo que necesitas:</p>

    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th></th>
            <th>Plan</th>
            <th>Cuota mensual</th>
            <th>Comparado con plan actual (<strong>{{ $currentProfile->perfil }}</strong>)</th>
          </tr>
        </thead>
        <tbody>
          @foreach($canOrderProfiles as $profile)
            <tr{{ $profile->id == $currentProfile->id ? ' class="change-plan-current-profile-row"' : '' }}>
              <td>              
                @if($profile->id == $currentProfile->id)
                  <span class="label label-default">Plan actual</span>
                @else
                  {{ Form::open(array(
                    'action' => 'ChangePlanController@postChange',
                    'class'  => 'form-inline change-plan'
                  )) }}
                  <input type="hidden" name="profile-id" value="{{ $profile->id }}">
                  <button type="button" class="btn btn-primary button-change-plan">
                    <i class="fa fa-shopping-cart"></i> Cambiar
                  </button>
                  {{ Form::close() }}
                @endif
              </td>
              <td>{{ $profile->perfil }}</td>
              <td><strong>{{ $profile->cuota }}€/mes</strong></td>
              <td>
                <ul class="change-plan-features">
                  @foreach($profile->pros['plus'] as $feature)
                  <li class="change-plan-feature change-plan-feature-plus">                  
                    <a href="#" data-toggle="tooltip" data-placement="top" title="{{ $feature->tooltip }}">
                      <span class="fa fa-plus"></span>
                      {{ $feature->pro }}
                    </a>
                  </li>
                  @endforeach

                  @foreach($profile->pros['minus'] as $feature)
                  <li class="change-plan-feature change-plan-feature-minus">
                    <a href="#" data-toggle="tooltip" data-placement="top" title="{{ $feature->tooltip }}">
                      <span class="fa fa-minus"></span>
                      {{ $feature->pro }}
                    </a>
                  </li>              
                  @endforeach
                </ul>
              </td>
            </tr></li>
          @endforeach      
        </tbody>
      </table>  
    </div>

    <script>
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
      
      $('.button-change-plan').click(function() {        
        if(confirm("Se procederá al cambio de plan. ¿Quieres continuar?")) {
          $(this).closest('form.change-plan').submit();
        }
      });
    });
    </script>
    @else
    <p>No tenemos más planes para el tipo de producto que tienes contratado. Es posible
      que sí tengamos planes especiales para este tipo de producto. Consúltanos en la
      dirección de correo <a href="mailto:info@fmeuropa.com">info@fmeuropa.com</a>.

    <p>Gracias.</p>
    @endif
  @endif
@endif

@stop