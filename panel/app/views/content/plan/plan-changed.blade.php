@extends('main')

@section('subpage')

<h1>Has cambiado el plan de {{ $number }}</h1>

@include('errors')

<p>El plan que tienes actualmente contratado para el número <strong>{{ $number }}</strong>
  es <strong>{{ $currentProfile->perfil }}</strong>.
  
<p>En la próxima factura te cobraremos los <strong>{{ $currentProfile->cuota }}€</strong> correspondientes
  a la cuota mensual.</p>

<p>Si tienes alguna consulta adicional o ha sido un error, contáctanos en el <strong>902 998 901</strong> o
  escríbenos a <a href="mailto:info@<?php echo Session::get('dominio'); ?>">info@<?php echo Session::get('dominio'); ?></a>.
  
<p>Gracias.</p>
@stop