@extends('main')

@section('subpage')
<h1>Mi perfil</h1>

@include('errors')

<div class="row">
	<div class="col-md-6">
    	<div class="panel panel-default">
        	<div class="panel-heading">
            	<h3 class="panel-title">Datos de facturación</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array(
                  'action' => 'UserController@update',
                  'class'  => 'form-horizontal'
                )) }}
                
                    
                    
                    <div class="form-group">
                      <label class="col-sm-5 control-label">Cliente desde</label>
                      <div class="col-sm-7">  
                        <div class="form-control-static">{{ (new \DateTime($signup_date))->format('d/m/Y') }}</div>
                      </div>
                    </div>
                    
                    @include('components.customer-form')
                    
                    <h3>Configuración cuenta de acceso</h3>
                    
                    <div class="form-group">
                      <label class="col-sm-5 control-label">Usuario</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="username" id="username"
                               value="{{ $username }}" title="No puedes cambiar el nombre de usuario"
                               readonly>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-5 control-label">Password</label>
                      <div class="col-sm-7">
                        <input type="password" class="form-control" name="password" id="password"
                               value="" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-5 control-label">Repite password</label>
                      <div class="col-sm-7">
                        <input type="password" class="form-control" name="cfm-password" id="cfm-password"
                               value="" />
                      </div>
                    </div>
                    
                    <div class="form-group col-sm-12">
                      <input type="submit" class="btn btn-primary pull-right" value="Guardar" />  
                    </div>
                
                {{ Form::close() }}
        	</div>
    	</div>
	</div>
    <div class="col-md-6">
    	<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Enlace de afiliación</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array(
                      'action' => 'UserController@generateCodigoAfiliacion',
                      'class'  => 'form-horizontal'
                    )) }}
                    <div class="col-sm-12">
                    	<div class="alert alert-success">
                    		<span class="glyphicon glyphicon-share"></span>
                        	El enlace de afiliación lo podras compartir con tus amistades, por cada afiliación que se realice con tu enlace se te compensará.
                        </div> 
                    </div>
                    @if(!$codigo_afiliacion)
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-primary pull-right" value="Generar enlace de afiliación" />  
                        </div>
                    @else
                    	<div class="form-group">
                          	<label class="col-sm-5 control-label"><strong>Enlace de afiliación</strong></label>                          
                        </div>
                    	<div class="col-sm-12">
                        	{{ url() }}/alta/codigo/{{$codigo_afiliacion->codigo}}
                        </div>
                    @endif
                {{ Form::close() }}
            </div>
        </div>
    </div>

</div>
@stop
