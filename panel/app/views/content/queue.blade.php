@extends('main')

@section('subpage')
<h1>Cola de llamada</h1>

@include('help', array('content' => 'queue'))

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if(($err = Session::get('error', NULL)) !== NULL)
<div class="alert alert-danger">{{ $err }}</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif

<div class="row">
  	<div class="col-sm-3">
    
    	<div class="panel panel-default">
      		<div class="panel-heading">
        		<h3 class="panel-title">Cuentas SIP</h3>
      		</div>
            <div class="panel-body">
                <ul class="queue-sip-accounts">
                    @foreach($sipPeers as $sipPeer)
                        <li class="queue-sip-account">{{ $sipPeer->defaultuser }}</li>
                    @endforeach
                    @foreach($grupos as $grupo)
                        <li class="queue-sip-account grupo">
                        	<span class="glyphicon glyphicon-list"></span>
                        	{{ $grupo->name }}
                        </li>
                    @endforeach
                </ul>
            </div>
    	</div>
        
        <div class="col-md-12">
            <a class="btn btn-primary" href="<?php print URL::to('groups'); ?>">
                Gestionar grupos
            </a>
        </div>
        
        <div class="margin-30"></div>
        
        <div class="panel panel-default">
      		<div class="panel-heading">
        		<h3 class="panel-title">Añadir número</h3>
      		</div>
            <div class="panel-body">
            	<div class="form-group">
            		<input type="text" class="form-control phone-number" name="nuevo_numero" id="nuevo_numero"/>
                </div>
                <div class="form-group">
                	<a class="btn btn-primary" id="add_numero"><span class="glyphicon glyphicon-plus"></span> Añadir número</a>
                </div>
            </div>
    	</div>
        
  	</div>

  <div class="col-sm-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Cola de llamada 
          <img class="queue-loading" src="{{ asset('img/loading-16.gif') }}">
        </h3>
      </div>
      <div class="panel-body" id="box_cola_llamada">
        <div class="table-responsive">
            <table class="queue" id="queue">
              <tbody>
                <?php
                $cols = 4;
                ?>
                
                @if($queue !== FALSE)
                
                  <?php $priorities = count($queue); ?>
                  
                  @foreach($queue as $i => $row)
                    <?php $j = 0; ?>
                    <tr class="queue-row">
                      <td class="queue-priority">{{ $i + 1 }}</td>
    
                      @while($j < $cols)
                        @if(isset($row[$j]))
                        	<?php
								$idUser = Session::get('user')->getID();  
								$grupo = Group::where('name','=',$row[$j])->where('id_client', '=', $idUser)->first();
							?>
                          	<td class="queue-col queue-sip-account">
                            @if(is_numeric($row[$j]))<span class="glyphicon glyphicon-phone-alt"></span> @endif
							<?php							
								if($grupo)
								{
									print '<span style="cursor:pointer;" data-toggle="popover" title="Miembros" data-content="'.$grupo->MiembrosToolTip().'" class="glyphicon glyphicon-list"></span>';
								}
								?>{{ $row[$j] }}</td>
                        @else
                          <td class="queue-col queue-col-empty"></td>
                        @endif
                        <?php $j++; ?>
                      @endwhile
                    </tr>
                  @endforeach
                @else
                  <tr class="queue-row">
                    <td class="queue-priority">1</td>
                    
                    @for($j=0; $j<$cols; $j++)
                      <td class="queue-col queue-col-empty"></td>
                    @endfor
                  </tr>
                @endif
              </tbody>
            </table>
        </div>
        <a href="#" class="margin-15 btn btn-primary queue-plus-row"><span class="glyphicon glyphicon-plus"></span> Añadir prioridad</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {  

	 
  
  var cols = {{ $cols }};
  var priorities = {{ isset($priorities) ? $priorities : 1 }};
  var aux;
  var queue = '#queue';
  var empty_row = '<tr class="queue-row"></tr>';
  var col_priority = '<td class="queue-priority"></td>';
  var col_sip = '<td class="queue-col queue-sip-account"></td>';
  var empty_col = '<td class="queue-col queue-col-empty"></td>';

  var dropOpts = {
    accept: 'li.queue-sip-account, td.queue-sip-account',
    hoverClass: 'queue-col-hover',
    drop: function(event, ui) {
      var repeated = false;
      var sip_account = ui.draggable.text();
	  var sip_account_html = ui.draggable.html();
	  
	  
      
      $(this).siblings().each(function(i, v) {
        if(i > 0) {
          if(sip_account == $(this).html()) {
            repeated = true;
          }
        }
      });

      if(!repeated)
	  {
        if(ui.draggable.is('td'))
		{
          ui.draggable.remove();
          var that = $(this);
          // Un timeout porque tarda el DOM en eliminar la celda de procedencia
          setTimeout(function() {
            that.addClass('queue-sip-account');      
            that.droppable('disable');    
            that.removeClass('queue-col-empty');
            that.draggable(dragOpts);    
            that.html(sip_account_html); 

            queue_save();                       
          }, 100);
        }
		else
		{
			if(ui.draggable.hasClass('grupo'))
			{
				queue_add_sip(sip_account, $(this),2);
			}
			else if(ui.draggable.hasClass('numero_externo'))
			{
				queue_add_sip(sip_account, $(this),1);
			}
			else
			{
				queue_add_sip(sip_account, $(this),false);
			}
          	
        }
      } else {
        alert("No puedes tener una misma cuenta SIP dos veces en una misma prioridad.");
      }
    }
  };
  
  var dragOpts = {
    revert: 'invalid',
    cursorAt: { top: 5, left: 24 },
    helper: 'clone',
    containment: 'document'
  };

  function queue_add_sip(sip, $where,numero_externo) {
    $where.addClass('queue-sip-account');      
    $where.droppable('disable');    
    $where.removeClass('queue-col-empty');
    $where.draggable(dragOpts);
	
	if(numero_externo == 1)
	{
		$where.append('<span class="glyphicon glyphicon-phone-alt"></span> '+sip);
		$('#box_cola_llamada').append('<div class="alert alert-danger msg_numero_externo" style="margin-top:5px;">El desvío a número externos puede ocasionar costes añadidos</div>');
		setTimeout(function() {
            $('.msg_numero_externo').remove();                    
          }, 4000);
		
	}
	else if(numero_externo == 2)
	{
		$where.append('<span class="glyphicon glyphicon-list"></span> '+sip);		
	}
	else
	{	    
    	$where.text(sip);
	}

    queue_save();           
  }
  
  function queue_add_row() {  
    priorities++;
    $priority = $(col_priority).html(priorities);
    
    $row = $(empty_row);
    $row.append($priority);
    for(var i=0; i<cols; i++) $row.append($(empty_col));

    $(queue).append($row);
    
    $('li.queue-sip-account').draggable(dragOpts);  
    $('.queue-col-empty').droppable(dropOpts);      
  }
  
  function queue_serialize()
  {
    var serialized = new Array();
    
    $(queue).find('tr').each(function(i, v) {
      serialized[i] = new Array();
      $(this).find('.queue-sip-account').each(function(i2, sip) {
        serialized[i][i2] = $(sip).text();
      });
    });
    
    return serialized;
  }  
  
  function queue_save()
  {
    $('.queue-loading').show();
    
    var serialized = queue_serialize();
    
    $.post('{{ URL::to('queue') }}/order', {
      order: serialized
    }, function(data) {
	  window.location = '{{ URL::to('queue') }}';
      $('.queue-loading').hide();
      console.log(data);
    });
  }
  
  $('.queue-plus-row').click(function() { queue_add_row(); });    
  $('li.queue-sip-account').draggable(dragOpts);  
  $('td.queue-sip-account').draggable(dragOpts);    
  $('.queue-col-empty').droppable(dropOpts);

  $(document).on('mouseenter', 'td.queue-sip-account', function() {
    $(this).append(
        '<a href="#" class="queue-remove-sip" title="Eliminar cuenta SIP de la cola de llamada">' + 
          '<span class="glyphicon glyphicon-remove"></span>' + 
        '</a>'
    );
  });

  $(document).on('mouseleave', 'td.queue-sip-account', function() {
    $(this).find('.queue-remove-sip').remove();
  });
  
  $(document).on('click', '.queue-remove-sip', function() {
    $sip = $(this).parent();
    $sip.empty();
    $sip.removeClass('queue-sip-account');      
    $sip.addClass('queue-col-empty');    
    $sip.droppable(dropOpts);

    queue_save();
  });
  
  	function comprobar_numeros(numero)
  	{
		var existe = false;
	  	$("li.queue-sip-account").each(function() { 
	  		if($(this).text() == numero)
			{
				existe = true;
			}
	  	});
		return existe;
  	}

  $('li.queue-sip-account, .queue-col').disableSelection();
  
  	$("#add_numero").click(function(){
		
		if($("#nuevo_numero").data('error') == true)
		{
			alert($("#nuevo_numero").prop('title'));
		}
		else
		{
			var nuevo_telefono = $("#nuevo_numero").val();
			//validar el numero luego
			if(comprobar_numeros(nuevo_telefono) == false)
			{
				var nuevo_li = '<li class="queue-sip-account ui-draggable ui-draggable-handle numero_externo"><span class="glyphicon glyphicon-phone-alt"></span> '+nuevo_telefono+'</li>';
				$('.queue-sip-accounts').append(nuevo_li);
			
				$('li.queue-sip-account').draggable(dragOpts); 
			
				$("#nuevo_numero").val("");
			}
			else
			{
				alert('Existe el número');
			}
		}	
		
	});
  
  $('[data-toggle="popover"]').popover(); 
  
});

</script>

@stop
