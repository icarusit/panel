<div class="panel panel-default margin-30">
  <div class="panel-heading">
    <h3 class="panel-title">Calculadora de tarifas de llamadas salientes y desvíos</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="form-group">
        <div class="col-md-4">
          <input class="form-control form-control-huge phone-number rates-filter"
                 type="text" name="phone-number" id="phone-number"
                 placeholder="p.e.: 00442072787871" />
        </div>        
      </div>
    </div>
    <div class="row margin-15 rate-calc-not-found" style="display: none;">
      <div class="col-md-4">
        <div class="alert alert-warning">
          <p>No hemos encontrado ningún destino que coincida con el número que has introducido :(</p>
          <p>¿Es posible que sea un error?</p>
        </div>
      </div>
    </div>
    <div class="row margin-15">
      <div class="form-group">
        <label class="col-sm-2">País de destino:</label>
        <div class="col-sm-3">
          <div class="from-control-static rate-result" id="rates-country"></div>
        </div>
      </div>
    </div>
    <div class="row margin-15">
      <div class="form-group">
        <label class="col-sm-2">Provincia u operadora:</label>
        <div class="col-sm-3">
          <div class="from-control-static rate-result" id="rates-detail"></div>
        </div>
      </div>
    </div>    
    <div class="row margin-15">
      <div class="form-group">
        <label class="col-sm-2">Establecimiento:</label>
        <div class="col-sm-3">
          <div class="from-control-static rate-result" id="rates-connection-charge"></div>
        </div>
      </div>
    </div>
    <div class="row margin-15">
      <div class="form-group">
        <label class="col-sm-2">Precio minuto:</label>
        <div class="col-sm-3">
          <div class="from-control-static rate-result" id="rates-rate"></div>
        </div>
      </div>
    </div>    
    <div class="row margin-15">
      <div class="form-group">
        <label class="col-sm-2">Puedes llamar...</label>
        <div class="col-sm-3">
          <div class="from-control-static rate-result" id="rates-timeout"></div>
        </div>
      </div>
    </div>        
    <div class="row margin-15">
      <div class="col-sm-5">
        @if($userHasTaxes)
        <div class="alert alert-info">* Estos precios no incluyen IVA</p></div>
        @endif
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var userHasTaxes = {{ $userHasTaxes ? 'true' : 'false' }};

$(document).ready(function() {
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  $('#phone-number').keyup(function() {
    $dest = $(this);
    
    delay(function() {
      if(!$dest.hasClass('input-text-error')) {
        $('.rate-calc-not-found').hide();
        $dest.css('background-image', 'url({{ asset("img/loading-16.gif") }})');                

        $.post('{{ url() }}/rate/' + $dest.val(), {}, function(data) {
          if(data.length === 0 || typeof data.rate === "undefined") {
            $dest.css('background-image', 'none');
            $('.rate-calc-not-found').fadeIn();
          } else {
            if(data.destination_country_es !== null) {
              $('#rates-country').html(data.destination_country_es + ' (' + data.destination_iso + ')');
              $dest.css('background-image', 'url({{ asset("img/flags") }}/' + data.destination_iso + '.png)');
            } else {
              $('#rates-country').html('');
              $dest.css('background-image', 'none');
            }

            $('#rates-detail').html(data.destination_detail_es);            
            $('#rates-connection-charge').html(data.initial_connection_charge + ' &euro;' + (userHasTaxes ? '*' : ''));            
            $('#rates-rate').html(data.rate + ' &euro;' + (userHasTaxes ? '*' : ''));
         
            var html;

            if(isNaN(data.call_timeout)) {
              html = 'Ilimitados minutos';
            } else {
              html = parseFloat(data.call_timeout / 60).toFixed(2) + ' minutos';
            }

            $('#rates-timeout').html(html);
            
          }
        }, 'json');
      }
    }, 500);
  });
});
</script>
