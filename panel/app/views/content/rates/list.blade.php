<div class="panel panel-default margin-30">
  <div class="panel-heading">
    <h3 class="panel-title">
      Listado de tarifas
      
      @if($userHasTaxes)
      (precios sin IVA)
      @endif
      
      <a class="inline-link" href="{{ URL::to('rates/export') }}">
        <span class="glyphicon glyphicon-export"></span>&nbsp;
        Descargar para Excel
      </a>
    </h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <form class="form-horizontal">
        <div class="form-group">
          <label class="col-sm-2 control-label">Filtrar por país:</label>
          <div class="col-md-5">
            <input class="form-control rates-filter"
                   type="text" name="country-filter" id="country-filter"
                   placeholder="p.e.: Gambia"
                   value="{{ ($country = Session::get('country', null)) !== null ? $country : '' }}" />
          </div>        
        </div>
      </form>
    </div>  
    
    @if(count($rates) > 0)
    <div id="rates-list-wrapper">
      <div class="table-responsive">      
        <table id="table-rates" class="table table-hover table-striped">
          <thead>        
            <tr>
              <th>País</th>
              <th>Detalle</th>
              <th>Tarifa</th>
              <th>Establecimiento</th>
            </tr>
          </thead>
          <tbody>    
            @foreach($rates as $rate)
            <tr>
              <td>
                <img class="rates-flag" src="{{ asset("img/flags") }}/{{ $rate->iso }}.png">
                <strong>{{ $rate->country_es }}</strong>
              </td>
              <td>{{ $rate->detail_es }}</td>
              <td><strong>{{ $rate->rate }} €</strong></td>
              <td><strong>{{ $rate->initial_connection_charge }} €</strong></td>
            </tr>          
            @endforeach
          </tbody>
        </table>
      </div>

      {{ $paginator->links() }}
      @endif       
    </div>
    <div id="rates-no-rates" class="media col-md-12">
      <span class="pull-left fa fa-frown-o panel-icon"></span>
      <div class="media-body">
        <h4 class="media-heading">No hemos encontrado el país.</h4>
        <p>Prueba con otros criterios de búsqueda.</p>
      </div>
    </div>      
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  $('#country-filter').keyup(function() {
    $country = $(this);
    
    delay(function() {
      $country.css('background-image', 'url({{ asset("img/loading-16.gif") }})');                

      $.getJSON('{{ url() }}/rates', {
        country: $country.val()
      }, function(data) {
        $('#table-rates tbody').empty();
        $('.pagination').remove();

        if(data.rates.length > 0) {
          var rates = data.rates;
          var paginator = data.paginator;
          var html = '';
          
          for(i in rates) {
            html+= '' +
              '<tr>' +
                '<td>' +
                  '<img class="rates-flag" src="{{ asset("img/flags") }}/' + rates[i].iso + '.png">' +
                  '<strong>' + rates[i].country_es + '</strong>' + 
                '</td>' +
                '<td>' + rates[i].detail_es + '</td>' +
                '<td><strong>' + rates[i].rate + ' €</strong></td>' +
                '<td><strong>' + rates[i].initial_connection_charge + ' €</strong></td>' +
              '</tr>';
          }
          
          $('#table-rates tbody').append(html);
          
          if(paginator !== '') {
            $('#rates-list-wrapper').append(paginator);
          }
          
          $('#rates-list-wrapper').show();
          $('#rates-no-rates').hide();          
        } else {
          $('#rates-list-wrapper').hide();
          $('#rates-no-rates').show();
        }
        $country.css('background-image', 'none');                
      });
    }, 200);
  });
});
</script>
