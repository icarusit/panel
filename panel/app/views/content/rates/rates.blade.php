@extends('main')

@section('subpage')
<h1><i class="fa fa-money"></i> Tarifas</h1>

@include('errors')

<div class="alert alert-warning">Los precios en esta página sólo serán válidos durante el mes de {{ date('F') }} de {{ date('Y') }}</div>

{{-- Listado y buscador de tarifas --}}
@include('content.rates.list')

{{-- Calculadora de tarifas --}}
@include('content.rates.calc')

@stop
