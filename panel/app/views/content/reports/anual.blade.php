@extends('main')
@section('subpage')

<link class="include" rel="stylesheet" type="text/css" href="{{ asset('jqplot/jquery.jqplot.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('jqplot/syntaxhighlighter/styles/shCoreDefault.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('jqplot/syntaxhighlighter/styles/shThemejqPlot.min.css') }}" />

<h1>{{$title}}</h1>

@include('errors')

<style>
.th_left
{
	border-right:1px solid #dddddd !important;
	font-weight:bold;
}
</style>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Filtro</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
      		{{ Form::open(array(
        		'action' => 'ReportsController@show',
        		'class'  => 'form-horizontal col-md-12'
      		)) }}
 
                <div class="form-group col-md-6">
         			<label for="name" class="col-md-2 control-label">Año</label>
         			<div class="col-md-10">
           				<select name="anno" required>
                        	<?php
                        	for($i=$extremos['inicio'];$i<=$extremos['fin'];$i++)
							{
                            	print '<option '.($anno == $i?'selected="selected"':'').' value="'.$i.'">'.$i.'</option>';
							}
                            ?>
                        </select>
         			</div>
                </div>
                <div class="form-group col-md-6">
         			<label for="name" class="col-md-2 control-label">Reporte</label>
         			<div class="col-md-10">
           				<select name="tipo" required>
                        	<option <?php print ($tipo == 'fecha_alta'?'selected="selected"':''); ?> value="fecha_alta">Altas</option>
                            <option <?php print ($tipo == 'fecha_baja'?'selected="selected"':''); ?> value="fecha_baja">Bajas</option>
                        </select>
         			</div>
                </div>
       			<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-stats"></span> Buscar</button>
     		{{ Form::close() }}
    	</div>
  	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">{{$title}}</h3>
  	</div>
  	<div class="panel-body">
    	<div class="table-responsive">
        	@if(count($resumen) > 0)
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Perfil</th>
                            <th title="Enero"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/1/{{$tipo}}">Ene</a></th>
                            <th title="Febrero"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/2/{{$tipo}}">Feb</a></th>
                            <th title="Marzo"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/3/{{$tipo}}">Mar</a></th>
                            <th title="Abril"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/4/{{$tipo}}">Abr</a></th>
                            <th title="Mayo"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/5/{{$tipo}}">May</a></th>
                            <th title="Junio"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/6/{{$tipo}}">Jun</a></th>
                            <th title="Julio"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/7/{{$tipo}}">Jul</a></th>
                            <th title="Agosto"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/8/{{$tipo}}">Ags</a></th>
                            <th title="Septiembre"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/9/{{$tipo}}">Sep</a></th>
                            <th title="Octubre"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/10/{{$tipo}}">Oct</a></th>
                            <th title="Noviembre"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/11/{{$tipo}}">Nov</a></th>
                            <th title="Diciembre"><a href="{{ URL::to('reporte-mensual') }}/{{$anno}}/12/{{$tipo}}">Dic</a></th>
                        </tr>
                        @if($tipo == 'fecha_alta')
                        <tr>
                            <th>Nuevos Clientes</th>
                            <th>{{$resumen_clientes->enero}}</th>
                            <th>{{$resumen_clientes->febrero}}</th>
                            <th>{{$resumen_clientes->marzo}}</th>
                            <th>{{$resumen_clientes->abril}}</th>
                            <th>{{$resumen_clientes->mayo}}</th>
                            <th>{{$resumen_clientes->junio}}</th>
                            <th>{{$resumen_clientes->julio}}</th>
                            <th>{{$resumen_clientes->agosto}}</th>
                            <th>{{$resumen_clientes->septiembre}}</th>
                            <th>{{$resumen_clientes->octubre}}</th>
                            <th>{{$resumen_clientes->noviembre}}</th>
                            <th>{{$resumen_clientes->diciembre}}</th>
                        </tr>
                        @endif
                    </thead>
                    <tbody>
                    	<?php
							$totales = array('enero'     => 0,
											'febrero'   => 0,
											'marzo'     => 0,
											'abril'     => 0,
											'mayo'      => 0,
											'junio'     => 0,
											'julio'     => 0,
											'agosto'    => 0,
											'septiembre'=> 0,
											'octubre'   => 0,
											'noviembre' => 0,
											'diciembre' => 0);
						?>
                        @foreach($resumen as $perfil)
                            <tr>
                                <td class="th_left">{{$perfil->PERFIL}}</td>
                                <td>{{$perfil->enero}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->enero,$perfil->febrero)}}">{{$perfil->febrero}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->febrero,$perfil->marzo)}}">{{$perfil->marzo}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->marzo,$perfil->abril)}}">{{$perfil->abril}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->abril,$perfil->mayo)}}">{{$perfil->mayo}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->mayo,$perfil->junio)}}">{{$perfil->junio}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->junio,$perfil->julio)}}">{{$perfil->julio}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->julio,$perfil->agosto)}}">{{$perfil->agosto}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->agosto,$perfil->septiembre)}}">{{$perfil->septiembre}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->septiembre,$perfil->octubre)}}">{{$perfil->octubre}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->octubre,$perfil->noviembre)}}">{{$perfil->noviembre}}</td>
                                <td style="background-color:{{@color_celda($tipo,$perfil->noviembre,$perfil->diciembre)}}">{{$perfil->diciembre}}</td>
                            </tr>
                            <?php
								$totales['enero']+= $perfil->enero;
								$totales['febrero']+= $perfil->febrero;
								$totales['marzo']+= $perfil->marzo;
								$totales['abril']+= $perfil->abril;
								$totales['mayo']+= $perfil->mayo;
								$totales['junio']+= $perfil->junio;
								$totales['julio']+= $perfil->julio;
								$totales['agosto']+= $perfil->agosto;
								$totales['septiembre']+= $perfil->septiembre;
								$totales['octubre']+= $perfil->octubre;
								$totales['noviembre']+= $perfil->noviembre;
								$totales['diciembre']+= $perfil->diciembre;
							?>
                        @endforeach      
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th>Totales</td>
                        	<th>{{$totales['enero']}}</th>
                            <th>{{$totales['febrero']}}</th>
                            <th>{{$totales['marzo']}}</th>
                            <th>{{$totales['abril']}}</th>
                            <th>{{$totales['mayo']}}</th>
                            <th>{{$totales['junio']}}</th>
                            <th>{{$totales['julio']}}</th>
                            <th>{{$totales['agosto']}}</th>
                            <th>{{$totales['septiembre']}}</th>
                            <th>{{$totales['octubre']}}</th>
                            <th>{{$totales['noviembre']}}</th>
                            <th>{{$totales['diciembre']}}</th>
                        </tr>
                    </tfoot>
                </table>
            @else
                <p>No existen estadísticas para el año seleccionado.</p>
            @endif
        </div>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">{{$title}}</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
        	<div id="chart1" class="col-md-12" style="height:300px;"></div>
        </div>
    </div>
</div>

<?php
$totales_js = array();
foreach($totales as $total)
{
	$totales_js[] = $total;
}
$totales_js = implode(',',$totales_js);
?>
<script class="code" type="text/javascript">
        $(document).ready(function(){
			$.jqplot.config.enablePlugins = true;
          	var line1 = <?php print '['.$totales_js.']'; ?>;
		  	var ticks = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
          	var plot1 = $.jqplot('chart1', [line1], {
			  	animate: !$.jqplot.use_excanvas,
              	title: '', 
              	seriesDefaults: { 
                	showMarker:false,
                	pointLabels: { show:true } 
              	},
			  	axes: {
						xaxis: {
							renderer: $.jqplot.CategoryAxisRenderer,
							ticks: ticks
						}
					},
			  	highlighter: { show: false }
          	});
			
			
			
        });
</script>

@if($tipo == 'fecha_alta')

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">Nuevos clientes</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
        	<div id="chart2" class="col-md-12" style="height:300px;"></div>
        </div>
    </div>
</div>
<?php
$totales_clientes_js = array();
$totales_clientes_js[] = $resumen_clientes->enero;
$totales_clientes_js[] = $resumen_clientes->febrero;
$totales_clientes_js[] = $resumen_clientes->marzo;
$totales_clientes_js[] = $resumen_clientes->abril;
$totales_clientes_js[] = $resumen_clientes->mayo;
$totales_clientes_js[] = $resumen_clientes->junio;
$totales_clientes_js[] = $resumen_clientes->julio;
$totales_clientes_js[] = $resumen_clientes->agosto;
$totales_clientes_js[] = $resumen_clientes->septiembre;
$totales_clientes_js[] = $resumen_clientes->octubre;
$totales_clientes_js[] = $resumen_clientes->noviembre;
$totales_clientes_js[] = $resumen_clientes->diciembre;

$totales_clientes_js = implode(',',$totales_clientes_js);
?>
<script>
	$(document).ready(function(){
		$.jqplot.config.enablePlugins = true;
          	var line1 = <?php print '['.$totales_clientes_js.']'; ?>;
		  	var ticks = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
          	var plot1 = $.jqplot('chart2', [line1], {
			  	animate: !$.jqplot.use_excanvas,
              	title: '', 
              	seriesDefaults: { 
                	showMarker:false,
                	pointLabels: { show:true } 
              	},
			  	axes: {
						xaxis: {
							renderer: $.jqplot.CategoryAxisRenderer,
							ticks: ticks
						}
					},
			  	highlighter: { show: false }
          	});
	});
</script>

@endif

<script class="include" type="text/javascript" src="{{ asset('jqplot/jquery.jqplot.min.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.barRenderer.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.categoryAxisRenderer.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.pointLabels.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.dateAxisRenderer.js') }}"></script>

@stop