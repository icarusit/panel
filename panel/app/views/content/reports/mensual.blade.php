@extends('main')
@section('subpage')

<link class="include" rel="stylesheet" type="text/css" href="{{ asset('jqplot/jquery.jqplot.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('jqplot/syntaxhighlighter/styles/shCoreDefault.min.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('jqplot/syntaxhighlighter/styles/shThemejqPlot.min.css') }}" />

<h1>{{$title}}</h1>

@include('errors')

<style>
.th_left
{
	border-right:1px solid #dddddd !important;
	font-weight:bold;
}
</style>

<div class="row">
	<div class="col-md-6">
    	<a class="btn btn-primary" href="<?php print URL::to('reporte-anual/'.$anno.'/'.$tipo); ?>">
            Regresar
        </a>
    </div>
</div>

<div class="margin-30"></div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">{{$title}}</h3>
  	</div>
  	<div class="panel-body">
    	<div class="table-responsive">
        	@if(count($resumen) > 0)
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Perfil</th>
                            @foreach($semanas as $semana)
                            	<th>{{$semana['label']}}</th>
                            @endforeach
                        </tr>
                        @if($tipo == 'fecha_alta')
                        <tr>
                            <th>Nuevos Clientes</th>
                            @foreach($semanas as $semana)
                            	<th>{{$resumen_clientes->$semana['identificador']}}</th>
                            @endforeach
                        </tr>
                        @endif
                    </thead>
                    <tbody>
                    	<?php
							$totales = array();
						?>
                        @foreach($resumen as $key_rango => $rango)
                            <tr>
                                <td class="th_left">{{$rango->PERFIL}}</td>
                                @foreach($semanas as $key => $semana)
                                	<?php
										$estilo_celda = '';
										$actual = $rango->$semana['identificador'];
										if($key > 1)
										{
											$anterior = $rango->$semanas[$key - 1]['identificador'];
											$estilo_celda = 'style="background-color:'.color_celda($tipo,$anterior,$actual).'"';
										}
									?>
                                	<td {{$estilo_celda}}>{{$actual}}</td>
                                    <?php
										if(!isset($totales[$key]))
										{
											$totales[$key] = $rango->$semana['identificador'];
										}
										else
										{
											$totales[$key]+= $rango->$semana['identificador'];
										}
									?>
								@endforeach
                            </tr>
                        @endforeach      
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<th>Totales</td>
                            @foreach($totales as $total)
                        		<th>{{$total}}</th>
                            @endforeach
                        </tr>
                    </tfoot>
                </table>
            @else
                <p>No existen estadísticas para el mes seleccionado.</p>
            @endif
        </div>
    </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
    	<h3 class="panel-title">{{$title}}</h3>
  	</div>
  	<div class="panel-body">
    	<div class="row">
        	<div id="chart1" class="col-md-12" style="height:300px;"></div>
        </div>
    </div>
</div>
<?php
$totales_js = array();
foreach($totales as $total)
{
	$totales_js[] = $total;
}
$totales_js = implode(',',$totales_js);

$semanas_js = array();
foreach($semanas as $semana)
{
	$semanas_js[] = "'".$semana['label']."'";
}
$semanas_js = implode(',',$semanas_js);
?>
<script class="code" type="text/javascript">
        $(document).ready(function(){
			$.jqplot.config.enablePlugins = true;
          	var line1 = <?php print '['.$totales_js.']'; ?>;
		  	var ticks = <?php print '['.$semanas_js.']'; ?>;
          	var plot1 = $.jqplot('chart1', [line1], {
			  	animate: !$.jqplot.use_excanvas,
              	title: '', 
              	seriesDefaults: { 
                	showMarker:false,
                	pointLabels: { show:true } 
              	},
			  	axes: {
						xaxis: {
							renderer: $.jqplot.CategoryAxisRenderer,
							ticks: ticks
						}
					},
			  	highlighter: { show: false }
          	});
        });
</script>
<script class="include" type="text/javascript" src="{{ asset('jqplot/jquery.jqplot.min.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.barRenderer.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.categoryAxisRenderer.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.pointLabels.js') }}"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ asset('jqplot/plugins/jqplot.dateAxisRenderer.js') }}"></script>

@stop