<div class="row">
  <div class="col-md-10">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Horarios actualmente configurados</h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-pills schedules-days schedules-tabs">
          @foreach($weekDays as $weekDay => $text)
            <li{{ $weekDay == $todayWeekDay ? ' class="active"' : '' }}>
              <a href="#{{ $text[0] }}" data-weekday="{{ $weekDay }}" data-toggle="tab">{{ $text[1] }}</a>
            </li>
          @endforeach
        </ul>
        
        <select id="select_week_day" class="form-control form-control-huge
                schedules-days schedules-select-day schedules-select-day-mobile">
        @foreach($weekDays as $weekDay => $text)
          <option id="option-{{ $text[0] }}" name="option-{{ $text[0] }}"
                  value="{{ $weekDay }}"
                  @if($weekDay == $todayWeekDay) selected="selected" @endif>
                  {{ $text[1] }}
          </option>
        @endforeach            
        </select>
       
        <div class="tab-content">            
          @foreach($weekDays as $weekDayNum => $weekDay)
          <div class="tab-pane fade in {{ $weekDayNum === $todayWeekDay ? ' active ' : '' }}"
            id="{{ $weekDay[0] }}">
            @if(is_array($weekSchedules) && isset($weekSchedules[$weekDayNum]))             
              <p>Horarios para <strong>los {{ strtolower($weekDay[2]) }}</strong>:</p>

              <div class="table-responsive">
                <table class="table table-striped" id="table-schedules-{{ $weekDayNum }}">
                  <thead>
                    <tr>
                      <th width="15%">Desde</th>
                      <th width="15%">Hasta</th>                  
                      <th width="40%">Acción a realizar</th>
                      <th width="30%">Operaciones</th>
                  </thead>
                  <tbody>            
                    @foreach($weekSchedules[$weekDayNum] as $schedule)
                    {{ Form::open(array('action' => 'SchedulesController@save')) }}                    
                    <tr>
                      <td>
                        <span class="label-clock">
                          {{ $schedule->getFrom()->format('H:i') }}
                        </span>                        
                        @include('components.timepicker', array('name' => 'schedule_from', 'value' => $schedule->getFrom()->format('H:i'), 'class' => 'form-control-huge schedule-edit-input'))
                        
                        <input type="hidden" name="schedule_id" value="{{ $schedule->getID() }}">
                        <input type="hidden" name="schedule_week_day" value="{{ $weekDayNum }}">                        
                      </td>
                      <td>
                        <span class="label-clock">
                          {{ $schedule->getUntil()->format('H:i') }}
                        </span>
                        @include('components.timepicker', array('name' => 'schedule_until', 'value' => $schedule->getUntil()->format('H:i'), 'class' => 'form-control-huge schedule-edit-input'))                        
                      </td>
                      <td style="padding: 10px 0;">
                        <span class="schedule-summary">
                          {{ (new Action($schedule->getAction()))->parse() }}                          
                        </span>
                        
                        @include('components.actions.selector', array('action' => array('name' => 'action_data_' . $schedule->getID(), 'value' => $schedule->getAction(), 'class' => 'schedule-edit-action')))
                      </td>
                      <td>                        
                        <a href="#"
                           class="btn btn-primary schedule-edit"
                           title="Editar este horario">
                          <span class="glyphicon glyphicon-edit"></span>
                          Editar
                        </a>                        
                        
                        <input type="submit" 
                               class="btn btn-success schedule-edit-save"
                               title="Guardar horario editado"
                               value="Guardar">

                        <a href="#"
                           class="btn btn-primary schedule-edit-cancel"
                           title="Cancelar edición de horario">
                          <span class="glyphicon glyphicon-remove"></span>                          
                          Cancelar
                        </a>
                        
                        <a href="{{ URL::to('schedules/remove/' . $schedule->getID()) }}"
                           class="btn btn-primary schedule-remove"
                           title="Eliminar el horario">
                          <span class="glyphicon glyphicon-remove"></span>
                          Eliminar
                        </a>
                      </td>
                    </tr>                    
                    {{ Form::close() }}                    
                    @endforeach
                  </tbody>
                </table>
              </div>  

              @if($weekDayNum < 5)
              <a href="{{ URL::to('schedules/repeat/' . $weekDayNum) }}" 
                 class="btn btn-primary margin-15 repeat-schedules">
                <span class="glyphicon glyphicon-repeat"></span>
                <span>Repetir los horarios de los {{ $weekDay[2] }} de lunes a viernes</span>
              </a>
              @else
                @if($weekDayNum == 5)
                <a href="{{ URL::to('schedules/repeat/' . $weekDayNum) }}"
                   data-week-day="5"
                   class="btn btn-primary margin-15 repeat-weekend-days-schedules">
                  <span class="glyphicon glyphicon-repeat"></span>
                  <span>Repetir los horarios de los sábados para los domingos</span>
                </a>                  
                @else
                <a href="{{ URL::to('schedules/repeat/' . $weekDayNum) }}" 
                   data-week-day="6"                   
                   class="btn btn-primary margin-15 repeat-weekend-days-schedules">
                  <span class="glyphicon glyphicon-repeat"></span>
                  <span>Repetir los horarios de los domingos para los sábados</span>
                </a>                                  
                @endif
              @endif
            @else
            {{-------------------------}}
            {{-- NO HAY HORARIOS AÚN --}}
            {{-------------------------}}            
            <div class="media col-md-12 no-holidays">
              <span class="pull-left glyphicon glyphicon-time panel-icon"></span>
              <div class="media-body">
                <h4 class="media-heading">No tienes aún horarios configurados
                  para <strong>los {{ ucfirst($weekDay[2]) }}</strong></h4>
              </div>
            </div>
            @endif
          </div>
          @endforeach
        </div>

      </div>  
    </div>
  </div>
</div>
