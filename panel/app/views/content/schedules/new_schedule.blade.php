<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Agregar un nuevo horario</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          {{ Form::open(array(
            'action' => 'SchedulesController@save',
            'class'  => 'form-horizontal col-md-6'
          )) }}
          
           <div class="form-group">
             <label for="week_day" class="col-md-2 control-label">Días</label>
             <div class="col-md-4">
               <select class="form-control form-control-huge schedules-select-day"
                       id="select_week_day" 
                       name="schedule_week_day">
                @foreach($weekDays as $weekDayNum => $weekDay)                 
                <option value="{{ $weekDayNum }}"{{ $weekDayNum === $todayWeekDay ? ' selected' : '' }}>
                  {{ ucfirst($weekDay[2]) }}
                </option>                 
                @endforeach
               </select>
             </div>
           </div>

           <div class="form-group">
             <label for="url" class="col-md-2 control-label">Desde</label>
             <div class="col-md-4">
                @include('components.timepicker', array('name' => 'schedule_from', 'value' => '00:00'))
             </div>
           </div>

           <div class="form-group">
             <label for="url" class="col-md-2 control-label">Hasta</label>
             <div class="col-md-4">
                @include('components.timepicker', array('name' => 'schedule_until', 'value' => '00:00'))
             </div>
           </div>          

           <div class="form-group">
             <label for="url" class="col-md-2 control-label">Acción</label>
             <div class="col-md-10">
                @include('components.actions.selector')
             </div>
           </div>                              
           
           <input type="submit" class="btn btn-success pull-left" value="Guardar" /> 
         {{ Form::close() }}
        </div>
        
      </div>
    </div>
  </div>
</div>





