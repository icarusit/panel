@extends('main')

@section('subpage')
<h1>Mis horarios</h1>

@if(!Session::get('is_mobile'))
	@include('help', array('content' => 'schedules'))
@endif

@include('errors')

@if(is_array($weekSchedules))
  @include('content.schedules.list_schedules')
  @include('content.schedules.new_schedule')
@else
  @include('content.schedules.new_schedule')
  @include('content.schedules.list_schedules')
@endif

@include('components.actions.modals.include')

<script>
$(document).ready(function() {    
  var weekDays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
  var posWDay;

  if((posWDay = $.inArray(window.location.hash.substr(1), weekDays)) !== -1) {
    $('.nav-pills a[href=' + window.location.hash + ']').tab('show');
    $('.schedules-select-day').prop('selectedIndex', posWDay);
  }  

  $('.schedules-select-day').change(function() {
    var weekDay = $(this).val();

    $('.schedules-select-day').each(function(i, v) {    
      this.selectedIndex = weekDay;
    });

    if(weekDay >= 0 && weekDay <= 6) {
      $('.nav-pills a[data-weekday=' + weekDay + ']').tab('show');    
    }
  });

  $('.nav-pills a').click(function (e) {
    var $that = $(this);
    $('.schedules-select-day').each(function(i, v) {    
      this.selectedIndex = $that.data('weekday');
    });
    $(this).tab('show');
  });

  $('.repeat-schedules').click(function() {
    return confirm("Si ya tenías horarios configurados para otros días (salvo el sábado y el domingo), se eliminarán y se añadirán éstos a su vez. ¿Quieres continuar?");
  });

  $('.repeat-weekend-days-schedules').click(function() {
    var otherDay = $(this).data('week-day') == 5 ? 'domingos' : 'sábados';
    return confirm("Si ya tenías horarios configurados para los " + otherDay + ", se eliminarán y se añadirán éstos a su vez. ¿Quieres continuar?");
  });
  
  $('.schedule-remove').click(function() {
    return confirm("Se eliminará el horario. ¿Quieres continuar?");
  });
  
  $('.schedule-edit').click(function() {
    $(this).hide();    
    $(this).siblings('.schedule-remove').hide();    
    $(this).siblings('.schedule-edit-save').show();
    $(this).siblings('.schedule-edit-cancel').show().css('display', 'inline-block');  
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('label-clock')) {
          $(this).hide();
          $(this).siblings('.schedule-edit-input').show();
        } else if($(this).hasClass('schedule-summary')) {
          $(this).hide();
          $(this).siblings('.selector').show();
        }
      });
    });
    
    return false;
  });  
  
  $('.schedule-edit-cancel').click(function() {
    $(this).hide();
    $(this).siblings('.schedule-edit-save').hide();    
    $(this).siblings('.schedule-remove').show();
    $(this).siblings('.schedule-edit').show();        
    
    $(this).parent().siblings('td').each(function() {      
      $(this).children().each(function() {
        if($(this).hasClass('schedule-edit-input')) {
          $(this).hide();
          $(this).siblings('.label-clock').show();
        } else if($(this).hasClass('selector')) {
          $(this).hide();
          $(this).siblings('.schedule-summary').show();
        }
      });
    });
    
    return false;    
  });
});
</script>

@stop
