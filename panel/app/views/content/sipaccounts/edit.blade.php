@extends('main')

@section('subpage')
<h1>Editar cuenta SIP '{{ $sipAccount->defaultuser }}'</h1>

@include('errors')

{{ Form::open(array(
  'action' => 'SIPAccountsController@update',
  'class'  => 'form-horizontal margin-30'
)) }}

<input type="hidden" name="id_sip_account" value="{{ $sipAccount->id_serial }}">

<div class="form-group">
  <label for="name" id="label-name" class="col-sm-2 control-label">Nombre</label>
  <div class="col-sm-3">
    <input type="text" class="form-control" name="name" id="sip-edit-name" 
           value="{{ $sipAccount->defaultuser }}" />
  </div>
</div>

<div class="form-group">
  <label for="password" id="label-password" class="col-sm-2 control-label">Contraseña</label>
  <div class="col-sm-3">
    <input type="password" class="form-control" name="password" id="sip-edit-password-hidden" 
           value="{{ $sipAccount->secret }}" readonly />
    <input type="text" class="form-control" name="password" id="sip-edit-password"
           value="{{ $sipAccount->secret }}" />
    <a href="#" id="show-edit-password">Mostrar y editar contraseña</a>
  </div>
</div>

<div class="form-group">
  <label for="comments" id="label-comments" class="col-sm-2 control-label">Comentarios</label>
  <div class="col-sm-3">
    <textarea class="form-control" maxlength="255" name="comments">{{ $sipAccount->comments }}</textarea>
  </div>
</div>

<div class="form-group">
  <div class="col-sm-5">
    <input type="submit" class="btn btn-primary pull-right" value="Guardar" />  
  </div>
</div>

{{ Form::close() }}

<script>
$(document).ready(function() {
  $('#show-edit-password').click(function() {
    $('#sip-edit-password-hidden, #show-edit-password').hide();
    $('#sip-edit-password').show();
  });
});
</script>
@stop