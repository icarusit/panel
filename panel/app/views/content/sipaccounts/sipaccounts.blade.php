@extends('main')

@section('subpage')
<h1>Cuentas SIP</h1>

@include('help', array('content' => 'sipaccounts'))

<div class="row">
  <div class="col-md-6">
    <a class="btn btn-primary" href="{{ URL::to('change-plan') }}">
      <span class="glyphicon glyphicon-plus"></span> Añade/elimina cuentas SIP o modifica tus canales
    </a>    
  </div>
  <div class="col-md-4">
    <a class="btn btn-primary" href="#configurar">
      <span class="glyphicon glyphicon-info-sign"></span> Ayuda para configurar la cuenta SIP
    </a>    
  </div>
  <div class="col-md-2">
    	<a class="btn btn-primary" href="<?php print URL::to('groups'); ?>">
            Configurar grupos
        </a>
    </div>
</div>

<div class="margin-30"></div>

@if(!isset($sipAccounts) || count($sipAccounts) == 0)
<p>No tienes aún ninguna cuenta SIP contratada.</p>
@else
<div class="table-responsive">
  <table class="table table-hover table-striped" style="width: auto;">
    <thead>
      <tr>
        <th>
          <a href="{{ URL::to('sip-accounts')}}?sort_by=status&sort={{ $sort === 'asc' ? 'desc' : 'asc' }}" title="Ordenar por estado">
            <i class="fa fa-sort"></i>
          </a>
        </th>        
        @if(Session::get('number')->getType() == 'pbx')
        <th>Ext</th>
        @endif
        <th>Cuenta</th>
        <th>Canales</th>
        <th>Registro</th>
        <th>User-agent</th>
        <th>Dirección IP</th>
        <th>Latencia</th>
        <th>CallerID</th>
        <th>Softphone</th>        
        <th>Comentario</th>
      </tr>
    </thead>
    <tbody data-provides="rowlink">
      @foreach($sipAccounts as $sipAccount)
      <tr>
        <td>
          @if(trim($sipAccount->ipaddr) != '' && $sipAccount->ipaddr != '(null)')
          <img src="{{ asset('img/led-on.png') }}" title="La cuenta SIP está registrada">
          @else 
          <img src="{{ asset('img/led-off.png') }}" title="La cuenta SIP no está registrada">
          @endif
        </td> 
                   
        @if(Session::get('number')->getType() == 'pbx')
        <?php
        // APAÑO ALERT!!!
        // Quitamos de la extensión el id_cliente
        $idUser = Session::get('user')->getID();
        $extension = (int)substr($sipAccount->extension, strlen($idUser), strlen($sipAccount->extension));
        ?>
        <td><span class="label label-default">{{ $extension }}</span></td>
        @endif

        <td><a href="{{ URL::to('sip-accounts/' . $sipAccount->id_serial) }}">{{ $sipAccount->defaultuser }}</a></td>
        <?php 
        $lastSeen = "";
        if($sipAccount->regseconds > 0) {
          $dt = DateTime::createFromFormat('U', $sipAccount->regseconds); 
          $lastSeen = $dt->format('d-m-Y H:i:s');
        }
        ?>
        <td style="text-align: center;">{{ $sipAccount->{'call-limit'} }}</td>
        <td>{{ $lastSeen }}</td>
        <td>{{ $sipAccount->useragent }}</td>

        @if(trim($sipAccount->ipaddr) != '' && $sipAccount->ipaddr != '(null)')
        <td>{{ $sipAccount->ipaddr }}</td>
        <td>~ {{ $sipAccount->lastms }} ms</td>
        @else
        <td>0.0.0.0</td>
        <td></td>
        @endif
        <td><a href="{{ URL::to('number/' . $sipAccount->callerid) }}">{{ $sipAccount->callerid }}</a></td>        
        <td>
          <a href="https://www.zoiper.com/en/page/81cb7377842f6306ec688b4105efe6a9?u={{ $sipAccount->defaultuser }}&h=sip.fmeuropa.com&p={{ $sipAccount->secret }}&o=&t=&x=&a=&tr=" 
             title="Descargar Zoiper preconfigurado">
            <img src="{{ asset('img/voip_clients/zoiper.jpg') }}"
                 alt="Zoiper"
                 title="Descargar Zoiper preconfigurado">
          </a>          
        </td>
        <td>{{ $sipAccount->comments }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{{ $sipAccounts->links() }}

<div class="row">
  	<div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a id="configurar"></a>
                Ayuda para configurar la cuenta SIP
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <tbody>
                    <?php
						$enlaces_ayuda = Config::get('ayudas.configurarip');
						foreach($enlaces_ayuda as $enlace)
						{
							print '<tr>';
								print '<td>';
									print '<a href="'.$enlace['url'].'" target="_blank">';
										print $enlace['title'];
									print '</a>';
								print '</td>';
							print '</tr>';
						}						
					?>                 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endif

@stop
