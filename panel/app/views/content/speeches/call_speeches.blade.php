<div class="row">
  <div class="col-md-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configurar locuciones para 
          <strong>{{ Session::get('number') }}</strong>
        </h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'url'   => 'speeches/update',
          'class' => 'form-horizontal'
        )) }}
        <div class="form-group">
          <label for="select-welcome-speech" class="col-md-4 control-label">
            <a href="#" class="label-with-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true"
               title="La locución de bienvenida la escucha la persona que te llama justo antes de ejecutar los desvíos.">
              Locución de bienvenida
            </a>
          </label>

          <div class="col-md-8">
            <select class="form-control" name="select-welcome-speech" id="select-welcome-speech" 
                    <?php if(count($general_speeches) == 0):
                      $title = "Sube primero alguna locución de tipo general para poderla elegir como locución de bienvenida";
                    ?>
                    title="{{ $title }}" disabled
                    <?php endif; ?>>
              <option value="disabled">Desactivada</option>              

              @if($general_speeches !== FALSE)
              @foreach($general_speeches as $speech)
              <option value="{{ $speech->id_speech }}"
                      @if($speech->id_speech == $welcomeSpeech) selected @endif>
                {{ $speech->public_name }}
              </option>
              @endforeach
              @endif              
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="select-queue-speech" class="col-md-4 control-label">
            <a href="#" class="label-with-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true"
               title="La locución de espera la escucha la persona que te llama cuando todos tus desvíos (teléfonos y/o cuentas SIP) comunican o no están disponible.<br><br>Sonará la locución junto a una música de espera durante un intervalo de tiempo hasta que alguno de tus desvíos atienda la llamada.">
              Locución de espera
            </a>
          </label>

          <div class="col-md-8">
            <select class="form-control" name="select-queue-speech" id="select-queue-speech"
                    <?php if(count($general_speeches) == 0):
                      $title = "Sube primero alguna locución de tipo general para poderla elegir como locución de espera";
                    ?>
                    title="{{ $title }}" disabled
                    <?php endif; ?>>                    
              <option value="disabled">Desactivada</option>
              
              @if($general_speeches !== FALSE)
              @foreach($general_speeches as $speech)
              <option value="{{ $speech->id_speech }}"
                      @if($speech->id_speech == $queueSpeech) selected @endif>
                {{ $speech->public_name }}
              </option>
              @endforeach
              @endif
            </select>
          </div>
        </div>        

        <div id="queue-speech-options" {{ $queueSpeech === null ? 'style="display: none;"' : ''}}>
          <div class="checkbox checkbox-subitem">
            <label 
              class="label-subitem {{ !$hasVoicemail ? 'disabled' : ''}}"
              @if(!$hasVoicemail)
              title="Para poder activar este servicio debes tener el servicio de buzón de voz general activado. Ve a 'Buzón de voz' en el menú de la izquierda y en la pestaña de 'Configuración' actívalo'"
              @endif
              >
              <input type="checkbox"{{ !$hasVoicemail ? ' disabled="disabled"' : ''}}
              name="check_minutes_after_queue"
              id="check_minutes_after_queue"
              value="true"
              @if(!$hasVoicemail)
              title="Para poder activar este servicio debes tener el servicio de buzón de voz general activado. Ve a 'Buzón de voz' en el menú de la izquierda y en la pestaña de 'Configuración' actívalo"
              @endif                 
              @if($afterQueueVoicemailActive)
              checked
              @endif
              > Pasados 
              <input type="text" class="form-control form-control-inline" 
              name="minutes_after_queue"
              id="minutes_after_queue"
              value="{{ $minutesAfterQueueVoicemail > 0 ? $minutesAfterQueueVoicemail : 3 }}"{{ !$hasVoicemail ? ' disabled="disabled"' : ''}}> 
              minutos de <strong>espera</strong>, desviar la llamada al <a href="{{ URL::to('voicemail') }}">buzón de voz general</a>
            </label>
          </div> 
             
          <div class="checkbox checkbox-subitem">
            <label>
              <input type="checkbox"
              name="check_queue_speech_mode"
              id="check_queue_speech_mode"
              value="true"
              @if($queueSpeechMode)
              checked
              @endif
              >
              Marcar para reproducir la locución de espera mientras se intenta la 
              llamada. Desmarcar para esperar varios tonos hasta reproducir la locución
              de espera.
            </label>                  
          </div>          
        </div>      
        
        <div class="form-group">
          <label for="select-operator-speech" class="col-md-4 control-label">
            <a href="#" class="label-with-tooltip" data-toggle="tooltip" data-placement="bottom" data-html="true"
               title="La locución al operador la escucha <u>únicamente</u> la persona que contesta la llamada en uno de tus desvíos o cuenta SIP.">
              Locución al operador
            </a>
          </label>

          <div class="col-md-8">
            <select class="form-control" name="select-operator-speech" id="select-operator-speech"
                    <?php if(count($general_speeches) == 0):
                      $title = "Sube primero alguna locución de tipo general para poderla elegir como locución al operador";
                    ?>
                    title="{{ $title }}" disabled
                    <?php endif; ?>>                    
              <option value="disabled">Desactivada</option>
              
              @if($general_speeches !== FALSE)
              @foreach($general_speeches as $speech)
              <option value="{{ $speech->id_speech }}"
                      @if($speech->id_speech == $operatorSpeech) selected @endif>
                {{ $speech->public_name }}
              </option>
              @endforeach
              @endif
            </select>
          </div>
        </div>        
        
        <input type="submit" class="btn btn-primary pull-right" value="Actualizar" />
        
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
