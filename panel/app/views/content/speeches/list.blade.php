<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
          Locuciones disponibles para todos tus números
          <a class="inline-link" href="#new-speech">
            <span class="fa fa-cloud-upload"></span>
            Nueva locución
          </a>
        </h3>
      </div>
      <div class="panel-body">
        <div class="row" style="margin-bottom: 10px;">
          <div class="form-group">
            
          </div>        
        </div>
        @if($speeches === FALSE)
        <p>No tienes aún locuciones subidas al sistema
        
        </p>
        @else
        <div class="table-responsive">
          <table class="table table-hover" style="width: auto;">
            <thead>        
              <tr>
                <th width="80"></th>      
                <th>Título</th>
                <th>Fichero</th>
                <th>Formato</th>
                
                <th>Tamaño (bytes)</th>
                <th>Añadida</th>
                <th>Procesada</th>
              </tr>
            </thead>
            <tbody data-provides="rowlink">
              @foreach($speeches as $speech)              
              <tr @if($speech->sample) style="background-color: #f3f3f3 !important;" @endif>
                <td>
                  <a href="{{ URL::to('speeches/download/' . $speech->id_speech) }}"
                     title="Escuchar la locución"
                     class="speech-play">
                    <span class="glyphicon glyphicon-play"></span>
                  </a>
                  @if(!$speech->sample)
                  <a href="{{ URL::to('speeches/download/' . $speech->id_speech) }}"
                     title="Descargar la locución">
                    <span class="glyphicon glyphicon-download-alt"></span>
                  </a>
                  <a href="{{ URL::to('speeches/remove/' . $speech->id_speech) }}" 
                     class="speech-remove"
                     title="Eliminar la locución">
                    <span class="glyphicon glyphicon-remove"></span>
                  </a>
                  @endif
                </td>
                <td>{{ Str::limit($speech->public_name, 20) }}</td>
                <td>{{ Str::limit($speech->original_filename, 20) }}</td>
                <td>{{ $speech->original_mimetype }}</td>
                
                <td>{{ $speech->size }}</td>
                <td>{{ $speech->date_added }}</td>
                <td>{{ $speech->converted ? "Sí" : "No" }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      @endif  
      </div>
    </div>
  </div>
</div>