<div class="alert alert-danger" id="speech-recording-audio-error">
  <p>Parece que hay un problema con la entrada de audio. ¿Tienes el micrófono conectado?
  ¿No has dado permisos en el navegador para que accedamos al micrófono?</p>
  <p><a href="{{ URL::to('speeches') }}">Refresca la página</a>.</p>
</div>        
<div class="alert alert-success" id="speech-recording-uploaded-alert">
  <p>La locución grabada ha sido correctamente subida.</p>
  <p><a href="{{ URL::to('speeches') }}">Refresca la página</a> para verla
  en la lista de locuciones subidas.</p>
</div>
<div class="alert alert-danger" id="speech-recording-not-uploaded-alert">
  Ha habido un error subiendo la locución grabada
</div>        

<form class="form-horizontal">
 
  <div class="form-group">
    <label for="speech_recorded_name" class="col-md-4 control-label">Título</label>

    <div class="col-md-8">    
      <input class="form-control" type="text" name="speech_recorded_name" id="speech_recorded_name" />
    </div>
  </div>
  <div class="form-group">
    <label for="recorded_speech" class="col-md-4 control-label">Grabación</label>

    <div class="col-md-8">
      @include('components.recorder')
    </div>
  </div>

  <div class="alert alert-warning" id="speech-recording-uploading-alert">
    Estamos subiendo tu grabación. Por favor, no refresques la página. 
    <img src="{{ asset('img/loading-16.gif') }}">
  </div>

  <button type="button" id="upload-recording" class="btn btn-primary disabled pull-right">Subir locución</button>                 
</form>
