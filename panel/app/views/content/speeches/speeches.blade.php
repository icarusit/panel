@extends('main')
@section('subpage')

<h1>Locuciones</h1>

@include('help', array('content' => 'speeches'))
@include('errors')

{{-- Configurar locución espera y bienvenida --}}
@include('content.speeches.call_speeches')

{{-- Relación de locuciones disponibles --}}
@include('content.speeches.list')

{{-- Nueva locución (sólo si no móvil) --}}
@if(!$isMobile)
<div id="new-speech" class="row">
  <div class="col-md-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Nueva locución</h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li role="presentation" class="active">
            <a class="speeches-tab" href="#upload">
              <span class="fa fa-cloud-upload"></span> Subir desde el ordenador
            </a>
          </li>
          <li role="presentation">
            <a class="speeches-tab" href="#record">
              <span class="fa fa-microphone"></span> Grabar con micrófono
            </a>
          </li>
          <li role="presentation">
            <a class="speeches-tab" href="#text2speech">
              <span class="fa fa-file-text"></span> Texto a voz <span class="label label-success" style="margin-left: 5px;">NUEVO</span>
            </a>
          </li>          
        </ul>
        
        <div class="tab-content tab-real-tab">
          <div role="tabpanel" class="tab-pane active" id="upload">
            @include('content.speeches.upload')
            <div class="clearfix"></div>
          </div>
          <div role="tabpanel" class="tab-pane" id="record">
            @include('content.speeches.record')     
            <div class="clearfix"></div>
          </div>
          <div role="tabpanel" class="tab-pane" id="text2speech">
            @include('content.speeches.text2speech')     
            <div class="clearfix"></div>
          </div>          
        </div>        
      </div>
    </div>
  </div>
</div>
@endif

<script type="text/javascript">
window.AudioContext = window.AudioContext || window.webkitAudioContext;
window.URL = window.URL || window.webkitURL;

var userName = "{{ Session::get('user')->getUsername() }}";
var audioContext = new AudioContext();
var audioRecorder;
var audioBlob;
var counterInterval;
var speechDuration;

function playAudio(audioData) {
  var reader = new FileReader();
  reader.onload = function(event) {  
    console.log(event.target.result);
    var raudio = $('#speech-recording-audio')[0];
    console.log(raudio);
    raudio.src = event.target.result;
    raudio.play();
  };
  reader.readAsDataURL(audioData);  
}

function uploadAudio(audioData) {
  if($.trim($('#speech_recorded_name').val()) !== '' &&
    $.trim($('#speech_recorded_name').val()) !== '') {
    $('#upload-recording').addClass('disabled');
    $('#speech-recording-uploading-alert').show();
    
    var reader = new FileReader();
    reader.onload = function(event) {
      var fd = new FormData();
      var speechName = encodeURIComponent();
      var speechFileName = encodeURIComponent('recorded-speech-' + userName + '-' + new Date().getTime() + '.wav');

      fd.append('file_name', speechFileName);
      fd.append('file_content', event.target.result);
      fd.append('speech_name', $('#speech_recorded_name').val());
      //fd.append('speech_type', $('#speech_recorded_type').val());    

      $.ajax({
        type: 'POST',
        url: '{{ URL::to('speeches/upload-recording') }}',
        data: fd,
        processData: false,
        contentType: false
      }).done(function(data) {
        $('#upload-recording').removeClass('disabled');
        $('#speech-recording-uploading-alert').hide();        
        
        if(data === "true") {
          $('#speech-recording-not-uploaded-alert').hide();
          $('#speech-recording-uploaded-alert').show();
        } else {       
          $('#speech-recording-uploaded-alert').hide();
          $('#speech-recording-not-uploaded-alert').show();
        }
      });
    };      
    reader.readAsDataURL(audioData);
  }
}

function convertToMono(input) {
  var splitter = audioContext.createChannelSplitter(2);
  var merger = audioContext.createChannelMerger(2);

  input.connect(splitter);
  splitter.connect(merger, 0, 0);
  splitter.connect(merger, 0, 1);
  return merger;
}

function gotStream(stream) {
  inputPoint = audioContext.createGain();

  realAudioInput = audioContext.createMediaStreamSource(stream);
  audioInput = realAudioInput;
  audioInput.connect(inputPoint);
  // audioInput = convertToMono(inputPoint);

  audioRecorder = new Recorder(inputPoint);

  zeroGain = audioContext.createGain();
  zeroGain.gain.value = 0.0;
  inputPoint.connect(zeroGain);
  zeroGain.connect(audioContext.destination);
}

function initAudio() {
  if(!navigator.getUserMedia)
      navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
  if(!navigator.cancelAnimationFrame)
      navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
  if(!navigator.requestAnimationFrame)
      navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

  navigator.getUserMedia({audio:true}, gotStream, function(e) {    
    $('#speech-recording-audio-error').show();   
    $('.speech-recording-record')
      .addClass('disabled')
      .click(function(e) { 
        e.preventDefault();
      });
    console.log(e);
  });
}

$(document).ready(function() {
  initAudio();  
  $('[data-toggle="tooltip"]').tooltip();
  
  $('.speeches-tab').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })  
  
  $('#upload-recording').click(function() {
    if(typeof audioBlob === 'object') {
      uploadAudio(audioBlob);
    }
  });
  
  $('.speech-recording-stop').click(function() {
    clearInterval(counterInterval);    
    var $that = $(this);
    audioRecorder.stop();
    $('a.speech-recording-record').parent().removeClass('rounded-right');
    audioRecorder.exportWAV(function(blob) {
      audioBlob = blob;
      Recorder.setupDownload(blob, 'recorded-speech-' + userName + '-' + new Date().getTime() + '.wav');
      
      $('#speech-recording-audio').prop('src', window.URL.createObjectURL(blob));
      $('#upload-recording').removeClass('disabled');
      $('.speech-recording-play').parent().removeClass('disabled');
      $that.hide();
      $('#speech-recording-record-text').html('Grabar de nuevo');
      $('.speech-recording-record, .speech-recording-play, .speech-recording-download').css('display', 'block');      
    });
    return false;
  });

  $('.speech-recording-record').click(function() {    
    if (!audioRecorder)
        return;
    $('#speech-recording-counter-text').html('00:00');
    $('.speech-recording-download').css('display', 'none');          
    $(this).hide();
    $('.speech-recording-stop').css('display', 'block');
    
    audioRecorder.clear();
    audioRecorder.record();  

    speechDuration = 0;
    counterInterval = setInterval(function() {
      ++speechDuration;
      m = Math.floor(speechDuration / 60);
      s = speechDuration - m * 60;
      $('#speech-recording-counter-text').html((m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s));
    }, 1000);
    
    return false;    
  });

  $('.speech-play').click(function() {
    jBeep($(this).prop('href'));
    return false;
  });

  $('.speech-recording-play').click(function() {
    if(!$(this).parent().hasClass('disabled')) {
      //var $audio = $('#speech-recording-audio')[0]
      //console.log($('#speech-recording-audio')[0]);
      playAudio(audioBlob);
    }
  });

  $('.speech-remove').click(function() {
    return confirm("Se eliminará la locución del sistema. ¿Deseas continuar?");
  }); 

  $('#select-speech-type').change(function() {
    window.location.href = '{{ URL::to('speeches') }}?type=' + $(this).val();
  });  
  
  $('#select-queue-speech').change(function() {
    if('disabled' === $(this).val()) {
      $('#queue-speech-options').hide();
    } else {
      $('#queue-speech-options').show();
    }
  });

  $('#minutes_after_queue').keyup(function() {
    $('#check_minutes_after_queue').prop('checked', true);
  });
});
</script>

<script src="{{ asset('js/recorder.js') }}"></script>
@stop

