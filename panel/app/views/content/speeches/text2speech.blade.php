{{ Form::open(array(
  'url'   => 'speeches/upload-text2speech',
  'class' => 'form-horizontal'
)) }}        


<div class="form-group">
  <label for="speech_name" class="col-md-4 control-label">Título</label>

  <div class="col-md-8">    
    <input class="form-control" type="text" name="speech_name" id="speech_name" />
  </div>
</div>
<div class="form-group">
  <label for="speech_gender" class="col-md-4 control-label">Sexo</label>

  <div class="col-md-8">    
    <select class="form-control" name="speech_gender" id="speech_gender">
      <option value="male">Masculino</option>
      <option value="female">Femenino</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label for="text2speech-text" class="col-md-4 control-label">Texto</label>

  <div class="col-md-8">
    <textarea id="text2speech-text" maxlength="250" name="text2speech-text" rows="6" class="form-control" style="resize: none;"></textarea>
  </div>
</div>

<input type="submit" class="btn btn-primary pull-right" value="Generar y guardar locución" />                  
<input data-loading-text="Espera por favor" id="text2speech-play" type="button" class="btn btn-primary pull-right" style="margin-right: 10px;" value="Escuchar" />                  
{{ Form::close() }}

<script>
var Sound = (function () {
  var df = document.createDocumentFragment();
  
  return function Sound(src) {
    var snd = new Audio(src);
    df.appendChild(snd);
    snd.addEventListener('ended', function () {df.removeChild(snd);});
    snd.play();
    return snd;
  }
}());

$(document).ready(function() {
  $('#text2speech-play').click(function() {
    var $btnPlay = $(this).button('loading');

    var text   = $('#text2speech-text').val();
    var gender = $('#speech_gender').val();

    $.post('{{ URL::to('speeches/text2speech_play') }}', {
      text: text,
      gender: gender
    }, function(data) {
      var snd = Sound("data:audio/wav;base64," + data);
      $btnPlay.button('reset');
    });
  });  
});
</script>
