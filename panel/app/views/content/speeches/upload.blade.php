{{ Form::open(array(
  'url'   => 'speeches/upload',
  'files' => true,
  'class' => 'form-horizontal'
)) }}        

<div class="form-group">
  <label for="speech_name" class="col-md-4 control-label">Título</label>

  <div class="col-md-8">    
    <input class="form-control" type="text" name="speech_name" id="speech_name" />
  </div>
</div>
<div class="form-group">
  <label for="speech" class="col-md-4 control-label">Fichero (<a href="#">formatos</a>)</label>

  <div class="col-md-8">
    <input class="form-control" type="file" name="speech" id="speech" />
  </div>
</div>

<input type="submit" class="btn btn-primary pull-right" value="Subir locución" />                  

{{ Form::close() }}