{{ Form::open(array(
    'action' => 'StatsController@filter',
    'id'     => 'calls_filter',
    'name'   => 'calls_filter'
)) }}
  <div class="panel panel-default">
    <div class="panel-heading">
      <a href="#" id="stats-filter-toggle">
        <i id="stats-filter-toggle-icon" class="fa fa-plus"></i>
        <span>Buscar</span>
      </a>
      <a class="inline-link" href="{{ count($calls) === 0 ? '' : URL::to('stats/export') }}">
        <span class="glyphicon glyphicon-export"></span>&nbsp;
        Descargar para Excel
      </a>
      <a class="inline-link">
      	DATOS DE ESTA BÚSQUEDA: Total minutos: {{ $totales['total_minutos'] }}
      </a>
      <a class="inline-link">
      	Coste total:{{ $totales['total_costo'] }}
      </a>     
    </div>
    <div id="stats-filter-body" class="panel-body" style="display: none;">
      <div class="row">
        <div class="col-sm-2">
          <div class="form-group">
            <label for="inoutbound">Llamadas:</label>
            <select id="inoutbound" name="inoutbound" class="form-control">
              <option value="inbound"@if($inoutbound === 'inbound') selected @endif>Entrantes</option>
              <option value="outbound"@if($inoutbound === 'outbound') selected @endif>Salientes</option>
            </select>
          </div>
        </div>           
        <div class="col-sm-2">
          <div class="form-group">
            <label for="caller">Origen:</label>
            <select id="select-caller" class="form-control" name="caller" @if($inoutbound === 'inbound') style="display: none;" disabled @endif>
              @if(isset($selectedNumber) && $selectedNumber === NULL)
              <option value="" disabled selected>Elige un origen</option>
              @endif              
              @foreach($userNumbers as $number)
              <option value="{{ $number->numero_visible }}"{{ (isset($selectedNumber) && $selectedNumber == $number->numero_visible) ? ' selected' : '' }}>{{ $number->numero_visible }}</option>
              @endforeach
            </select>
            <input id="text-caller" name="caller" type="text" class="form-control" @if($inoutbound === 'outbound') style="display: none;" disabled @endif>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label for="called">Destino:</label>
            <select id="select-called" class="form-control" name="called" @if($inoutbound === 'outbound') style="display: none;" disabled @endif>
              @if(isset($selectedNumber) && $selectedNumber === NULL)
              <option value="" disabled selected>Elige un destino</option>              
              @endif
              @if(isset($selectedNumber) && $selectedNumber === '')
              <option value="">TODOS</option>              
              @endif
              
              	@foreach($userNumbers as $number)              
              		<option value="{{ $number->numero_visible }}"{{ (isset($selectedNumber) && $selectedNumber == $number->numero_visible) ? ' selected' : '' }}>{{ $number->numero_visible }}</option>
              	@endforeach
            </select>
            <input id="text-called" name="called" type="text" class="form-control" @if($inoutbound === 'inbound') style="display: none;" disabled @endif>            
          </div>
        </div>        
        <div class="col-sm-2">
          <div class="form-group">
            <label for="start">Inicio:</label>
            <input type="text" class="form-control date-picker" id="start" name="start"  readonly>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <label for="caller">Fin:</label>
            <input type="text" class="form-control date-picker" id="end" name="end" readonly>
          </div>
        </div>
        <div class="col-sm-2">
          <input type="submit" class="btn btn-primary stats-btn-search" value="Buscar">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <label for="current_filters">Filtros de búsqueda aplicados:</label> 
          @if(empty($filters))
          <p>No hay filtros aplicados aún</p>          
          @else
          <ul class="stats-filters">
            @foreach($filters as $filter => $values)
              @foreach($values as $value)
              <li data-filter="{{ $filter }}" data-value="{{ $value }}">
                {{ $filter_public[$filter] }} <strong>{{ $value }}</strong>
                <a href="{{ URL::to('stats/filter/remove/?' . $filter . '=' . $value) }}"
                   class="stats-filters-filter-remove"
                   title="Eliminar este filtro">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>
              </li>              
              @endforeach
            @endforeach
          </ul>
          @endif
        </div>
      </div>
    </div>
    @if(count($calls) === 0)
    <div class="media col-md-12">
      <span class="pull-left fa fa-frown-o panel-icon"></span>
      <div class="media-body">
        <h4 class="media-heading">No hay llamadas que mostrar.</h4>
        <p>Prueba con otros criterios de búsqueda.</p>
      </div>
    </div>      
    @else    
    <div class="table-responsive">
      <table class="table table-hover table-striped" id="table-received-calls">
        <thead>
          <tr>
            <th>Comienzo</th>        
            <th>Origen</th>
            <th>Destino</th>
            <th>Duración</th>
            <th>Final</th>
            @if($inoutbound === 'inbound')
            <th>Entrante&nbsp;(€)</th>
            @else
            <th>Saliente&nbsp;(€)</th>
            @endif
            <th>SIP</th>            
          </tr>
        </thead>
        <tbody>
          @foreach($calls as $call)
          
          <?php          
          $cost_in = $call->cost_in;                    
          $cost_in_css_rule = 'zero';          
          if($cost_in < 0)  { $cost_in_css_rule = 'plus'; }
          if($cost_in > 0)  { $cost_in_css_rule = 'minus'; }                   
          $cost_in = number_format($call->cost_in, 4, ',', '.');                    
          
          $cost_out = $call->cost_out;
          $cost_out_css_rule = 'zero';
          if($cost_out > 0) { $cost_out_css_rule = 'minus'; }                   
          $cost_out = number_format($call->cost_out, 4, ',', '.');          
          ?>
          
          <tr>
            <td>{{ (new DateTime($call->start))->format('d/m/Y H:i:s') }}</td>
            <td class="stats-cell-caller">{{ $call->ContactoCaller() }}</td>
            <td class="stats-cell-called">{{ $call->ContactoCalled() }}</td>
            <td>{{ gmdate('H:i:s', $call->duration) }}</td>
            <td>{{ (new DateTime($call->end))->format('d/m/Y H:i:s') }}</td>
            @if($inoutbound === 'inbound')
            <td class="stats-cell-cost-{{ $cost_in_css_rule }}">{{ $cost_in }}</td>
            <td class="stats-cell-called">{{ $call->who_picked_up }}</td>            
            @else
            <td class="stats-cell-cost-{{ $cost_out_css_rule }}">{{ $cost_out }}</td>
            <td class="stats-cell-called">{{ $call->who_called }}</td>
            @endif
          </tr>
          @endforeach
        </tbody>      
      </table>
      @endif      
    </div>  
  </div>
{{ Form::close() }}

{{ $calls->links() }}
