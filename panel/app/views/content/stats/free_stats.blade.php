@extends('main')

@section('additional-css')

@stop

@section('additional-js')

@stop

@section('subpage')
<h1><i class="fa fa-bar-chart-o"></i> Estadísticas</h1>

@include('help', array('content' => 'stats'))


{{ Form::open(array(
    'action' => 'FreeStatsController@filter',
    'id'     => 'calls_filter',
    'name'   => 'calls_filter'
)) }}
<div class="panel panel-default">
	<div class="panel-heading">
      	<a href="#" id="stats-filter-toggle">
        	<i id="stats-filter-toggle-icon" class="fa fa-plus"></i>
        	<span>Buscar</span>
      	</a>
      	<a class="inline-link" href="{{ count($calls) === 0 ? '' : URL::to('free-stats/export') }}">
        	<span class="glyphicon glyphicon-export"></span>&nbsp;
        	Descargar para Excel
      	</a>
      	<a class="inline-link">
      		DATOS DE ESTA BÚSQUEDA: Total minutos: {{ $totales['total_minutos'] }}
      	</a>
      	<a class="inline-link">
      		Coste total:{{ $totales['total_costo'] }}
      	</a>     
	</div>
	<div id="stats-filter-body" class="panel-body" style="display: none;">
      	<div class="row">
      
        	<div class="col-sm-2">
          		<div class="form-group">
            		<label for="called">Destino:</label>
            		<select id="select-called" class="form-control" name="called">
            			<option value="">TODOS</option>
              
              				@foreach($userNumbersDest as $number)              
              					<option value="{{ $number->dst }}" <?php print (isset($filters['called']) && $filters['called'] == $number->dst) ? ' selected' : ''; ?>>{{ $number->dst }}</option>
              				@endforeach
            		</select>                       
          		</div>
        	</div>        
        	<div class="col-sm-2">
          		<div class="form-group">
            		<label for="start">Inicio:</label>
            		<input type="text" class="form-control date-picker" id="start" name="start"  value="{{ isset($filters['start'])?$filters['start']:'' }}" readonly>
          		</div>
        	</div>
        	<div class="col-sm-2">
          		<div class="form-group">
            		<label for="caller">Fin:</label>
            		<input type="text" class="form-control date-picker" id="end" name="end" value="{{ isset($filters['end'])?$filters['end']:'' }}" readonly>
          		</div>
        	</div>
        	<div class="col-sm-2">
          		<input type="submit" class="btn btn-primary stats-btn-search" value="Buscar">
        	</div>
      	</div>
      
	</div>
    @if(count($calls) === 0)
    <div class="media col-md-12">
      	<span class="pull-left fa fa-frown-o panel-icon"></span>
      	<div class="media-body">
        	<h4 class="media-heading">No hay llamadas que mostrar.</h4>
        	<p>Prueba con otros criterios de búsqueda.</p>
      	</div>
    </div>      
    @else    
    <div class="table-responsive">
      	<table class="table table-hover table-striped" id="table-received-calls">
        	<thead>
          		<tr>
            		<th>Comienzo</th>        
            		<th>Origen</th>
            		<th>Destino</th>
           			<th>Duración</th>
            		<th>Final</th>
            		<th>Saliente&nbsp;(€)</th>          
          		</tr>
        	</thead>
        	<tbody>
          	@foreach($calls as $call)
          
			<?php             
              $cost_out = $call->credit_before - $call->credit_after;
              $cost_out_css_rule = 'zero';
              if($cost_out > 0)
			  {
				  $cost_out_css_rule = 'minus';
			  }                   
              $cost_out = number_format($cost_out, 4, ',', '.');          
			?>
          
                <tr>
                    <td>{{ (new DateTime($call->start))->format('d/m/Y H:i:s') }}</td>
                    <td class="stats-cell-caller">{{ $call->CuentaSip() }}</td>
                    <td class="stats-cell-called">{{ $call->ContactoCalled() }}</td>
                    <td>{{ gmdate('H:i:s', $call->duration) }}</td>
                    <td>{{ (new DateTime($call->end))->format('d/m/Y H:i:s') }}</td>   
                    <td class="stats-cell-cost-{{ $cost_out_css_rule }}">{{ $cost_out }}</td>    
                </tr>
          	@endforeach
			</tbody>      
		</table>
      	@endif      
    </div>  
</div>
{{ Form::close() }}

<script type="text/javascript">
$(document).ready(function() { 

  var datePickerOpts = {
    format: 'dd/mm/yyyy',
    weekStart: 1,
    autoclose: true,
    language: 'es'
  };
    
  var $start = $('#start').datepicker(datePickerOpts).data('datepicker');
  var $end = $('#end').datepicker(datePickerOpts).data('datepicker');  
  
  $('#stats-filter-toggle').click(function() {
    var $filters = $('#stats-filter-body');    
    var shown = $filters.css('display') === 'block' ? true : false;
    var $icon = $('#stats-filter-toggle-icon');
    
    if(shown) { 
      $filters.hide(300, 'linear');
      $icon.removeClass('fa-minus');
      $icon.addClass('fa-plus');
    }
    else {
      $filters.show(300, 'linear');
      $icon.removeClass('fa-plus');
      $icon.addClass('fa-minus');      
    }
  });   

});
</script>
@stop
