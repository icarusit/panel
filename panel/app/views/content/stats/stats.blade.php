@extends('main')

@section('additional-css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop

@section('additional-js')
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script src="{{ asset('js/cookies.min.js') }}?{{ filemtime(public_path() . '/js/cookies.min.js') }}"></script>
@stop

@section('subpage')
<h1><i class="fa fa-bar-chart-o"></i> Estadísticas</h1>

@include('help', array('content' => 'stats'))

@if(!$isMobile)
  <ul class="nav nav-tabs">
    <li role="presentation" class="active">
      <a class="stats-tab" id="stats-tab-summary" data-tab="summary" href="#summary">
        <span class="glyphicon glyphicon-list-alt"></span>
        Resumen
      </a>
    </li>
    <li role="presentation">
      <a class="stats-tab" id="stats-tab-calls" data-tab="calls" href="#calls">
        <span class="glyphicon glyphicon-earphone"></span> Detalle llamadas
      </a>
    </li> 
  </ul>

  <div class="tab-content tab-real-tab">
    <div role="tabpanel" class="tab-pane active" id="summary">
      @include('content.stats.summary')
      <div class="clearfix"></div>
    </div>
    <div role="tabpanel" class="tab-pane" id="calls"> 
      @include('content.stats.calls')     
      <div class="clearfix"></div>
    </div>
  </div>
@else
  @include('content.stats.calls')
@endif

<script type="text/javascript">
$(document).ready(function() {
  var incomingSummaryLoadIntent = false;
  var outgoingSummaryLoadIntent = false;
  
  var viewing = typeof Cookies.get('stats-viewing') === 'undefined'
    ? 'summary'
    : Cookies.get('stats-viewing');
  
  if(viewing == 'calls') {
    $('#stats-tab-calls').tab('show');   
  }
    
  {{-- Sólo mostramos resumen si no estamos en móvil --}}  
  @if(!$isMobile)
  $('.stats-tab').click(function(e) {
    var tabName = $(this).data('tab');
    Cookies.set('stats-viewing', tabName);
    
    e.preventDefault();
    $(this).tab('show');

    {{-- Sólo mostramos tráfico saliente si el número es voip-ish --}}  
    @if(in_array(Session::get('number')->getProfile()->getType(), array('voip', 'pbx', 'trunk')))
    if(tabName === 'summary-outgoing' && !outgoingSummaryLoadIntent) {
      $.getJSON('{{ URL::to('stats/summary/outgoing') }}', function(data) {
        outgoingSummaryLoadIntent = true;
        
        var calls = data.month.calls;

        $('.stats-summary-outgoing-loading').remove();

        $('#stats-summary-outgoing-day-count').html(data.day.count);
        $('#stats-summary-outgoing-day-cost').html(data.day.cost + '&euro;');

        $('#stats-summary-outgoing-week-count').html(data.week.count);
        $('#stats-summary-outgoing-week-cost').html(data.week.cost + '&euro;');

        $('#stats-summary-outgoing-month-count').html(data.month.count);
        $('#stats-summary-outgoing-month-cost').html(data.month.cost + '&euro;');

        var html = '';

        for(i in data.latestCalls) {
          var c = data.latestCalls[i];
          html+= '<tr>' +
                    '<td>' + c.called + '</td>' +                  
                    '<td>' + c.date + '</td>' +
                    '<td>' + c.duration + '</td>' +
                  '</tr>';
        }

        $('#stats-summary-outgoing-latest-calls tbody').append(html);


        $('.stats-summary-outgoing-group-count-label').css('display', 'block');

        new Morris.Line({
          parseTime: false,
          lineColors: ['#ff5d28', '#7A92A3', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
          element: 'month-activity-outgoing',
          data: calls,
          xkey: 'day',
          ykeys: ['total'],
          labels: ['Llamadas']
        });
      });        
    }
    @endif
    
  });
  
  $.getJSON('{{ URL::to('stats/summary/incoming') }}', function(data) {    
    incomingSummaryLoadIntent = true;    
    
    var calls = data.month.calls;
    
    $('.stats-summary-incoming-loading').remove();

    $('#stats-summary-incoming-day-count').html(data.day.count);
    $('#stats-summary-incoming-day-minutes').html(data.day.minutes);

    $('#stats-summary-incoming-week-count').html(data.week.count);
    $('#stats-summary-incoming-week-minutes').html(data.week.minutes);

    $('#stats-summary-incoming-month-count').html(data.month.count);
    $('#stats-summary-incoming-month-minutes').html(data.month.minutes);
    
    var html = '';
    
    for(i in data.latestCalls) {
      var c = data.latestCalls[i];
      html+= '<tr>' +
                '<td>' + c.caller + '</td>' +              
                '<td>' + c.date + '</td>' +
                '<td>' + c.duration + '</td>' +
              '</tr>';
    }
    
    $('#stats-summary-incoming-latest-calls tbody').append(html);
    
    $('.stats-summary-incoming-group-count-label').css('display', 'block');
    
    new Morris.Line({
      parseTime: false,
      lineColors: ['#ff5d28', '#7A92A3', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
      element: 'month-activity-incoming',
      data: calls,
      xkey: 'day',
      ykeys: ['total'],
      labels: ['Llamadas']
    });
  });  
  @endif

  var datePickerOpts = {
    format: 'dd/mm/yyyy',
    weekStart: 1,
    autoclose: true,
    language: 'es'
  };
    
  var $start = $('#start').datepicker(datePickerOpts).data('datepicker');
  var $end = $('#end').datepicker(datePickerOpts).data('datepicker');     

  $('#inoutbound').change(function() {
    if($(this).val() === 'inbound') {
      $('#select-caller').hide();
      $('#select-caller').prop('disabled', true);
      $('#text-caller').show();
      $('#text-caller').prop('disabled', false);      
      $('#select-called').prop('disabled', false);            
      $('#select-called').show();
      $('#text-called').prop('disabled', true);      
      $('#text-called').hide();
    } else {
      $('#select-caller').show();
      $('#select-caller').prop('disabled', false);
      $('#text-caller').hide();
      $('#text-caller').prop('disabled', true);      
      $('#select-called').prop('disabled', true);            
      $('#select-called').hide();      
      $('#text-called').prop('disabled', false);      
      $('#text-called').show();
    }
  });
  
  $('#stats-filter-toggle').click(function() {
    var $filters = $('#stats-filter-body');    
    var shown = $filters.css('display') === 'block' ? true : false;
    var $icon = $('#stats-filter-toggle-icon');
    
    if(shown) { 
      $filters.hide(300, 'linear');
      $icon.removeClass('fa-minus');
      $icon.addClass('fa-plus');
    }
    else {
      $filters.show(300, 'linear');
      $icon.removeClass('fa-plus');
      $icon.addClass('fa-minus');      
    }
  });
});
</script>
@stop
