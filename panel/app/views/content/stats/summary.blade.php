@if(in_array(Session::get('number')->getProfile()->getType(), array('voip', 'pbx', 'trunk')))
<ul class="nav nav-tabs">
  <li role="presentation" class="active">
    <a class="stats-tab" data-tab="summary-incoming" href="#summary-incoming">
      <span class="glyphicon glyphicon-arrow-down"></span>
      <span>Llamadas entrantes</span>
    </a>
  </li>
  <li role="presentation">
    <a class="stats-tab" data-tab="summary-outgoing" href="#summary-outgoing">
      <span class="glyphicon glyphicon-arrow-up"></span>
      <span>Llamadas salientes</span>
    </a>
  </li> 
</ul>

<div class="tab-content tab-real-tab">
  <div role="tabpanel" class="tab-pane active" id="summary-incoming">
    @include('content.stats.summary.incoming')
    <div class="clearfix"></div>
  </div>
  <div role="tabpanel" class="tab-pane" id="summary-outgoing"> 
    @include('content.stats.summary.outgoing') 
    <div class="clearfix"></div>
  </div>
</div>
@else
  @include('content.stats.summary.incoming')
@endif
