<div class="row stats-summary">
  <div class="col-md-3">        
    <h3>Llamadas último día</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-incoming-day-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">
          llamadas
        </span>
      </div>
      <div class="col-md-6">
        <span id="stats-summary-incoming-day-minutes" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">            
          minutos
        </span>
      </div>          
    </div>

    <h3>Llamadas última semana</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-incoming-week-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">                
          llamadas
        </span>              
      </div>
      <div class="col-md-6">
        <span id="stats-summary-incoming-week-minutes" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">
          minutos
        </span>
      </div>  
    </div>

    <h3>Llamadas último mes</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-incoming-month-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">
          llamadas
        </span>
      </div>
      <div class="col-md-6">
        <span id="stats-summary-incoming-month-minutes" class="stats-summary-group-count-number">         
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-incoming-group-count-label">
          minutos
        </span>
      </div>            
    </div>
  </div>
  <div class="col-md-4">
    <h3>Últimas llamadas</h3>
    
    <table class="table" id="stats-summary-incoming-latest-calls">
      <thead>
        <tr>
          <th>Origen</th>
          <th>Comienzo</th>
          <th>Duración</th>
        </tr>
      </thead>
      <tbody> 
      </tbody>
    </table>
    <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
  </div>
  <div class="col-md-5">
    <h3>Actividad este mes</h3>
    
    <div id="month-activity-incoming" style="height: 300px;">
      <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-incoming-loading">
    </div>
  </div>      
</div>