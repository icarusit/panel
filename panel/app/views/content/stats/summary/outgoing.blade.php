<div class="row stats-summary">
  <div class="col-md-3">        
    <h3>Llamadas último día</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-outgoing-day-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">
          llamadas
        </span>
      </div>
      <div class="col-md-6">
        <span id="stats-summary-outgoing-day-cost" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">            
          coste
        </span>
      </div>          
    </div>

    <h3>Llamadas última semana</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-outgoing-week-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">                
          llamadas
        </span>              
      </div>
      <div class="col-md-6">
        <span id="stats-summary-outgoing-week-cost" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">
          coste
        </span>
      </div>  
    </div>

    <h3>Llamadas último mes</h3>

    <div class="row">          
      <div class="col-md-6">
        <span id="stats-summary-outgoing-month-count" class="stats-summary-group-count-number">
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">
          llamadas
        </span>
      </div>
      <div class="col-md-6">
        <span id="stats-summary-outgoing-month-cost" class="stats-summary-group-count-number">         
          <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
        </span>
        <span class="stats-summary-group-count-label stats-summary-outgoing-group-count-label">
          coste
        </span>
      </div>            
    </div>
  </div>
  <div class="col-md-4">
    <h3>Últimas llamadas</h3>
    
    <table class="table" id="stats-summary-outgoing-latest-calls">
      <thead>
        <tr>
          <th>Destino</th>
          <th>Comienzo</th>
          <th>Duración</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
    
  </div>
  <div class="col-md-5">
    <h3>Actividad este mes</h3>
    
    <div id="month-activity-outgoing" style="height: 300px;">
      <img src="{{ asset('img/loading-40.gif') }}" class="stats-summary-outgoing-loading">
    </div>
  </div>      
</div>