@extends('main')
@section('subpage')
<h1><span class="fa fa-list-alt"></span> Resumen <strong>{{ $number }}</strong></h1>

@include('help', array('content' => 'summary'))

<?php $end = false; global $end; ?>

@include('errors')

@if(in_array($number->getType(), array('analog', 'voip', 'pbx')))
<div class="panel panel-default">
  <div class="panel-heading">
  	@if(isset($mens))
    	<div class="alert alert-success"><strong>{{ $mens }}</strong></div>
    @endif
    <h3 class="panel-title">Resumen de llamadas entrantes para el
    {{ Form::open(array(
      'action' => 'SummaryController@show',
      'class'  => 'form-inline',
      'id'     => 'set-date'
    )) }}
      <input id="summary-date" class="form-control summary-date-picker date-picker">  
      <input type="hidden" id="summary-formatted-date" 
             name="summary-formatted-date" value="{{ $summaryDate->format('Y-d-m') }}">        
    {{ Form::close() }}
  </div>
  <div class="panel-body" style="padding-top: 0; padding-bottom: 0;">
    <ul class="timeline">
      {{-- ENTRA LA LLAMADA --}}
      <li>
        <div class="timeline-badge"><i class="fa fa-phone"></i></div>
        <div class="timeline-panel">
          <div class="timeline-heading">
            <h4 class="timeline-title">Entra la llamada en tu número {{ $number }}.</h4>
          </div>
        </div>
      </li>
      
      {{--- SUSPENSIÓN DEL NÚMERO ---}}
      @include('content.summary.suspended')  
      
      {{-- SI ES 800/900 COMPROBAMOS SALDO --}}
      @include('content.summary.900')

      {{--- MODO NO MOLESTAR ---}}
      @include('content.summary.dnd')

      {{--- NÚMERO A MEDIDA ---}}
      @include('content.summary.customized')

      @if(!$end)
        {{--- BLOQUEO DE LLAMADAS ENTRANTES ---}}
        @include('content.summary.inbound_block')  

        {{--- LISTA BLANCA ---}}
        @include('content.summary.whitelist')

        {{--- LISTA NEGRA ---}}
        @include('content.summary.blacklist')

        {{--- FESTIVOS NACIONALES ---}} 
        @include('content.summary.national_holidays')

        {{--- FESTIVOS PERSONALIZADOS ---}}
        @include('content.summary.custom_holidays')

        {{--- HORARIOS ---}}   
        @include('content.summary.schedules')    

        {{--- ACCIONES POR ORIGEN (PROVINCIAS) ---}}
        @include('content.summary.origin')

        {{--- LOCUCIÓN DE BIENVENIDA ---}}
        @include('content.summary.welcome_speech')

        {{--- MENÚ ---}}
        @include('content.summary.menus')

        @if($number->getType() === 'analog')
          {{-- DESVÍOS POR OCUPADO O NO CONTESTA --}}
          {{---> GRABACION DE LLAMADAS           --}} 
          {{---> LOCUCION DE ESPERA              --}}
          @include('content.summary.forwards')
        @else
          @include('content.summary.queue')
        @endif

        {{-- BUZÓN GENERAL --}}
        @include('content.summary.voicemail', array('options' => $options))      

        {{--- LLAMADA PERDIDA ---}}
        @include('content.summary.missed_calls', array('options' => $options))              
      @endif

    </ul>
  </div>
</div>
@endif

{{-----------------------}}
{{--- TARIFAS PLANAS  ---}}
{{-----------------------}}
@include('content.summary.flatrates')

<script type="text/javascript">
$('#summary-date').datepicker({
  format: 'DD, d MM',
  weekStart: 1,
  autoclose: true,
  language: 'es'
});  

{{-- Ojo que el formato que parece aceptar el datepicker este es yyyy-dd-mm --}}

@if($summaryDate !== false)
$('#summary-date').datepicker('update', "{{ $summaryDate->format('Y-d-m') }}");
@endif

$('#summary-date').datepicker().on('changeDate', function(e) {
  var formattedDate = e.format('yyyy-dd-mm');  
  $('#summary-formatted-date').prop('value', formattedDate);
  $('#set-date').submit();  
});
</script>
  
@stop
