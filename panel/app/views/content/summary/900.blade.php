<?php global $end; ?>
@if(!$end)    
@if(preg_match('/^(?:8|9)00[0-9]{6}$/', $number->getNumber()) && $number->getUser()->getBalance() <= 0)
<?php $end = true; ?>
<li class="timeline">
  <div class="timeline-badge danger"><i class="fa fa-warning"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title" style="color: red;">Tu saldo está agotado.</h4>
    </div>
    <div class="timeline-body">
      <p>Recarga ahora saldo para seguir recibiendo llamadas en tu número.</p>
      <a class="btn btn-primary" href="{{ URL::to('balance') }}">Recarga de saldo</a>
    </div>
  </div>
</li>
@endif
@endif