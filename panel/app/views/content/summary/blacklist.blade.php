@if(($blacklist = $number->getBlacklist()->getArrayBlacklist()) !== false)
<li class="timeline">
  <div class="timeline-badge"><i class="fa fa-ban"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Lista negra.</h4>
    </div>
    <div class="timeline-body">
      <p>Si llama alguno de los siguientes números, la llamada <strong>se colgará</strong>:</p>

      <ul class="summary-list">
        @foreach($blacklist as $bnumber)
        <li><i class="fa fa-phone fa-fw"></i>{{ $bnumber }}</li>
        @endforeach
      </ul>
      <a href="{{ URL::to('whiteblacklist') }}" 
         class="btn btn-primary summary-btn">
        Cambiar configuración de lista negra
      </a>
    </div>
  </div>
</li>      
@endif {{-- end blacklist --}}