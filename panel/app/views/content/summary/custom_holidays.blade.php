<?php global $end; ?>
@if(in_array($plan, ['plus', 'premium', 'pro']))    
@if(!$end)
@if(($currentHoliday = $number->getHolidays($summaryDate)) !== null)
<?php $end = true; ?>
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="fa fa-sun-o"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">¡Hoy es festivo para ti!</h4>
    </div>
    <div class="timeline-body">
      <p>{{ (new Action($currentHoliday->getAction()))->parse() }}</p>

      <a href="{{ URL::to('holidays') }}" 
         class="btn btn-primary summary-btn">
        Cambiar configuración de festivos
      </a>                              
    </div>
  </div>
</li>
@endif
@endif
@endif