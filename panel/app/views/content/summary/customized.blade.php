<?php global $end; ?>
@if($number->isCustomized())
<?php $end = true; ?>        
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="fa fa-puzzle-piece"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">El número está configurado a medida.</h4>
    </div>
    <div class="timeline-body">
      <p>Como tu número lo hemos configurado nosotros a medida, su configuración no
         aparecerá en el resumen (al menos de momento).</p>
      <p>Además, dependiendo del tipo de configuración que te hayamos hecho
         es probable que cambiar los campos de configuración del número desde
         el panel no altere el funcionamiento de las llamadas entrantes.</p>
      <p>Por favor, si quieres hacer algún cambio contacta con nosotros.</p>
    </div>
  </div>
</li>      
@endif
