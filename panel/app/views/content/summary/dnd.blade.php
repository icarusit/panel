<?php global $end; ?>
@if(!$end && $plan === 'plus')        
@if($number->isDNDActive())        
<?php $end = true; ?>
<li class="timeline-inverted">
  <div class="timeline-badge danger"><i class="fa fa-ban"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title" style="color: red;">El número está en modo 'No molestar'.</h4>
    </div>
    <div class="timeline-body">
      <p>Se cuelga la llamada.</p>
      
      {{ Form::open(array('action' => 'DoNotDisturbController@setDND')) }}
      <input type="hidden" name="return" value="summary">                
      <input type="hidden" name="dndmode" value="disable">
      <input type="submit" class="btn btn-primary summary-btn" value="Desactivar el modo 'No molestar'">
      {{ Form::close() }}       
      
    </div>
  </div>
</li>
@endif
@endif
