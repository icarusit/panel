<?php
if($tarifaplana == 'si'){
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Tarifas planas</h3>
  </div>
  <div class="panel-body">
    @if(empty($flatrates))
    <p>No tienes ninguna tarifa plana contratada.</p>
    @else
    <p>Estas son las tarifas planas que tienes contratadas en el número:</p>
    
    <div class="table-responsive">
      <table class="table table-striped table-hover" style="width: auto;">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Restante este mes</th>
            <th>Fecha contratación</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          @foreach($flatrates as $flatrate)
            <tr>
              <td>{{ $flatrate->name }}</td>
              <td>{{ $flatrate->description }}</td>
              @if($profile->getType() == 'fax')
              
              <?php
              // <ÑAPA>
              $saldoFax = Number::find($number->getID())->saldo;
              $pages = (int)($saldoFax / 0.02);
              // </ÑAPA>
              ?>
              
              <td style="text-align: right;"><strong>{{ $pages }}</strong> de <strong>{{ $flatrate->minutes }}</strong> páginas</td>
              @else
              <td style="text-align: right;"><strong>{{ number_format($flatrate->minutes_left, 2) }}</strong> de <strong>{{ $flatrate->minutes }}</strong> minutos</td>
              @endif
              <td>{{ (new \DateTime($flatrate->since))->format('d/m/Y') }}</td>
              <td><a href="{{ URL::to('summary/del/' . $flatrate->id_flatrate . '/'. $id_numero) }}" class="btn btn-primary" onClick="return confirma('Al eliminar la tarifa plana perderás inmediatamente los beneficios que esta te otorga incluyendo los minutos que aún no hayas utilizado. Quieres continuar');">
      <i class="fa fa-times"></i> Eliminar esta tarifa plana
    </a>   </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @endif

    @if($profile->getType() !== 'fax')
    <a href="{{ URL::to('flatrates/order') }}" class="btn btn-primary">
      <i class="fa fa-shopping-cart"></i> Contratar tarifas planas
    </a>    
    @endif

  </div>
</div>
<?php
}
?>