<?php global $end; ?>
@if(!$end)
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="glyphicon glyphicon-th"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Desvíos.</h4>
    </div>
    <div class="timeline-body">
      <p>Desviamos la llamada al desvío principal:</p>
      <p>
        <span style="font-size: 140%; font-weight: bold;">{{ $mainForward->forward }}
        @if($panelMode === 'advanced')
        <span style="font-size: 50%;">(durante {{ $mainForward->timeout }} segundos)</span>
        @endif   
        <span class="badge">1</span></span>
      </p>

      @if(is_array($forwards) && count($forwards) > 0)
      <p>Si en <span class="badge">1</span> no descuelgan o comunica, ejecutaremos
        los siguientes desvíos en cascada, es decir, uno tras otro.</p>

      <ul class="summary-list">
        @foreach($forwards as $i => $forward)
        <li><i class="fa fa-phone fa-fw"></i> {{ $forward->forward }}
        @if($panelMode === 'advanced')
        <span style="font-size: 80%;">(durante {{ $forward->timeout }} segundos)</span>
        @endif
        <span class="badge">{{ $i+2 }}</span></li>
        @endforeach
      </ul>
      @endif

      @if(($queueSpeech = $number->getSpeeches()->queue_speech) !== FALSE)
      <p>Si comunican todos los desvíos se reproducirá la locución de espera
      <strong>{{ $queueSpeech->public_name }}</strong> hasta que alguno de
      los desvíos descuelgue.</p>
      @endif       

      @if(($operatorSpeech = $number->getSpeeches()->operator_speech) !== FALSE)
      <p>Cuando uno de los desvíos atienda la llamada se reproducirá a la persona
        que descuelga la locución <strong>{{ $operatorSpeech->public_name }}</strong> 
        antes de conectar la llamada.</p>
      @endif
      
      @if($number->isCallRecordingActive())
      <p>Las llamadas serán grabadas.</p>

      {{ Form::open(array(
        'action' => 'CallRecordingController@setCallRecording',
        'style'  => 'float: left; margin-right: 3px;'
      )) }}            
      <input type="hidden" name="callrecording"
             value="disable">
      <input type="hidden" name="return" value="summary">
      <input type="submit"
             class="btn btn-primary summary-btn"
             value="Desactivar grabación de llamadas">
      {{ Form::close() }}
      @endif            
      
      @if($queueSpeech !== false)
      <a href="{{ URL::to('speeches') }}" 
         class="btn btn-primary summary-btn">
        Modificar locución de espera
      </a>
      @endif
      
      <a href="{{ URL::to('forwards') }}" 
         class="btn btn-primary summary-btn">
        Modificar los desvíos
      </a>
    </div>
  </div>
</li>
@endif
