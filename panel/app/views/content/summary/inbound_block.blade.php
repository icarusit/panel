<?php global $end; ?>
@if(!$end)
@if($options->block_international_inbound_calls ||
    $options->block_mobile_inbound_calls ||
    $options->block_anonymous_inbound_calls)
<li class="timeline">
  <div class="timeline-badge"><i class="fa fa-ban"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Bloqueos por origen.</h4>
    </div>
    <div class="timeline-body">
      <ul class="summary-list">
        @if($options->block_international_inbound_calls)
        <li><i class="fa fa-globe fa-fw"></i>&nbsp;Se colgará la llamada si el
          llamante es un <strong>número internacional</strong></li>
        @endif
        @if($options->block_mobile_inbound_calls)
        <li><i class="fa fa-mobile fa-fw"></i>&nbsp;Se colgará la llamada si el
          llamante es <strong>móvil</strong></li>
        @endif
        @if($options->block_anonymous_inbound_calls)
        <li><i class="fa fa-question fa-fw"></i>&nbsp;Se colgará la llamada si el
          llamante es <strong>anónimo</strong></li>
        @endif
      </ul>
      <a href="{{ URL::to('whiteblacklist') }}#block_inbound_calls" 
         class="btn btn-primary summary-btn">
        Cambiar configuración del bloqueo por origen
      </a>
    </div>
  </div>
</li>  
@endif
@endif  