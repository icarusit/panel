@if(!$end && $plan === 'plus')    
@if($number->menu()->isActive())
<?php $menu = $number->menu(); ?>
<?php $options = $menu->getOptions(); ?>
<?php global $end; ?>
<?php $end = true; ?>
<li class="timeline">
  <div class="timeline-badge"><i class="glyphicon glyphicon-th"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Menú.</h4>
    </div>
    <div class="timeline-body">
      <p>Se ejecuta el menú con locución <strong>{{ $menu->getSpeech()->name }}</strong> y las siguientes opciones:</p>
      <div class="table-responsive">
        <table class="table" style="width: auto;">
          <thead>
            <th></th>
            <th>Dígito</th>
            <th>Acción</th>      
          </thead>
          <tbody>
            @foreach($options as $dtmf => $action)
            <tr>
              <td>
                <a href="{{ URL::to('menus/remove/' . $dtmf) }}" title="Eliminar la acción configurada al pulsar '{{ $dtmf }}'">
                  <span class="glyphicon glyphicon-remove"></span>
                </a>
              </td>
              <td>{{ $dtmf }}</td>
              <td>{{ (new Action($action))->parse() }}</td>          
            </tr>
            @endforeach
          </tbody>
        </table>           
      </div>

      <a href="{{ URL::to('menus') }}" 
         class="btn btn-primary summary-btn">
        Cambiar configuración menús
      </a> 
    </div>
  </div>
</li>         
@endif
@endif {{-- end if plan == plus --}} 