@if($options->missed_calls->enabled)
<li class="timeline">
  <div class="timeline-badge"><i class="fa fa-inbox"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Llamadas perdidas.</h4>
    </div>
    <div class="timeline-body">
      {{ Form::open(array('action' => 'MissedCallsController@setMissedCalls')) }}

      <input type="hidden" name="return" value="summary">                  
      <input type="hidden" name="missedcalls" value="disable">                        

      <p>Si comunican los desvíos, ninguno contesta, el llamante cuelga,
        es festivo o entra en un horario, te notificamos de la llamada
        perdida vía correo electrónico a 
        <strong>{{ $options->missed_calls->email }}</strong>.</p>                           

      <button class="btn btn-primary">
        Desactivar notificación de llamada perdida
      </button>
      {{ Form::close() }}          
    </div>
  </div>
</li>     
@endif