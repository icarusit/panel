<?php global $end; ?>
@if(!$end)
<?php $numberPrefixesActions = $number->getActions(); ?>
@if(is_array($numberPrefixesActions))
<li class="timeline">
  <div class="timeline-badge"><i class="glyphicon glyphicon-globe"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Acciones por origen provincial</h4>
    </div>
    <div class="timeline-body">
      <div style="margin: 20px 0;">
        <table class="table">
          <thead>
            <tr>
              <th>Llamadas desde</th>
              <th>Acción que ejecutamos</th>   
            </tr>
          </thead>
          <tbody>
            @foreach($numberPrefixesActions as $numberPrefixesAction)
            <tr>
              <td>{{ $numberPrefixesAction->detail }}</td>
              <td>{{ (new Action($numberPrefixesAction->action))->parse() }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <a href="{{ URL::to('origin') }}" 
         class="btn btn-primary summary-btn">
        Modificar las acciones por origen provincial
      </a> 
    </div>
  </div>
</li>    
@endif
@endif      
