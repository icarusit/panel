<?php global $end; ?>
@if(!$end)
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="glyphicon glyphicon-th"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Cola de llamada.</h4>
    </div>
    <div class="timeline-body">
      @if($queue !== false && count($queue))
        @foreach($queue as $i => $priority)
        <table class="table summary-queue-table">
          <tr>
            @foreach($priority as $extension)
            <td>
            	@if(is_numeric($extension))
                	<span class="glyphicon glyphicon-phone-alt"></span>
                @endif
            	{{ $extension }}
            </td>              
            @endforeach
          </tr>
          
          @if($i < count($queue) - 1)
          <table class="table summary-queue-table">
            <tr>
              <td class="arrow">
                <i class="fa fa-long-arrow-down"></i>
              </td>
            </tr>
          </table>
          @endif          
        </table>
        @endforeach
      @else
      <p>No has asignado aún extensiones en la cola de llamada</p>
      @endif
      
      <div class="margin-15"></div>
      
      @if(($queueSpeech = $number->getSpeeches()->queue_speech) !== FALSE)
      <p>Si comunican o no están disponibles todas las cuentas SIP se reproducirá 
        la locución de espera <strong>{{ $queueSpeech->public_name }}</strong>
        hasta que alguna de las extensiones atienda la llamada.</p>
      @endif       
      
      @if($number->isCallRecordingActive())
      <p>Las llamadas serán grabadas.</p>

      {{ Form::open(array(
        'action' => 'CallRecordingController@setCallRecording',
        'style'  => 'float: left; margin-right: 3px;'
      )) }}            
      <input type="hidden" name="callrecording"
             value="disable">
      <input type="hidden" name="return" value="summary">
      <input type="submit"
             class="btn btn-primary summary-btn"
             value="Desactivar grabación de llamadas">
      {{ Form::close() }}
      @endif            
      
      @if($queueSpeech !== false)
      <a href="{{ URL::to('speeches') }}" 
         class="btn btn-primary summary-btn">
        Modificar locución de espera
      </a>
      @endif
      
      <a href="{{ URL::to('queue') }}" 
         class="btn btn-primary summary-btn">
        Modificar cola de llamada
      </a>
    </div>
  </div>
</li>
@endif
