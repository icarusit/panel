<?php global $end; ?>
@if(!$end)                     
@if(isset($number->getSchedules()[$summaryDateWeekDay]) && count($schedules = $number->getSchedules()[$summaryDateWeekDay]) > 0)
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="fa fa-clock-o"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Horarios para este día</h4>
    </div>
    <div class="timeline-body">
      <div style="margin: 20px 0;">
        @foreach($schedules as $schedule)
        <h5 class="summary-sub-heading">Desde las
          {{ $schedule->getFrom()->format('H:i') }} hasta las
          {{ $schedule->getUntil()->format('H:i') }}
        </h5>
        <p>{{ (new Action($schedule->getAction()))->parse() }}</p>
        @endforeach            
        <h5 class="summary-sub-heading">Resto de horarios</h5>
        <p>La llamada continúa a los desvíos</p>                      

        <a href="{{ URL::to('schedules') }}" 
           class="btn btn-primary summary-btn">
          Modificar los horarios
        </a>                            
      </div>                            
    </div>
  </div>
</li>
@endif
@endif