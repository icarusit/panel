<?php global $end; ?>
@if($number->isSuspended())
<?php $end = true; ?>        
<li class="timeline-inverted">
  <div class="timeline-badge danger"><i class="fa fa-warning"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title" style="color: red;">El número está temporalmente suspendido.</h4>
    </div>
    <div class="timeline-body">
      <p>Se cuelga la llamada.</p>
      <p>Por favor, contacta con nosotros.</p>
      <a class="btn btn-primary" href="{{ URL::to('invoices') }}">Mis facturas</a>            
    </div>
  </div>
</li>      
@endif
