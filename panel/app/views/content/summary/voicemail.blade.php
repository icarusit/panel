@if($options->voicemail->enabled)
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="fa fa-inbox"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Buzón de voz.</h4>
    </div>
    <div class="timeline-body">
      {{ Form::open(array('action' => 'VoicemailController@setVoicemail')) }}

      <input type="hidden" name="return" value="summary">                  
      <input type="hidden" name="voicemail" value="disable">                        

      <p>Si comunican los desvíos o ninguno contesta, mandamos la
        llamada a buzón de voz.</p>           

      <p>La locución para el buzón de voz es 
        <strong>{{ $options->voicemail->speech->public_name }}</strong>.
      </p>

      <p>Los mensajes de voz que te dejen los recibirás en la cuenta
        de correo <strong>{{ $options->voicemail->email }}</strong>.</p>    

      <button class="btn btn-primary">Desactivar buzón de voz</button>              

      <a href="{{ URL::to('voicemail') }}" 
         class="btn btn-primary">Cambiar configuración buzón de voz
      </a>
      {{ Form::close() }}          
    </div>
  </div>
</li> 
@endif