<?php global $end; ?>
@if(!$end)              
@if(in_array($plan, ['plus', 'premium']))
@if(($welcomeSpeech = $number->getSpeeches()->welcome_speech) !== false)        
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="glyphicon glyphicon-globe"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Locución de bienvenida.</h4>
    </div>
    <div class="timeline-body">
      <p>Se reproduce la locución de bienvenida <strong>{{ $welcomeSpeech->public_name }}</strong>.</p>

      <a href="{{ URL::to('speeches') }}" 
         class="btn btn-primary summary-btn">
        Cambiar configuración locuciones
      </a> 
    </div>
  </div>
</li>            
@endif
@endif {{-- end if plan in (plus, premium) --}}
@endif