<?php global $end; ?>
@if(!$end)
@if($number->hasWhitelist())
<li class="timeline-inverted">
  <div class="timeline-badge"><i class="fa fa-ban"></i></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <h4 class="timeline-title">Lista blanca.</h4>
    </div>
    <div class="timeline-body">
      <p>Sólo permitirás que continúen la llamada los siguientes números:</p>

      <ul class="summary-list">
        <?php $whitelist = $number->getWhitelist()->getArrayWhitelist(); ?>
        @foreach($whitelist as $wnumber)
        <li><i class="fa fa-phone fa-fw"></i>{{ $wnumber }}</li>
        @endforeach
      </ul>
      <a href="{{ URL::to('whiteblacklist') }}" 
         class="btn btn-primary summary-btn">
        Cambiar configuración de lista blanca
      </a>          
    </div>
  </div>
</li>     
@endif
@endif