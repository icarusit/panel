@extends('main')

@section('subpage')
<h1>Configuración de trunk</h1>

<div class="frame frame-collapsable frame-info">
  <a href="#" class="toggle-show-help"><span class="glyphicon glyphicon-info-sign"></span> <span class="toggle-text"></span></a>

  <div class="frame-content">
    <p>El servicio de configuración de trunk te permitirá enrutar las llamadas
      entrantes y salientes del número <strong>{{ Session::get('number') }}</strong>
      por cualquiera de los trunks que tengas contratados.</strong>
    </p>
  </div>
</div>

@if(($info = Session::get('info', NULL)) !== NULL)
<div class="alert alert-success">{{ $info }}</div>
@endif

@if(($err = Session::get('error', NULL)) !== NULL)
<div class="alert alert-danger">{{ $err }}</div>
@endif
 
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Configuración de trunk</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      {{ Form::open(array(
        'action' => 'TrunkController@update',
        'class'  => 'form-horizontal col-md-5'
      )) }}


       @if(count($trunks) > 0)
       <div class="form-group">
         <label for="event" class="col-md-2 control-label">Trunk</label>
         <div class="col-md-10">
           <select class="form-control" name="trunk">        
           @foreach($trunks as $trunk)
            <option value="{{ $trunk->defaultuser }}"
                    @if($selectedTrunk === $trunk->defaultuser) selected @endif>
                    {{ $trunk->defaultuser }} ({{ $trunk->host }})
            </option>
           @endforeach
           </select>
           </div>
       </div>

       <input type="submit" class="btn btn-primary" value="Guardar" /> 
       @else
        <p>No tienes ningún trunk contratado</p>
       @endif
 
     {{ Form::close() }}
    </div>
  </div>
</div>

@stop
