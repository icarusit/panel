@if(count($vmMessages) > 0)
<div class="table-responsive">
  <table class="table table-hover" style="width: auto;">
    <thead>        
      <tr>     
        <th width="80"></th>
        <th>Fecha - Hora</th>
        <th>Origen</th>
        <th>Duración</th>
      </tr>
    </thead>
    <tbody data-provides="rowlink">
      @foreach($vmMessages as $vmMessage)
      <tr>
        <td>
          <a href="{{ URL::to('voicemail/download/' . $vmMessage->id_message) }}"
             title="Escuchar mensaje"
             class="voicemail-play">
            <span class="glyphicon glyphicon-play"></span>
          </a>
          <a href="{{ URL::to('voicemail/download/' . $vmMessage->id_message) }}"
             title="Descargar mensaje de voz">
            <span class="glyphicon glyphicon-download-alt"></span>
          </a>
          <a href="{{ URL::to('voicemail/remove/' . $vmMessage->id_message) }}" 
             class="voicemail-remove"
             title="Eliminar mensaje de voz">
            <span class="glyphicon glyphicon-remove"></span>
          </a>
        </td>
        <td>{{ (new \DateTime($vmMessage->date))->format('d/m/Y H:i:s') }}</td>
        <td>{{ $vmMessage->caller }}</td>
        <td>{{ gmdate('i:s', $vmMessage->duration) }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>  
{{ $vmMessages->links() }}
@else
  <div class="media col-md-12">
    <span class="pull-left fa fa-thumbs-up panel-icon"></span>
    <div class="media-body">
      <h4 class="media-heading">No tienes mensajes de voz.</h4>
    </div>
  </div>       
@endif