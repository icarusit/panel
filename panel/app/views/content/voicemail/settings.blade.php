<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Configuración buzón de voz</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'VoicemailController@setConfig',
          'class'  => 'form-horizontal'
        )) }}        
        <div class="form-group">
          <label for="voicemail-speech" class="col-md-5 control-label">
            Locución del buzón
          </label>
          <div class="col-md-6">
            <select class="form-control" name="voicemail-speech" id="voicemail-speech" 
                    <?php if(count($speeches) == 0):
                      $title = "Sube primero alguna locución para poderla elegir como locución del buzón de voz";
                    ?>
                    title="{{ $title }}" disabled
                    <?php endif; ?>>
              @if(count($speeches) > 0)
              @if($selectedSpeech === null)
              <option disabled selected>No has seleccionado ninguna aún</option>              
              @endif
              @foreach($speeches as $speech)
              <option value="{{ $speech->id_speech }}"@if($speech->id_speech == $selectedSpeech) selected @endif>{{ $speech->public_name }}</option>
              @endforeach
              @endif              
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="voicemail-email" class="col-md-5 control-label">
            Email recepción
          </label>
          <div class="col-md-6">
            <input class="form-control email" name="voicemail-email" type="text"
                   value="{{ $email !== NULL ? $email : '' }}">
          </div>
        </div>
        <input type="submit" class="btn btn-primary pull-right" value="Guardar" />
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>