<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Buzón de voz general</h3>
      </div>
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'VoicemailController@setVoicemail',
          'class'  => 'form-horizontal',
          'id'     => 'form-toggle'
        )) }}
          <div class="form-group">
            <label class="col-md-5 control-label">Servicio activo</label>
            <div class="col-sm-3">  
              <input type="checkbox" id="voicemail-toggle" 
                     name="voicemail-toggle" 
                     value="1" {{ $hasVoicemail ? ' checked' : '' }}>
            </div>    
          </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>