@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-envelope"></span> Buzón de voz</h1>

@include('help', array('content' => 'voicemail'))
@include('errors')

<ul class="nav nav-tabs">
  <li role="presentation" class="active">
    <a href="#inbox" class="voicemail-tab">
      <span class="fa fa-inbox"></span> Bandeja de entrada
    </a>
  </li>
  <li role="presentation">
    <a href="#settings" class="voicemail-tab">
      <span class="fa fa-cog"></span> Configuración
    </a>
  </li>
</ul>

<div class="tab-content tab-real-tab">
  <div role="tabpanel" class="tab-pane active" id="inbox">
    @include('content.voicemail.list')
    <div class="clearfix"></div>
  </div>
  <div role="tabpanel" class="tab-pane" id="settings">
    @include('content.voicemail.toggle')
    @include('content.voicemail.settings')
    @include('content.whatsapp.whatsapp')   
    <div class="clearfix"></div>
  </div>
</div> 

<script type="text/javascript">
$(document).ready(function() {
  $('#voicemail-toggle').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-toggle').submit();
    }
  });
  
  $('.voicemail-tab').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  });

  $('.voicemail-play').click(function() {
    jBeep($(this).prop('href'));
    return false;
  });
  
  $('.voicemail-remove').click(function() {
    return confirm("Se eliminará definitivamente el mensaje de voz. ¿Deseas continuar?");
  });
});
</script>

@stop
