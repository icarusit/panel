@extends('main')
@section('subpage')
<div class="jumbotron">
  <h2>Rellena tu ficha de cliente</h2>
  
  @include('errors')
  
  <p>Completa los siguientes campos con los datos de tu empresa.</p>
  
  {{ Form::open(array(
    'action' => 'UserController@update',
    'class'  => 'form-horizontal'
  )) }}  

  <input type="hidden" name="return" value="welcome/payment-method">
  <input type="hidden" name="return_on_error" value="welcome/new-customer-form">
  
  @include('components.customer-form')
  
  <div class="row margin-30">
    <div class="col-md-5">
      <input type="submit" class="btn btn-primary btn-lg pull-right" value="Continuar y elegir una forma de pago">    
    </div>
  </div>
  
  {{ Form::close() }}
</div>
@stop