@extends('main')
@section('subpage')
<div class="jumbotron">
  <h2>Elige una forma de pago de la cuota</h2>
  
  @include('errors') 
  
  {{ Form::open(array(
    'action' => 'PaymentController@update',
    'class'  => 'form-horizontal'
  )) }}  
  
  <p>Elige cómo quieres pagar las cuotas de mantenimiento.</p>  
  
  <div class="radio">
    <label>
      <input type="radio" class="radio-payment-method" 
             name="payment-method" id="payment-method-1" value="direct-debit" checked>
      Mediante domiciliación bancaria
    </label>
  </div>
  
  <div id="bank-account-number-container" class="margin-15">
    <div class="form-group">
      <label class="col-sm-3 control-label">Número de cuenta:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" name="bank-account-number" id="bank-account-number"
               placeholder="Número de cuenta">
      </div>
    </div>    
  </div> 
  
  <div class="radio">
    <label>
      <input type="radio" class="radio-payment-method" 
             name="payment-method" id="payment-method-2" value="regular">
      Mediante pago con tarjeta desde la web / Ingreso en cuenta / Transferencia
    </label>
  </div>
  
  <div class="radio">
    <label>
      <input type="radio" class="radio-payment-method" 
             name="payment-method" id="payment-method-3" value="remesar">
     Daré mis datos más adelante 
    </label>
  </div>
  

  <div class="row margin-30">
    <div class="col-md-5">
      <input type="submit" class="btn btn-primary btn-lg" value="Finalizar y configurar mi nuevo número">    
    </div>
  </div>  
  
  {{ Form::close() }}  
  
  <div class="margin-45"></div>
  
  <h2>Haz tu primera recarga (opcional)</h2>
  
  <p>El saldo sirve para hacer llamadas o desvíos a móviles o números internacionales.</p>  
  
  <p>Si has dado de alta un número 900, el coste de las llamadas entrantes también lo
    descontaremos de tu saldo.</p>

  <div class="row">
    <div class="col-md-5">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('components.top-up-credit')      
        </div>
      </div>
    </div>    
  </div>    
</div>

<script>
$(document).ready(function() {
  $('#bank-account-number').keypress(function(event) {
    if(!$.isNumeric(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  });
  
  $('.radio-payment-method').change(function() {
    if($(this).val() === 'direct-debit')
	{
      $('#bank-account-number-container').show();
    }
  });
});  
</script>
@stop