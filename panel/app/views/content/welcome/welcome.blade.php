@extends('main')
@section('subpage')
<div class="jumbotron">
  <h1>¡Hola!</h1>
  
  <p>Es la primera vez que das de alta un número en <?php echo Session::get('distribuidor'); ?> por lo que
    te damos las gracias y la bienvenida.</p>
  <p>Ya tienes tu número operativo pero antes de pasar a configurarlo, consultar
    tus estadísticas o cargar saldo por primera vez necesitamos que completes
    tu ficha de cliente.</p>
  <p><a class="btn btn-primary btn-lg" href="{{ URL::to('welcome/new-customer-form') }}" 
        role="button">Rellena ahora la ficha de cliente</a></p>
</div>
@stop