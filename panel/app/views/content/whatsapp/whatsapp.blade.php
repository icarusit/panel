<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Notificar Whatsapp</h3>
      </div>
      @if($wp != '')
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'WhatsAppController@setWhatsAppActive',
          'class'  => 'form-horizontal',
          'id'     => 'form-number-toggle-wp'
        )) }}
			<div class="form-group">
            	@if($wp_activo)
                    <div class="col-md-12" style="text-align:left;">
                        
                        @if($wp_estado_api == 0)
                            <div class="alert alert-danger">Antes de poder recibir mensajes en este numero debes agregar <strong>+34 634307339</strong> a tu lista de contactos y enviar la palabra <strong>ACTIVAR</strong>.</div>
                        @elseif($wp_estado_api == 1)
                            <div class="alert alert-danger">Ha pasado más de 7 días desde tu ultimo mensaje, los mensajes estan desactivados, y debes enviar un nuevo mensaje al número <strong>+34 634307339</strong> de activación con la palabra <strong>ACTIVAR</strong>, debido a las limitaciones de nuestro proveedor.</div>
                        @elseif($wp_estado_api == 2)
                            <div class="alert alert-success">El servicio de mensajería esta activo.</div>
                        @endif 
                                           
                    </div>
                    <br/>
                @endif 
            	<label class="col-md-5 control-label">Servicio activo</label>
            	<input type="hidden" name="return" value="{{ $return }}"/>
            	<input type="hidden" name="accion" value="{{ $accion }}"/>
            	<div class="col-sm-3">  
              		<input type="checkbox" id="number-toggle-wp" 
                     	name="numero-toggle-wp" 
                     	value="1" {{ $wp_activo ? ' checked' : '' }}>
            	</div>         		
            </div>
        {{ Form::close() }}
      </div>
      @endif
      <div class="panel-body">
        {{ Form::open(array(
          'action' => 'WhatsAppController@setWhatsAppNumber',
          'class'  => 'form-horizontal',
          'id'     => 'form-numero-wp'
        )) }}
		<div class="form-group">
          	<label for="numero-wp" class="col-md-5 control-label">Número whatsapp</label>
          	<input type="hidden" name="return" value="{{ $return }}"/>
          	<input type="hidden" name="accion" value="{{ $accion }}"/>
          	<div class="col-md-6">
            	<input id="numero-wp" class="form-control" type="text" name="numero-wp" value="{{ (isset($wp) && $wp != '' )? '+'.$wp : '' }}" required/>
            	<input id="phone-full" type="hidden" name="phone-full"/>
            	<span id="valid-msg" class="hide">✓ Válido</span>
				<span id="error-msg" class="hide">Inválido</span>
          	</div>            
        </div>
        
        
        <input type="button" id="btn_submit" class="btn btn-primary pull-right" value="Guardar" />
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
<link rel="stylesheet" href="{{ asset('intl-tel-input-master/build/css/intlTelInput.css') }}">
<script src="{{ asset('intl-tel-input-master/build/js/intlTelInput.js') }}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#number-toggle-wp').bootstrapSwitch({
    onText: 'SÍ',
    offText: 'NO',
    onSwitchChange: function(event, state) {
      $('#form-number-toggle-wp').submit();
    }
  });      
});

	var telInput = $("#numero-wp"),
  		errorMsg = $("#error-msg"),
  		validMsg = $("#valid-msg");

	// initialise plugin
	telInput.intlTelInput({
  		utilsScript: "{{ asset('intl-tel-input-master/libs/libphonenumber/build/utils.js') }}"
	});

	var reset = function() {
  		telInput.removeClass("error");
  		errorMsg.addClass("hide");
  		validMsg.addClass("hide");
	};

	// on blur: validate
	telInput.blur(function()
	{
	  
	});

	// on keyup / change flag: reset
	telInput.on("keyup change", reset);
	
	$("#btn_submit").click(function(evento) {
	  	
		reset();
		if ($.trim(telInput.val()))
		{
			if (telInput.intlTelInput("isValidNumber"))
			{
				$("#phone-full").val(telInput.intlTelInput("getNumber"));
				$("#numero-wp").val(telInput.intlTelInput("getNumber"));
				validMsg.removeClass("hide");
				$("#form-numero-wp").submit();
			}
			else
			{
				telInput.addClass("error");
				errorMsg.removeClass("hide");
				evento.preventDefault();
			}
		}		
		
	});	
</script>