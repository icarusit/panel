<div class="col-md-5">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Lista negra</h3>
    </div>
    <div class="panel-body">        
      <table class="table table-striped table-bordered table-hover">
        @if($blacklist !== FALSE)
          @foreach($blacklist as $number)
          <tr>
            <td>{{ $number }} <a href="{{ URL::to('blacklist/remove') . '/' . $number }}"
                                 class="remove" title="Eliminar número de la lista negra">
                <span class="glyphicon glyphicon-remove"></span></a>
            </td>
          </tr>        
          @endforeach    
        @endif
        <tr class="row-button-add-new">
          <td>
            <a href="#" title="Agrega un número a la lista negra del número">
              <span class="glyphicon glyphicon-plus-sign plus-white-blacklist"></span>          
            </a>
            @if($blacklist === FALSE)
            <span>No tienes números en tu lista negra, ¡<a href="#">agrega el primero</a>!</span>
            @endif
          </td>
        </tr>
        <tr class="row-add-new">
          <td>
            <p>Agrega un número a la lista negra del número</p>
            {{ Form::open(array(
              'action' => 'WhiteBlackListController@addToBlacklist',
              'class'  => 'form-inline'
            )) }}
              <div class="form-group">
                <label class="sr-only">Número</label>
                <input type="text" class="form-control" name="number" id="new-number-blacklist" placeholder="Introduce el número">
              </div>
              <button type="submit" title="Guardar" class="btn btn-default"><span class="glyphicon glyphicon-floppy-disk"></span></button>            
              <a href="#" title="Cancelar" class="btn btn-default"><span class="glyphicon glyphicon-ban-circle"></span></a>                        
            {{ Form::close() }}
          </td>
        </tr>      
      </table>
    </div>
  </div>
</div>  