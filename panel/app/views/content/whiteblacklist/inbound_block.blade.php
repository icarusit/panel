<div class="col-md-10">
  <div class="panel panel-default">
    <div class="panel-heading" id="block_inbound_calls">
      <h3 class="panel-title">Bloqueo de llamadas entrantes</h3>
    </div>
    <div class="panel-body">
      {{ Form::open(array(
        'action' => 'WhiteBlackListController@settings'
      )) }}
      <div class="checkbox">
        <label>
          <input type="hidden" name="block_international_inbound_calls" value="false">
          <input type="checkbox" name="block_international_inbound_calls" 
          value="true"{{ ($settings !== NULL && $settings->block_international_inbound_calls) ? ' checked ' : '' }}>
          Bloquear llamadas desde números <strong>internacionales</strong>
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="hidden" name="block_mobile_inbound_calls" value="false">            
          <input type="checkbox" name="block_mobile_inbound_calls"
          value="true"{{ ($settings !== NULL && $settings->block_mobile_inbound_calls) ? ' checked ' : '' }}>
          Bloquear llamadas desde números <strong>móviles</strong>
        </label>
      </div>  
      <div class="checkbox">
        <label>
          <input type="hidden" name="block_anonymous_inbound_calls" value="false">
          <input type="checkbox" name="block_anonymous_inbound_calls"
          value="true"{{ ($settings !== NULL && $settings->block_anonymous_inbound_calls) ? ' checked ' : '' }}>
          Bloquear llamadas desde números <strong>anónimos</strong>
        </label>
      </div>

      <div class="form-group margin-15">
        <input type="submit" class="btn btn-primary" value="Guardar preferencias" />  
      </div>  

      {{ Form::close() }}
    </div>
  </div>
</div>