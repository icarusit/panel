@extends('main')
@section('subpage')

<h1><span class="glyphicon glyphicon-ban-circle"></span> Lista blanca/negra</h1>

@include('help', array('content' => 'whiteblacklist'))
@include('errors')

<div class="row">
  @include('content.whiteblacklist.whitelist')
  @include('content.whiteblacklist.blacklist')  
</div>

<div class="row">
  @include('content.whiteblacklist.inbound_block')
</div>

<script type="text/javascript">
$('.row-button-add-new').click(function() {
  $(this).hide();
  $('~ .row-add-new', this).show();
});

$('.row-add-new a').click(function() {
  $tr = $(this).parents('tr');
  $tr.siblings('.row-button-add-new').show();
  $tr.hide();
});

$('.remove').click(function() {
  return confirm("Se eliminará el número de la lista. ¿Quieres continuar?");
});
</script>
@stop
