@extends('main')

@section('subpage')
<h1><span class="fa fa-frown-o"></span> Oops, no hemos encontrado la página que buscas</h1>

<p>Probablemente hayamos cambiado la dirección y se nos ha olvidado cambiar
  algún botón que enlazaba a la página.</p>

<p>Hemos registrado el evento y nos pondremos manos a la obra tan pronto como podamos.</p>

<p>Mientras tanto, por favor llámanos al <strong>800 00 77 66</strong> o
  escríbenos un correo a <a href="mailto:soporte@fmeuropa.com">soporte@fmeuropa.com</a>.</p>

<p>Disculpa las molestias.</p>

@stop
