@extends('main')

@section('subpage')
<h1><span class="fa fa-frown-o"></span> Algo no ha funcionado como debería...</h1>

<p>Hemos registrado el error y trabajaremos en él para que no te vuelva a ocurrir.</p>

<p>Mientras tanto, llámanos por favor al <strong>800 00 77 66</strong> o escríbenos
  un correo a <a href="mailto:soporte@fmeuropa.com">soporte@fmeuropa.com</a>.</p>

<p>Disculpa las molestias.</p>

@stop
