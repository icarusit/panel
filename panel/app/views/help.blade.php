@if(!Session::get('is_mobile'))
<div class="frame frame-collapsable frame-info">
  <a href="#" class="toggle-show-help"><span class="glyphicon glyphicon-info-sign"></span> <span class="toggle-text"></span></a>

  <div class="frame-content">
    @include('help/' . $content)
  </div>
</div>
@endif