<p>Puedes <strong>Recargar Saldo</strong> desde tu panel de usuario, eliges tarjeta o paypal.</p>

<img src="{{ asset('img/help/factura/recargar-saldo.png') }}" />

<p>Tu saldo actual es de: <strong>{{ Session::get('user')->getBalance() }} &euro;</strong></p>