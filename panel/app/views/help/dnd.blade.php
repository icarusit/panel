<p>Con el servicio 'No molestar' puedes bloquear todas las llamadas entrantes, sin importar la configuración que tengas hecha en el número.
Esto puede resultarte útil si en algún momento necesitas que no te suene el teléfono.
Una vez activado el modo 'No molestar', todas las personas que te llamen oirán la señal de comunicando.
Para volver a recibir llamadas con normalidad solo desactiva el servicio.</p>
<img src="{{ asset('img/help/numero/dnd.png') }}" width="100%" />