<p>Las tarifas planas o bonos de minutos te permiten ahorrar hasta un 15% en tus llamadas salientes.</p>

<p>El funcionamiento es sencillo. <a href="{{ URL::to('flatrates/order') }}">Compra un bono de minutos</a> y de forma automática será renovado mes a mes.</p>

<p>La baja es tan sencilla como el alta, sencillamente pulsa el botón de "Dar de baja" en cualquiera de las tarifas planas que tienes contratadas y la baja será inmediata.</p>

<p>¿Buscas una tarifa plana que no está contemplada aquí? <a href="mailto:info@<?php echo Session::get('dominio'); ?>">Escríbenos a info@<?php echo Session::get('dominio'); ?></a>.</p>