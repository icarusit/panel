<p>Desde aquí puedes configurar adónde desviamos la llamada una vez que nos 
  entra en tu número.</p>

<p>Los desvíos secundarios se ejecutarán tras el desvío principal en cascada.
  Es decir, se irán ejecutando uno a uno tras un pequeño tiempo de espera
  entre ellos hasta que uno de ellos descuelgue.</p>

<p>Los desvíos pueden ser a un número de teléfono o cuenta SIP.</p>

<p>Si desvías a números de teléfonos, ten en cuenta que el formato de los
  números debe ser el siguiente:</p>

<ul> 
  <li><strong>Teléfono fijo nacional:</strong> 9XXXXXXXX/8XXXXXXXX</li>
  <li><strong>Teléfono móvil nacional:</strong> 6XXXXXXXX/7XXXXXXXX</li>
  <li><strong>Teléfono internacional:</strong> 00XXXXX...</li>
</ul>

<p>Ten en cuenta que los desvíos a móvil y teléfonos internacionales tienen
  un coste por minuto. <a href="https://panel.fmeuropa.com/rates">Consulta las tarifas</a>.</p>    

<p>Para desviar a una cuenta SIP, haz click en el desplegable a la izquierda
  que pone 'Teléfono' y selecciona 'SIP'. Acto seguido, se mostrará a la derecha
  un desplegable con las cuentas SIP que tienes <a href="https://fmeuropa.com/lineas-ip-sip-voip/"
  target="_blank">contratadas con <?php echo Session::get('distribuidor'); ?></a>.</p>
  
<p>Puedes añadir tantos desvíos como quieras, sólo ten en cuenta que la
  operadora desde que se llame puede dar la llamada por no contestada si
  pasa mucho tiempo dando el tono de llamada.</p>
