<p>Desde aquí podrás configurar acciones que se realizarán en <strong>periodos de vacaciones o días festivos puntuales</strong>.<br/><strong>Para los festivos puntuales</strong>, tienes que añadir si son varios días por ejemplo 5 días, Desde y Hasta, si simplemente es un día sólo tienes que escribir la fecha Desde.</p>
<img src="{{ asset('img/help/numero/festivos-1.png') }}" width="100%" />
<br/>
<img src="{{ asset('img/help/numero/festivos-2.png') }}" width="100%" />
<p><strong>Para festivos nacionales</strong>. Nosotros nos encargamos de mantener el calendario de festivos nacionales actualizado cada año, así lo único que tienes que hacer es configurar una acción a realizar común para todos los días festivos nacionales del año.</p>
<img src="{{ asset('img/help/numero/festivos-3.png') }}" width="100%" />