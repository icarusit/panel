<p>Desde aquí puedes consultar tus facturas de estos últimos doce meses.</p>

<p>Si no tienes el recibo de la cuota domiciliado, también podrás hacer el pago
  desde esta misma página.</p>

<p>Si tienes alguna factura pendiente te aparecerá en la última columna de la
  derecha la opción de abonarla con tarjeta.</p>
  <img src="{{ asset('img/help/factura/mis-facturas.png') }}" />