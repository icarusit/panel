<p>Con este servicio recibirás un aviso en la dirección de e-mail que configures cada vez que recibas una llamada en tu número y no haya sido contestada, controlando así a qué números no has podido atender.
<br/>
Una vez activado el servicio se te enviarán automáticamente tanto avisos como llamadas que no hayan sido contestadas, por tanto es posible que recibas una gran cantidad de e-mails si lo mantienes activado en periodos largos que no te sea posible contestar llamadas.</p>
<img src="{{ asset('img/help/numero/missedcalls.png') }}" width="100%" />