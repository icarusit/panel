<p>Este servicio te permite añadir números con los que podrás hacer llamadas salientes desde tu número <strong>{{ $number }}</strong>.</p>


<p><strong>¿Cómo funciona?</strong> Simplemente añade un número de origen a la lista de orígenes permitidos y llama al número <strong>918299393</strong> desde el teléfono que hayas añadido a la lista. Oirás un tono continuo como cuando descuelgas una línea fija. Marca el número al que quieras llamar y eso es todo.</p> 

<p>Este servicio funciona con el saldo de tu cuenta. Debes <a href="{{ URL::to('balance') }}">recargar saldo</a> para hacer llamadas. El coste total de la llamada será lo que te cobre la operadora del teléfono de origen para llamadas locales-interprovinciales más el precio de la llamada que haces con nosotros. <a href="{{ URL::to('rates') }}">Consulta nuestras tarifas</a>.</p>

<p>Sólo permitimos hacer llamadas salientes a fijos y móviles nacionales (en España). Si necesitas este servicio para llamadas internacionales, <a href="mailto:info@<?php echo Session::get('dominio'); ?>">contacta con nosotros</a>.</p>
