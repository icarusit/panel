<p>Desde aquí podrás consultar o descargar tus tarifas para hacer llamadas o desvíos.</p>

<p>Además te proporcionamos una herramienta para calcular el precio de la llamada
  introduciendo el número tal y como lo harías en tu teléfono VoIP (p.e.: 0044202787871, 976301011, 666000000)
  te aparecerán los datos del destino y coste de la llamada justo debajo.</p>