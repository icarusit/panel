<p>Tus llamadas normalmente se desviarán siempre a los desvíos que configures desde este panel y en su defecto al desvío principal, te ofrecemos también la opción de que durante un <strong>horario determinado</strong>, se ejecute una acción que puede ser reproducir una locución, desviar a un buzón o número teléfono, etc.</p>
<img src="{{ asset('img/help/numero/horarios-1.png') }}" width="60%" />
<p>Por ejemplo puedes configurarlo para que todos los Miércoles de 09.00 a 10.00 salga una locución, tienes que elegir el día, la hora, la acción y guardar. Lo que se programa son las excepciones.</p>
<img src="{{ asset('img/help/numero/horarios-2.png') }}" width="70%" />
<p>Si quieres que sea para un día completo, lo tienes que configurar de la siguiente manera</p>
<img src="{{ asset('img/help/numero/horarios-3.png') }}" width="100%" />
<p>Si, por ejemplo quieres configurar el Buzón de Voz en el horario en el que no estas en la oficina y tu horario es de 8 a 17h tienes que configurarlo de esta manera
Desde las 00:00 hasta las 08:00h y
Desde las 17:00 hasta las 23:00h
Una vez configurado te aparecerá en tus Horarios Configurados, si has configurado para un día concreto de la semana, puedes Repetir para el resto de los días de la semana</p>
<img src="{{ asset('img/help/numero/horarios-4.png') }}" width="100%" />