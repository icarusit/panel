<p>Te aparecen todas tus cuentas SIP, así como el estado de las mismas. En verde cuáles están registradas para recibir llamadas y en rojo cuáles no. Puedes añadir o eliminar cuentas.<br/>
También puedes descargar Zoiper, un software gratuito para usar tu cuenta sip, ya preconfigurado haciendo clic en el icono de cada cuenta.</p>
<img src="{{ asset('img/help/numero/sipaccounts.png') }}" width="100%" />
