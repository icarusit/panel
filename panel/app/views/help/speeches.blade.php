<p>La configuración de locuciones</p>
<img src="{{ asset('img/help/numero/speeches-1.png') }}" width="100%" />
<p>Puedes grabar las distintas locuciones, para locución general, para locución de buzón de voz, locución para menú. Puedes subir este locución si ya la tienes grabada, grabarla o subir un texto. Las locuciones pueden estar en formato WAV o MP3 y no te ponemos límite en el número de locuciones a subir</p>
<img src="{{ asset('img/help/numero/speeches-2.png') }}" width="100%" />
<br/>
<img src="{{ asset('img/help/numero/speeches-3.png') }}" width="100%" />
<br/>
<img src="{{ asset('img/help/numero/speeches-4.png') }}" width="100%" />
<p>Configurar tus locuciones, puedes activar o desactivar y elegir cada tipo de locución y el listado de las mismas para cada uno de tus números de teléfono.</p>
<img src="{{ asset('img/help/numero/speeches-5.png') }}" width="100%" />
<br/>
<img src="{{ asset('img/help/numero/speeches-6.png') }}" width="100%" />