<p>En Estadísticas puedes ver un pequeño resumen con las últimas llamadas, la suma del día, de la semana y del mes. En Detalle Llamadas puedes ver todas tus llamadas en forma de listado, mostrando la fecha/hora, el origen, el destino, la duración y el coste o ingreso por cada llamada. Además tienes un buscador donde puedes buscar por origen, destino, fecha, etc</p>
<img src="{{ asset('img/help/numero/stats-1.png') }}" width="100%" />
<br/>
<img src="{{ asset('img/help/numero/stats-2.png') }}" width="100%" />
<br/>
<p>En Estadísticas también tienes un Buscador de llamadas</p>
<img src="{{ asset('img/help/numero/stats-3.png') }}" width="100%" />