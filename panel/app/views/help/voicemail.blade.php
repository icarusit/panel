<p>Te aparecen dos opciones <strong>Bandeja de Entrada y Configuración</strong>. En este pestaña podrás descargar, escuchar o eliminar los mensajes de voz que has recibido, en los últimos quince días.</p>
<img src="{{ asset('img/help/numero/voicemail-1.png') }}" width="100%" />
<p><strong>Configuración</strong><br/>Desde esta pestaña podrás elegir una locución para el buzón general, activar/desactivar el servicio y modificar la dirección de correo donde quieres que se te envie los mensajes de voz.
Al activar el buzón de voz general, saltará el contestador en los siguientes casos:
Cuando sólo exista un teléfono de desvío y no se conteste la llamada.
Cuando existan varios teléfonos de desvíos (salto de llamada de uno a otro) y el último no conteste la llamada.
En desvío por provincias cuando cualquiera de los números no conteste la llamada.
En resumen, funcionaría como cualquier contestador físico. La llamada se envía y si no se contesta salta el contestador.
Elige la locución que quieres que se oiga en el menú desplegable.</p>
<img src="{{ asset('img/help/numero/voicemail-2.png') }}" width="100%" />
