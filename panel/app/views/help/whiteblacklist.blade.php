<p><strong>Lista blanca/negra</strong> en la <strong>lista blanca</strong>, si añades números sólo esos números te podrán llamar, en la <strong>lista negra</strong> son números que no te podrán llamar. La <strong>lista blanca</strong> tiene prioridad sobre la negra, de tal forma que si hay números en ambas listas, la negra no es válida. También puedes bloquear o desbloquear llamadas de fuera del país, móviles o anónimos.</p>
<img src="{{ asset('img/help/numero/listas-1.png') }}" width="100%" />
<p>Para <strong>añadir un número en la lista blanca o en la lista negra</strong> simplemente tienes que ir a la lista respectiva e ir añadiendo cada uno.
Para bloquear</p>
<img src="{{ asset('img/help/numero/listas-2.png') }}" width="100%" />
<p>Para <strong>bloquear números</strong> internacionales, móviles o anónimos en la parte de abajo en Bloquear números.</p>
<img src="{{ asset('img/help/numero/listas-2.png') }}" width="100%" />