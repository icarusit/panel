<?php 
if($distribuidor !== NULL){
	$dominio = explode(".",$distribuidor[0]->dominio);
	Session::put('id_distribuidor', $distribuidor[0]->id);
	Session::put('logo', $distribuidor[0]->logo);
	Session::put('distribuidor', $distribuidor[0]->nombre);
	Session::put('dominio', $dominio[1].".".$dominio[2]);
	Session::put('telefono', $distribuidor[0]->telefono);
	if(Session::get('dominio') != 'fmeuropa.com'){
		Session::put('ocultar','si');
	}else{
		Session::put('ocultar','no');
	}
	Session::put('css',$distribuidor[0]->css);
	Session::put('dominio_master',$distribuidor[0]->dominio_master);
	Session::put('prefijo',$dominio[1]);
}else{
	Session::put('id_distribuidor', 1);
	Session::put('logo', 'logo-in-logo.png');
	Session::put('distribuidor', 'Flash Telecom');
	Session::put('dominio', 'fmeuropa.com');
	Session::put('telefono', '800007766');
	Session::put('logo-alta','logo-web.png');
	Session::put('ocultar','no');
	Session::put('css','common.css');
	Session::put('dominio_master','http://www.fmeuropa.com');
	Session::put('prefijo','');
}
$css = Session::get('css');
$prefijo = Session::get('prefijo');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ES">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">

    <title><?php echo Session::get('distribuidor'); ?></title>

    <link rel="icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />

    <link rel="shortcut icon" href="{{ asset('img/'.$prefijo.'favicon.ico') }}?{{ filemtime(public_path() . '/img/'.$prefijo.'favicon.ico') }}">      
    
    <link media="all" href="{{ asset('css/bootstrap.css') }}?{{ filemtime(public_path() . '/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/' . $css) }}?{{ filemtime(public_path() . '/css/' . $css) }}" rel="stylesheet" type="text/css">
    <link media="all and (min-width: 768px)" href="{{ asset('css/screen.css') }}?{{ filemtime(public_path() . '/css/screen.css') }}" rel="stylesheet" type="text/css">
    
    <script type="text/javascript" src="{{ asset('js/jquery-1.9.1.min.js') }}?{{ filemtime(public_path() . '/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript">    
    @if(!(Session::get('is_mobile')) && Session::get('id_distribuidor') == 1)      
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
    $.src='//v2.zopim.com/?z3vnvWNM1LX5TAXWUyJBLzLAoXX2mOv3';z.t=+new Date;$.
    type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

    @if(!is_null($user = Session::get('user', NULL)) && Session::get('id_distribuidor') == 1)
    $zopim(function() {
      $zopim.livechat.setName('{{ $user->getUsername() }}');
      $zopim.livechat.setEmail('{{ $user->getEmail() }}');
      $zopim.livechat.setNotes('ID cliente: {{ $user->getID() }}');          
    });
    @endif
    @endif
    </script>
  </head>

  <body style="background-color: #eaeaea;">
    <div class="container container-log-in">
    	<?php if($distribuidor !== NULL){
		?>
      	<div style="width:258px; height:31px; margin: 20px 0 40px;"><img src="img/<?php echo Session::get('logo'); ?>"></div>
        <?php }else{?>
		<div class="log-in-logo"></div>
		<?php }
		?>
      <!-- precarga -->
      <img src="{{ asset('img/loading-16-white.gif') }}" style="display: none;">
        
      @if(($info = Session::get('info', NULL)) !== NULL)
      <div class="alert alert-info">{{ $info }}</div>
      @endif      
      
      @if(($err = Session::get('error', NULL)) !== NULL)
      <div class="alert alert-danger">{{ $err }}</div>
      @endif
            
      {{ Form::open(array(
          'action' => 'HomeController@postLogin',
          'method' => 'post',
          'id'     => 'form-login'
      )) }}

      @if($redirect !== NULL)
      <input type="hidden" value="{{ $redirect }}" name="redirect">
      @endif

      <?php $err = (bool)($errors->first('user')); ?>
      <div class="form-group {{ $err ? 'has-error' : '' }}">
        @if ($err)
        <label class="control-label" for="user">{{ $errors->first('user') }}</label>
        @endif
        <input type="text" placeholder="Nombre de usuario" id="user" 
               name="user" class="form-control form-control-huge error" autocapitalize="off">
        <span class="input-icon fui-user"></span>
      </div>

      <?php $err = (bool)($errors->first('password')); ?>
      <div class="form-group {{ $err ? 'has-error' : '' }}">
        @if ($err)
        <label class="control-label" for="password">{{ $errors->first('password') }}</label>
        @endif
        <input type="password" placeholder="Contraseña" id="password" name="password" 
               class="form-control form-control-huge">
        <span class="input-icon fui-lock"></span>
      </div>        

      <div class="form-group">   
        <a href="#" class="btn btn-primary btn-login">Acceder</a>
        <input class="input-submit-login" type="submit" value="">
      </div>            
      
      {{ Form::close() }}
      
      <ul class="log-in-links">
        <li>
          <a href="{{ URL::to('forgot-password') }}">¿Olvidaste tu contraseña?</a>
        </li>
        <li>
          <a href="{{ URL::to('alta') }}">¿Aún no eres cliente?</a>
        </li>        
      </ul>
    </div>

    @include('footer_legal_links')
    
    <script>
    $(document).ready(function() {
      $('.btn-login').click(function() {
        $(this).addClass('disabled');  
        $(this).html('<img src="{{ asset('img/loading-16-white.gif') }}">');
        
        $('#form-login').submit();
      });
    })
    </script>
  </body>
</html>
