@extends('master')

@section('content')
    
  @include('top_bar') 
  
  <?php $selectedNumber = Session::get('number', NULL); ?>
  
  @if(isset($selectedNumber) && $selectedNumber !== NULL)
    <?php $type = $selectedNumber->getType(); ?>
    @include('sidebars/sidebar-' . $type)      
  @else
  	<?php $cuenta_gratuita = true; ?>
    @include('sidebars/sidebar')  
  @endif
  
  <div class="subpage col-md-9">
    @yield('subpage')
  </div>
@stop