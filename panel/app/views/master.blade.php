<?php
$css = Session::get('css');
$prefijo = Session::get('prefijo');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ES">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">

    <title>{{ isset($title) ? $title . ' - ' . Session::get('distribuidor') : Session::get('distribuidor') }}</title>

    <link rel="icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />

    <link rel="shortcut icon" href="{{ asset('img/'.$prefijo.'favicon.ico') }}?{{ filemtime(public_path() . '/img/'.$prefijo.'favicon.ico') }}">    
    
    <link media="all" href="{{ asset('css/bootstrap.css') }}?{{ filemtime(public_path() . '/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/selectize.bootstrap3.css') }}?{{ filemtime(public_path() . '/css/selectize.bootstrap3.css') }}" rel="stylesheet" type="text/css">            
    <link media="all" href="{{ asset('css/' . $css) }}?{{ filemtime(public_path() . '/css/' . $css) }}" rel="stylesheet" type="text/css">
    <link media="all and (min-width: 768px)" href="{{ asset('css/screen.css') }}?{{ filemtime(public_path() . '/css/screen.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/datepicker.css') }}?{{ filemtime(public_path() . '/css/datepicker.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/bootstrap-switch.min.css') }}?{{ filemtime(public_path() . '/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css">      
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <link media="all" href="{{ asset('css/jquery-ui.min.css') }}?{{ filemtime(public_path() . '/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"> 
    <link media="all" href="{{ asset('css/datatables/dataTables.bootstrap.css') }}?{{ filemtime(public_path() . '/css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/tour/bootstrap-tour.css') }}?{{ filemtime(public_path() . '/css/tour/bootstrap-tour.css') }}" rel="stylesheet" type="text/css">      

    @yield('additional-css')
      
    <script type="text/javascript" src="{{ asset('js/jquery-1.9.1.min.js') }}?{{ filemtime(public_path() . '/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}?{{ filemtime(public_path() . '/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/selectize.min.js') }}?{{ filemtime(public_path() . '/js/selectize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}?{{ filemtime(public_path() . '/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-switch.min.js') }}?{{ filemtime(public_path() . '/js/bootstrap-switch.min.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('js/bootstrap-hover-dropdown.min.js') }}?{{ filemtime(public_path() . '/js/bootstrap-hover-dropdown.min.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('js/jBeep.js') }}?{{ filemtime(public_path() . '/js/jBeep.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}?{{ filemtime(public_path() . '/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/common.js') }}?{{ filemtime(public_path() . '/js/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables/jquery.dataTables.js') }}?{{ filemtime(public_path() . '/js/datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables/dataTables.bootstrap.js') }}?{{ filemtime(public_path() . '/js/datatables/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables/dateFormatES.js') }}?{{ filemtime(public_path() . '/js/datatables/dateFormatES.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables/numHtml.js') }}?{{ filemtime(public_path() . '/js/datatables/numHtml.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tour/bootstrap-tour-standalone.js') }}?{{ filemtime(public_path() . '/js/tour/bootstrap-tour-standalone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts.js') }}?{{ filemtime(public_path() . '/js/scripts.js') }}"></script>

    @yield('additional-js')
    
    <script type="text/javascript">
    @if(Session::get('is_mobile') === false && Session::get('id_distribuidor') == 1)
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
    $.src='//v2.zopim.com/?z3vnvWNM1LX5TAXWUyJBLzLAoXX2mOv3';z.t=+new Date;$.
    type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

    @if(!is_null($user = Session::get('user', NULL)) && Session::get('id_distribuidor') == 1)
    $zopim(function() {
      $zopim.livechat.setName('{{ $user->getUsername() }}');
      $zopim.livechat.setEmail('{{ $user->getEmail() }}');
      $zopim.livechat.setNotes('ID cliente: {{ $user->getID() }}');          
    });
    @endif
    @endif
    </script>
    
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  	<link rel="manifest" href="{{ asset('manifest.json') }}?{{ filemtime(public_path() . '/manifest.json') }}">
  
  	<script>
    	var OneSignal = OneSignal || [];

    	OneSignal.push(["init", {path: "/", appId: "b6db0b28-8cfe-48df-8e1a-fac6b44b8a9b", autoRegister: true,
			welcomeNotification: {
				title: 'Notificaciones en tiempo real',
				message: 'Activado. Te enviaremos notificaciones de llamadas perdidas, buzon de voz,etc'
			}}]);
		 OneSignal.push(["getIdsAvailable", function(ids) {		
			
			jQuery.ajax({
			  method: "POST",
			  url: "{{ URL::to('setsubscriptor') }}",
			  data: {'id':ids.userId,'registration':ids.registrationId},
			  dataType: 'json',
			}).done(function( resultado ) {
				
			});
			
		  }]);
  	</script>
  </head>

  <body>
    @yield('content')

    <script type="text/javascript">
    $(document).ready(function() {      
      $.get("{{ URL::to('invoices/unpaid') }}", function(data) {
        if((numInvoices = parseInt(data)) > 0) {
          $('#alert-outstanding-invoices').html(data);
          $('#alert-outstanding-invoices').fadeIn();        
        }
      });
    });
    
    $(document).on('click', 'a[data-target=#action-selector-modal]', function(ev) {
      // http://stackoverflow.com/a/12513100     
      ev.preventDefault();
      $(this).button('loading');      
      
      var target = $(this).attr("href");
      var modalTrigger = $(this).data('modal-trigger');        
      var jsonAction = $('input[name="' + modalTrigger + '"]').val();
      var $that = $(this);

      $("#action-selector-modal").load(target, {
          jsonAction: jsonAction
        }, function() { 
          var modalTrigger = $that.data('modal-trigger');
          $('#modal-trigger').val(modalTrigger);
          $("#action-selector-modal").modal("show"); 
          $that.button('reset');
        }
      );                
    });

    $(document).on('click', '.action-switch', function() {
      $(this).parent().hide();
      $(this).parent().siblings('.selector-select').show();
      return false;
    });

    $(document).on('change', '.action-type', function() {
      var $configButton = $(this).siblings('a.action-config-button');

      var public_action = $(':selected', this).html();
      var action = $(this).val();
      
      if($configButton.hasClass('disabled')) {
        $configButton.removeClass('disabled');
      }

      $configButton.prop('href', '{{ url() }}/component/actions.modals.' + action);

      $('#action-selector-modal-title .label').html($(':selected', this).html());        
    });

    $(document).on('keypress', '.phone-number', function(event) {
      if(!$.isNumeric(String.fromCharCode(event.which))) {
        event.preventDefault();
      } 
    });
    
    $(document).on('keyup', '.phone-number', function() {

		//Expresion regular de patrones que no debe tener el numero
      	var re = /^(?:8|9)[1-9][0-9]{7}$|^6[0-9]{8}$|^7[1-9][0-9]{7}$|^00\d+$/;
		
		var error_prefijo = false;
		
		<?php
			$prefijos_no = Config::get('prefijos.no_validos');
			$prefijos_no_re = "";
			foreach($prefijos_no as $keypn => $pn)
			{
				$prefijos_no[$keypn] = "^".$pn."\d+$";
			}
			$prefijos_no_re = implode('|',$prefijos_no);
			print 'var re_no = /'.$prefijos_no_re.'/;';
		?>
		
		
		if(re_no.test($(this).val()))
		{
			error_prefijo = true;  
		}
		else
		{
			if(!re.test($(this).val()))
		   	{			   
			  	error_prefijo = true; 
		   	}
		   	else
		   	{
			 	$(this).removeClass('input-text-error');
				$(this).addClass('input-text-success');
				var $that = $(this);
				setTimeout(function() {
				  $that.removeClass('input-text-success', 30);
				}, 1000);
				$(this).data('error', false);
				$(this).prop('title', '');
			}
		}
		  
		  
		 if(error_prefijo == true)
		 {        
			$(this).removeClass('input-text-success');
			$(this).addClass('input-text-error');
			$(this).data('error', true);
			$(this).prop('title', 'El número es inválido.');
		 } 
		   
    });

    $(document).on('keyup', '.email', function() {      
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(!re.test($(this).val()))
	  {
			if($(this).hasClass('norequired') && $(this).val() == '')
		  	{
				$(this).removeClass('input-text-error');
				$(this).addClass('input-text-success');
				$(this).data('error', false);
        		$(this).prop('title', '');
			  	return true;
		  	}
       	  	$(this).removeClass('input-text-success');
        	$(this).addClass('input-text-error');        
        	$(this).data('error', true);
        	$(this).prop('title', 'La dirección de e-mail no es válida.');
      }
	  else
	  {        
        $(this).removeClass('input-text-error');
        $(this).addClass('input-text-success');
        var $that = $(this);
        setTimeout(function() {
          $that.removeClass('input-text-success', 30);
        }, 1000);
        $(this).data('error', false);
        $(this).prop('title', '');
      }      
    });
    
    $(document).on('hidden.bs.modal', '.modal', function () {
      $(this).empty();
    });
    
    $(document).on('hide.bs.modal', '#modal-btn-save', function() {
      var error = false;
      
      $('.phone-number').each(function() {
        if($(this).data('error') === true) {
          alert('Por favor, revisa el formato de los números de teléfono introducidos. Alguno es erróneo.');
          error = true;
        }
      });
      
      return !error;
    });

    $(document).on('submit', function() {
      var error = false;
      
      $('.phone-number').each(function() {
        if($(this).data('error') === true) {
          alert('Por favor, revisa el formato de los desvíos introducidos. Alguno es erróneo.');
          error = true;
        }
      });

      $('.email').each(function() {
        if($(this).data('error') === true)
		{
          alert('Por favor, revisa el formato de las direcciones de correo ' + 
          'electrónico introducidas. Alguna es incorrecta.');
          error = true;
        }
      });

      return !error;
    });

    $(document).on('change', '.select-forward-type', function() {
      $sibling = $(this).parent().siblings('.form-group');
      
      if($(this).val() === 'sip-account') {
        $sibling.find('.phone-number').hide().prop('disabled', true);        
        $sibling.find('select.forward-sip-accounts').show().prop('disabled', false);
      } else {
        $sibling.find('.forward-sip-accounts').hide().prop('disabled', true);              
        $sibling.find('.phone-number').show().prop('disabled', false);
      }
    });

    /*
    $(document).on('submit', function() {
      if($('.selector').length > 0) {
        var error = false;

        $('.selector').each(function() {
          $actionType = $(this).find('.action-type');
          if($actionType.val() === null && $actionType.attr('disabled') !== 'disabled') {
            error = true;
          }
        });

        if(error) { alert("Por favor, revisa las acciones. Alguna se ha quedado sin configurar."); }

        return !error;
      }
    });
    */
   
    $('li.disabled > a:link').on('click', function(e) { 
      e.preventDefault(); 
    });
    
    $('.bs-switch').bootstrapSwitch({
      onText: 'SÍ',
      offText: 'NO'
    });    
    </script>          

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-49619351-2', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
