@extends('main')

@section('subpage')
<h1>Sin acceso avanzado</h1>

@if($plan === 'premium')
<p>El plan <strong>Premium</strong> que tienes contratado en tu número 
  (<strong>{{ $number }}</strong>) no dispone de acceso completo a la configuración
  avanzada del número.</p>
<p>Si quieres acceder a los servicios completos de Routing Manager online, puedes
  contratar el plan <strong>Plus</strong>.</p>
  
  <a href="{{ URL::to('change-plan') }}" class="btn btn-primary">Quiero cambiar mi plan</a>
@else
<?php
$auxNumber = Number::where('numero_visible', '=', $number)->first();
if($auxNumber->id_perfil == 200)
{
	print '<p>El número <strong>'.$number.'</strong> pertenece a una centralita.<br/>Debe configurar el número principal de la centralita y el resto clonarán esa configuración.</p>';
}
else
{
?>
<p>El plan contratado actualmente para el número elegido 
  (<strong>{{ $number }}</strong>) no dispone de acceso a los servicios 
  avanzados de configuración online.</p>
<p>Si quieres acceder a los servicios de Routing Manager online, puedes 
  contratar un plan <strong>Plus</strong> o <strong>Premium</strong> para este 
  número.</p>
  
  <a href="{{ URL::to('change-plan') }}" class="btn btn-primary">Quiero cambiar mi plan</a>
<?php
}
?>
@endif


@stop
