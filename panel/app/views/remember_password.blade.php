<?php
$css = Session::get('css');
$prefijo = Session::get('prefijo');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-ES">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">

    <title>Recordar tu contraseña - <?php echo Session::get('distribuidor'); ?></title>

    <link rel="icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/'.$prefijo.'logo-icon-72.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/'.$prefijo.'logo-icon-114.png') }}" />

    <link rel="shortcut icon" href="{{ asset('img/'.$prefijo.'favicon.ico') }}?{{ filemtime(public_path() . '/img/'.$prefijo.'favicon.ico') }}">    
    
    <link media="all" href="{{ asset('css/bootstrap.css') }}?{{ filemtime(public_path() . '/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link media="all" href="{{ asset('css/' . $css) }}?{{ filemtime(public_path() . '/css/' . $css) }}" rel="stylesheet" type="text/css">
    <link media="all and (min-width: 768px)" href="{{ asset('css/screen.css') }}?{{ filemtime(public_path() . '/css/screen.css') }}" rel="stylesheet" type="text/css">
    
    <script type="text/javascript" src="{{ asset('js/jquery-1.9.1.min.js') }}?{{ filemtime(public_path() . '/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript">    
    @if(!(Session::get('is_mobile')))      
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
    $.src='//v2.zopim.com/?z3vnvWNM1LX5TAXWUyJBLzLAoXX2mOv3';z.t=+new Date;$.
    type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

    @if(!is_null($user = Session::get('user', NULL)))
    $zopim(function() {
      $zopim.livechat.setName('{{ $user->getUsername() }}');
      $zopim.livechat.setEmail('{{ $user->getEmail() }}');
      $zopim.livechat.setNotes('ID cliente: {{ $user->getID() }}');          
    });
    @endif
    @endif
    </script>
  </head>

  <body style="background-color: #eaeaea;">
    <div class="container container-log-in">
      <?php if(Session::get('distribuidor') !== NULL){
		?>
      	<div style="width:258px; height:31px; margin: 20px 0 40px;"><img src="img/<?php echo Session::get('logo'); ?>"></div>
        <?php }else{?>
		<div class="log-in-logo"></div>
		<?php }
		?>
      
      <!-- precarga -->
      <img src="{{ asset('img/loading-16-white.gif') }}" style="display: none;">
        
      @if(($info = Session::get('info', NULL)) !== NULL)
      <div class="alert alert-info">{{ $info }}</div>
      @endif      
      
      @if(($err = Session::get('error', NULL)) !== NULL)
      <div class="alert alert-danger">{{ $err }}</div>
      @endif
      
      @if($info === NULL && $err === NULL)
      
      {{ Form::open(array(
          'action' => 'UserController@postForgotPassword',
          'method' => 'post',
          'id'     => 'form-forgot-password'
      )) }}

      <div class="alert alert-info">
        <p>Escribe el e-mail que usaste para el alta y te enviaremos instrucciones
        para generar una nueva contraseña.</p>        
      </div>
      
      <?php $err = (bool)($errors->first('email')); ?>
      <div class="form-group {{ $err ? 'has-error' : '' }}">
        @if ($err)
        <label class="control-label" for="email">{{ $errors->first('email') }}</label>
        @endif
        <input type="text" placeholder="Tu correo electrónico" id="email" 
               name="email" class="form-control form-control-huge error email" autocapitalize="off">
        <span class="input-icon fui-user"></span>
      </div>

      <div class="form-group">
        <input class="btn btn-primary btn-login" type="submit" value="Generar nueva contraseña">
      </div>            
      
      {{ Form::close() }}
      
      @endif
      
      <ul class="log-in-links">
        <li>
          <a href="{{ URL::to('/') }}">Volver</a>
        </li>        
      </ul>
    </div>
    
    @include('footer_legal_links')
    
  </body>
</html>
