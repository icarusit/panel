@extends('sidebars/sidebar')

@section('sidebar-number')
<p>Configuración <strong>{{ Session::get('number')->getPrettyNumber() }}</strong> <span class="label label-default">VOZ</span></p>
<li<?=Request::is('summary*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('summary')?>">
    <span class="glyphicon glyphicon-list-alt"></span>Resumen
  </a>
</li>
<li<?=Request::is('whiteblacklist*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('whiteblacklist')?>">
    <span class="glyphicon glyphicon-ban-circle"></span>Lista blanca/negra
  </a>
</li>
<li<?=Request::is('forwards*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('forwards')?>">
    <span class="glyphicon glyphicon-share-alt"></span>Desvíos
  </a>
</li>
<li<?=Request::is('holidays*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('holidays')?>">
    <span class="glyphicon glyphicon-calendar"></span>Festivos y vacaciones
  </a>
</li>
<li<?=Request::is('schedules*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('schedules')?>">
    <span class="glyphicon glyphicon-time"></span>Horarios
  </a>
</li>
<li<?=Request::is('origin*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('origin')?>">
    <span class="glyphicon glyphicon-globe"></span>Provincias
  </a>
</li>
<li<?=Request::is('voicemail*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('voicemail')?>">
    <span class="glyphicon glyphicon-envelope"></span>Buzón de voz
  </a>
</li>
<li<?=Request::is('speeches*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('speeches')?>">
    <span class="glyphicon glyphicon-volume-up"></span>Locuciones
  </a>
</li>
<li<?=Request::is('outgoing-calls*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('outgoing-calls')?>">
    <span class="glyphicon glyphicon-phone-alt"></span>Llamadas salientes
  </a>
</li>
<li<?=Request::is('missed-calls*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('missed-calls')?>">
    <span class="glyphicon glyphicon-inbox"></span>Llamadas perdidas
  </a>
</li>
<li<?=Request::is('stats*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('stats')?>">
    <span class="glyphicon glyphicon-stats"></span>Estadísticas
  </a>
</li>
<li<?=Request::is('callrecording*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('callrecording')?>">
    <span class="glyphicon glyphicon-play"></span>Grabación de llamadas
  </a>
</li>
<li<?=Request::is('dnd*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('dnd')?>">
    <span class="glyphicon glyphicon-minus-sign"></span>No molestar
  </a>
</li>
<li<?=Request::is('menus*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('menus')?>">
    <span class="glyphicon glyphicon-th"></span>Menús
  </a>
</li>
<li<?=Request::is('hooks*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('hooks')?>">
    <span class="glyphicon glyphicon-link"></span>Hooks
  </a>
</li>
<li<?=Request::is('misc*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('misc')?>">
    <span class="glyphicon glyphicon-plus"></span>Más opciones
  </a>
</li>

@yield('sidebar-voip')
@include('sidebars/sidebar-common')

@stop
