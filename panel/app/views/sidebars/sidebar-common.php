<li<?=Request::is('contact-agenda*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('contact-agenda')?>">
    <span class="glyphicon glyphicon-user"></span>Agenda
    <span class="label label-success label-sidebar-new-feature" title="Novedad en el panel">N</span>
  </a>
</li>
<li<?=Request::is('cancel*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('cancel')?>">
    <span class="fa fa-frown-o"></span>Baja del servicio
  </a>
</li>