@extends('sidebars/sidebar')

@section('sidebar-number')
<p>Configuración <strong>{{ Session::get('number')->getPrettyNumber() }}</strong> <span class="label label-default">FAX</span></p>
<li<?=Request::is('summary*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('summary')?>">
    <span class="glyphicon glyphicon-list-alt"></span>Resumen
  </a>
</li>
<li<?=Request::is('fax/received*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('fax/received')?>">
    <span class="glyphicon glyphicon-download"></span>Recibidos
  </a>
</li>
<li<?=Request::is('fax/sent*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('fax/sent')?>">
    <span class="glyphicon glyphicon-upload"></span>Enviados
  </a>
</li>
<li<?=Request::is('fax/config*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('fax/config')?>">
    <span class="glyphicon glyphicon-cog"></span>Configuración
  </a>
</li>
@include('sidebars/sidebar-common')
@stop
