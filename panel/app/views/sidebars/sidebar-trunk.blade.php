@extends('sidebars/sidebar')

@section('sidebar-number')
<p>Configuración <strong>{{ Session::get('number')->getPrettyNumber() }}</strong> <span class="label label-default">Trunk</span></p>
<li<?=Request::is('trunk*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('trunk')?>">
    <span class="glyphicon glyphicon-sort"></span>Trunk
  </a>
</li>
<li<?=Request::is('rates*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('rates')?>">
    <i class="fa fa-money"></i>Tarifas
  </a>
</li>
<li<?=Request::is('flatrates*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('flatrates')?>">
    <i class="fa fa-sort-numeric-asc"></i>Tarifas planas
  </a>
</li>
<li<?=Request::is('stats*') ? ' class="active"' : ''?>>
  <a href="<?=URL::to('stats')?>">
    <span class="glyphicon glyphicon-stats"></span>Estadísticas
  </a>
</li>
@include('sidebars/sidebar-common')
@stop
