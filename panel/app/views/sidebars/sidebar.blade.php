<ul class="sidebar nav nav-pills nav-stacked">
  <p id="sidebar-title-my-account">Mi cuenta</p>
  <li<?=Request::is('balance*') ? ' class="active"' : ''?> id="sidebar-li-balance">
    <a href="<?=URL::to('balance')?>">
      <span class="glyphicon glyphicon-euro"></span>Saldo: <strong>{{ Session::get('user')->getBalance(); }} €</strong>
    </a>
  </li>
  <li<?=Request::is('me*') ? ' class="active"' : ''?> id="sidebar-li-me">
    <a href="<?=URL::to('me')?>">
      <span class="glyphicon glyphicon-user"></span>Mi perfil
    </a>
  </li>  
  <li id="sidebar-li-help">
    <a href="http://docs.fmeuropa.com">
      <span class="glyphicon glyphicon-question-sign"></span>Ayuda
    </a>
  </li>    
  <li id="sidebar-li-logout">
    <a href="<?=URL::to('logout')?>">
      <span class="glyphicon glyphicon-log-out"></span>Cerrar sesión
    </a>
  </li>    
  <p id="sidebar-title-admin">Administración</p>    
  <li<?=Request::is('numbers*') ? ' class="active"' : ''?> id="mis-numeros">
    <a href="<?=URL::to('numbers')?>">
      <span class="glyphicon glyphicon-phone-alt"></span>Mis números
    </a>
  </li>
  <?php if(Session::get('id_distribuidor') == 1){?>
  <li<?=Request::is('invoices*') ? ' class="active"' : ''?> id="mis-facturas">
    <a href="<?=URL::to('invoices')?>">
      <span class="glyphicon glyphicon-folder-open"></span>Mis facturas
      
      <span id="alert-outstanding-invoices" 
            class="badge"
            title="Tienes facturas pendientes de pago"></span>
    </a>
  </li>
  <li<?=Request::is('balance*') ? ' class="active"' : ''?> id="recargar-saldo">
    <a href="<?=URL::to('balance')?>">
      <span class="glyphicon glyphicon-euro"></span>Recargar saldo
    </a>
  </li>
  @if(isset($cuenta_gratuita) && $cuenta_gratuita == true)
  	<li<?=Request::is('free-stats*') ? ' class="active"' : ''?>>
      <a href="<?=URL::to('free-stats')?>">
        <span class="glyphicon glyphicon-list-alt"></span>Sip Stats
      </a>
    </li>
  @endif
  <?php }
  if(Session::get('master') == 'si'){
  ?>
  <li<?=Request::is('misclientes*') ? ' class="active"' : ''?> id="mis-clientes">
    <a href="<?=URL::to('misclientes')?>">
      <span class="glyphicon glyphicon-briefcase"></span>Mis Clientes
    </a>
  </li>
  <?php
  }
  	$usuarios_administradores = Config::get('administracion.administradores');
   	if(in_array(Session::get('user')->getID(),$usuarios_administradores))
   	{
	?>
       <li<?=Request::is('clientes*') ? ' class="active"' : ''?> id="mis-clientes">
        	<a href="<?=URL::to('clientes')?>">
          		<span class="glyphicon glyphicon-user"></span>Clientes
        	</a>
      </li>
      
        <li<?=Request::is('reporte-altas-anual*') ? ' class="active"' : ''?> id="reporte-altas">
            <a href="<?=URL::to('reporte-anual')?>">
              <span class="glyphicon glyphicon-stats"></span>Reporte Anual
            </a>
         </li>
	<?php
   	}
	if(Session::get('simulador-origen') && Session::get('simulador-origen') > 0)
	{
	?>
    	<li<?=Request::is('clientes*') ? ' class="active"' : ''?> id="mis-clientes">
        	<a href="<?php print URL::to('parar-simulacion/'.Session::get('simulador-origen')); ?>">
          		<span class="glyphicon glyphicon-stop"></span>Parar simulación
        	</a>
      	</li>
    <?php
	}
  	?>
  @yield('sidebar-number')
</ul>
