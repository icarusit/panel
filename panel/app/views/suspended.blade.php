@extends('main')

@section('subpage')
<h1>Tu número está temporalmente suspendido</h1>

<p>Por motivos administrativos el número <strong>{{ $number }}</strong> se
  encuentra temporalmente suspendido.</p>

<p>Ponte en contacto con nosotros si tienes alguna duda.</p>

<a href="{{ URL::to('invoices') }}" class="btn btn-primary">Consulta tus facturas</a>
@stop
