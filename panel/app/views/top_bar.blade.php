<?php
$logo = Session::get('logo');
?>
<nav class="top-bar navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
      <?php if (Session::get('id_distribuidor') == 1){?>
      <a class="top-bar-logo navbar-brand" href="{{ url() }}"></a>    
      <?php }else{ ?>
      <a style="display: table; width: 260px; height: 25px; margin: 10px !important;" href="{{ url() }}"><img src="{{ asset('img/'.$logo) }}" style="height: 40px; margin-top: -5px;" /></a>
      <?php } ?>
      <a class="top-bar-display-menu navbar-brand" href="#"><span class="glyphicon glyphicon-align-justify" style="color: #fff;"></span></a>    
    </div>

    <?php $panelMode = Session::get('panel_mode'); ?>
  
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">          
          <li class="dropdown">
            <a href="#" class="dropdown-toggle dropdown-toggle-number-select" 
               data-toggle="dropdown" data-hover="dropdown">
              <span class="dropdown-number-select-config-label">Modo</span>
              <span class="dropdown-number-select-selected">
                {{ $panelMode === 'basic' ? 'Básico' : 'Avanzado' }} <b class="caret"></b>
              </span>
            </a>  
            <ul class="dropdown-menu" id="dropdown-mode">
              <li{{ $panelMode === 'basic' ? ' class="active"' : '' }}>
                <a href="{{ URL::to('mode/basic') }}?redirect={{ Request::path() }}"><i class="fa fa-user"></i> Básico</a>
              </li>
              <li{{ $panelMode === 'advanced' ? ' class="active"' : '' }}>
                <a href="{{ URL::to('mode/advanced') }}?redirect={{ Request::path() }}"><i class="fa fa-code"></i> Avanzado</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">            
            <?php $selectedNumber = Session::get('number', NULL); ?>
            
            @if($selectedNumber !== NULL)
            <a href="#" class="dropdown-toggle dropdown-toggle-number-select"                 
               data-toggle="dropdown" data-hover="dropdown">
              <span class="dropdown-number-select-config-label">Configuración</span>
              <span class="dropdown-number-select-selected">
              {{ $selectedNumber->getPrettyNumber() }} <b class="caret"></b>
              </span>
            </a>
            @else
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
              Selecciona un número <b class="caret"></b>
            </a>              
            @endif
            <ul class="dropdown-menu" id="dropdown-number-select">
              <li>
                <form role="search">
                  <div class="form-group">
                    <input type="text" class="form-control dropdown-number-select-textbox" placeholder="Busca un número" />
                  </div>
                </form>
              </li>
            </ul>
          </li>

          <li id="saldo">
            <?php
            //$balance = Session::get('user')->getBalance();
			$user = User::find(Session::get('user')->getId());
			$balance = $user->userbalance;
			?>
            <a href="{{ URL::to('balance') }}" 
               class="topbar-balance-balance {{ $balance < 1 ? 'balance-low' : '' }}"
               title="Haz click para recargar saldo">
              <span class="topbar-balance-label">saldo</span>
              {{ $balance }} €
            </a>
          </li>
          <li class="dropdown">           
            <?php $user = Session::get('user'); ?> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
              {{ (trim($user->getName()) == '' || trim($user->getName()) == "'") ? $user->getUsername() : Str::limit($user->getName(), 20) }} <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <li<?=Request::is('me*') ? ' class="active"' : ''?>>
                <a href="{{ URL::to('me') }}">
                  <span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Mi perfil
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="{{ URL::to('alta') }}">
                  <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Contratar nuevo número
                </a>
              </li>              
              <li>
                <a href="<?php echo Session::get('dominio_master'); ?>" target="_blank">
                  <span class="glyphicon glyphicon-new-window"></span>&nbsp;&nbsp;Ir a nuestra web
                </a>
              </li>
              <li class="divider"></li>
              <li><a href="{{ URL::to('logout') }}"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Cerrar sesión</a></li>
            </ul>
          </li>

        </ul>
    </div>
</nav>

<script type="text/javascript">
$(document).ready(function() {
  var glyphicon = {
    fax    : 'print',
    analog : 'phone-alt',
    voip   : 'earphone',
    pbx    : 'transfer',
    trunk  : 'random'
  };
  
  var selectedNumber = '{{ $selectedNumber !== NULL ? $selectedNumber->getNumber() : "" }}';
  
  $.getJSON('{{ url() }}/numbers.json', {}, function(data) {
    var html; var selected;
    $.each(data, function(i, v) {      
      selected = $.trim(selectedNumber) === data[i].number ? ' class="active"' : '';
      html = '<li' + selected + '><a href="{{ url() }}/number/' + data[i].number + '">';
      html+= '<span class="glyphicon glyphicon-' + glyphicon[data[i].type] + '">';
      html+= '</span> ' + data[i].number + '</a></li>';
      
      $("#dropdown-number-select").append(html);
    });
  });
  
  $('.top-bar-display-menu').click(function() {
    if($('.sidebar').css('display') === 'none') {
      $('.sidebar').css('display', 'table');
    } else {
      $('.sidebar').css('display', 'none');
    }
  });
  
  $(".dropdown-number-select-textbox").keydown(function(event) {
    if(event.which == 13) {
      path = '{{ url() }}/number/' + $(this).val();

      document.location = path;
      
      return false;
    }
  });
});
</script>
