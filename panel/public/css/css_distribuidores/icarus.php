<?php
session_start();
header("Content-type: text/css; charset: UTF-8");

function DB(){
	$db["hostname"] = "localhost";
	$db["user"] = "root";
	$db["password"] = "eisw020508";
	$db["db_name"] = "fmeuropa";
	
	$link = new mysqli($db["hostname"], $db["user"], $db["password"], $db["db_name"], 3306);
	if($link->connect_errno){
		die(header("Location: 503.php"));
	}else{
		return $link;
	}
} 

$link = DB();
$idDist = $_GET['idDist'];
$sql = $link->query("SELECT * FROM css_distribuidores WHERE id_distribuidor = '$idDist'");
if(!$sql->num_rows){
	echo 'nanai=> '.$idDist;
}
$row = $sql->fetch_array();
?>
/** estructura **/
body {    
  font-family: 'Lato', sans-serif;
  font-size: 16px;
  height: 100%;
  background-color: #fcfcfc;
}

.margin-15 { margin-top: 15px; }
.margin-30 { margin-top: 30px; }
.margin-45 { margin-top: 45px; }

.margin-btm-15 { margin-bottom: 15px; }
.margin-btm-30 { margin-bottom: 30px; }
.margin-btm-45 { margin-bottom: 45px; }

.sidebar:after {
  margin-bottom: 50px;
}

/** components **/
.frame {
  border-left: 3px solid #EEEEEE;
  margin: 20px 0;
  padding: 20px;
}

.frame-info {
  background-color: #F5F5F5;
  border: 1px #D9D9D9 solid;  
  border-left: 3px #D9D9D9 solid;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;  
  padding: 10px 20px;
}

.frame-danger {
  background-color: #FFC7C7;
  border-color: #FF0000;
  color: #000;
}

.frame-content {
  display: none;
  margin-top: 20px;
}

.panel-title-toggle {
  font-size: 0.6em;
  margin-left: 20px;
  color: #51555c;
  text-transform: uppercase;
  vertical-align: middle;  
}

.panel-icon {
  font-size: 60px !important;
  color: #ddd;
}

.badge {
  background-color: <?php echo $row['fondo']; ?>;
}

label {
  font-weight: normal;
}

.label-sidebar-new-feature {
  font-size: 10px;
  margin-left: 5px;
}

.label-with-tooltip {
  color: #333333;
  border-bottom: 1px dotted #ebc956;
}

.label-with-tooltip:hover,
.label-with-tooltip:active,
.label-with-tooltip:focus {
  text-decoration: none;
  color: #333333;
}

.label-with-tooltip:after {
  font-family: 'FontAwesome';
  content: "\f059";
  color: #ebc956;
}

h1,
.h1 { font-size: 26px; }

h1,
h3 {
  color: #000;
}

h3 {
  font-size: 20px;
}

h1 small,
h2 small,
h3 small,
h1 .small,
h2 .small,
h3 .small {
  font-size: 75%;
}

.orange-hightlight {
  color: <?php echo $row['fondo']; ?>;
  font-weight: bold;
  text-transform: uppercase;  
}

.btn.btn-primary {
  background-color: <?php echo $row['boton']; ?>;
  border: 0;
  white-space: normal;
  color: <?php echo $row['enlaces_color']; ?>;
}

.btn-huge {
  font-size: 18px;
  margin: 5px 0;
  padding: 15px 0;  
}

.btn-full-width {
  width: 100% !important;
}

.btn.btn-primary:hover,
.btn.btn-primary:focus,
.btn-group:focus .btn.btn-primary.dropdown-toggle {
  background-color: <?php echo $row['boton_hover']; ?>;
  border: 0;  
}

.btn-success {
  font-weight: bold;
  width: 100%;
}

.form-control {
  -webkit-box-shadow: none;
  border-width: 2px;
  box-shadow: none;    
}

.form-group.focus .form-control,
.form-control:focus {
  border: 2px <?php echo $row['fondo']; ?> solid;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.form-control-huge {
  height: 45px;
  font-size: 105%;
}

.form-control-inline {
  width: 40px;
  display: inline;
  border-width: 1px;
  height: 20px;
  text-align: center;
  padding: 0;
  margin: 0 5px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px; 
}

.jumbotron {
  font-size: 18px;
  line-height: 1.428571429;
}

.date-picker {
  text-align: center;
}

.datepicker {
  z-index: 100 !important;
}

.phone-number {
  
}

.input-text-success {
  background-color: #C6F5C6;
  border: 2px #00f500 solid !important;
}

.input-text-error {
  background-color: #fdc6c6;
  border: 2px #ff0000 solid !important;
}

.date-picker[readonly] {
  background-color: #fff;
}

a {
  color: <?php echo $row['enlaces']; ?>;
}

a:hover,
a:focus {
  color: <?php echo $row['enlaces_hover']; ?>;
}

a.bottom-dashed { border-bottom: 1px #ff5d28 dashed; }
a.bottom-dashed:hover { text-decoration: none; }

.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  background-color: <?php echo $row['fondo']; ?>;
  border-color: #dddddd;
  font-weight: bold;
}

.pagination > li > .no-click {
  cursor: default;
  pointer-events: none;
}

table .remove {
  float: right;
  display: inline;
}

table tr a {
  text-decoration: none;
}

.table > thead > tr > td.active,
.table > tbody > tr > td.active,
.table > tfoot > tr > td.active,
.table > thead > tr > th.active,
.table > tbody > tr > th.active,
.table > tfoot > tr > th.active,
.table > thead > tr.active > td,
.table > tbody > tr.active > td,
.table > tfoot > tr.active > td,
.table > thead > tr.active > th,
.table > tbody > tr.active > th,
.table > tfoot > tr.active > th {
  background-color: <?php echo $row['menu_hover']; ?>;
}

table tbody[data-provides=rowlink] {
  cursor: pointer;
}

tr.row-button-add-new span {
  text-align: center;
  display: block;
  height: 38px;
  margin: 10px 0;
  width: 100%;  
}

tr.row-button-add-new .glyphicon {
  font-size: 30px;
}

tr.row-add-new {
  display: none;
}

.bootstrap-switch .bootstrap-switch-label {
  background: #e3e3e3 !important;
}

.bootstrap-switch .bootstrap-switch-handle-on {
  background: <?php echo $row['fondo']; ?> !important;
}

.edit-save,
.edit-cancel,
.edit-input,
.edit-action {
  display: none;
}

.label.label-experimental {
  background-color: #B85C81;
}

.label.label-alta {
  background-color: #fff;
  color: #1D1B1B;
}

label.disabled {
  color: #c9c9c9;
}

.checkbox-subitem {
  margin-left: 60px;
  font-size: 80%;
  margin-bottom: 20px !important;  
}

.tooltip.top .tooltip-arrow{
  border-top: 5px solid <?php echo $row['boton_hover']; ?>;
}
.tooltip.left .tooltip-arrow{
  border-left: 5px solid <?php echo $row['boton_hover']; ?>;
}
.tooltip.bottom .tooltip-arrow{
  border-bottom: 5px solid <?php echo $row['boton_hover']; ?>;
}
.tooltip.right .tooltip-arrow{
  border-right: 5px solid <?php echo $row['boton_hover']; ?>;
}

.tooltip-inner {
  background-color: <?php echo $row['boton_hover']; ?>;
  text-align: start;
  max-width: 300px;
  white-space: pre-wrap;
  font-size: 14px;
  padding: 8px;
}

/** component > forward **/
.form-control.forward-sip-accounts {
  display: none;
  width: 180px !important;
}

.forward-timeout {
  width: 80px;  
}

/** legal links **/
.footer-legal-links {
  margin: 0 auto 20px;
  display: block;
  width: 300px;
  text-align: center;
  list-style: none;
  padding: 0;
  font-size: 12px;
}

.footer-legal-links li a {
  display: block;
  color: #A3A3A3;
  text-decoration: none;
}

.footer-legal-links li a:hover {
  text-decoration: underline;
}

/** component > selector **/
.selector {
  margin-left: 0 !important;
  margin-right: 3px !important;
  float: left;
  width: 100%;
}

.action-summary {
  float: left;
  background: #fff8c4;
  border: 2px solid #f2c779;
  border-radius: 3px;
  padding: 5px;
  width: 250px;
}

.action-type,
.action-config-button {
  float: left;
}

.action-config-button {
  display: block;
  clear: both;
  margin-top: 10px;
  margin-left: 0;
  width: 100%;
}

.action-summary .action-config-button {
  margin-left: 0;
}

.label-clock,
.label-retro-digits {
  background-color: #ddd;
  font-family: 'Digi';
  font-size: 1.5em;
  color: #C9FF77;
  padding: 3px;
  width: 75px;
  text-align: center;
  border: 1px #D1D0D0 solid;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;  
  -webkit-box-shadow: 0px 0px 6px 3px #f3f3f3;
  -moz-box-shadow: 0px 0px 6px 3px #f3f3f3;
  box-shadow: 0px 0px 6px 3px #f3f3f3;
  height: 42px;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-weight: bold;
  display: inline-block;
  color: #FF3F00;
}

.inline-link {
  font-size: 80%;
  color: #493DFF !important;
  margin-left: 30px;
}

.inline-link:hover {
  text-decoration: none;
}

/** tabs **/
.nav-tabs {
  border-bottom: 1px #d9d9d9 solid;
}

.nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus {
  font-weight: bold;
  color: #999;
  border-color: #d9d9d9 #d9d9d9 rgba(0, 0, 0, 0);
}

.tab-real-tab {
  border: 1px #D9D9D9 solid;
  border-top: none;
  padding: 10px;  
  background-color: #fff;
}

/** pills **/
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus {
  background-color: #000;
  color: #fff;
}

.nav-stacked > li.active > a,
.nav-stacked > li.active > a:hover,
.nav-stacked > li.active > a:focus {
  /*padding-left: 25px;*/
}

.nav-stacked.nav-pills > li > a {
  border-radius: 0;
}

.nav-pills > li > a:hover,
.nav-pills > li > a:focus {
  background-color: #999;
  color: #fff;
}

/** topbar **/
.navbar {
  margin-bottom: 0;
}

.navbar-header .top-bar-logo {
  display: none;
}

.navbar-default .navbar-nav > li > a {
  color: #fff;
}

.navbar-stats .navbar-nav > li > a {
  color: #333;
}

.navbar-default .navbar-nav > li > a:hover {
  background-color: #f3f3f3;  
}

.navbar-default .navbar-nav > .dropdown > a .caret {
  border-top-color: #fff;
  border-bottom-color: #fff;  
}

.navbar-default .navbar-nav > .dropdown > .dropdown-menu {
  padding: 0;
  border: 1px #EBEBEB solid;
  border-top: 0;
  border-radius: 0;
  background-color: #f3f3f3;
  min-width: 200px;
  box-shadow: 0px 1px 3px #BEBEBE;
  -webkit-box-shadow: 0px 1px 3px #BEBEBE;
}

.top-bar .dropdown-menu > .active > a,
.top-bar .dropdown-menu > .active > a:hover,
.top-bar .dropdown-menu > .active > a:focus {
  background-color: <?php echo $row['menu_hover']; ?>;
}

.dropdown-toggle-number-select {  
  background-color: <?php echo $row['menu']; ?>;  
  padding: 0 15px !important;
  height: 49px;
  max-height: 49px;
  overflow: hidden;
  font-size: 18px;
}

.dropdown-number-select-selected {
  font-size: 20px !important;
  font-weight: bold;
}

.dropdown-number-select-config-label {
  display: block;
  text-transform: uppercase;
  font-size: 10px;
}

#dropdown-number-select .form-group {
  margin-bottom: 0;
}

.dropdown-number-select-textbox {
  border-radius: 0;
  border: 1px #c9c9c9 solid;
}

.topbar-balance-label {
  display: block;
  text-transform: uppercase;
  font-size: 10px;  
  font-weight: normal;
}

.topbar-balance-balance {  
  background-color: #FFA589;  
  padding: 0 15px !important;  
  height: 49px;
  max-height: 49px;
  overflow: hidden;
  font-size: 20px !important;
  font-weight: bold;  
}

.balance-low {
  background-color: #C9C9C9;
  color: #000 !important;
}

.navbar-default .navbar-nav > .dropdown > .dropdown-menu > li > a {
  padding: 15px 20px;
}

.navbar-default .navbar-nav > .dropdown > .dropdown-menu .divider {
  margin: 0;
}

.navbar-default .navbar-nav > .dropdown > .dropdown-menu > li > a:hover,
.navbar-default .navbar-nav > .dropdown > .dropdown-menu > li > a:focus {
  background-color: #CCC;
}

.nav-stacked > li + li {
  margin-top: 0;
}

.table-hover tbody tr:hover td,
.table-hover tbody tr:hover th {
  background-color: <?php echo $row['menu_hover']; ?>;
}

/** sidebar **/
.sidebar {
  z-index: 3;
  position: absolute;
  display: none;
  float: left;
  border-right: 1px #d9d9d9 solid;
  height: 100%;
  background-color: #f3f3f3;
  width: 260px;
}

.sidebar .glyphicon,
.sidebar .fa {
  margin-right: 10px;
}

.sidebar p {
  text-decoration: none;
  background-color: #e3e3e3;
  border-top: 1px #c9c9c9 solid;
  border-bottom: 1px #c9c9c9 solid;  
  padding: 5px;
  margin-bottom: 0;
  text-transform: uppercase;
  font-size: 12px;  
}

.sidebar p a {
  color: #333;
  text-decoration: none;
}

.sidebar p a:hover {
  color: #ff5d28;
}

#sidebar-title-admin {
  border-top: 1px #c9c9c9 solid;
}

#sidebar-title-my-account {
  border-top: none;
}

#sidebar-title-my-account,
#sidebar-li-balance,
#sidebar-li-me,
#sidebar-li-help,
#sidebar-li-logout {
  display: block;
}

.sidebar #alert-outstanding-invoices {
  display: none;
}

/*******************/
/** log in window **/
/*******************/
.container-log-in {
  padding: 20px;
  margin: 30px auto 10px;
  width: 300px;
  background-color: #f9f9f9;
  border: 1px #d9d9d9 solid;    
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;  
  -webkit-box-shadow: 0px 0px 11px 0px rgba(180, 180, 180, 0.75);
  -moz-box-shadow:    0px 0px 11px 0px rgba(180, 180, 180, 0.75);
  box-shadow:         0px 0px 11px 0px rgba(180, 180, 180, 0.75);  
}

.btn-login {
  width: 100%;
  font-size: 16px;
  padding: 10px;
}

.btn-login.disabled {
  opacity: 1;
  filter: alpha(opacity=100);  
}

.input-submit-login {
  border: none;
  width: 0;
  position: absolute;
  height: 0;
  line-height: 0;
  padding: 0;
  margin: 0;
}

.log-in-logo {
  width: 258px;
  height: 31px;
  margin: 20px 0 40px;
}

.log-in-links {
  list-style: none;
  padding: 0;
  margin: 20px auto;
}

.log-in-links li {
  font-size: 80%;
  margin-top: 5px;
}

/*****************/
/** main window **/
/*****************/
.top-bar,
.top-bar-inner {
  height: 40px;    
  background: #000;
  border-radius: 0;
  width: 100%;
}

/** content > summary **/
form#set-date {
  display: block;
}

#summary-date {
  width: 100% !important;
  margin: 10px 0;
}

.summary-list {
  list-style: none;
  padding: 0;
  margin-bottom: 15px;
}

.summary-list li {
  margin-top: 15px;
}

.summary-list li i {
  color: #FF855E;
  margin-right: 10px;
  font-size: 22px;
}

.summary-btn {
  white-space: nowrap;
}

.summary-icon-danger  { color: #ff0000; }
.summary-icon-warning { color: #f6f622; }

.summary-date-picker {
  display: inline-block !important;
  width: auto !important;
  margin-left: 10px;
}

.summary-queue-table {
  border-spacing: 5px;
  border-collapse: separate;  
  margin-bottom: 0;
}

.summary-queue-table td {
  text-align: center;
  background-color: <?php echo $row['fondo']; ?>;
  color: #fff;
  padding-left:4px !important;
  padding-right:4px !important;
}

.summary-queue-table td.arrow {
  background-color: initial;
  color: #C3C3C3;
  font-size: 40px;
  padding: 0;
  border-top: 0;
  line-height: initial;
}

/** content > numbers **/
.numbers-list-col-plan,
.numbers-list-col-sign-up-date,
.numbers-list-col-pricing,
.numbers-list-col-reversion {
  display: none !important;
}

/** content > stats **/
.stats-filters {
  list-style: none; 
  padding: 0;
}

.stats-filters li {
  background-color: #ff5d28;
  color: #fff;
  float: left;
  margin-top: 5px;
  margin-right: 5px;
  border-radius: 3px;
  padding: 5px;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;  
}

.stats-filters-filter-remove {
  margin-left: 10px;
  color: #fff !important;
}

.stats-btn-search {
  width: 100%;
  margin-top: 27px;
}

.stats-cell-caller,
.stats-cell-called {
  font-size: 105%;
  color: #666;
  font-weight: bold;
}

.stats-cell-cost-zero  { color: #b1b100 }
.stats-cell-cost-minus { color: red }
.stats-cell-cost-plus  { color: green }

.stats-summary h3 {
  text-transform: uppercase;
  font-size: 12px;
  margin: 15px 0 5px;  
}

.stats-summary-group-count-number,
.stats-summary-group-count-label {
  display: block;
  text-align: center;
}

.stats-summary-group-count-number {
  font-size: 25px;
  color: #ff5d28;
}

.stats-summary-group-count-label {
  display: none;
  color: #333;
  text-transform: uppercase;
  margin-top: -5px;
  font-size: 11px;  
}

.stats-summary .table > thead > tr > td,
.stats-summary .table > tbody > tr > td,
.stats-summary .table > tfoot > tr > td {
  border-top: 0;
  font-size: 13px; 
  padding: 3px;
}

.stats-summary th {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: normal;
}

/** content > callrecording **/
.recording-custom-period {
  display: none;
  background-color: #f3f3f3;
  padding: 10px;
  margin-top: 10px;  
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;  
}

/** content > menus **/
.menu-config-container {}

.menu {
  float: left;
  margin-right: 25px;
}

.menu-row { 
  list-style: none;  
  padding: 0;
  list-style: none;
  float: left;
}

.menu-row { 
  list-style: none;  
  padding: 0;
  margin-top: 10px;
}

.menu-row li {
  float: left;  
}

.menu-option-button {
  border: 2px <?php echo $row['fondo']; ?> solid;  
  margin-right: 20px;
  display: block;
  padding: 10px;
  width: 40px;
  text-decoration: none;

  background-color: <?php echo $row['fondo']; ?>;
  text-align: center;
  color: #fff;
  font-size: 20px;
  font-weight: bold;
}    

.menu-option-button:hover,
.menu-option-button:focus,
.menu-option-button-active {
  background-color: <?php echo $row['menu_hover']; ?>;
  text-decoration: none;
  color: #fff;
}

#menu-option-config {
  float: left;
  display: none;
}

.menu-option-action-config {
  display: none;
}

.menu-option-action-config-forward {
  display: inline-block;
}

.menu-option-edit-save,
.menu-option-edit-cancel,
.menu-option-edit-input,
.menu-option-edit-action {
  display: none;
}

/** content > whiteblacklist **/
.plus-white-blacklist {
  display: block !important;
  margin: 0 auto !important;
}

/** content > schedules **/
.schedules-days {
  margin-bottom: 20px;
}

.schedules-tabs {
  display: none;
}

.schedules-select select {
  font-size: 16px;
  background-color: <?php echo $row['fondo']; ?>;
  color: #fff;
  font-weight: bold;
}

.schedule-edit-save,
.schedule-edit-cancel,
.schedule-edit-input,
.schedule-edit-action {
  display: none;
}

/** content > holidays **/
.holiday-edit-save,
.holiday-edit-cancel,
.holiday-edit-input,
.holiday-edit-action {
  display: none;
}

/* modals */
.modal-forward-forwards {
  height: 300px;
  overflow-x: hidden;
  overflow-y: auto;
}

.modal-forward-add-new-forward {
  margin: 10px 0;
}

/** queue **/
.queue-sip-accounts {
  padding: 0;
  margin: 0;
  list-style: none;
}

li.queue-sip-account {
  margin: 0;
  margin-bottom: 5px;
}

.queue-sip-account {
  position: relative;
  z-index: 10;
  margin-left: 10px;
  background-color: <?php echo $row['fondo']; ?>;
  border: 1px #ddd solid;
  color: #fff;
  font-weight: bold;
  width: auto;
  text-align: center;
  cursor: move;
  padding: 15px;
}

.queue-remove-sip {
  display: block;
  color: #fff;
  margin: 0 auto;
  width: 20px;
}

.queue-remove-sip:hover {
  color: #fff;
}

.queue-col {
  width: 100px;
  max-width: 100px;
  height: 55px;
  max-height: 55px;
  overflow: hidden;
}

.queue-col-empty {
  cursor: normal;
  border: 1px #e5e5e5 solid;  
  background-color: transparent;    
}

.queue {  
  width: 100%;
}

.queue-priority {
  font-weight: bold;
  width: 60px;
  text-align: center;  
  border: 1px #ddd solid;
  color: #333333;  
  background-color: #f5f5f5;  
}

.queue-col-hover {
  background-color: <?php echo $row['menu_hover']; ?>;
  border: 0;
}

.queue-loading {
  display: none;
}

/** rates **/
.rates-flag {
  width: 20px;
  margin-right: 10px;
}

.rates-filter {
  background-position: 95% center;
  background-repeat: no-repeat;
}

.rate-result {
  font-weight: bold;
}

#rates-no-rates {
  display: none;
}

@font-face {
  font-family: 'Digi';
  src: url(../../fonts/DS-DIGII.TTF);
}

/** recorder **/
.recorder {
  background-color: <?php echo $row['fondo']; ?>;
  border-radius: 5px;
}

.recorder ul {
  list-style: none;
  padding: 0;
}

.recorder ul li {
  float: left;
}

.recorder ul li a {
  background-color: <?php echo $row['menu_hover']; ?>;  
  color: #fff;
  display: block;
  padding: 10px;
}

.recorder ul li a.disabled,
.recorder ul li a.disabled:hover {
  color: #d9d9d9;
  text-decoration: none;
}

.recorder ul li:first-child a,
.recorder .speech-recording-counter {
  -webkit-border-top-left-radius: 5px;
  -webkit-border-bottom-left-radius: 5px;
  -moz-border-radius-topleft: 5px;
  -moz-border-radius-bottomleft: 5px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.recorder ul li:last-child a,
.recorder ul li.rounded-right a {
  -webkit-border-top-right-radius: 5px;
  -webkit-border-bottom-right-radius: 5px;
  -moz-border-radius-topright: 5px;
  -moz-border-radius-bottomright: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.speech-recording-counter {
  cursor: default;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;  
}

.speech-recording-counter:hover {
  text-decoration: none;
}

.speech-recording-counter span {
  color: #FFFF9A;
}

.recorder ul li.disabled a {
  color: #d7d7d7;
  text-decoration: none;
  cursor: not-allowed;
}

.recorder .speech-recording-download,
.recorder .speech-recording-stop {
  display: none;
}

#speech-recording-uploaded-alert,
#speech-recording-not-uploaded-alert,
#speech-recording-uploading-alert,
#speech-recording-audio-error {
  display: none;
}

/* timeline by http://bootsnipp.com/snippets/featured/timeline-responsive */
.timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
}

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
        }

        .timeline > li > .timeline-panel {
            width: 46%;
            float: left;
            border: 1px solid #d4d4d4;
            border-radius: 2px;
            padding: 20px;
            position: relative;
            -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        }

            .timeline > li > .timeline-panel:before {
                position: absolute;
                top: 26px;
                right: -15px;
                display: inline-block;
                border-top: 15px solid transparent;
                border-left: 15px solid #ccc;
                border-right: 0 solid #ccc;
                border-bottom: 15px solid transparent;
                content: " ";
            }

            .timeline > li > .timeline-panel:after {
                position: absolute;
                top: 27px;
                right: -14px;
                display: inline-block;
                border-top: 14px solid transparent;
                border-left: 14px solid #fff;
                border-right: 0 solid #fff;
                border-bottom: 14px solid transparent;
                content: " ";
            }

        .timeline > li > .timeline-badge {
            color: #fff;
            width: 50px;
            height: 50px;
            line-height: 50px;
            font-size: 1.4em;
            text-align: center;
            position: absolute;
            top: 16px;
            left: 50%;
            margin-left: -25px;
            background-color: #999999;
            z-index: 1;
            border-top-right-radius: 50%;
            border-top-left-radius: 50%;
            border-bottom-right-radius: 50%;
            border-bottom-left-radius: 50%;
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
        }

            .timeline > li.timeline-inverted > .timeline-panel:before {
                border-left-width: 0;
                border-right-width: 15px;
                left: -15px;
                right: auto;
            }

            .timeline > li.timeline-inverted > .timeline-panel:after {
                border-left-width: 0;
                border-right-width: 14px;
                left: -14px;
                right: auto;
            }

.timeline-badge.primary {
    background-color: #fefefe !important;
}

.timeline-badge.success {
    background-color: #3f903f !important;
}

.timeline-badge.warning {
    background-color: #f0ad4e !important;
}

.timeline-badge.danger {
    background-color: red !important;
}

.timeline-badge.info {
    background-color: #5bc0de !important;
}

.timeline-title {

    margin-top: 0;
    color: inherit;
}

.timeline-body > p,
.timeline-body > ul {

}

    .timeline-body > p + p {
        margin-top: 5px;
    }

    .timeline .btn {
      margin-top: 10px;
    }
    
@media (max-width: 767px) {
    ul.timeline:before {
        left: 40px;
    }

    .timeline > li > .timeline-panel:before,
    .timeline > li > .timeline-panel:after {
      display: none;
    }
 

    ul.timeline > li > .timeline-panel {
        margin-top: 80px;
        background-color: #fff;
        float: none;
        width: 100%;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
        float: none;
    }

    ul.timeline > li > .timeline-badge {
        left: 15px;
        margin-left: 0;
        top: 16px;
    }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
}

/** alta **/
.alta-body {
  background-color: #eaeaea;
}

.alta-container {
  border: 1px #d9d9d9 solid;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0px 0px 11px 0px rgba(180, 180, 180, 0.75);
  -moz-box-shadow: 0px 0px 11px 0px rgba(180, 180, 180, 0.75);
  box-shadow: 0px 0px 11px 0px rgba(180, 180, 180, 0.75);  
  padding: 20px;
  max-width: 960px;
  margin: 10px auto;
  background-color: #fff;
}

.alta-container h1, 
.alta-container h2,
.alta-container h3 {
  color: #333;
}

.alta-user-box span.glyphicon {
  color: #d5d5d5;
  margin-right: 10px;
}

.alta-product-selection,
.alta-number-selection {
  list-style: none;
  padding: 0;
}

.alta-btn-plan-price {
  font-weight: bold;
}

.alta-btn-plan-price-annual {
  display: block;
  background: url(../../img/annual.png) no-repeat right center;
}

.alta-pros {
  display: none;
  list-style: none;
  height: 200px;
  overflow-y: auto;
}

.alta-pros li {
  margin: 10px 0;
}

.alta-pros li:before {
  display: inline-block;
  background-image: url(../../img/arrow-bullet.png);
  background-repeat: no-repeat;
  background-position: center center;
  background-color: <?php echo $row['boton_hover']; ?>;
  height: 18px;
  -moz-border-radius: 75px;
  -webkit-border-radius: 75px;
  border-radius: 75px;
  width: 18px;
  content: ' ';
  float: left;
  margin-right: 0;
  margin-left: -25px;  
}

.alta-pros li a {
  border-bottom: 1px dashed <?php echo $row['boton_hover']; ?>; 
}

.alta-pros li a:after {
  font-family: 'FontAwesome';
  content: "\f059";
  color: <?php echo $row['boton_hover']; ?>;
}

.alta-pros li a:hover {
  text-decoration: none;
}

.alta-btn-number {
  float: left;
  margin-right: 10px;  
}

/** alta > map **/
.province-selector-container {
  padding: 0 30px;
}

.geo-fetch-more-numbers {
  display: none;
  margin: 20px auto;
  border: 3px #ccc solid !important;  
}

/** alta > ip-a-medida */
.alta-custom-ip-block-wrapper {
  padding: 15px;
  background-color: #fffaf8;  
}

.alta-custom-ip-slider.ui-slider-handle .ui-state-default {
  color: #fff;
}

.alta-custom-ip-slider.ui-widget-content {
  border: 1px solid #eee;
  background-color: #FFECE5;
  color: #333;
  margin: 30px auto;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;    
  width: 90%;
}

.alta-custom-ip-slider .ui-slider-handle {  
  width: 2.3em !important;
  height: 2em !important;
  top: -0.6em !important;
  -moz-border-radius: 6px;
  -webkit-border-radius: 6px;
  border-radius: 6px;  
}

.alta-custom-ip-slider-ip-lines .ui-slider-handle { background: #ff5d28 url('../../img/slider-num-ip-lines.gif') center center no-repeat !important; }
.alta-custom-ip-slider-channels .ui-slider-handle { background: #ff5d28 url('../../img/slider-num-channels.gif') center center no-repeat !important; }

.alta-custom-ip-block-wrapper .bootstrap-switch {
  margin: 10px 25px;
}

#alta-custom-ip-err-centralita {
  display: none;
}

.cart-custom-ip {
  background-color: <?php echo $row['fondo']; ?>;
  padding: 15px;
  color: #fff;
}

.cart-custom-ip h3 {
  color: #fff;
  margin-top: 0;
  margin-bottom: 20px;
  font-weight: bold;
  font-size: 28px;  
}

.cart-custom-ip p {
  font-weight: bold;
}

/** custom-ip > number-selection **/
.selected-numbers-row {
  margin: 0 0 10px 0;
  padding: 10px 0 2px;  
  background-color: <?php echo $row['menu_hover']; ?>;
  border-radius: 5px;
  display: none;
}

.selected-numbers-row p {
  color: #636363;
  display: inline-block;
  float: left;
  margin-right: 10px;
  margin-top: 5px;  
}

.selected-numbers-list-wrapper {
  max-width: 75%;
  float: left;
}

ul.selected-numbers-list {
  white-space:nowrap;  
  list-style: none;
  display: inline-block;
  overflow-y: hidden;
  max-width: 100%;
  padding: 0;
  padding-bottom: 10px;
}

ul.selected-numbers-list li {
  position: relative;
  color: #000;
  background-color: #FFECE5;
  font-weight: bold;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  padding: 5px;
  text-align: center;
  margin-right: 10px;
  display: inline-block;
  vertical-align: top;  
  border: 1px #C3BEBB solid;
  -webkit-box-shadow: 1px 1px 1px #928B8B;
  -moz-box-shadow: 1px 1px 1px #928B8B;
  box-shadow: 1px 1px 1px #928B8B;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;  
}

ul.selected-numbers-list li span {
  color: #636363;
}

ul.selected-numbers-list li span.glyphicon {
  color: #ff0000;
}

span.alta-custom-ip-price {
  font-weight: bold;
}

input.alta-custom-ip-continue {
  margin-bottom: 10px;
  width: 100%;
  font-size: 18px;  
}

/********/
.number-selection-content .nav {
  margin: 10px auto 20px;
}

.number-selection-col-map {
  display: none;
}

.cart-price {
  font-size: 40px;
}

/** alta > summary **/
.alta-chosen-plan,
.alta-chosen-number {
  font-size: 30px;
  font-weight: bold;
}

.alta-chosen-number {
  color: <?php echo $row['fondo']; ?>;
}

.alta-btn-confirm {
  font-size: 18px;
  padding: 15px;
  margin: 30px auto;
  display: block;
  width: 100%;
}

p.main-number {
  font-size: 37px;
  font-weight: bold;
  color: #ff5d28;
}

/* user profile */

/* flatrates */
#flatrates-cart-container {
  display: none;
}

/* change plan */
.change-plan-current-profile-row {
  color: #ccc;
}

.change-plan-features {
  list-style: none;
  padding: 0;
}

.change-plan-feature-plus a {
  color: #38A138;
}

.change-plan-feature-minus a {
  color: #D70000;
}

.change-plan-feature a:hover {
  text-decoration: none;
}

.change-plan-feature a:after {
  font-family: 'FontAwesome';
  content: "\f059";
  color: #FFD8CB;
}

/* edit sip account */
#sip-edit-password {
  display: none;
}
