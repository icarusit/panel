$(document).ready(function() {  
  // frame help
  var toggleShowHelpText = 'Mostrar ayuda';
  var toggleHideHelpText = 'Ocultar ayuda';
  
  // remove button on row hover
  $('td').hover(function() {
    $(".remove", this).show();
  }, function() {
    $(".remove", this).hide();
  });
  
  $('.toggle-text').html(toggleShowHelpText);
  
  $('.toggle-show-help').click(function() {
    var $toggleText = $(this).find('.toggle-text');
    var $frameContent = $(this).next('.frame-content');
    
    if ($toggleText.html() === toggleShowHelpText) {
      $toggleText.html(toggleHideHelpText);
      $frameContent.show(100);      
    } else {
      $toggleText.html(toggleShowHelpText);
      $frameContent.hide(100);
    }
  });
});

Date.prototype.format = function() {
  var d = {
    date    : this.getDate(),
    month   : this.getMonth() + 1,
    year    : this.getFullYear(),
    hours   : this.getHours(),
    minutes : this.getMinutes(),
    seconds : this.getSeconds()
  };
  
  for(i in d) if(d[i] < 10) d[i] = '0' + d[i];
 
  return d.date + '-' + d.month + '-' + d.year + ' ' +
          d.hours + ':' + d.minutes + ':' + d.seconds;
};