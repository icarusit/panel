// Documento con funciones Javas para Panel Flash
$(function () {
var tour = new Tour({
	onShown: function(){
		$('.tour-step-backdrop').closest(".nav").addClass("tour-step-backdrop-parent").css("z-index", "1101");
		$('.tour-step-backdrop').closest(".navbar").addClass("tour-step-backdrop-parent").css("z-index", "1101");
	},
  onHidden: function(){
    $('.tour-step-backdrop-parent').removeClass("tour-step-backdrop-parent").css("z-index", "");
  }
	});

tour.addSteps([{
    element: "#mis-numeros",
    title: "Aquí aparecen tus numeros contratados",
    content: "Es un listado donde puedes ver la fecha de alta, la cuota que pagas, el plan que tienes contratado, etc. Desde aquí puedes cambiar online el tipo de plan, contratar más números o contratar tarifas planas",
	backdrop: false,
	placement: 'bottom'
}, {
    element: "#mis-facturas",
    title: "Aquí aparecen tus facturas",
    content: "Es un listado con todas las facturas y el estado (pagada, en curso, tarjeta, etc). Si pagas tus facturas por tarjeta o Paypal desde aquí puedes hacer el pago",
	backdrop: false,
	placement: 'bottom'
}, {
    element: "#saldo",
    title: "Saldo de tu cuenta",
    content: "El saldo sirve para hacer llamadas salientes o desvíos a móviles, etc. Si quieres puedes recargar online mediante tarjeta o Paypal",
	backdrop: true,
	placement: 'bottom'
}, {
    element: "#recargar-saldo",
    title: "¿Te quedaste sin saldo?",
    content: "Recarga aquí tu saldo online mediante tarjeta o Paypal",
	backdrop: false,
	placement: 'bottom'
}]) ;

// Initialize the tour
tour.init();

 $('#tour-go').click(function () {
        // Start the tour
        tour.start();
    });

});
function confirma(texto){
	if(confirm(""+texto+"?")){
		return true;
	}else{
		return false;
	}
}